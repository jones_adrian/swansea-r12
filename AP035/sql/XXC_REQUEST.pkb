CREATE OR REPLACE
PACKAGE BODY "XXC_REQUEST" AS
/*******************************************************************************
*
* $Header$
*
* Module         : XML Publisher report submission program
*
* Module Type    : PL/SQL Package Body
*
* Original Author: Richard Pickett
*
* Description    : Submits a report then optionally submits an XML Publisher
*                  post-process and a Design Print queue job. The XML Publisher
*                  submission is not required if the report is run standalone
*                  but is necessary when it is called, e.g., from the Payables
*                  payment run.
*
* Change History
*    15-Nov-2006   Richard Pickett Modified to use XXCDU804 and not XXCDU803
*                                  as print spooler if an ETEXT output. This
*                                  type of output will only be produced on the
*                                  app server whereas other types will be produced
*                                  on the db server.
*
*    23-Feb-2007   Dhaval Thakore  Bugfix #EARS00020117886
*******************************************************************************/
gc_package_name   CONSTANT xxc_globals.ident_type := 'xxc_request.' ;

--+
-- put_message
-- Outputs a message to fnd_file.log and to dbms_output.
--
PROCEDURE put_message (
  p_message_num IN VARCHAR2,
  p_message     IN VARCHAR2,
  p_module      IN VARCHAR2)
IS
  l_message VARCHAR2(255);
BEGIN
  l_message := gc_package_name||p_module||' '||p_message;
  DBMS_OUTPUT.PUT_LINE(SUBSTR(p_message_num||': '||l_message,1,255));
  FND_FILE.PUT_LINE(FND_FILE.LOG,l_message);
END put_message;

--+
-- set_apps_env
-- Sets the apps environment if running outside of the application.
-- Default sysadmin user and payables responsibility and application are used.
--
PROCEDURE set_apps_env
IS
  c_module CONSTANT VARCHAR2(30) := 'set_apps_env';
  l_message      VARCHAR2(4001);
  l_user_id      NUMBER;
  l_resp_id      NUMBER;
  l_resp_appl_id NUMBER;
BEGIN
  IF FND_GLOBAL.USER_ID = -1 THEN
    l_user_id := 0;
    l_resp_id := 50243;
    l_resp_appl_id := 200;
  ELSE
    l_user_id := FND_GLOBAL.USER_ID;
    l_resp_id := FND_GLOBAL.RESP_ID;
    l_resp_appl_id := FND_GLOBAL.RESP_APPL_ID;
  END IF;

  FND_GLOBAL.APPS_INITIALIZE(l_user_id,l_resp_id,l_resp_appl_id);

  l_message := 'Appl ID = '||l_resp_appl_id||' User ID = '||l_user_id
               ||' Resp ID = '||l_resp_id;

  put_message('10000',l_message,c_module);

END set_apps_env;

--++
-- Submits a report. If p_output_format is set, it first sets the layout
-- options in the concurrent request, causing XML Pulisher output to
-- be generated. The XML Publisher template must exist and have the
-- same name as the report short name. It must also be registered under
-- the same Application as the report.
--
PROCEDURE submit_report_request (
  p_application   IN VARCHAR2,
  p_program       IN VARCHAR2,
  p_description   IN VARCHAR2,
  p_template_code IN VARCHAR2,
  p_locale        IN VARCHAR2,
  p_output_format IN VARCHAR2,
  p_print_queue   IN VARCHAR2,
  p_sub_request   IN BOOLEAN,
  p_arg1          IN VARCHAR2,
  p_arg2          IN VARCHAR2,
  p_arg3          IN VARCHAR2,
  p_arg4          IN VARCHAR2,
  p_arg5          IN VARCHAR2,
  p_arg6          IN VARCHAR2,
  p_arg7          IN VARCHAR2,
  p_arg8          IN VARCHAR2,
  p_arg9          IN VARCHAR2,
  p_arg10         IN VARCHAR2,
  p_arg11         IN VARCHAR2,
  p_arg12         IN VARCHAR2,
  p_arg13         IN VARCHAR2,
  p_arg14         IN VARCHAR2,
  p_arg15         IN VARCHAR2,
  p_req_id        OUT NUMBER,
  p_req_status    OUT VARCHAR2)
IS
  c_module CONSTANT VARCHAR2(30) := 'submit_report_request';
  l_xdo_req_id NUMBER;
  l_appl_id NUMBER;
  l_rphase         VARCHAR2(80);
  l_rstatus        VARCHAR2(80);
  l_dphase         VARCHAR2(30);
  l_dstatus        VARCHAR2(30);
  l_message        VARCHAR2(240);
  l_report_req_id  NUMBER;
  l_call_status    BOOLEAN;
  l_layout_status  BOOLEAN;
  l_report_message VARCHAR2(4001);
  l_print_queue    VARCHAR2(100);
  x_submit_error   EXCEPTION;
  x_timeout_error  EXCEPTION;
  x_layout_failure EXCEPTION;
  
  l_file           VARCHAR2(100);
  l_file_conc_id	 NUMBER;

BEGIN
  set_apps_env;

    IF NVL(p_output_format,'REPORT') != 'REPORT' THEN
      l_layout_status := FND_REQUEST.add_layout (
        template_appl_name => p_application,
        template_code      => p_template_code,
        template_language  => SUBSTR(p_locale,1,2),
        template_territory => SUBSTR(p_locale,4,2),
        output_format      => p_output_format);
      IF l_layout_status THEN
        l_report_message := 'Added layout options successfully';
        put_message('10010',l_report_message,c_module);
      END IF;
    ELSE
      l_layout_status := TRUE;
      l_report_message := 'Running report with no layout options';
      put_message('10020',l_report_message,c_module);
    END IF;

    IF l_layout_status THEN
      l_report_req_id := apps.Fnd_Request.submit_request(
                        application => p_application,
                        program     => p_program,
                        description => p_description,
                        start_time  => NULL,
                        sub_request => p_sub_request,
                        argument1   => p_arg1,
                        argument2   => p_arg2,
                        argument3   => p_arg3,
                        argument4   => p_arg4,
                        argument5   => p_arg5,
                        argument6   => p_arg6,
                        argument7   => p_arg7,
                        argument8   => p_arg8,
                        argument9   => p_arg9,
                        argument10  => p_arg10,
                        argument11  => p_arg11,
                        argument12  => p_arg12,
                        argument13  => p_arg13,
                        argument14  => p_arg14,
                        argument15  => p_arg15
                        );
    ELSE
      l_report_message := 'Failed to set layout options for report output. '||
        'Call to package function FND_REQUEST.ADD_LAYOUT failed.';
      put_message('10030',l_report_message,c_module);
      RAISE x_layout_failure;
    END IF;

  IF l_report_req_id = 0 THEN
    l_report_message := 'Error while submitting Report request for : '||p_program;
    put_message('10040',l_report_message,c_module);
    RAISE x_submit_error;
  ELSE
    COMMIT;
    p_req_id := l_report_req_id;
    l_report_message := 'Report '||p_program||' submitted successfully, Request ID : '||l_report_req_id;
    put_message('10050',l_report_message,c_module);
    FOR n IN 1..gc_wait_events LOOP
      l_call_status := FND_CONCURRENT.WAIT_FOR_REQUEST(l_report_req_id,
                                                       gc_wait_time,
                                                       NULL,
                                                       l_rphase ,
                                                       l_rstatus ,
                                                       l_dphase ,
                                                       l_dstatus ,
                                                       l_message) ;

      IF l_call_status THEN
        l_report_message := 'Report request '||l_report_req_id||' completed successfully';
        p_req_status := '0';
        put_message('10060',l_report_message,c_module);
        EXIT;
      END IF;
    END LOOP;
    IF NOT l_call_status THEN
      l_report_message := 'Report request '||l_report_req_id
                          ||' timed out during '||l_rphase||'. '
        ||'Please ask support to increase timeout variables in package XXC_REQUEST';
      put_message('10070',l_report_message,c_module);
      RAISE x_timeout_error;
    END IF;
    IF NVL(p_print_queue,'noprint') = 'noprint' THEN
      NULL;
    ELSE

      --------------------------------------------------------------
      -- Call XXCAP035C - XXC: BACS Remittance Copy File to App tier
      --------------------------------------------------------------
      l_report_message := 'Calling XXC: BACS Remittance Copy File to App tier';
      fnd_file.put_line(fnd_file.log, l_report_message
						|| l_report_req_id );

      l_file := 'XXCAP035B_' || l_report_req_id || '_1.ETEXT';

      --l_file := 'o' || l_report_req_id || '.out';


      l_file_conc_id := apps.Fnd_Request.submit_request(
                         application => 'XXC',
                         program     => 'XXCAP035C',
                         description => 'XXC: BACS Remittance Copy File to App tier',
                         start_time  => NULL,
                         sub_request => FALSE,
                         argument1   => l_file);                                          


      IF l_report_req_id = 0 THEN
         l_report_message := 'Error while submitting Report request for : '||p_program;
         put_message('10040',l_report_message,c_module);
         RAISE x_submit_error;
      ELSE
         COMMIT;
      END IF;
    
    
    -- p_print_queue may be hardcoded or set in a profile option
    l_print_queue := NVL(FND_PROFILE.VALUE(p_print_queue),p_print_queue);
    submit_to_print_queue (
      p_print_queue   => l_print_queue,
      p_request_id    => l_report_req_id,
      p_sub_request   => p_sub_request,
      p_output_format => p_output_format,
      p_req_status    => p_req_status);
    END IF;
  END IF;
EXCEPTION
  WHEN x_submit_error THEN
    p_req_status := '-1';
  WHEN x_timeout_error THEN
    p_req_status := '-2';
  WHEN x_layout_failure THEN
    p_req_status := '-3';
  WHEN OTHERS THEN
    p_req_status := SQLCODE;
END submit_report_request;

--
-- Submits the output file to a print queue (Design Print PARIS system)
-- using the shell script XXCDU803
--
PROCEDURE submit_to_print_queue (
  p_print_queue   IN VARCHAR2,
  p_request_id    IN NUMBER,
  p_sub_request   IN BOOLEAN,
  p_output_format IN VARCHAR2,
  p_req_status    OUT VARCHAR2
  )
IS
  c_module CONSTANT VARCHAR2(30) := 'submit_to_print_queue';
  CURSOR c_file (
    p_request_id IN NUMBER,
    p_output_format IN VARCHAR2)
  IS
    SELECT file_name, file_type
    FROM FND_CONC_REQ_OUTPUTS_V
    WHERE request_id = p_request_id
    AND file_type = p_output_format
    ORDER BY file_creation_date;

  c_db_server_spool_prg CONSTANT VARCHAR2(30) := 'XXCDU803';
  c_app_server_spool_prg CONSTANT VARCHAR2(30) := 'XXCDU804';
  l_rphase       VARCHAR2(80);
  l_rstatus      VARCHAR2(80);
  l_dphase       VARCHAR2(30);
  l_dstatus      VARCHAR2(30);
  l_print_req_id NUMBER;
  l_message      VARCHAR2(4001);
  l_error_flag   BOOLEAN := FALSE;
  l_call_status  BOOLEAN;
  -- x_timeout_error EXCEPTION;
BEGIN
  set_apps_env;

  ------------------------------------------------------------
  -- Change submission time based on file copy to required 
  -- application tier
  ------------------------------------------------------------

  FOR file_rec IN c_file (p_request_id,p_output_format) LOOP
    l_print_req_id := apps.Fnd_Request.submit_request(
                        application => 'XXC',
                        program     => CASE file_rec.file_type
                                       WHEN 'ETEXT' THEN
                                         c_app_server_spool_prg
                                       ELSE
                                         c_db_server_spool_prg
                                       END,
                        description => 'Print Job',
                        -- Changed from NULL to to_char(sysdate+0.75/(24*30),'DD-MON-RRRR HH24:MI:SS')
                        start_time  => to_char(sysdate+0.75/(24*30),'DD-MON-RRRR HH24:MI:SS'), 
                        sub_request => p_sub_request,
                        argument1   => p_print_queue,
                        argument2   => file_rec.file_name,
                        argument3   => '', argument4   => '',
                        argument5   => '', argument6   => '',
                        argument7   => '', argument8   => '',
                        argument9   => '', argument10  => '',
                        argument11  => '', argument12  => '',
                        argument13  => '', argument14  => '',
                        argument15  => '', argument16  => '',
                        argument17  => '', argument18  => '',
                        argument19  => '', argument20  => '',
                        argument21  => '', argument22  => '',
                        argument23  => '', argument24  => '',
                        argument25  => '', argument26  => '',
                        argument27  => '', argument28  => '',
                        argument29  => '', argument30  => '',
                        argument31  => '', argument32  => '',
                        argument33  => '', argument34  => '',
                        argument35  => '', argument36  => '',
                        argument37  => '', argument38  => '',
                        argument39  => '', argument40  => '',
                        argument41  => '', argument42  => '',
                        argument43  => '', argument44  => '',
                        argument45  => '', argument46  => '',
                        argument47  => '', argument48  => '',
                        argument49  => '', argument50  => '',
                        argument51  => '', argument52  => '',
                        argument53  => '', argument54  => '',
                        argument55  => '', argument56  => '',
                        argument57  => '', argument58  => '',
                        argument59  => '', argument60  => '',
                        argument61  => '', argument62  => '',
                        argument63  => '', argument64  => '',
                        argument65  => '', argument66  => '',
                        argument67  => '', argument68  => '',
                        argument69  => '', argument70  => '',
                        argument71  => '', argument72  => '',
                        argument73  => '', argument74  => '',
                        argument75  => '', argument76  => '',
                        argument77  => '', argument78  => '',
                        argument79  => '', argument80  => '',
                        argument81  => '', argument82  => '',
                        argument83  => '', argument84  => '',
                        argument85  => '', argument86  => '',
                        argument87  => '', argument88  => '',
                        argument89  => '', argument90  => '',
                        argument91  => '', argument92  => '',
                        argument93  => '', argument94  => '',
                        argument95  => '', argument96  => '',
                        argument97  => '', argument98  => '',
                        argument99  => '', argument100 => ''
                         );
    IF l_print_req_id = 0 THEN
      l_message := 'Error submitting print job for '||file_rec.file_name;
      put_message('10040',l_message,c_module);
      l_error_flag := TRUE;
    ELSE
      l_message := 'Print job submitted successfully, Request ID : '||l_print_req_id;
      put_message('10050',l_message,c_module);
 /*
      FOR n IN 1..gc_wait_events LOOP
        l_call_status := FND_CONCURRENT.WAIT_FOR_REQUEST(l_print_req_id,
                                                           gc_wait_time,
                                                           NULL,
                                                           l_rphase ,
                                                           l_rstatus ,
                                                           l_dphase ,
                                                           l_dstatus ,
                                                           l_message) ;
        IF l_call_status THEN
          l_message := 'Print request '||l_print_req_id||' completed successfully';
          p_req_status := '0';
          put_message('10060',l_message,c_module);
          EXIT;
        END IF;
      END LOOP;
      IF NOT l_call_status THEN
        l_message := 'Print request '||l_print_req_id
          ||' timed out during '||l_rphase||'. '
          ||'Please ask support to increase timeout variables in package XXC_REQUEST';
        put_message('10070',l_message,c_module);
        l_error_flag := TRUE;
      END IF;
      */
    END IF;
  END LOOP;
  IF l_error_flag THEN
    p_req_status := '-1';
  ELSE
    p_req_status := '0';
  END IF;
EXCEPTION
  WHEN OTHERS THEN
  l_message := SQLERRM;
  put_message('10080','SQLERRM',c_module);
  p_req_status := SQLCODE;
END submit_to_print_queue;

--
-- Submits a standalone XML Publisher post-process
--
PROCEDURE submit_xdo_request (
  p_report_req_id IN NUMBER,
  p_output_format IN VARCHAR2,
  p_req_id        OUT NUMBER,
  p_message       OUT VARCHAR2,
  p_req_status    OUT VARCHAR2)
IS
  c_module CONSTANT VARCHAR2(30) := 'submit_xdo_request';
  --cursor for getting the specification template
  CURSOR CUR_TEMPLATE
  IS
    SELECT XT.TEMPLATE_CODE,XT.TEMPLATE_TYPE_CODE,FCP.APPLICATION_ID
    FROM XDO_TEMPLATES_VL XT,
         XDO_DS_DEFINITIONS_B XDS,
         FND_CONCURRENT_PROGRAMS FCP,
         FND_CONCURRENT_REQUESTS FCR
    WHERE XT.DS_APP_SHORT_NAME = XDS.APPLICATION_SHORT_NAME
    AND XT.DATA_SOURCE_CODE = XDS.DATA_SOURCE_CODE
    AND FCP.CONCURRENT_PROGRAM_NAME = XDS.DATA_SOURCE_CODE
    AND XDS.APPLICATION_SHORT_NAME = (SELECT application_short_name
                                      FROM fnd_application
                                      WHERE application_id=FCP.APPLICATION_ID)
    AND FCP.CONCURRENT_PROGRAM_ID = FCR.CONCURRENT_PROGRAM_ID
    AND FCR.REQUEST_ID = p_report_req_id
    AND TRUNC(XT.START_DATE) <= TRUNC(SYSDATE)
    AND (XT.END_DATE IS NULL OR TRUNC(XT.END_DATE) > TRUNC(SYSDATE));

--Variable Declaration
    l_call_status   BOOLEAN;
    l_rphase        VARCHAR2(80);
    l_rstatus       VARCHAR2(80);
    l_dphase        VARCHAR2(30);
    l_dstatus       VARCHAR2(30);
    l_message       VARCHAR2(4001);
    l_count_records NUMBER :=   0;
    l_success_records NUMBER := 0;
    l_xdo_status    VARCHAR2(4001);
    l_xdo_req_id    NUMBER;
    x_submit_error  EXCEPTION;
  BEGIN
    set_apps_env;
    FOR template_rec IN CUR_TEMPLATE
    LOOP
      l_xdo_req_id := apps.Fnd_Request.submit_request(
                           application => 'XDO',
                           program     => 'XDOREPPB',
                           description => NULL,
                           start_time  => SYSDATE,
                           sub_request => FALSE,
                           argument1   => p_report_req_id,             -- Request Id
                           argument2   => template_rec.template_code,  -- Template
                           argument3   => template_rec.application_id, -- Rep Appl Id
                           argument4   => 'en-GB',                     -- Template Locale
                           argument5   => 'N',                         -- Debug Flag
                           argument6   => template_rec.template_type_code,                     -- Template Type
                           argument7   => p_output_format              -- Output Format
                         );

      IF l_xdo_req_id = 0 THEN

        FND_FILE.PUT_LINE(FND_FILE.LOG,'Error while submitting XML Report Publisher request for template : '||template_rec.template_code);
        RAISE x_submit_error;
      ELSE
        COMMIT;
        l_message := 'Report for template '
                      ||template_rec.template_code
                      ||' submitted Successfully, Request ID : '
                      ||l_xdo_req_id;
        put_message('10080',l_message,c_module);
        l_xdo_status := l_xdo_status||template_rec.template_code||': '||l_xdo_req_id||' ';
        FOR n IN 1..10 LOOP
          l_call_status := Fnd_Concurrent.WAIT_FOR_REQUEST(l_xdo_req_id,
                                                         5,
                                                         NULL,
                                                         l_rphase ,
                                                         l_rstatus ,
                                                         l_dphase ,
                                                         l_dstatus ,
                                                         l_message) ;

          IF l_call_status THEN
            l_xdo_status := l_xdo_status||', completed OK in about '||n*5||' seconds';
            EXIT;
        --  p_req_status := l_dstatus;
          ELSE
            dbms_output.put_line('Looping '||n||'...');
          END IF;
        END LOOP;
        IF NOT l_call_status THEN
          l_xdo_status := l_xdo_status||', error in post-process - '||l_message;
        END IF;
      END IF;
    END LOOP;
    --p_req_id := l_req_id;
    p_message := 'XXC_REQUEST.SUBMIT_XDO_REQUEST - '||l_xdo_status;
    p_req_status := '0';
  EXCEPTION
  WHEN x_submit_error THEN
    p_message := 'XXC_REQUEST.SUBMIT_XDO_REQUEST - post-process submission_failure';
    p_req_status := '-1';
  WHEN OTHERS THEN
    p_message := 'XXC_REQUEST.SUBMIT_XDO_REQUEST - Error while printing templates, Report Request ID : '||p_report_req_id;
    p_req_status  := SQLCODE;
  END submit_xdo_request;
-- ++
-- version
-- Returns the source control version of the package body
-- Used for configuration management and debugging
-- --
FUNCTION version RETURN VARCHAR2 IS
BEGIN
--
-- Revision tag is expanded by Source Code control tool
--
   RETURN '$Revision$' ;
END version ;
END xxc_request;
/
