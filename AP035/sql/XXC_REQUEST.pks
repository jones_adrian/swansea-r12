CREATE OR REPLACE
PACKAGE "XXC_REQUEST" AS
/*******************************************************************************
*
* $Header$
*
* Module      : Package containing the procedures for submitting
*               a chain of concurrent requests for module XXCAP035.
*
* Module Type : PL/SQL Package
*
* Original Author: Richard Pickett
*
* Description    : 
*
* Change History
* $Log$
*
*******************************************************************************/
-- Global constants

-- time in seconds program will wait for request completion
gc_wait_time   PLS_INTEGER := 5; 
-- number of times program will wait above number of seconds for completion
-- before timing out
gc_wait_events PLS_INTEGER := 1;

--++
-- put_message
--
PROCEDURE put_message (
  p_message_num IN VARCHAR2,
  p_message     IN VARCHAR2,
  p_module      IN VARCHAR2);
  
--+
-- set_apps_env
-- Sets the apps environment if running outside of the application.
-- Default sysadmin user and payables responsibility and application are used.
--
PROCEDURE set_apps_env;  
  
-- ++
-- submit_report_request
-- Submits a report to a concurrent manager.
-- If p_output_format is set to 'ETEXT', 'PDF', etc, also sets the
-- layout options for any XML Publisher template associated with the report.
-- --
PROCEDURE submit_report_request (
  p_application   IN VARCHAR2,
  p_program       IN VARCHAR2,
  p_description   IN VARCHAR2,
  p_template_code IN VARCHAR2,
  p_locale        IN VARCHAR2 := 'en-GB',
  p_output_format IN VARCHAR2 := 'REPORT',
  p_print_queue   IN VARCHAR2 := 'noprint',
  p_sub_request   IN BOOLEAN := FALSE,
  p_arg1          IN VARCHAR2 := CHR(0),
  p_arg2          IN VARCHAR2  := CHR(0),
  p_arg3          IN VARCHAR2 := CHR(0),
  p_arg4          IN VARCHAR2 := CHR(0),
  p_arg5          IN VARCHAR2 := CHR(0),
  p_arg6          IN VARCHAR2 := CHR(0),
  p_arg7          IN VARCHAR2 := CHR(0),
  p_arg8          IN VARCHAR2 := CHR(0),
  p_arg9          IN VARCHAR2 := CHR(0),
  p_arg10         IN VARCHAR2 := CHR(0),
  p_arg11         IN VARCHAR2 := CHR(0),
  p_arg12         IN VARCHAR2 := CHR(0),
  p_arg13         IN VARCHAR2 := CHR(0),
  p_arg14         IN VARCHAR2 := CHR(0),         
  p_arg15         IN VARCHAR2 := CHR(0),
  p_req_id        OUT NUMBER,
  p_req_status    OUT VARCHAR2);

-- ++
-- submit_to_print_queue
-- Submits an output file in the layout format specified to
-- a print queue - used at CCS for Design Print PARIS output.
-- --
PROCEDURE submit_to_print_queue (
  p_print_queue   IN VARCHAR2,
  p_request_id    IN NUMBER,
  p_sub_request   IN BOOLEAN :=   FALSE,
  p_output_format IN VARCHAR2 := 'ETEXT',
  p_req_status    OUT VARCHAR2
  );
-- ++
-- submit_xdo_request
-- Submits a standalone XML Publisher post-process
-- for the report request id passed in.
-- --
PROCEDURE submit_xdo_request (
  p_report_req_id IN NUMBER,
  p_output_format IN VARCHAR2 := 'ETEXT',
  p_req_id        OUT NUMBER,
  p_message       OUT VARCHAR2,
  p_req_status    OUT VARCHAR2);
  
-- ++
-- version
-- Returns the source control version of the package body
-- Used for configuration management and debugging
-- --
FUNCTION version RETURN VARCHAR2 ;

END xxc_request;
/

