#!/bin/ksh
#*************************************************************
#
#  $Header$
#
#  Original Author: Richard Pickett
#
#  Module Type    : UNIX Shell Script 
#
#  Description    :
#  General purpose utility for sending a text file to a 
#  Design Print queue. The name of the queue is the first 
#  parameter and the name of the file (full path) is the second
#  parameter. Both must be supplied by the calling program which
#  will normally be a concurrent request.
#  Design Print queues are set up by the UNIX administrator using
#  the "sam" utility. A queue of the same name is also set up in
#  the Design Print PARIS system. 
#
#  XXCDU803 - runs on the database server and copies the file to
#  the application server before spooling to Design Print
#
#  XXCDU804 - runs on the application server and spools the file
#  to Design Print without copying it first.
#
#  Modification History:
#  $Log$
#  31-10-2006 dthakore		Changes based on architecture 
#  13-11-2006 R Pickett		Created XXCDU804 based on XXCDU803
#  08-06-2006 Sandeep		Changes done for solving the issue - EARS00020950109 
#
#**************************************************************/

. /app2.sh
#. /app.sh

# Exit function called on error
errExitFunc() {
echo "*** ERROR $1"
echo "*** ====================================================="
exit 1
}

SETDEBUG="set -$-"

#Redirect stderr to stdout
exec 2>&1

# Banner (for apps log file)
echo "*** ============================================================"
echo "*** XXCDU804: Send report output from app server to Design Print"
echo "*** ============================================================"

# Set local variables after verifying parameters
# Note: first 4 parameters are apps standard

echo "Total No of Parameter " $#

#if [ $# -ne 2 ] || [ $# -ne 104 ]
#then
#  errExitFunc "Wrong No(s) of input parameters." 
#fi

if [ $# = 2 ] ;
then
  echo 'Printing Queue program is invoked from UNIX'

  PRINTQ="$1"
  FILENAME="$2"


echo "*** Input parameters"
echo "*** ================"
echo '*** Invoked at: '`date +%d-%m-%Y_%H:%M:%S`' by unix user: '`/usr/bin/whoami`
echo '*** print queue: ' $1
echo '*** print data file: ' $2

elif [ $# = 104 ] || [ $# = 6 ]
then
  echo 'Printing Queue program is invoked from APPS'
 
  PRINTQ="$5"

  # FILENAME="$6"						# Commented for EARS00020950109

  FILENAME=$APPLCSF/$APPLOUT/`echo "$6" | cut -f9 -d\/`		# Added for EARS00020950109

echo "*** Input parameters"
echo "*** ================"
echo '*** Invoked at: '`date +%d-%m-%Y_%H:%M:%S`' by unix user: '`/usr/bin/whoami`
echo '*** Program Name : '$0
# echo '*** Param 1 : '$1
# commented out as this would display the apps username and password
echo '*** apps user id : ' $2
echo '*** apps username: ' $3
echo '*** request id : ' $4
echo '*** print queue: ' $5
echo '*** print data file: ' $FILENAME				# Added for EARS00020950109


else

  errExitFunc "Wrong No(s) of input parameters." 

fi

#if [ -z "$5" ]
#then
#  errExitFunc "Parameter 5, print queue, not set"
#else
#  PRINTQ="$5"
#fi

#if [ -z "$6" ]
#then
#  errExitFunc "Parameter 6, file name, not set"
#else
#  FILENAME="$6"
#fi


# Verify that the file to be printed exists
##if [ ! -s "$FILENAME" ]
##then
##  errExitFunc "Print file $FILENAME does not exist or is empty"
##else

  echo "*** Sending file $FILENAME to queue $PRINTQ"

  # Execute call to print spooler
  
  lp -d $PRINTQ $FILENAME
  RESULT=$?

  # Exit with non-zero code if call failed otherwise exit with success 
  if [ "$RESULT" -ne 0 ]
  then
    errExitFunc "Call to lp spooler failed with return code $RESULT"
  else
    echo "*** Print job submission succeeded"
    echo "*** ====================================================="
    exit $RESULT
  fi
##fi