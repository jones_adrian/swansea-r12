#!/bin/ksh
#*************************************************************
#
#  $Header$
#
#  Original Author: Dhaval Thakore
#
#  Module Type    : UNIX Shell Script 
#
#  Description    :
#  Purpose of this script is to copy output file to other 
#  application tier for BACS program.
#
#  This script is applicable only for multiple application tier
#
#  Modification History:
#  $Log$
#  21-02-2007 dthakore   Initial Creation
#  10-04-2007 dthakore   Changes based on Application tier
#                        context file name path.
#**************************************************************/


# Gets shell options such as -x and use to pass into functions
SETDEBUG="set -$-"

# -----------------------------------------------------------------------------
# Diverts stderr to stdout so we capture all output in logfile
# -----------------------------------------------------------------------------

exec 2>&1	 


# -----------------------------------------------------------------------------

CreateFTP ()
{
  $SETDEBUG
  echo "**** Creating FTP script for to tranfer $FILENAME file from $CURHOSTNAME to $REMOTEHOSTNAME"
  echo 
  if [[ -a $SCRIPT ]]; then rm $SCRIPT ; fi
  touch $SCRIPT
  echo "open" $REMOTEHOSTNAME >> $SCRIPT
  echo "user" $REMOTEUSERNAME $REMOTEPASSWORD >> $SCRIPT
  echo "bin" >> $SCRIPT
  echo "cd" $REMOTEDIR >> $SCRIPT
  echo "lcd" $LOCALDIR >> $SCRIPT
  echo "put" $FILENAME >> $SCRIPT
  echo "bye" >> $SCRIPT

  chmod 700 $SCRIPT
}

CheckFileAppTier ()
{
  $SETDEBUG
  echo "**** Creating FTP script for to tranfer $FILENAME file from $REMOTEHOSTNAME to $CURHOSTNAME"
  #echo '**** Checking file on other Application tier $REMOTEHOSTNAME for '$FILENAME
  
  if [[ -a $SCRIPT ]]; then rm $SCRIPT ; fi
  touch $SCRIPT
  echo "open" $REMOTEHOSTNAME >> $SCRIPT
  echo "user" $REMOTEUSERNAME $REMOTEPASSWORD >> $SCRIPT
  echo "bin" >> $SCRIPT
  echo "cd" $REMOTEDIR >> $SCRIPT
  echo "lcd" $LOCALDIR >> $SCRIPT
  echo "ls " $NEWFILENAME >> $SCRIPT
  echo "get" $NEWFILENAME >> $SCRIPT
  #echo "echo file does not exist not remote server " >> $SCRIPT
  #echo "else" >> $SCRIPT
  #echo "get" $FILENAME >> $SCRIPT
  #echo "fi"
  echo "bye" >> $SCRIPT

  chmod 700 $SCRIPT
}

# -----------------------------------------------------------------------------

# -----------------------------------------------------------------------------
# Main Logic Starts Here
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# Grab datetime and unix process id to make unique id for temp files
# -----------------------------------------------------------------------------
DATERUN=`date +%d-%m-%Y_%H:%M:%S`
DATEANDPID=`/usr/bin/date +%C%y%m%d%H%M%S-$$`

# -----------------------------------------------------------------------------
# Start log output
# -----------------------------------------------------------------------------
echo '\n\n==========================================================='
echo '   XXCAP035C R@S File Transfer for n-tier Applications'
echo '==========================================================='
echo '**** Invoked at: '$DATERUN

# -----------------------------------------------------------------------------
# Assign variables
# -----------------------------------------------------------------------------

echo "Total No of Parameter " $#

if [ $# = 5 ] || [ $# = 104 ];
then
   echo "Invoke program from Oracle Application."
   FILENAME=$5
elif [ $# = 1 ] 
then
   echo "Invoke program from UNIX environment."
   FILENAME=$1
fi

CURHOSTNAME=`hostname`

#echo 'File Name ' $FILENAME 
NEWFILENAME=`echo $FILENAME | cut -f 9 -d /`
echo 'File Name ' $NEWFILENAME 

ENVNAME=`echo $APPLCSF  | cut -f 4 -d /`
#envname=`echo $ENVNAME | tr "[:upper:]" "[:lower:]"`
envname=`whoami`

ls -a $HOME/appsfile > /dev/null
if [ $? -ne 0 ] 
 then
 echo "ERROR: Password file does not exist"
 exit 1
fi

PASSWORD_FILE=$HOME/appsfile

APPSPASS=`cat $PASSWORD_FILE | grep '^APPS:' | awk '{print $2 ;}'`

RESULT=0

#
# Determine apps tiers that run Concurrent Managers from fnd_nodes
#

CURRENT_HOST=$(hostname)
echo Current host = $CURRENT_HOST

NODES=`sqlplus -s apps/"$APPSPASS" <<\!
set pages 0 feed off
SELECT node_name FROM fnd_nodes WHERE support_cp = 'Y';
whenever sqlerror exit 16;
whenever oserror exit 8; 
! `

if [ $? -ne 0 ]
  then 
  echo Error connecting to Database. Failed to Retrieve Node Details. 
  exit 1
fi

COUNTNODES=`sqlplus -s apps/"$APPSPASS" <<\!
set pages 0 feed off
SELECT count(node_name) FROM fnd_nodes WHERE support_cp = 'Y';
whenever sqlerror exit 16;
whenever oserror exit 8; 
! `

if [ $? -ne 0 ]
  then 
  echo Error connecting to Database. Failed to Retrieve Node Count. 
  exit 1
fi

NODES=$(echo $NODES|tr "[:upper:]" "[:lower:]")
echo Found $COUNTNODES nodes $NODES in fnd_nodes table

if [ $COUNTNODES -lt 2 ]
then
   echo Single Application Tier. Copy not required.
   RESULT=1
   exit 0
fi

echo Copy to follow:

#
# scp to remote nodes
#
#first check to see if file exists on localhost...

if [ -s $APPLCSF/$APPLOUT/$NEWFILENAME ]
then
   echo file exists on $CURRENT_HOST
   for a in $NODES
   do
   if [ "$a" = "$CURRENT_HOST" ]
   then
      echo Target Host = $a' - No need to copy to current host, skipping'
   else
      echo Target Host = $a - Initiating copy as follows
      echo FROM $CURRENT_HOST:$APPLCSF/$APPLOUT/$NEWFILENAME
      echo TO   $envname@$a:$APPLCSF/out/$ENVNAME'_'$a
      scp -pr $APPLCSF/$APPLOUT/$NEWFILENAME \
               $envname@$a:$APPLCSF/out/$ENVNAME'_'$a/              
      if [ $? -eq 0 ]
      then 
         RESULT=1
      fi
   fi
   done
    
else

   echo file does not exist on local host
   for a in $NODES
   do
   if [ "$a" != "$CURRENT_HOST" ]
   then
      for b in $NODES
      do
      if [ "$a" != "$b" ]
      then
         echo Source Host = $a 
         echo Target Host = $b - Initiating copy as follows
         echo FROM $a:$APPLCSF/out/$ENVNAME'_'$a/$NEWFILENAME
         echo TO   $envname@$b:$APPLCSF/out/$ENVNAME'_'$b
         echo '\n\n'
         scp -pr $a:$APPLCSF/out/$ENVNAME'_'$a/$NEWFILENAME \
                  $envname@$b:$APPLCSF/out/$ENVNAME'_'$b/            
         if [ $? -eq 0 ]
         then 
            RESULT=1
         fi
      fi         
      done
  fi
  done 
fi

if [ $RESULT -eq 1 ]
then
   echo '\n==========================================================='
   echo File Transfer Successful
   exit 0
else
   echo File Transfer Error
   exit 1
fi

echo '\n\n==========================================================='cat $SESSIONLOG 
echo
echo '**** SCP complete' 
echo '**** Execution finished at: '`date +%d-%m-%Y_%H:%M:%S`

exit 0

# ------------------------------------------------------------------
# End of Main Logic
# ------------------------------------------------------------------