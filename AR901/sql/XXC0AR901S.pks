create or replace PACKAGE APPS.XXC_AR_DUNNING_LETTER AS
/*************************************************************

$Header$

Original Author: Navin Kamat

Module Type    : SQL*Plus

Description    : This is package specification.
                 This package will extract all Dunning Letter info from 		
	         Receivables along with the customer details.


Modification History:
$Log$

25-AUG-2006     Navin Kamat    Initial Creation

**************************************************************/

   

PROCEDURE DUNNING_LETTER_OUT (  p_errbuf              OUT   VARCHAR2,
                                p_retcode             OUT     NUMBER,
                                p_order_by_number     IN    VARCHAR2,
                              	p_dunning_date        IN        DATE,
     				            p_letter_set_low      IN    VARCHAR2,
                                p_letter_set_high     IN    VARCHAR2,
				                p_customer_low        IN    VARCHAR2,
                                p_customer_high       IN    VARCHAR2,
				                p_collector_low       IN    VARCHAR2,
                                p_collector_high      IN    VARCHAR2,
				                p_cust_num_low        IN    VARCHAR2,
                                p_cust_num_high       IN    VARCHAR2,
				                p_dunning_method      IN    VARCHAR2,
				                p_transaction_type_l  IN    VARCHAR2,
                                p_transaction_type_h  IN    VARCHAR2,
				                p_country_code        IN    VARCHAR2,
				                p_preliminary_flag    IN    VARCHAR2,
				                p_dunning_level_l     IN    VARCHAR2,
				                p_dunning_level_h     IN    VARCHAR2);

END XXC_AR_DUNNING_LETTER;
/