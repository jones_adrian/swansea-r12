CREATE OR REPLACE PACKAGE BODY XXC_AR_DUNNING_LETTER
AS
  /*************************************************************
  $Header$
  Original Author: Navin Kamat
  Module Type    : SQL*Plus
  Description    : This is package body.
  This package will extract all Dunning Letter info from
  Receivables along with the customer details.
  Modification History:
  $Log$
  25-AUG-2006     Navin Kamat    Initial Creation
  19-OCT-2006     Navin Kamat    Added Custom Table For Copy Invoice Functionality,
  Null handling in Address Fields(Roll Up) ,
  Headers to be Printed when No Invoices,
  Invoices to be Printed on Separate Lines without
  headers info being repeated.
  17-NOV-2006     Dhaval Thakore Change to add '$END' at the end of file
  as per suzanne mail
  23-NOV-2006     Dhaval Thakore Changes to call AR007 - VAT Notice for Copy invoice
  criteria is true for a transaction
  29-NOV-2006     Dhaval Thakore Changes as for Print Queue Profile option
  01-MAR-2007     Sunil Kalal    Changes for additional layout for $NET, $TOTAL
  AND $ PAGE as per mail from Anthony/Phil Clark
  21-MAR-2007     Sunil Kalal    Added tag $LetterDateEnd after Letter Date as per
  Mark Bharucha's suggestion
  04-APR-2007     Sunil Kalal    Changes for the layout as suggested by Jonathan
  Shuker as a result of Design Print
  requirements.
  03-MAY-2007     Sunil Kalal    The amount needs to be in the 99,999,999,990.99
  format as a change request for
  rec_finance_charges,rec_total and
  rec_balance_due.
  9-JUNE-2011     Suyog Kamble   Capture and print full details of the first
  two transaction lines only for each invoice.
  **************************************************************/
PROCEDURE DUNNING_LETTER_OUT(
    p_errbuf OUT VARCHAR2,
    p_retcode OUT NUMBER,
    p_order_by_number    IN VARCHAR2,
    p_dunning_date       IN DATE,
    p_letter_set_low     IN VARCHAR2,
    p_letter_set_high    IN VARCHAR2,
    p_customer_low       IN VARCHAR2,
    p_customer_high      IN VARCHAR2,
    p_collector_low      IN VARCHAR2,
    p_collector_high     IN VARCHAR2,
    p_cust_num_low       IN VARCHAR2,
    p_cust_num_high      IN VARCHAR2,
    p_dunning_method     IN VARCHAR2,
    p_transaction_type_l IN VARCHAR2,
    p_transaction_type_h IN VARCHAR2,
    p_country_code       IN VARCHAR2,
    p_preliminary_flag   IN VARCHAR2,
    p_dunning_level_l    IN VARCHAR2,
    p_dunning_level_h    IN VARCHAR2 )
IS
  ---------------------------------------------
  -- Variable Declaration
  ---------------------------------------------
  l_data    VARCHAR2 (6000);
  l_string  CHAR(240) :=0;
  L_STR     CHAR(240) :=0;
  l_string1 CHAR(60)  := 0;
  l_string2 CHAR(60)  := 0;
  l_string3 CHAR(60)  := 0;
  l_string4 CHAR(60)  := 0;
  l_string5 CHAR(60)  := 0;
  L_STR1    CHAR(60)  := 0;
  L_STR2    CHAR(60)  := 0;
  L_STR3    CHAR(60)  := 0;
  L_STR4    CHAR(60)  := 0;
  l_file_handle UTL_FILE.file_type;
  l_output_dir              VARCHAR2 (100);
  l_output_file             VARCHAR2 (100);
  l_request_id              NUMBER ;
  l_total_succeed_rec       NUMBER := 0 ;
  l_total_error_rec         NUMBER := 0 ;
  l_csr_rec1_count          NUMBER := 0 ;
  l_csr_rec2_count          NUMBER := 0 ;
  l_csr_rec3_count          NUMBER := 0;
  l_errcode                 NUMBER ;
  l_errdesc                 VARCHAR2(250) ;
  l_contact                 VARCHAR2(500) ;
  l_description             VARCHAR2(50) ;
  l_submit_request_id       NUMBER;
  l_profile                 VARCHAR2(250);
  l_printQ_profile          VARCHAR2(250);
  l_file_location           VARCHAR2(250);
  l_request_id_file         NUMBER;
  l_Count                   NUMBER;
  l_Fianance_charges        NUMBER;
  l_Total                   NUMBER;
  l_correspondence_id_check NUMBER;
  l_trx_id_check            NUMBER;
  l_customer_trx_id         NUMBER;
  l_copy_Inv_Count          NUMBER;
  l_rec_total               NUMBER :=0;
  l_rec_finance             NUMBER :=0;
  l_rec_count               NUMBER :=0;
  l_line_count              NUMBER :=0;
  var1						char(25) :=0;
  var2						char(20) :=0;
  var3						char(15) :=0;


--------------------------------------------------------------------------------------------

-------
  -- Table Declaration.


--------------------------------------------------------------------------------------------

-------
TYPE l_reminder_letter_out
IS
  RECORD
  (
    rec_correspondence_id ar_correspondences.correspondence_id%TYPE,
    rec_letter_name_tag ar_dunning_letters.letter_name%TYPE,
    rec_bill_to_customer hz_parties.party_name%TYPE,
    rec_bill_to_customer_id hz_cust_accounts.cust_Account_id%TYPE,
    rec_bill_to_contact VARCHAR2(500),
    rec_bill_to_address_1 hz_locations.address1%TYPE,
    rec_bill_to_address_2 hz_locations.address2%TYPE,
    rec_bill_to_address_3 hz_locations.address3%TYPE,
    rec_city hz_locations.city%TYPE,
    rec_county hz_locations.county%TYPE,
    rec_postal_code hz_locations.postal_code%TYPE,
    rec_customer_trx_id ra_customer_trx_all.customer_trx_id%type,
    rec_transaction_number ar_payment_schedules.trx_number%TYPE,
    rec_line_number RA_CUSTOMER_TRX_LINES_all.line_number%TYPE,
    rec_description RA_CUSTOMER_TRX_LINES_all.description%TYPE,
    rec_quantity_invoiced RA_CUSTOMER_TRX_LINES_all.quantity_invoiced%TYPE,
    rec_UNIT_SELLING_PRICE RA_CUSTOMER_TRX_LINES_all.UNIT_SELLING_PRICE%TYPE,
    rec_revenue_amount RA_CUSTOMER_TRX_LINES_all.revenue_amount%TYPE,
    rec_TAX_CODE RA_CUSTOMER_TRX_LINES_v.tax_code%TYPE,
    rec_letter_date ar_correspondences.correspondence_date%TYPE,
    rec_due_date ar_payment_schedules.due_date%TYPE,
    rec_balance_due     NUMBER,
    rec_finance_charges NUMBER,
    rec_total           NUMBER,
    rec_count           NUMBER,
    rec_copy_invoice    VARCHAR2(1) );
TYPE l_reminder_letter_out_table
IS
  TABLE OF l_reminder_letter_out INDEX BY BINARY_INTEGER;
  l_extract_reminder_tab l_reminder_letter_out_table;
BEGIN
  fnd_file.put_line(fnd_file.log, '------------------------------------------------');
  Fnd_File.put_line(Fnd_File.LOG,'XXC Dunning Letter Start.');
  fnd_file.put_line(fnd_file.log, '------------------------------------------------');
  -----------------------------------------------------------------------
  -- Deleting  From Temp Table used for Copy Invoices
  -----------------------------------------------------------------------
  fnd_file.put_line(fnd_file.log, 'Deleting Record from Temporary Table for ' || 'VAT Notice Functionality');
  DELETE XXC_AR901_COPY_INVOICES;
  COMMIT;
  l_copy_Inv_Count :=0;
  l_request_id     := fnd_global.conc_request_id;
  l_output_file    := 'XXCOF_AR_DUNNING' || '_' || TO_CHAR (SYSDATE, 'YYYYMMDDHHMISS') || '.dat' ;
  l_profile        := FND_PROFILE.value('CCS_AR_DUN_LET');
  -- l_profile :='/usr/tmp';
  l_output_dir := l_profile;
  -- Following  IF THEN  is for  Order By Customer   ELSE  Order By Postal_code
  IF p_order_by_number = '3  ASC' THEN
    fnd_file.put_line(fnd_file.log, 'Start of Customer name Order By');
    FOR l_csr_rec1 IN
    (
	SELECT DISTINCT 
	  cor.correspondence_id correspondence_id, 
      dl.letter_name, 
      NVL(a_send_to.translated_customer_name,substrb(party.party_name,1,50)) customer_name, 
      cus.cust_Account_id customer_id, 
      cus.account_number customer_number, 
      cus.cust_account_id , 
      a_send_to.CUST_ACCT_SITE_ID , 
      a_send_to_loc.address1 send_to_address1, 
      a_send_to_loc.address2 send_to_address2, 
      a_send_to_loc.address3 send_to_address3, 
      a_send_to_loc.city send_to_city, 
      a_send_to_loc.county send_to_county, 
      a_send_to_loc.postal_code postal_code, 
      cor.correspondence_date dun_date, 
      adlsl.invoice_copies copy_invoice, 
	  TRX.TRX_NUMBER trx_no, 
	  cor.correspondence_date, 
	  adls.name adname, 
	  party.party_name, 
	  col.name coname, 
	  cus.account_number,
	  TRX.CUSTOMER_TRX_ID, 
      ps.trx_number trx_number, 
      ps.due_date due_date, 
      cps.amount_due_remaining amount_due_remaining
 FROM ar_correspondences cor, 
      hz_customer_profiles cust_cp, 
      hz_customer_profiles site_cp, 
      ar_collectors col, 
      ar_dunning_letters dl, 
      fnd_territories_vl t_send_to, 
      fnd_territories_vl t_remit_to, 
      hz_cust_acct_sites a_send_to, 
      hz_party_sites a_send_to_ps, 
      hz_locations a_send_to_loc, 
      hz_cust_acct_sites a_remit_to, 
      hz_party_sites a_remit_to_ps, 
      hz_locations a_remit_to_loc, 
      hz_cust_site_uses su_send_to, 
      hz_cust_accounts cus, 
      hz_parties party, 
      ar_dunning_letter_sets adls, 
      ar_dunning_letter_set_lines adlsl, 
	  ar_correspondence_pay_sched cps, 
      ar_payment_schedules ps, 
      ra_customer_trx_ALL trx, 
      ar_lookups l_credit, 
      ar_lookups l_debit, 
      fnd_document_sequences f
    WHERE cust_cp.cust_account_id 	   = cor.customer_id 
    AND cust_cp.site_use_id            IS NULL 
    AND site_cp.site_use_id(+)         = DECODE('N', 'N', cor.site_use_id, ARPT_SQL_FUNC_UTIL.GET_BILL_ID(cor.site_use_id)) 
    AND col.collector_id               = NVL(site_cp.collector_id,cust_cp.collector_id) 
    AND dl.dunning_letter_id           = cor.reference2 
    AND su_send_to.site_use_id         = cor.site_use_id 
    AND a_send_to.cust_acct_site_id    = su_send_to.cust_acct_site_id 
    AND a_send_to.party_site_id        = a_send_to_ps.party_site_id 
    AND a_send_to_loc.location_id      = a_send_to_ps.location_id 
    AND a_send_to_loc.country          = t_send_to.territory_code 
    AND cus.cust_account_id            = cor.customer_id 
    AND cus.party_id                   = party.party_id 
    AND a_remit_to.cust_acct_site_id(+)= cor.remit_to_address_id 
    AND a_remit_to.party_site_id       = a_remit_to_ps.party_site_id(+) 
    AND a_remit_to_loc.location_id(+)  = a_remit_to_ps.location_id 
    AND a_remit_to_loc.country         = t_remit_to.territory_code (+) 
    AND cor.reference1                 = adls.dunning_letter_set_id 
    AND adlsl.dunning_letter_set_id    = cor.reference1 
    AND adlsl.dunning_letter_id        = cor.reference2 
	AND ps.payment_schedule_id         = cps.payment_schedule_id 
    AND trx.customer_trx_id(+)   	   = ps.customer_trx_id 
    AND f.doc_sequence_id(+)     	   = trx.doc_sequence_id 
    AND l_credit.lookup_type     	   ='DEBIT/CREDIT' 
    AND l_credit.lookup_code     	   ='C' 
    AND l_debit.lookup_type     	   ='DEBIT/CREDIT' 
    AND l_debit.lookup_code     	   ='D' 
    AND ps.invoice_currency_code 	   = 'GBP' 
    AND cps.correspondence_id    	   = cor.correspondence_id 
    AND cor.printed_flag         	   = 'Y' 
    AND DECODE (SIGN(ps.amount_due_original),-1,l_credit.meaning,l_debit.meaning) = 'Debit' 
    AND TRUNC(cor.correspondence_date) = NVL(TRUNC(p_dunning_date),TRUNC(cor.correspondence_date))
    AND (adls.name BETWEEN NVL(p_letter_set_low,adls.name) AND NVL(p_letter_set_high,adls.name))
    AND (party.party_name BETWEEN NVL(p_customer_low,party.party_name) AND NVL(p_customer_high,party.party_name))
    AND (col.name BETWEEN NVL(P_Collector_Low,col.name) AND NVL(P_Collector_high,col.name))
    AND (cus.account_number BETWEEN NVL(p_cust_num_low,cus.account_number) AND NVL(p_cust_num_high,cus.account_number))
    AND adls.dunning_type      = NVL(p_dunning_method,adls.dunning_type)
    AND a_remit_to_loc.country = NVL(p_country_code,a_remit_to_loc.country)
    AND cor.preliminary_flag   = NVL(p_preliminary_flag,cor.preliminary_flag)
    AND (ps.trx_number BETWEEN NVL(p_transaction_type_l,ps.trx_number) AND NVL(p_transaction_type_h,ps.trx_number))
    AND (ps.staged_dunning_level BETWEEN NVL(P_DUNNING_LEVEL_L,ps.staged_dunning_level) AND NVL(P_DUNNING_LEVEL_H,ps.staged_dunning_level))	
	ORDER BY 3
    )
    LOOP
      l_contact        := NULL;
      l_csr_rec1_count := l_csr_rec1_count + 1;
      Fnd_File.put_line(Fnd_File.LOG,'Inside Outer Loop' ||l_csr_rec1_count);
      Fnd_File.put_line(Fnd_File.LOG,'Before Letter Name Description '||l_csr_rec1.correspondence_id);
      --  To Validate Letter Name Tag from  Value Set
      BEGIN
        SELECT ffvv.description
        INTO l_description
        FROM fnd_flex_values_vl ffvv,
          fnd_flex_value_sets ffvs
        WHERE ffvs.flex_value_set_id = ffvv.flex_value_set_id
        AND ffvs.flex_value_set_name = 'CCS_AR_DUNNING_LETTERS'
        AND ffvv.flex_value          = l_csr_rec1.letter_name;
      EXCEPTION
      WHEN OTHERS THEN
        l_description := NULL;
      END;
      Fnd_File.put_line(Fnd_File.LOG,'Checking Contact Info exist for '||l_csr_rec1.correspondence_id);
      BEGIN
        --SELECT Title||' '||first_name||' '||last_name
        SELECT first_name
          ||' '
          ||last_name
        INTO l_contact
        FROM ar_contacts_v
        WHERE customer_id = l_csr_rec1.cust_account_id
        AND address_id    = l_csr_rec1.cust_acct_site_id
        AND rownum        = 1 ;
      EXCEPTION
      WHEN OTHERS THEN
        l_contact := NULL;
      END;
      Fnd_File.put_line(Fnd_File.LOG,'Checking Invoice exist for '||l_csr_rec1.correspondence_id);
      --  To Check  whether invoices are exsisting
	  BEGIN
        SELECT SUM(cps.amount_accrue   + cps.amount_unaccrue),
          SUM(cps.amount_due_remaining + cps.amount_accrue + cps.amount_unaccrue )
        INTO l_Fianance_charges,
          l_Total
        FROM ar_correspondence_pay_sched cps,
          ar_payment_schedules ps,
          ra_customer_trx trx,
          ar_lookups l_credit,
          ar_lookups l_debit,
          fnd_document_sequences f
        WHERE ps.payment_schedule_id                                                      = cps.payment_schedule_id
        AND trx.customer_trx_id(+)                                                        = ps.customer_trx_id
        AND f.doc_sequence_id(+)                                                          = trx.doc_sequence_id
        AND l_credit.lookup_type                                                          = 'DEBIT/CREDIT'
        AND l_credit.lookup_code                                                          = 'C'
        AND l_debit.lookup_type                                                           = 'DEBIT/CREDIT'
        AND l_debit.lookup_code                                                           = 'D'
        AND cps.correspondence_id                                                         = l_csr_rec1.correspondence_id
		and trx.trx_number                                                                = l_csr_rec1.trx_no
        AND ps.invoice_currency_code                                                      = 'GBP'
        AND DECODE( SIGN(ps.amount_due_original), -1, l_credit.meaning, l_debit.meaning ) = 'Debit'
        AND (ps.trx_number BETWEEN NVL(p_transaction_type_l,ps.trx_number) AND NVL(p_transaction_type_h,ps.trx_number))
        AND (ps.staged_dunning_level BETWEEN NVL(P_DUNNING_LEVEL_L,ps.staged_dunning_level) AND NVL(P_DUNNING_LEVEL_H,ps.staged_dunning_level));
      EXCEPTION
      WHEN OTHERS THEN
        l_Fianance_charges:= NULL;
        l_Total           := NULL;

	  END;
	  
	  begin
	  
	  	select count(rctla.line_number) into l_line_count
		from ra_customer_trx_lines_all rctla, ra_customer_trx_all rcta
		where rcta.customer_trx_id = rctla.customer_trx_id
		and rcta.trx_number = l_csr_rec1.trx_no
		and rctla.LINE_TYPE = 'LINE'
		group by rcta.trx_number;
		exception when others then l_line_count := 0;
	  
	  end;

	    l_extract_reminder_tab (l_csr_rec1_count).rec_correspondence_id  := l_csr_rec1.correspondence_id;
        l_extract_reminder_tab (l_csr_rec1_count).rec_letter_name_tag    := l_description;
        l_extract_reminder_tab (l_csr_rec1_count).rec_bill_to_customer   := l_csr_rec1.customer_name;
        l_extract_reminder_tab (l_csr_rec1_count).rec_bill_to_customer_id:= l_csr_rec1.customer_id;
        l_extract_reminder_tab (l_csr_rec1_count).rec_bill_to_contact    := l_contact;
        l_extract_reminder_tab (l_csr_rec1_count).rec_bill_to_address_1  := l_csr_rec1.send_to_address1;
        l_extract_reminder_tab (l_csr_rec1_count).rec_bill_to_address_2  := l_csr_rec1.send_to_address2;
        l_extract_reminder_tab (l_csr_rec1_count).rec_bill_to_address_3  := l_csr_rec1.send_to_address3;
        l_extract_reminder_tab (l_csr_rec1_count).rec_city               := l_csr_rec1.send_to_city;
        l_extract_reminder_tab (l_csr_rec1_count).rec_county             := l_csr_rec1.send_to_county;
        l_extract_reminder_tab (l_csr_rec1_count).rec_postal_code        := l_csr_rec1.postal_code;
        l_extract_reminder_tab (l_csr_rec1_count).rec_transaction_number := l_csr_rec1.TRX_NO;
        l_extract_reminder_tab (l_csr_rec1_count).rec_letter_date        := l_csr_rec1.dun_date;
        l_extract_reminder_tab (l_csr_rec1_count).rec_due_date           := l_csr_rec1.DUE_DATE;
        l_extract_reminder_tab (l_csr_rec1_count).rec_balance_due        := l_csr_rec1.AMOUNT_DUE_REMAINING;
		l_extract_reminder_tab (l_csr_rec1_count).rec_customer_trx_id    := l_csr_rec1.customer_trx_id;
        l_extract_reminder_tab (l_csr_rec1_count).rec_finance_charges    := l_Fianance_charges;
        l_extract_reminder_tab (l_csr_rec1_count).rec_total              := l_Total;
        l_extract_reminder_tab (l_csr_rec1_count).rec_line_number        := l_line_count;
        l_extract_reminder_tab (l_csr_rec1_count).rec_description        := NULL;
        l_extract_reminder_tab (l_csr_rec1_count).rec_quantity_invoiced  := NULL;
        l_extract_reminder_tab (l_csr_rec1_count).rec_UNIT_SELLING_PRICE := NULL;
        l_extract_reminder_tab (l_csr_rec1_count).rec_revenue_amount     := NULL;
        l_extract_reminder_tab (l_csr_rec1_count).rec_TAX_CODE           := NULL;
        l_extract_reminder_tab (l_csr_rec1_count).rec_copy_invoice       := l_csr_rec1.copy_invoice;
    END LOOP;
    Fnd_File.put_line(Fnd_File.LOG, 'End of Outer Loop ' || l_csr_rec1_count);
    fnd_file.put_line(fnd_file.log, 'End of Customer name Order By');
    ----------------------------------------------------------------------
    -- End of Customer Name If
    ----------------------------------------------------------------------
    --  Following ELSE is For Order By Postal_Code
  ELSE
    fnd_file.put_line(fnd_file.log, 'Inside Order by Postal Code');
    FOR l_csr_rec1 IN
    (
	SELECT DISTINCT 
	  cor.correspondence_id correspondence_id, 
      dl.letter_name, 
      NVL(a_send_to.translated_customer_name,substrb(party.party_name,1,50)) customer_name, 
      cus.cust_Account_id customer_id, 
      cus.account_number customer_number, 
      cus.cust_account_id , 
      a_send_to.CUST_ACCT_SITE_ID , 
      a_send_to_loc.address1 send_to_address1, 
      a_send_to_loc.address2 send_to_address2, 
      a_send_to_loc.address3 send_to_address3, 
      a_send_to_loc.city send_to_city, 
      a_send_to_loc.county send_to_county, 
      a_send_to_loc.postal_code postal_code, 
      cor.correspondence_date dun_date, 
      adlsl.invoice_copies copy_invoice, 
	  TRX.TRX_NUMBER trx_no, 
	  cor.correspondence_date, 
	  adls.name adname, 
	  party.party_name, 
	  col.name coname, 
	  cus.account_number,
	  TRX.CUSTOMER_TRX_ID, 
      ps.trx_number trx_number, 
      ps.due_date due_date, 
      cps.amount_due_remaining amount_due_remaining
 FROM ar_correspondences cor, 
      hz_customer_profiles cust_cp, 
      hz_customer_profiles site_cp, 
      ar_collectors col, 
      ar_dunning_letters dl, 
      fnd_territories_vl t_send_to, 
      fnd_territories_vl t_remit_to, 
      hz_cust_acct_sites a_send_to, 
      hz_party_sites a_send_to_ps, 
      hz_locations a_send_to_loc, 
      hz_cust_acct_sites a_remit_to, 
      hz_party_sites a_remit_to_ps, 
      hz_locations a_remit_to_loc, 
      hz_cust_site_uses su_send_to, 
      hz_cust_accounts cus, 
      hz_parties party, 
      ar_dunning_letter_sets adls, 
      ar_dunning_letter_set_lines adlsl, 
	  ar_correspondence_pay_sched cps, 
      ar_payment_schedules ps, 
      ra_customer_trx_ALL trx, 
      ar_lookups l_credit, 
      ar_lookups l_debit, 
      fnd_document_sequences f
    WHERE cust_cp.cust_account_id 	   = cor.customer_id 
    AND cust_cp.site_use_id            IS NULL 
    AND site_cp.site_use_id(+)         = DECODE('N', 'N', cor.site_use_id, ARPT_SQL_FUNC_UTIL.GET_BILL_ID(cor.site_use_id)) 
    AND col.collector_id               = NVL(site_cp.collector_id,cust_cp.collector_id) 
    AND dl.dunning_letter_id           = cor.reference2 
    AND su_send_to.site_use_id         = cor.site_use_id 
    AND a_send_to.cust_acct_site_id    = su_send_to.cust_acct_site_id 
    AND a_send_to.party_site_id        = a_send_to_ps.party_site_id 
    AND a_send_to_loc.location_id      = a_send_to_ps.location_id 
    AND a_send_to_loc.country          = t_send_to.territory_code 
    AND cus.cust_account_id            = cor.customer_id 
    AND cus.party_id                   = party.party_id 
    AND a_remit_to.cust_acct_site_id(+)= cor.remit_to_address_id 
    AND a_remit_to.party_site_id       = a_remit_to_ps.party_site_id(+) 
    AND a_remit_to_loc.location_id(+)  = a_remit_to_ps.location_id 
    AND a_remit_to_loc.country         = t_remit_to.territory_code (+) 
    AND cor.reference1                 = adls.dunning_letter_set_id 
    AND adlsl.dunning_letter_set_id    = cor.reference1 
    AND adlsl.dunning_letter_id        = cor.reference2 
	AND ps.payment_schedule_id         = cps.payment_schedule_id 
    AND trx.customer_trx_id(+)   	   = ps.customer_trx_id 
    AND f.doc_sequence_id(+)     	   = trx.doc_sequence_id 
    AND l_credit.lookup_type     	   ='DEBIT/CREDIT' 
    AND l_credit.lookup_code     	   ='C' 
    AND l_debit.lookup_type     	   ='DEBIT/CREDIT' 
    AND l_debit.lookup_code     	   ='D' 
    AND ps.invoice_currency_code 	   = 'GBP' 
    AND cps.correspondence_id    	   = cor.correspondence_id 
    AND cor.printed_flag         	   = 'Y' 
    AND DECODE (SIGN(ps.amount_due_original),-1,l_credit.meaning,l_debit.meaning) = 'Debit' 
    AND TRUNC(cor.correspondence_date) = NVL(TRUNC(p_dunning_date),TRUNC(cor.correspondence_date))
    AND (adls.name BETWEEN NVL(p_letter_set_low,adls.name) AND NVL(p_letter_set_high,adls.name))
    AND (party.party_name BETWEEN NVL(p_customer_low,party.party_name) AND NVL(p_customer_high,party.party_name))
    AND (col.name BETWEEN NVL(P_Collector_Low,col.name) AND NVL(P_Collector_high,col.name))
    AND (cus.account_number BETWEEN NVL(p_cust_num_low,cus.account_number) AND NVL(p_cust_num_high,cus.account_number))
    AND adls.dunning_type      = NVL(p_dunning_method,adls.dunning_type)
    AND a_remit_to_loc.country = NVL(p_country_code,a_remit_to_loc.country)
    AND cor.preliminary_flag   = NVL(p_preliminary_flag,cor.preliminary_flag)
    AND (ps.trx_number BETWEEN NVL(p_transaction_type_l,ps.trx_number) AND NVL(p_transaction_type_h,ps.trx_number))
    AND (ps.staged_dunning_level BETWEEN NVL(P_DUNNING_LEVEL_L,ps.staged_dunning_level) AND NVL(P_DUNNING_LEVEL_H,ps.staged_dunning_level))	
    ORDER BY 13 ASC
    )
    LOOP
      l_contact        := NULL;
      l_csr_rec1_count := l_csr_rec1_count + 1;
      Fnd_File.put_line(Fnd_File.LOG,'Inside Outer Loop' ||l_csr_rec1_count);
      Fnd_File.put_line(Fnd_File.LOG,'Before Letter Name Description '||l_csr_rec1.correspondence_id);
      --  To Validate Letter Name Tag from  Value Set
      BEGIN
        SELECT ffvv.description
        INTO l_description
        FROM fnd_flex_values_vl ffvv,
          fnd_flex_value_sets ffvs
        WHERE ffvs.flex_value_set_id = ffvv.flex_value_set_id
        AND ffvs.flex_value_set_name = 'CCS_AR_DUNNING_LETTERS'
        AND ffvv.flex_value          = l_csr_rec1.letter_name;
      EXCEPTION
      WHEN OTHERS THEN
        l_description := NULL;
      END;
      Fnd_File.put_line(Fnd_File.LOG,'Checking Contact Info exist for '||l_csr_rec1.correspondence_id);
      BEGIN
        --SELECT Title||' '||first_name||' '||last_name
        SELECT first_name
          ||' '
          ||last_name
        INTO l_contact
        FROM ar_contacts_v
        WHERE customer_id = l_csr_rec1.cust_account_id
        AND address_id    = l_csr_rec1.cust_acct_site_id
        AND rownum        = 1 ;
      EXCEPTION
      WHEN OTHERS THEN
        l_contact := NULL;
      END;

	  BEGIN

        SELECT SUM(cps.amount_accrue   + cps.amount_unaccrue),
          SUM(cps.amount_due_remaining + cps.amount_accrue + cps.amount_unaccrue )
        INTO l_Fianance_charges,
          l_Total
        FROM ar_correspondence_pay_sched cps,
          ar_payment_schedules ps,
          ra_customer_trx trx,
          ar_lookups l_credit,
          ar_lookups l_debit,
          fnd_document_sequences f
        WHERE ps.payment_schedule_id                                                      = cps.payment_schedule_id
        AND trx.customer_trx_id(+)                                                        = ps.customer_trx_id
        AND f.doc_sequence_id(+)                                                          = trx.doc_sequence_id
        AND l_credit.lookup_type                                                          = 'DEBIT/CREDIT'
        AND l_credit.lookup_code                                                          = 'C'
        AND l_debit.lookup_type                                                           = 'DEBIT/CREDIT'
        AND l_debit.lookup_code                                                           = 'D'
        AND cps.correspondence_id                                                         = l_csr_rec1.correspondence_id
		and trx.trx_number                                                                = l_csr_rec1.trx_no
        AND ps.invoice_currency_code                                                      = 'GBP'
        AND DECODE( SIGN(ps.amount_due_original), -1, l_credit.meaning, l_debit.meaning ) = 'Debit'
        AND (ps.trx_number BETWEEN NVL(p_transaction_type_l,ps.trx_number) AND NVL(p_transaction_type_h,ps.trx_number))
        AND (ps.staged_dunning_level BETWEEN NVL(P_DUNNING_LEVEL_L,ps.staged_dunning_level) AND NVL(P_DUNNING_LEVEL_H,ps.staged_dunning_level));
      EXCEPTION
      WHEN OTHERS THEN
        l_Fianance_charges:= NULL;
        l_Total           := NULL;

	  END;
	  
	  begin
	  
	  	select count(rctla.line_number) into l_line_count
		from ra_customer_trx_lines_all rctla, ra_customer_trx_all rcta
		where rcta.customer_trx_id = rctla.customer_trx_id
		and rcta.trx_number = l_csr_rec1.trx_no
		and rctla.LINE_TYPE = 'LINE'
		group by rcta.trx_number;
		exception when others then l_line_count := 0;
	  
	  end;


	    l_extract_reminder_tab (l_csr_rec1_count).rec_correspondence_id  := l_csr_rec1.correspondence_id;
        l_extract_reminder_tab (l_csr_rec1_count).rec_letter_name_tag    := l_description;
        l_extract_reminder_tab (l_csr_rec1_count).rec_bill_to_customer   := l_csr_rec1.customer_name;
        l_extract_reminder_tab (l_csr_rec1_count).rec_bill_to_customer_id:= l_csr_rec1.customer_id;
        l_extract_reminder_tab (l_csr_rec1_count).rec_bill_to_contact    := l_contact;
        l_extract_reminder_tab (l_csr_rec1_count).rec_bill_to_address_1  := l_csr_rec1.send_to_address1;
        l_extract_reminder_tab (l_csr_rec1_count).rec_bill_to_address_2  := l_csr_rec1.send_to_address2;
        l_extract_reminder_tab (l_csr_rec1_count).rec_bill_to_address_3  := l_csr_rec1.send_to_address3;
        l_extract_reminder_tab (l_csr_rec1_count).rec_city               := l_csr_rec1.send_to_city;
        l_extract_reminder_tab (l_csr_rec1_count).rec_county             := l_csr_rec1.send_to_county;
        l_extract_reminder_tab (l_csr_rec1_count).rec_postal_code        := l_csr_rec1.postal_code;
        l_extract_reminder_tab (l_csr_rec1_count).rec_transaction_number := l_csr_rec1.TRX_NO;
        l_extract_reminder_tab (l_csr_rec1_count).rec_letter_date        := l_csr_rec1.dun_date;
        l_extract_reminder_tab (l_csr_rec1_count).rec_due_date           := l_csr_rec1.DUE_DATE;
        l_extract_reminder_tab (l_csr_rec1_count).rec_balance_due        := l_csr_rec1.AMOUNT_DUE_REMAINING;
	    l_extract_reminder_tab (l_csr_rec1_count).rec_customer_trx_id    := l_csr_rec1.customer_trx_id;
        l_extract_reminder_tab (l_csr_rec1_count).rec_finance_charges    := l_Fianance_charges;
        l_extract_reminder_tab (l_csr_rec1_count).rec_total              := l_Total;
        l_extract_reminder_tab (l_csr_rec1_count).rec_line_number        := l_line_count;
        l_extract_reminder_tab (l_csr_rec1_count).rec_description        := NULL;
        l_extract_reminder_tab (l_csr_rec1_count).rec_quantity_invoiced  := NULL;
        l_extract_reminder_tab (l_csr_rec1_count).rec_UNIT_SELLING_PRICE := NULL;
        l_extract_reminder_tab (l_csr_rec1_count).rec_revenue_amount     := NULL;
        l_extract_reminder_tab (l_csr_rec1_count).rec_TAX_CODE           := NULL;
        l_extract_reminder_tab (l_csr_rec1_count).rec_copy_invoice       := l_csr_rec1.copy_invoice;
    END LOOP;
  END IF;
  ---------------------------------------------------------------------------
  -- End of Order By Clause
  ---------------------------------------------------------------------------
  /*
  --------------------------------------------------------------------------
  -- This will generate the Header Record of extract file
  --------------------------------------------------------------------------
  Fnd_File.put_line(Fnd_File.LOG,'Before UTL File');
  Fnd_File.put_line(Fnd_File.LOG,'l_output_dir'||l_output_dir);
  l_file_handle := UTL_FILE.fopen (l_output_dir, l_output_file, 'w');
  */
  --------------------------------------------------------------------------
  -- Extract Data from PL-SQL Table for file generation
  -- Check for any records exist to generate data file for Dunning Letter or
  -- not
  --------------------------------------------------------------------------
  --l_extract_reminder_tab.COUNT > 0
  IF l_extract_reminder_tab.EXISTS(1) THEN
    --------------------------------------------------------------------------
    -- This will generate the Header Record of extract file
    --------------------------------------------------------------------------
    Fnd_File.put_line(Fnd_File.LOG,'Start of UTL File Generation in -' || l_output_dir);
    l_file_handle             := UTL_FILE.fopen (l_output_dir, l_output_file, 'w');
    l_correspondence_id_check := 999999999;
	l_trx_id_check            := 0;
    FOR l_ctr                 IN l_extract_reminder_tab.FIRST .. l_extract_reminder_tab.LAST
    LOOP
      FND_FILE.PUT_LINE(FND_FILE.LOG, 'Building l_data ');
      l_rec_count := l_rec_count +1;
      BEGIN
        IF l_correspondence_id_check <> l_extract_reminder_tab (l_ctr).rec_correspondence_id
		and l_trx_id_check <> l_extract_reminder_tab (l_ctr).rec_customer_trx_id
		THEN
          FND_FILE.PUT_LINE(FND_FILE.LOG, 'Inside Customer Name and Address: ' || l_extract_reminder_tab (l_ctr).rec_bill_to_customer);
          l_data := '$PAGE'||CHR(10)||l_extract_reminder_tab (l_ctr).rec_letter_name_tag ||CHR(10)
                    ||ltrim(TO_CHAR(l_extract_reminder_tab (l_ctr).rec_finance_charges,'99,999,999,990.99'))
                    ||CHR(10) ||ltrim(TO_CHAR(l_extract_reminder_tab (l_ctr).rec_total,'99,999,999,990.99'))
                    ||CHR(10) ||TO_CHAR(l_extract_reminder_tab (l_ctr).rec_letter_date,'DD-MON-RRRR') ||CHR(10)
                    ||l_extract_reminder_tab (l_ctr).rec_bill_to_customer ||CHR(10) ||l_extract_reminder_tab(l_ctr).rec_bill_to_contact;
        IF l_extract_reminder_tab (l_ctr).rec_bill_to_address_1 IS NOT NULL THEN
            l_data                                                := L_data||chr(10)||l_extract_reminder_tab (l_ctr).rec_bill_to_address_1;
          END IF;
          IF l_extract_reminder_tab (l_ctr).rec_bill_to_address_2 IS NOT NULL THEN
            l_data                                                := L_data||chr(10)||l_extract_reminder_tab (l_ctr).rec_bill_to_address_2;
          END IF;
          IF l_extract_reminder_tab (l_ctr).rec_bill_to_address_3 IS NOT NULL THEN
            l_data                                                := L_data||chr(10)||l_extract_reminder_tab (l_ctr).rec_bill_to_address_3;
          END IF;
          IF l_extract_reminder_tab (l_ctr).rec_city IS NOT NULL THEN
            l_data                                   := L_data||chr(10)||l_extract_reminder_tab (l_ctr).rec_city;
          END IF;
          IF l_extract_reminder_tab (l_ctr).rec_county IS NOT NULL THEN
            l_data                                     := L_data||chr(10)||l_extract_reminder_tab (l_ctr).rec_county;
          END IF;
          IF l_extract_reminder_tab (l_ctr).rec_postal_code IS NOT NULL THEN
            l_data                                          := L_data||chr(10)||l_extract_reminder_tab (l_ctr).rec_postal_code;
          END IF;
 		  IF 1 <> 2 THEN
          l_data                    := l_data || CHR(10) || '$END'; --Added by Sunil Kalal on 27 Feb 07
		  END IF;

		  IF l_extract_reminder_tab(l_ctr).rec_transaction_number IS NOT NULL THEN
          l_string1 := rpad((l_extract_reminder_tab(l_ctr).rec_transaction_number),25)||rpad((l_extract_reminder_tab(l_ctr).rec_due_date),15)||lpad((TO_CHAR(l_extract_reminder_tab(l_ctr).rec_balance_due,'99,999,999,990.99')),20);
		  l_data    :=  L_data||chr(10)|| l_string1;
 		  l_correspondence_id_check := l_extract_reminder_tab (l_ctr).rec_correspondence_id;
		  l_trx_id_check            := l_extract_reminder_tab (l_ctr).rec_customer_trx_id;
		  END IF;

	    IF l_extract_reminder_tab(l_ctr).rec_transaction_number IS NOT NULL THEN
		BEGIN
		
		IF l_extract_reminder_tab(l_ctr).rec_line_number  = 1 THEN
		BEGIN
		SELECT DESCRIPTION INTO l_string
		FROM RA_CUSTOMER_TRX_LINES_ALL WHERE LINE_TYPE = 'LINE' AND LINE_NUMBER = 1
		AND CUSTOMER_TRX_ID in (select distinct customer_trx_id from ra_customer_trx_all where
		trx_number = l_extract_reminder_tab(l_ctr).rec_transaction_number) ;

		exception when no_data_found then l_string := 0;
		END;

        L_DATA := L_data||chr(10)||substr(l_string,1,60)||CHR(10)||substr(l_string,61,60)||CHR(10)||substr(l_string,121,60)||CHR(10)||substr(l_string,181,240)||CHR(10)||CHR(10)||CHR(10)||CHR(10);

		ELSIF l_extract_reminder_tab(l_ctr).rec_line_number >=2 THEN
		BEGIN
		SELECT DESCRIPTION INTO l_string
		FROM RA_CUSTOMER_TRX_LINES_ALL WHERE LINE_TYPE = 'LINE' AND LINE_NUMBER = 1 AND CUSTOMER_TRX_ID = trim(l_trx_id_check);
		SELECT DESCRIPTION INTO l_str
		FROM RA_CUSTOMER_TRX_LINES_ALL WHERE LINE_TYPE = 'LINE' AND LINE_NUMBER = 2 AND CUSTOMER_TRX_ID = trim(l_trx_id_check);
		END;
		L_DATA := L_data||chr(10)||substr(l_string,1,60)||CHR(10)||substr(l_string,61,60)||CHR(10)||substr(l_string,121,60)||CHR(10)||substr(l_string,181,240)
		          ||CHR(10)||substr(l_str,1,60)||CHR(10)||substr(l_str,61,60)||CHR(10)||substr(l_str,121,60)||CHR(10)||substr(l_str,181,240);
		END IF;
		END;
		END IF;

        END IF;


      fnd_file.put_line(fnd_file.log, 'Inserting Record into temporary table for ' || l_extract_reminder_tab (l_ctr).rec_transaction_number || ' - ' || l_extract_reminder_tab(l_ctr).rec_correspondence_id );
        IF l_extract_reminder_tab (l_ctr).rec_copy_invoice = 'Y' THEN
          ----------------------------------------------------
          -- GET TRX_NUMBER based on Correspondence ID
          ----------------------------------------------------
          INSERT
          INTO XXC_AR901_COPY_INVOICES
          SELECT A.TRX_NUMBER
          FROM ra_customer_trx_all A,
            ar_payment_schedules B,
            ar_correspondence_pay_sched C
          WHERE A.customer_trx_id   = B.customer_trx_id
          AND B.payment_schedule_id = C.payment_schedule_id
          AND C.correspondence_id   = l_extract_reminder_tab (l_ctr).rec_correspondence_id ;
          COMMIT;
          fnd_file.put_line(fnd_file.log, 'Record(s) Inserted successully into temporary table XXC_AR901_COPY_INVOICES');
          ----Insert into XXC_AR901_COPY_INVOICES
          ----      values(l_extract_reminder_tab (l_ctr).rec_transaction_number);
        END IF;
      EXCEPTION
      WHEN OTHERS THEN
        l_errcode := SQLCODE;
        l_errdesc := SUBSTR (SQLERRM, 1, 250);
        Fnd_File.put_line(Fnd_File.LOG, l_extract_reminder_tab (l_ctr).rec_transaction_number || ' with Oracle Error Number : '|| l_errcode || ' and error description as '||l_errdesc);
        l_total_error_rec := l_total_error_rec+1;
      END;
      -------------------------------------------------
      -- Added by Dhaval on 17-Nov-06 for $END as per request from
      -- Suzanne Boydell
      -------------------------------------------------
      --      l_data := l_data || CHR(10) || '$END';
      UTL_FILE.put_line (l_file_handle, l_data);
    END LOOP;
    -- Added by Dhaval
    COMMIT;
    UTL_FILE.fclose (l_file_handle);
    Fnd_File.put_line(Fnd_File.LOG,'Dunning Letter file generated with name: '||l_output_file);
    ----------------------------------------------------
    -- Changed By Dhaval on 28-Nov-06 as in production
    -- it pointing to $XXC_TOP/out and XXCDU803 is
    -- taking care for this location
    ----------------------------------------------------
    --l_file_location := l_output_dir||'/'||l_output_file;
    l_file_location := l_output_file;
    Fnd_File.put_line(Fnd_File.LOG,'Total No. of Records Processed: '||l_extract_reminder_tab.COUNT);
    -- Changed By Dhaval on 29-Nov-2006 as Production will have different
    -- set up as per richard
    --l_printQ_profile :=FND_PROFILE.value('CCS_DP_PRINT');
    l_printQ_profile :=FND_PROFILE.value('XXC_REMINDERS_DP_QUEUE');
    --Fnd_File.put_line(Fnd_File.LOG,'Profile name'||l_printQ_profile);
    Fnd_File.put_line(Fnd_File.LOG,'Calling Print Queue Program for Dunning Letter for '||l_printQ_profile );
    ------------------------------------------------------------------
    -- Submit requet for Printing Queue functionality Utility XXCDU803
    ------------------------------------------------------------------
    l_request_id_file := FND_REQUEST.SUBMIT_REQUEST ('XXC', -- application
    'XXCDU803',                                             -- program
    '',                                                     -- description
    NULL,                                                   -- start_time
    FALSE,                                                  -- sub_request
    l_printQ_profile,                                       -- argument1
    l_file_location,                                        -- argument2
    '',                                                     -- argument3
    '',                                                     -- argument4
    '',                                                     -- argument5
    '',                                                     -- argument6
    '',                                                     -- argument7
    '',                                                     -- argument8
    '',                                                     -- argument9
    '',                                                     -- argument10
    '',                                                     -- argument11
    '',                                                     -- argument12
    '',                                                     -- argument13
    '',                                                     -- argument14
    '',                                                     -- argument15
    '',                                                     -- argument16
    '',                                                     -- argument17
    '',                                                     -- argument18
    '',                                                     -- argument19
    '',                                                     -- argument20
    '',                                                     -- argument21
    '',                                                     -- argument22
    '',                                                     -- argument23
    '',                                                     -- argument24
    '',                                                     -- argument25
    '',                                                     -- argument26
    '',                                                     -- argument27
    '',                                                     -- argument28
    '',                                                     -- argument29
    '',                                                     -- argument30
    '',                                                     -- argument31
    '',                                                     -- argument32
    '',                                                     -- argument33
    '',                                                     -- argument34
    '',                                                     -- argument35
    '',                                                     -- argument36
    '',                                                     -- argument37
    '',                                                     -- argument38
    '',                                                     -- argument39
    '',                                                     -- argument40
    '',                                                     -- argument41
    '',                                                     -- argument42
    '',                                                     -- argument43
    '',                                                     -- argument44
    '',                                                     -- argument45
    '',                                                     -- argument46
    '',                                                     -- argument47
    '',                                                     -- argument48
    '',                                                     -- argument49
    '',                                                     -- argument50
    '',                                                     -- argument51
    '',                                                     -- argument52
    '',                                                     -- argument53
    '',                                                     -- argument54
    '',                                                     -- argument55
    '',                                                     -- argument56
    '',                                                     -- argument57
    '',                                                     -- argument58
    '',                                                     -- argument59
    '',                                                     -- argument60
    '',                                                     -- argument61
    '',                                                     -- argument62
    '',                                                     -- argument63
    '',                                                     -- argument64
    '',                                                     -- argument65
    '',                                                     -- argument66
    '',                                                     -- argument67
    '',                                                     -- argument68
    '',                                                     -- argument69
    '',                                                     -- argument70
    '',                                                     -- argument71
    '',                                                     -- argument72
    '',                                                     -- argument73
    '',                                                     -- argument74
    '',                                                     -- argument75
    '',                                                     -- argument76
    '',                                                     -- argument77
    '',                                                     -- argument78
    '',                                                     -- argument79
    '',                                                     -- argument80
    '',                                                     -- argument81
    '',                                                     -- argument82
    '',                                                     -- argument83
    '',                                                     -- argument84
    '',                                                     -- argument85
    '',                                                     -- argument86
    '',                                                     -- argument87
    '',                                                     -- argument88
    '',                                                     -- argument89
    '',                                                     -- argument90
    '',                                                     -- argument91
    '',                                                     -- argument92
    '',                                                     -- argument93
    '',                                                     -- argument94
    '',                                                     -- argument95
    '',                                                     -- argument96
    '',                                                     -- argument97
    '',                                                     -- argument98
    '',                                                     -- argument99
    ''                                                      -- argument100
    );
    IF l_request_id_file IS NOT NULL THEN
      COMMIT;
      Fnd_File.put_line(Fnd_File.LOG,'XXC Dunning Letter Request Submitted Successfully request ID :' || l_request_id_file);
    END IF;
    -----------------------------------------------------------
    -- Check for VAT Notice program
    -----------------------------------------------------------
    SELECT COUNT(*)
    INTO l_copy_Inv_Count
    FROM XXC_AR901_COPY_INVOICES;
    ---------------------------------------------------
    -- Changed by Dhaval on 17-Nov-06 as it was not calling AR007
    -- for one invoice.
    ---------------------------------------------------
    IF l_copy_Inv_Count >= 1 THEN
      fnd_file.put_line(fnd_file.log,'====================================================');
      fnd_file.put_line(fnd_file.log, 'Calling XXC Print Copy Transaction (AR007) for total invoices : ' || l_copy_inv_count);
      fnd_file.put_line(fnd_file.log, '====================================================');
      l_submit_request_id    := fnd_request.submit_request(
'XXC','XXC0AR007',NULL,NULL,FALSE,'CUSTOMER', NULL,NULL,NULL,NULL,
NULL,NULL,NULL,NULL,NULL,'Y','N',NULL,'SEL','1',
'N','10',NULL,NULL,'Y',NULL,NULL,NULL,NULL,NULL,
NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, NULL,NULL,NULL,NULL,NULL);
      IF l_submit_request_id IS NOT NULL THEN
        COMMIT;
        Fnd_File.put_line(Fnd_File.LOG, 'XXC Print Copy Transaction (AR007) Submitted successfully Request ID: ' ||l_submit_request_id);
      END IF;
      -- Add by Dhaval on 23-Nov-06
      --fnd_file.put_line(fnd_file.log, 'Deleting Record from Temporary Table');
      --DELETE FROM XXC_AR901_COPY_INVOICES;
      fnd_file.put_line(fnd_file.log, '====================================================');
      fnd_file.put_line(fnd_file.log, 'XXC Print Copy Transaction completed please check Request ID ' ||l_submit_request_id);
      fnd_file.put_line(fnd_file.log, '====================================================');
    END IF;
  ELSE
    Fnd_File.put_line(Fnd_File.LOG,'No Data Available For entered parameter for XXC Dunning Letter.');
  END IF;
  --------------------------------------------------------------------------
  -- This called procedure will generate a output file as per standard
  --------------------------------------------------------------------------

xxc_utils.get_run_report_output(l_request_id,l_total_succeed_rec,l_extract_reminder_tab.COUNT,l_total_error_rec,'I');
  fnd_file.put_line(fnd_file.log, '------------------------------------------------');
  Fnd_File.put_line(Fnd_File.LOG,'XXC Dunning Letter End.');
  fnd_file.put_line(fnd_file.log, '------------------------------------------------');
EXCEPTION
WHEN UTL_FILE.INVALID_FILEHANDLE THEN
  Fnd_File.put_line(Fnd_File.LOG,'File Handle Exception raised');
  APP_EXCEPTION.RAISE_EXCEPTION;
  UTL_FILE.fclose(l_file_handle);
  ROLLBACK;
  p_retcode:=2;
WHEN UTL_FILE.INVALID_PATH THEN
  Fnd_File.put_line(Fnd_File.LOG,'Path Exception');
  APP_EXCEPTION.RAISE_EXCEPTION;
  UTL_FILE.fclose(l_file_handle);
  ROLLBACK;
  p_retcode:=2;
WHEN UTL_FILE.INVALID_MODE THEN
  Fnd_File.put_line(Fnd_File.LOG,'MODE Exception');
  APP_EXCEPTION.RAISE_EXCEPTION;
  UTL_FILE.fclose(l_file_handle);
  ROLLBACK;
  p_retcode:=2;
WHEN UTL_FILE.INVALID_OPERATION THEN
  Fnd_File.put_line(Fnd_File.LOG,'OPERATION Exception');
  APP_EXCEPTION.RAISE_EXCEPTION;
  UTL_FILE.fclose(l_file_handle);
  ROLLBACK;
  p_retcode:=2;
WHEN OTHERS THEN
  p_errbuf  := SUBSTR (SQLERRM, 1, 250);
  p_retcode := SQLCODE;
  APP_EXCEPTION.RAISE_EXCEPTION;
END DUNNING_LETTER_OUT;
END XXC_AR_DUNNING_LETTER;
/
