CREATE OR REPLACE PACKAGE APPS.XXC_AR_DUNNING_LETTER_NEW_NG AS
/*************************************************************

$Header$

Original Author: Navin Kamat

Module Type    : SQL*Plus

Description    : This is package specification.
                 This package will extract all Dunning Letter info from
             Receivables along with the customer details.


Modification History:
$Log$

25-AUG-2006     Navin Kamat    Initial Creation

**************************************************************/



PROCEDURE DUNNING_LETTER_OUT(
           P_ERRBUF                   OUT NOCOPY     VARCHAR2,
           P_RETCODE                OUT NOCOPY     VARCHAR2,
            P_DUNNING_PLAN_ID          IN         NUMBER,
            p_order_by                 IN         VARCHAR2,
            p_dunning_date            IN   VARCHAR2,
            p_parent_request_id     IN   NUMBER,
            p_dunning_mode          IN   VARCHAR2,
            p_single_staged_letter  IN   VARCHAR2,
            p_customer_name_low     IN   VARCHAR2,               
            p_customer_name_high    IN   VARCHAR2,               
            p_account_number_low    IN   VARCHAR2,               
            p_account_number_high   IN   VARCHAR2,               
            p_billto_location_low   IN   VARCHAR2,               
            p_billto_location_high  IN   VARCHAR2);

END XXC_AR_DUNNING_LETTER_NEW_NG;
/
