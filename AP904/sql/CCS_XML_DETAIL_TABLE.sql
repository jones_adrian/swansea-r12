create table CCS_XML_DETAIL_TABLE
as
select invoice_number,
substr(Supplier_details,instr(Supplier_details,'<NAME index="1">',1,1)+16,instr(Supplier_details,'</NAME>',1,1)-(instr(Supplier_details,'<NAME index="1">',1,1)+16)) 
SUPPLIER_NAME,
substr(Supplier_details,instr(Supplier_details,'<ADDRLINE index="1">',1,1)+20,instr(Supplier_details,'</ADDRLINE>',1,1)-(instr(Supplier_details,'<ADDRLINE index="1">',1,1)+20)) 
SUPPLIER_ADDRESS,
substr(Supplier_details,instr(Supplier_details,'<CITY>',1,1)+6,instr(Supplier_details,'</CITY>',1,1)-(instr(Supplier_details,'<CITY>',1,1)+6)) 
SUPPLIER_CITY,
substr(Supplier_details,instr(Supplier_details,'<COUNTRY>',1,1)+9,instr(Supplier_details,'</COUNTRY>',1,1)-(instr(Supplier_details,'<COUNTRY>',1,1)+9)) 
SUPPLIER_COUNTRY,
substr(Supplier_details,instr(Supplier_details,'<STATEPROVN>',1,1)+12,instr(Supplier_details,'</STATEPROVN>',1,1)-(instr(Supplier_details,'<STATEPROVN>',1,1)+12)) 
SUPPLIER_STATE_PROVINCE,
substr(Supplier_details,instr(Supplier_details,'<POSTALCODE>',1,1)+12,instr(Supplier_details,'</POSTALCODE>',1,1)-(instr(Supplier_details,'<POSTALCODE>',1,1)+12)) 
SUPPLIER_POSTAL_CODE,
substr(Supplier_details,instr(Supplier_details,'<NAME index="1">',1,2)+16,instr(Supplier_details,'</NAME>',1,2)-(instr(Supplier_details,'<NAME index="1">',1,2)+16)) 
SUPPLIER_CONTACT_NAME,
substr(Supplier_details,instr(Supplier_details,'<EMAIL>',1,1)+7,instr(Supplier_details,'</EMAIL>',1,1)-(instr(Supplier_details,'<EMAIL>',1,1)+7)) 
SUPPLIER_EMAIL_ADDRESS,
substr(sold_to_details,instr(sold_to_details,'<DUNSNUMBER>',1,1)+12,instr(sold_to_details,'</DUNSNUMBER>',1,1)-(instr(sold_to_details,'<DUNSNUMBER>',1,1)+12)) 
SOLD_TO_DUNS_NUMBER,
substr(sold_to_details,instr(sold_to_details,'<NAME index="1">',1,1)+16,instr(sold_to_details,'</NAME>',1,1)-(instr(sold_to_details,'<NAME index="1">',1,1)+16)) 
SOLD_TO_NAME,
substr(sold_to_details,instr(sold_to_details,'<EMAIL>',1,1)+7,instr(sold_to_details,'</EMAIL>',1,1)-(instr(sold_to_details,'<EMAIL>',1,1)+7)) 
SOLD_TO_EMAIL,
substr(sold_to_details,instr(sold_to_details,'<TELEPHONE index="1">',1,1)+21,instr(sold_to_details,'</TELEPHONE>',1,1)-(instr(sold_to_details,'<TELEPHONE index="1">',1,1)+21)) 
SOLD_TO_TELEPHONE_INDEX,
substr(bill_to_details,instr(bill_to_details,'<NAME index="1">',1,1)+16,instr(bill_to_details,'</NAME>',1,1)-(instr(bill_to_details,'<NAME index="1">',1,1)+16)) 
BILL_TO_NAME,
substr(bill_to_details,instr(bill_to_details,'<ADDRLINE index="1">',1,1)+20,instr(bill_to_details,'</ADDRLINE>',1,1)-(instr(bill_to_details,'<ADDRLINE index="1">',1,1)+20)) 
BILL_TO_ADDRESS1,
substr(bill_to_details,instr(bill_to_details,'<ADDRLINE index="2">',1,1)+20,instr(bill_to_details,'</ADDRLINE>',1,2)-(instr(bill_to_details,'<ADDRLINE index="2">',1,1)+20)) 
BILL_TO_ADDRESS2,
substr(bill_to_details,instr(bill_to_details,'<CITY>',1,1)+6,instr(bill_to_details,'</CITY>',1,1)-(instr(bill_to_details,'<CITY>',1,1)+6)) 
BILL_TO_CITY,
substr(bill_to_details,instr(bill_to_details,'<POSTALCODE>',1,1)+12,instr(bill_to_details,'</POSTALCODE>',1,1)-(instr(bill_to_details,'<POSTALCODE>',1,1)+12)) 
BILL_TO_POSTAL_CODE,
substr(bill_to_details,instr(bill_to_details,'<TELEPHONE index="1">',1,1)+21,instr(bill_to_details,'</TELEPHONE>',1,1)-(instr(bill_to_details,'<TELEPHONE index="1">',1,1)+21)) BILL_TO_TELEPHONE
from CCS_XML_SEPERATE_HEADER_TABLE
where 1=2;
/