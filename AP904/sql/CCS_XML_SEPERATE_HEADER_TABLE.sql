create table CCS_XML_SEPERATE_HEADER_TABLE as
select
dbms_lob.substr(substr(payload,instr(payload,'<PARTNER>',1,1),(instr(payload,'</PARTNER>',1,
1)-instr(payload,'<PARTNER>',1,1)))) Supplier_Details,
dbms_lob.substr(substr(payload,instr(payload,'<PARTNER>',1,2),(instr(payload,'</PARTNER>',1,
2)-instr(payload,'<PARTNER>',1,2)))) SOLD_TO_DETAILS,
dbms_lob.substr(substr(payload,instr(payload,'<PARTNER>',1,4),(instr(payload,'</PARTNER>',1,
4)-instr(payload,'<PARTNER>',1,4)))) BILL_TO_DETAILS,
dbms_lob.substr(substr(payload,instr(payload,'<DOCUMENTID>',1)+12,instr(payload,'</DOCUMENTID>',1)-(instr(payload,'<DOCUMENTID>',1)+12))) 
INVOICE_NUMBER
from ecx_doclogs where transaction_type='INVOICE'
and 1=2;
/