CREATE OR REPLACE
PACKAGE BODY CCS_XML_INSERT_TABLE as
PROCEDURE CCS_XML_INSERT_INTO_TABLE( o_errbuf OUT VARCHAR2,o_retcode OUT VARCHAR2,T_Invoice IN varchar2,T_Supplier IN varchar2)
AS
---------------------------------------------------------------------------------------------------------------------------------------
cursor c1 is
select dbms_lob.substr(substr(payload,instr(payload,'<LOGICALID>',1)+11,instr(payload,'</LOGICALID>',1)-(instr(payload,'<LOGICALID>',1)+11))) SENDER_LOGIVALID,
dbms_lob.substr(substr(payload,instr(payload,'<COMPONENT>',1)+11,instr(payload,'</COMPONENT>',1)-(instr(payload,'<COMPONENT>',1)+11))) SENDER_COMPONENT,
dbms_lob.substr(substr(payload,instr(payload,'<TASK>',1)+6,instr(payload,'</TASK>',1)-(instr(payload,'<TASK>',1)+6))) SENDER_TASK,
dbms_lob.substr(substr(payload,instr(payload,'<REFERENCEID>',1)+13,instr(payload,'</REFERENCEID>',1)-(instr(payload,'<REFERENCEID>',1)+13))) REFERENCE_ID,
to_date(dbms_lob.substr(substr(payload,instr(payload,'<YEAR>',1)+6,instr(payload,'</YEAR>',1)-(instr(payload,'<YEAR>',1)+6)))||'-'||dbms_lob.substr(substr(payload,instr(payload,'<MONTH>',1)+7,instr(payload,'</MONTH>',1)-(instr(payload,'<MONTH>',1)+7)))||'-'||dbms_lob.substr(substr(payload,instr(payload,'<DAY>',1)+5,instr(payload,'</DAY>',1)-(instr(payload,'<DAY>',1)+5))),'YYYY-MM-DD') INVOICE_CREATION_DATE,
dbms_lob.substr(substr(payload,instr(payload,'<CONFIRMATION>',1)+14,instr(payload,'</CONFIRMATION>',1)-(instr(payload,'<CONFIRMATION>',1)+14))) CONFIRMATION_IDENTIFIER,
dbms_lob.substr(substr(payload,instr(payload,'<DOCUMENTID>',1)+12,instr(payload,'</DOCUMENTID>',1)-(instr(payload,'<DOCUMENTID>',1)+12))) INVOICE_NUMBER,
dbms_lob.substr(substr(payload,instr(payload,'<DESCRIPTN>',1)+11,instr(payload,'</DESCRIPTN>',1)-(instr(payload,'<DESCRIPTN>',1)+11))) INVOICE_DESCRIPTION,
dbms_lob.substr(substr(payload,instr(payload,'<DOCTYPE>',1)+9,instr(payload,'</DOCTYPE>',1)-(instr(payload,'<DOCTYPE>',1)+9))) Document_type,
dbms_lob.substr(substr(payload,instr(payload,'<REASONCODE>',1)+12,instr(payload,'</REASONCODE>',1)-(instr(payload,'<REASONCODE>',1)+12))) Reason_Code,
dbms_lob.substr(substr(payload,instr(payload,'<NAME index="1">',1)+16,instr(payload,'</NAME>',1)-(instr(payload,'<NAME index="1">',1)+16))) Supplier_Name,
dbms_lob.substr(substr(payload,instr(payload,'<PARTNRTYPE>',1)+12,instr(payload,'</PARTNRTYPE>',1)-(instr(payload,'<PARTNRTYPE>',1)+12))) PARTNER_TYPE,
dbms_lob.substr(substr(payload,instr(payload,'<DUNSNUMBER>',1)+12,instr(payload,'</DUNSNUMBER>',1)-(instr(payload,'<DUNSNUMBER>',1)+12))) DUNS_NUMBER_SUPPLIER,
dbms_lob.substr(substr(payload,instr(payload,'<PARTNRIDX>',1)+11,instr(payload,'</PARTNRIDX>',1)-(instr(payload,'<PARTNRIDX>',1)+11))) PARTNER_ID_SUPPLIER,
dbms_lob.substr(substr(payload,instr(payload,'<TAXID>',1)+7,instr(payload,'</TAXID>',1)-(instr(payload,'<TAXID>',1)+7))) TAX_ID_SUPPLIER,
dbms_lob.substr(substr(payload,instr(payload,'<ADDRLINE index="1">',1)+20,instr(payload,'</ADDRLINE>',1)-(instr(payload,'<ADDRLINE index="1">',1)+20))) SUPPLIER_ADDRESS1,
dbms_lob.substr(substr(payload,instr(payload,'<CITY>',1)+6,instr(payload,'</CITY>',1)-(instr(payload,'<CITY>',1)+6))) SUPPLIER_CITY,
dbms_lob.substr(substr(payload,instr(payload,'<COUNTRY>',1)+9,instr(payload,'</COUNTRY>',1)-(instr(payload,'<COUNTRY>',1)+9))) SUPPLIER_COUNTRY,
dbms_lob.substr(substr(payload,instr(payload,'<POSTALCODE>',1)+12,instr(payload,'</POSTALCODE>',1)-(instr(payload,'<POSTALCODE>',1)+12))) SUPPLIER_POSTAL_CODE,
dbms_lob.substr(substr(payload,instr(payload,'<STATEPROVN>',1)+12,instr(payload,'</STATEPROVN>',1)-(instr(payload,'<STATEPROVN>',1)+12))) SUPPLIER_STATE_PROVINCE,
dbms_lob.substr(substr(payload,instr(payload,'<TELEPHONE index="1">',1)+21,instr(payload,'</TELEPHONE>',1)-(instr(payload,'<TELEPHONE index="1">',1)+21))) SUPPLIER_TELEPHONE_INDEX,
dbms_lob.substr(substr(payload,instr(payload,'<NAME index="1">',1,2)+16,instr(payload,'</NAME>',1,2)-(instr(payload,'<NAME index="1">',1,2)+16))) SUPPLIER_CONTACT_NAME,
dbms_lob.substr(substr(payload,instr(payload,'<EMAIL>',1,1)+7,instr(payload,'</EMAIL>',1,1)-(instr(payload,'<EMAIL>',1,1)+7))) SUPPLIER_EMAIL_ADDRESS,
dbms_lob.substr(substr(payload,instr(payload,'<TELEPHONE index="1">',1,2)+21,instr(payload,'</TELEPHONE>',1,2)-(instr(payload,'<TELEPHONE index="1">',1,2)+21)))  SUPPLIER_CONT_TELEPHONE,
dbms_lob.substr(substr(payload,instr(payload,'<NAME index="1">',1,3)+16,instr(payload,'</NAME>',1,3)-(instr(payload,'<NAME index="1">',1,3)+16))) SOLD_TO_NAME,
dbms_lob.substr(substr(payload,instr(payload,'<PARTNRTYPE>',1,2)+12,instr(payload,'</PARTNRTYPE>',1,2)-(instr(payload,'<PARTNRTYPE>',1,2)+12))) SOLD_TO_PARTNER_TYPE,
dbms_lob.substr(substr(payload,instr(payload,'<DUNSNUMBER>',1,2)+12,instr(payload,'</DUNSNUMBER>',1,2)-(instr(payload,'<DUNSNUMBER>',1,2)+12))) SOLD_TO_DUNS_NUMBER,
dbms_lob.substr(substr(payload,instr(payload,'<EMAIL>',1,2)+7,instr(payload,'</EMAIL>',1,2)-(instr(payload,'<EMAIL>',1,2)+7))) SOLD_TO_CONTACT_EMAIL,
dbms_lob.substr(substr(payload,instr(payload,'<TELEPHONE index="1">',1,3)+21,instr(payload,'</TELEPHONE>',1,3)-(instr(payload,'<TELEPHONE index="1">',1,3)+21))) SOLD_TO_TELEPHONE_INDEX,
dbms_lob.substr(substr(payload,instr(payload,'<NAME index="1">',1,4)+16,instr(payload,'</NAME>',1,4)-(instr(payload,'<NAME index="1">',1,4)+16))) REMIT_TO_NAME,
dbms_lob.substr(substr(payload,instr(payload,'<PARTNRTYPE>',1,3)+12,instr(payload,'</PARTNRTYPE>',1,3)-(instr(payload,'<PARTNRTYPE>',1,3)+12))) REMIT_TO_PARTNER_TYPE,
dbms_lob.substr(substr(payload,instr(payload,'<DUNSNUMBER>',1,3)+12,instr(payload,'</DUNSNUMBER>',1,3)-(instr(payload,'<DUNSNUMBER>',1,3)+12))) REMIT_TO_DUNS_NUMBER,
dbms_lob.substr(substr(payload,instr(payload,'<TAXID>',1,2)+7,instr(payload,'</TAXID>',1,2)-(instr(payload,'<TAXID>',1,2)+7))) REMIT_TO_TAXID,
dbms_lob.substr(substr(payload,instr(payload,'<NAME index="1">',1,5)+16,instr(payload,'</NAME>',1,5)-(instr(payload,'<NAME index="1">',1,5)+16))) REMIT_TO_CONTACT_NAME,
dbms_lob.substr(substr(payload,instr(payload,'<EMAIL>',1,3)+7,instr(payload,'</EMAIL>',1,3)-(instr(payload,'<EMAIL>',1,3)+7))) REMIT_TO_CONTACT_EMAIL,
dbms_lob.substr(substr(payload,instr(payload,'<NAME index="1">',1,6)+16,instr(payload,'</NAME>',1,6)-(instr(payload,'<NAME index="1">',1,6)+16))) BILL_TO_NAME,
dbms_lob.substr(substr(payload,instr(payload,'<PARTNRTYPE>',1,4)+12,instr(payload,'</PARTNRTYPE>',1,4)-(instr(payload,'<PARTNRTYPE>',1,4)+12))) BILL_TO_PARTNERTYPE,
dbms_lob.substr(substr(payload,instr(payload,'<DUNSNUMBER>',1,4)+12,instr(payload,'</DUNSNUMBER>',1,4)-(instr(payload,'<DUNSNUMBER>',1,4)+12))) BILL_TO_DUNSNUMBER,
dbms_lob.substr(substr(payload,instr(payload,'<PARTNRIDX>',1,2)+11,instr(payload,'</PARTNRIDX>',1,2)-(instr(payload,'<PARTNRIDX>',1,2)+11))) BILL_TO_PARTNERID,
dbms_lob.substr(substr(payload,instr(payload,'<DOCTYPE>',1,2)+9,instr(payload,'</DOCTYPE>',1,2)-(instr(payload,'<DOCTYPE>',1,2)+9))) DOCUMENT_REFERENCE_TYPE,
dbms_lob.substr(substr(payload,instr(payload,'<DOCUMENTID>',1,3)+12,instr(payload,'</DOCUMENTID>',1,3)-(instr(payload,'<DOCUMENTID>',1,3)+12))) DOCUMENT_NUMBER,
dbms_lob.substr(substr(payload,instr(payload,'<ADDRLINE index="1">',1,3)+20,(instr(payload,'</ADDRLINE>',1,5)-(instr(payload,'<ADDRLINE index="1">',1,3)+20)))) BILL_TO_ADDRESS1,
dbms_lob.substr(substr(payload,instr(payload,'<ADDRLINE index="2">',1,3)+20,(instr(payload,'</ADDRLINE>',1,6)-(instr(payload,'<ADDRLINE index="2">',1,3)+20)))) BILL_TO_ADDRESS2,
dbms_lob.substr(substr(payload,instr(payload,'<CITY>',1,3)+6,(instr(payload,'</CITY>',1,3)-(instr(payload,'<CITY>',1,3)+6)))) BILL_TO_CITY,
dbms_lob.substr(substr(payload,instr(payload,'<POSTALCODE>',1,3)+12,(instr(payload,'</POSTALCODE>',1,3)-(instr(payload,'<POSTALCODE>',1,3)+12)))) BILL_TO_POSTAL_CODE,
dbms_lob.substr(substr(payload,instr(payload,'<FAX index="1">',1,1)+15,(instr(payload,'</FAX>',1,1)-(instr(payload,'<FAX index="1">',1,1)+15)))) BILL_TO_FAX,
dbms_lob.substr(substr(payload,instr(payload,'<CURRENCY>',1,1)+10,(instr(payload,'</CURRENCY>',1,1)-(instr(payload,'<CURRENCY>',1,1)+10)))) CURRENCY
FROM ecx_doclogs where transaction_type='INVOICE' AND
dbms_lob.substr(substr(payload,instr(payload,'<DOCUMENTID>',1)+12,instr(payload,'</DOCUMENTID>',1)-(instr(payload,'<DOCUMENTID>',1)+12)))=T_Invoice  AND
dbms_lob.substr(substr(payload,instr(payload,'<NAME index="1">',1)+16,instr(payload,'</NAME>',1)-(instr(payload,'<NAME index="1">',1)+16)))=T_Supplier;

--------------------------------------------------------------------------------------------------------------------------------------------
cursor c3 is select
dbms_lob.substr(substr(payload,instr(payload,'<DOCUMENTID>',1)+12,instr(payload,'</DOCUMENTID>',1)-(instr(payload,'<DOCUMENTID>',1)+12))) INVOICE_NUMBER,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,1),(instr(payload,'</INVLINE>',1,1)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,1)))) LINE_DETAILS1,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,2),(instr(payload,'</INVLINE>',1,2)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,2)))) LINE_DETAILS2,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,3),(instr(payload,'</INVLINE>',1,3)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,3)))) LINE_DETAILS3,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,4),(instr(payload,'</INVLINE>',1,4)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,4)))) LINE_DETAILS4,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,5),(instr(payload,'</INVLINE>',1,5)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,5)))) LINE_DETAILS5,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,6),(instr(payload,'</INVLINE>',1,6)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,6)))) LINE_DETAILS6,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,7),(instr(payload,'</INVLINE>',1,7)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,7)))) LINE_DETAILS7,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,8),(instr(payload,'</INVLINE>',1,8)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,8)))) LINE_DETAILS8,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,9),(instr(payload,'</INVLINE>',1,9)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,9)))) LINE_DETAILS9,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,10),(instr(payload,'</INVLINE>',1,10)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,10)))) LINE_DETAILS10,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,11),(instr(payload,'</INVLINE>',1,11)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,11)))) LINE_DETAILS11,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,12),(instr(payload,'</INVLINE>',1,12)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,12)))) LINE_DETAILS12,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,13),(instr(payload,'</INVLINE>',1,13)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,13)))) LINE_DETAILS13,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,14),(instr(payload,'</INVLINE>',1,14)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,14)))) LINE_DETAILS14,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,15),(instr(payload,'</INVLINE>',1,15)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,15)))) LINE_DETAILS15,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,16),(instr(payload,'</INVLINE>',1,16)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,16)))) LINE_DETAILS16,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,17),(instr(payload,'</INVLINE>',1,17)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,17)))) LINE_DETAILS17,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,18),(instr(payload,'</INVLINE>',1,18)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,18)))) LINE_DETAILS18,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,19),(instr(payload,'</INVLINE>',1,19)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,19)))) LINE_DETAILS19,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,20),(instr(payload,'</INVLINE>',1,20)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,20)))) LINE_DETAILS20,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,21),(instr(payload,'</INVLINE>',1,21)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,21)))) LINE_DETAILS21,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,22),(instr(payload,'</INVLINE>',1,22)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,22)))) LINE_DETAILS22,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,23),(instr(payload,'</INVLINE>',1,23)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,23)))) LINE_DETAILS23,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,24),(instr(payload,'</INVLINE>',1,24)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,24)))) LINE_DETAILS24,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,25),(instr(payload,'</INVLINE>',1,25)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,25)))) LINE_DETAILS25,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,26),(instr(payload,'</INVLINE>',1,26)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,26)))) LINE_DETAILS26,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,27),(instr(payload,'</INVLINE>',1,27)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,27)))) LINE_DETAILS27,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,28),(instr(payload,'</INVLINE>',1,28)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,28)))) LINE_DETAILS28,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,29),(instr(payload,'</INVLINE>',1,29)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,29)))) LINE_DETAILS29,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,30),(instr(payload,'</INVLINE>',1,30)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,30)))) LINE_DETAILS30,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,31),(instr(payload,'</INVLINE>',1,31)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,31)))) LINE_DETAILS31,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,32),(instr(payload,'</INVLINE>',1,32)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,32)))) LINE_DETAILS32,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,33),(instr(payload,'</INVLINE>',1,33)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,33)))) LINE_DETAILS33,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,34),(instr(payload,'</INVLINE>',1,34)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,34)))) LINE_DETAILS34,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,35),(instr(payload,'</INVLINE>',1,35)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,35)))) LINE_DETAILS35,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,36),(instr(payload,'</INVLINE>',1,36)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,36)))) LINE_DETAILS36,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,37),(instr(payload,'</INVLINE>',1,37)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,37)))) LINE_DETAILS37,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,38),(instr(payload,'</INVLINE>',1,38)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,38)))) LINE_DETAILS38,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,39),(instr(payload,'</INVLINE>',1,39)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,39)))) LINE_DETAILS39,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,40),(instr(payload,'</INVLINE>',1,40)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,40)))) LINE_DETAILS40,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,41),(instr(payload,'</INVLINE>',1,41)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,41)))) LINE_DETAILS41,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,42),(instr(payload,'</INVLINE>',1,42)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,42)))) LINE_DETAILS42,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,43),(instr(payload,'</INVLINE>',1,43)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,43)))) LINE_DETAILS43,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,44),(instr(payload,'</INVLINE>',1,44)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,44)))) LINE_DETAILS44,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,45),(instr(payload,'</INVLINE>',1,45)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,45)))) LINE_DETAILS45,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,46),(instr(payload,'</INVLINE>',1,46)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,46)))) LINE_DETAILS46,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,47),(instr(payload,'</INVLINE>',1,47)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,47)))) LINE_DETAILS47,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,48),(instr(payload,'</INVLINE>',1,48)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,48)))) LINE_DETAILS48,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,49),(instr(payload,'</INVLINE>',1,49)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,49)))) LINE_DETAILS49,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,50),(instr(payload,'</INVLINE>',1,50)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,50)))) LINE_DETAILS50,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,51),(instr(payload,'</INVLINE>',1,51)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,51)))) LINE_DETAILS51,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,52),(instr(payload,'</INVLINE>',1,52)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,52)))) LINE_DETAILS52,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,53),(instr(payload,'</INVLINE>',1,53)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,53)))) LINE_DETAILS53,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,54),(instr(payload,'</INVLINE>',1,54)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,54)))) LINE_DETAILS54,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,55),(instr(payload,'</INVLINE>',1,55)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,55)))) LINE_DETAILS55,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,56),(instr(payload,'</INVLINE>',1,56)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,56)))) LINE_DETAILS56,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,57),(instr(payload,'</INVLINE>',1,57)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,57)))) LINE_DETAILS57,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,58),(instr(payload,'</INVLINE>',1,58)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,58)))) LINE_DETAILS58,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,59),(instr(payload,'</INVLINE>',1,59)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,59)))) LINE_DETAILS59,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,60),(instr(payload,'</INVLINE>',1,60)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,60)))) LINE_DETAILS60,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,61),(instr(payload,'</INVLINE>',1,61)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,61)))) LINE_DETAILS61,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,62),(instr(payload,'</INVLINE>',1,62)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,62)))) LINE_DETAILS62,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,63),(instr(payload,'</INVLINE>',1,63)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,63)))) LINE_DETAILS63,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,64),(instr(payload,'</INVLINE>',1,64)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,64)))) LINE_DETAILS64,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,65),(instr(payload,'</INVLINE>',1,65)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,65)))) LINE_DETAILS65,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,66),(instr(payload,'</INVLINE>',1,66)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,66)))) LINE_DETAILS66,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,67),(instr(payload,'</INVLINE>',1,67)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,67)))) LINE_DETAILS67,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,68),(instr(payload,'</INVLINE>',1,68)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,68)))) LINE_DETAILS68,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,69),(instr(payload,'</INVLINE>',1,69)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,69)))) LINE_DETAILS69,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,70),(instr(payload,'</INVLINE>',1,70)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,70)))) LINE_DETAILS70,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,71),(instr(payload,'</INVLINE>',1,71)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,71)))) LINE_DETAILS71,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,72),(instr(payload,'</INVLINE>',1,72)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,72)))) LINE_DETAILS72,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,73),(instr(payload,'</INVLINE>',1,73)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,73)))) LINE_DETAILS73,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,74),(instr(payload,'</INVLINE>',1,74)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,74)))) LINE_DETAILS74,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,75),(instr(payload,'</INVLINE>',1,75)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,75)))) LINE_DETAILS75,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,76),(instr(payload,'</INVLINE>',1,76)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,76)))) LINE_DETAILS76,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,77),(instr(payload,'</INVLINE>',1,77)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,77)))) LINE_DETAILS77,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,78),(instr(payload,'</INVLINE>',1,78)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,78)))) LINE_DETAILS78,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,79),(instr(payload,'</INVLINE>',1,79)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,79)))) LINE_DETAILS79,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,80),(instr(payload,'</INVLINE>',1,80)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,80)))) LINE_DETAILS80,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,81),(instr(payload,'</INVLINE>',1,81)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,81)))) LINE_DETAILS81,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,82),(instr(payload,'</INVLINE>',1,82)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,82)))) LINE_DETAILS82,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,83),(instr(payload,'</INVLINE>',1,83)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,83)))) LINE_DETAILS83,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,84),(instr(payload,'</INVLINE>',1,84)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,84)))) LINE_DETAILS84,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,85),(instr(payload,'</INVLINE>',1,85)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,85)))) LINE_DETAILS85,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,86),(instr(payload,'</INVLINE>',1,86)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,86)))) LINE_DETAILS86,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,87),(instr(payload,'</INVLINE>',1,87)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,87)))) LINE_DETAILS87,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,88),(instr(payload,'</INVLINE>',1,88)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,88)))) LINE_DETAILS88,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,89),(instr(payload,'</INVLINE>',1,89)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,89)))) LINE_DETAILS89,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,90),(instr(payload,'</INVLINE>',1,90)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,90)))) LINE_DETAILS90,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,91),(instr(payload,'</INVLINE>',1,91)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,91)))) LINE_DETAILS91,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,92),(instr(payload,'</INVLINE>',1,92)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,92)))) LINE_DETAILS92,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,93),(instr(payload,'</INVLINE>',1,93)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,93)))) LINE_DETAILS93,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,94),(instr(payload,'</INVLINE>',1,94)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,94)))) LINE_DETAILS94,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,95),(instr(payload,'</INVLINE>',1,95)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,95)))) LINE_DETAILS95,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,96),(instr(payload,'</INVLINE>',1,96)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,96)))) LINE_DETAILS96,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,97),(instr(payload,'</INVLINE>',1,97)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,97)))) LINE_DETAILS97,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,98),(instr(payload,'</INVLINE>',1,98)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,98)))) LINE_DETAILS98,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,99),(instr(payload,'</INVLINE>',1,99)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,99)))) LINE_DETAILS99,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,100),(instr(payload,'</INVLINE>',1,100)-instr(payload,'<AMOUNT qualifier="TOTAL" type="T" index="1">',1,100)))) LINE_DETAILS100,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,1),(instr(payload,'</INVTAX>',1,1)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,1)))) TAX_DETAILS1,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,2),(instr(payload,'</INVTAX>',1,2)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,2)))) TAX_DETAILS2,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,3),(instr(payload,'</INVTAX>',1,3)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,3)))) TAX_DETAILS3,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,4),(instr(payload,'</INVTAX>',1,4)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,4)))) TAX_DETAILS4,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,5),(instr(payload,'</INVTAX>',1,5)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,5)))) TAX_DETAILS5,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,6),(instr(payload,'</INVTAX>',1,6)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,6)))) TAX_DETAILS6,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,7),(instr(payload,'</INVTAX>',1,7)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,7)))) TAX_DETAILS7,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,8),(instr(payload,'</INVTAX>',1,8)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,8)))) TAX_DETAILS8,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,9),(instr(payload,'</INVTAX>',1,9)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,9)))) TAX_DETAILS9,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,10),(instr(payload,'</INVTAX>',1,10)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,10)))) TAX_DETAILS10,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,11),(instr(payload,'</INVTAX>',1,11)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,11)))) TAX_DETAILS11,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,12),(instr(payload,'</INVTAX>',1,12)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,12)))) TAX_DETAILS12,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,13),(instr(payload,'</INVTAX>',1,13)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,13)))) TAX_DETAILS13,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,14),(instr(payload,'</INVTAX>',1,14)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,14)))) TAX_DETAILS14,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,15),(instr(payload,'</INVTAX>',1,15)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,15)))) TAX_DETAILS15,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,16),(instr(payload,'</INVTAX>',1,16)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,16)))) TAX_DETAILS16,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,17),(instr(payload,'</INVTAX>',1,17)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,17)))) TAX_DETAILS17,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,18),(instr(payload,'</INVTAX>',1,18)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,18)))) TAX_DETAILS18,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,19),(instr(payload,'</INVTAX>',1,19)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,19)))) TAX_DETAILS19,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,20),(instr(payload,'</INVTAX>',1,20)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,20)))) TAX_DETAILS20,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,21),(instr(payload,'</INVTAX>',1,21)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,21)))) TAX_DETAILS21,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,22),(instr(payload,'</INVTAX>',1,22)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,22)))) TAX_DETAILS22,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,23),(instr(payload,'</INVTAX>',1,23)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,23)))) TAX_DETAILS23,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,24),(instr(payload,'</INVTAX>',1,24)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,24)))) TAX_DETAILS24,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,25),(instr(payload,'</INVTAX>',1,25)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,25)))) TAX_DETAILS25,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,26),(instr(payload,'</INVTAX>',1,26)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,26)))) TAX_DETAILS26,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,27),(instr(payload,'</INVTAX>',1,27)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,27)))) TAX_DETAILS27,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,28),(instr(payload,'</INVTAX>',1,28)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,28)))) TAX_DETAILS28,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,29),(instr(payload,'</INVTAX>',1,29)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,29)))) TAX_DETAILS29,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,30),(instr(payload,'</INVTAX>',1,30)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,30)))) TAX_DETAILS30,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,31),(instr(payload,'</INVTAX>',1,31)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,31)))) TAX_DETAILS31,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,32),(instr(payload,'</INVTAX>',1,32)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,32)))) TAX_DETAILS32,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,33),(instr(payload,'</INVTAX>',1,33)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,33)))) TAX_DETAILS33,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,34),(instr(payload,'</INVTAX>',1,34)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,34)))) TAX_DETAILS34,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,35),(instr(payload,'</INVTAX>',1,35)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,35)))) TAX_DETAILS35,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,36),(instr(payload,'</INVTAX>',1,36)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,36)))) TAX_DETAILS36,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,37),(instr(payload,'</INVTAX>',1,37)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,37)))) TAX_DETAILS37,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,38),(instr(payload,'</INVTAX>',1,38)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,38)))) TAX_DETAILS38,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,39),(instr(payload,'</INVTAX>',1,39)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,39)))) TAX_DETAILS39,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,40),(instr(payload,'</INVTAX>',1,40)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,40)))) TAX_DETAILS40,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,41),(instr(payload,'</INVTAX>',1,41)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,41)))) TAX_DETAILS41,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,42),(instr(payload,'</INVTAX>',1,42)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,42)))) TAX_DETAILS42,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,43),(instr(payload,'</INVTAX>',1,43)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,43)))) TAX_DETAILS43,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,44),(instr(payload,'</INVTAX>',1,44)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,44)))) TAX_DETAILS44,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,45),(instr(payload,'</INVTAX>',1,45)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,45)))) TAX_DETAILS45,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,46),(instr(payload,'</INVTAX>',1,46)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,46)))) TAX_DETAILS46,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,47),(instr(payload,'</INVTAX>',1,47)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,47)))) TAX_DETAILS47,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,48),(instr(payload,'</INVTAX>',1,48)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,48)))) TAX_DETAILS48,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,49),(instr(payload,'</INVTAX>',1,49)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,49)))) TAX_DETAILS49,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,50),(instr(payload,'</INVTAX>',1,50)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,50)))) TAX_DETAILS50,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,51),(instr(payload,'</INVTAX>',1,51)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,51)))) TAX_DETAILS51,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,52),(instr(payload,'</INVTAX>',1,52)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,52)))) TAX_DETAILS52,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,53),(instr(payload,'</INVTAX>',1,53)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,53)))) TAX_DETAILS53,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,54),(instr(payload,'</INVTAX>',1,54)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,54)))) TAX_DETAILS54,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,55),(instr(payload,'</INVTAX>',1,55)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,55)))) TAX_DETAILS55,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,56),(instr(payload,'</INVTAX>',1,56)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,56)))) TAX_DETAILS56,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,57),(instr(payload,'</INVTAX>',1,57)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,57)))) TAX_DETAILS57,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,58),(instr(payload,'</INVTAX>',1,58)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,58)))) TAX_DETAILS58,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,59),(instr(payload,'</INVTAX>',1,59)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,59)))) TAX_DETAILS59,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,60),(instr(payload,'</INVTAX>',1,60)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,60)))) TAX_DETAILS60,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,61),(instr(payload,'</INVTAX>',1,61)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,61)))) TAX_DETAILS61,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,62),(instr(payload,'</INVTAX>',1,62)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,62)))) TAX_DETAILS62,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,63),(instr(payload,'</INVTAX>',1,63)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,63)))) TAX_DETAILS63,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,64),(instr(payload,'</INVTAX>',1,64)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,64)))) TAX_DETAILS64,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,65),(instr(payload,'</INVTAX>',1,65)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,65)))) TAX_DETAILS65,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,66),(instr(payload,'</INVTAX>',1,66)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,66)))) TAX_DETAILS66,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,67),(instr(payload,'</INVTAX>',1,67)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,67)))) TAX_DETAILS67,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,68),(instr(payload,'</INVTAX>',1,68)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,68)))) TAX_DETAILS68,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,69),(instr(payload,'</INVTAX>',1,69)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,69)))) TAX_DETAILS69,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,70),(instr(payload,'</INVTAX>',1,70)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,70)))) TAX_DETAILS70,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,71),(instr(payload,'</INVTAX>',1,71)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,71)))) TAX_DETAILS71,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,72),(instr(payload,'</INVTAX>',1,72)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,72)))) TAX_DETAILS72,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,73),(instr(payload,'</INVTAX>',1,73)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,73)))) TAX_DETAILS73,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,74),(instr(payload,'</INVTAX>',1,74)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,74)))) TAX_DETAILS74,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,75),(instr(payload,'</INVTAX>',1,75)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,75)))) TAX_DETAILS75,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,76),(instr(payload,'</INVTAX>',1,76)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,76)))) TAX_DETAILS76,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,77),(instr(payload,'</INVTAX>',1,77)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,77)))) TAX_DETAILS77,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,78),(instr(payload,'</INVTAX>',1,78)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,78)))) TAX_DETAILS78,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,79),(instr(payload,'</INVTAX>',1,79)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,79)))) TAX_DETAILS79,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,80),(instr(payload,'</INVTAX>',1,80)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,80)))) TAX_DETAILS80,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,81),(instr(payload,'</INVTAX>',1,81)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,81)))) TAX_DETAILS81,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,82),(instr(payload,'</INVTAX>',1,82)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,82)))) TAX_DETAILS82,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,83),(instr(payload,'</INVTAX>',1,83)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,83)))) TAX_DETAILS83,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,84),(instr(payload,'</INVTAX>',1,84)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,84)))) TAX_DETAILS84,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,85),(instr(payload,'</INVTAX>',1,85)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,85)))) TAX_DETAILS85,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,86),(instr(payload,'</INVTAX>',1,86)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,86)))) TAX_DETAILS86,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,87),(instr(payload,'</INVTAX>',1,87)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,87)))) TAX_DETAILS87,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,88),(instr(payload,'</INVTAX>',1,88)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,88)))) TAX_DETAILS88,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,89),(instr(payload,'</INVTAX>',1,89)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,89)))) TAX_DETAILS89,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,90),(instr(payload,'</INVTAX>',1,90)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,90)))) TAX_DETAILS90,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,91),(instr(payload,'</INVTAX>',1,91)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,91)))) TAX_DETAILS91,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,92),(instr(payload,'</INVTAX>',1,92)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,92)))) TAX_DETAILS92,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,93),(instr(payload,'</INVTAX>',1,93)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,93)))) TAX_DETAILS93,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,94),(instr(payload,'</INVTAX>',1,94)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,94)))) TAX_DETAILS94,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,95),(instr(payload,'</INVTAX>',1,95)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,95)))) TAX_DETAILS95,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,96),(instr(payload,'</INVTAX>',1,96)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,96)))) TAX_DETAILS96,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,97),(instr(payload,'</INVTAX>',1,97)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,97)))) TAX_DETAILS97,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,98),(instr(payload,'</INVTAX>',1,98)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,98)))) TAX_DETAILS98,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,99),(instr(payload,'</INVTAX>',1,99)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,99)))) TAX_DETAILS99,
dbms_lob.substr(substr(payload,instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,100),(instr(payload,'</INVTAX>',1,100)-instr(payload,'<AMOUNT qualifier="TAX" type="T" index="1">',1,100)))) TAX_DETAILS100
from ecx_doclogs where transaction_type='INVOICE'AND
dbms_lob.substr(substr(payload,instr(payload,'<DOCUMENTID>',1)+12,instr(payload,'</DOCUMENTID>',1)-(instr(payload,'<DOCUMENTID>',1)+12)))=T_Invoice  AND
dbms_lob.substr(substr(payload,instr(payload,'<NAME index="1">',1)+16,instr(payload,'</NAME>',1)-(instr(payload,'<NAME index="1">',1)+16)))=T_Supplier;
--------------------------------------------------------------------------------------------------------------------------------------------
cursor c5 is select INVOICE_NUMBER,
substr(LINE_DETAILS1,instr(LINE_DETAILS1,'<LINENUM>',1,1)+9,instr(LINE_DETAILS1,'</LINENUM>',1,1)-(instr(LINE_DETAILS1,'<LINENUM>',1,1)+9)) LINE_NUM1,
(substr(LINE_DETAILS1,instr(LINE_DETAILS1,'<VALUE>',1,1)+7,instr(LINE_DETAILS1,'</VALUE>',1,1)-(instr(LINE_DETAILS1,'<VALUE>',1,1)+7)))*.01 LINE1_AMOUNT,
substr(LINE_DETAILS1,instr(LINE_DETAILS1,'<ITEM>',1,1)+6,instr(LINE_DETAILS1,'</ITEM>',1,1)-(instr(LINE_DETAILS1,'<ITEM>',1,1)+6)) ITEM_NAME_LINE1,
substr(LINE_DETAILS1,instr(LINE_DETAILS1,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS1,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS1,'<DESCRIPTN>',1,1)+11)) ITEM_Description1,
(substr(TAX_DETAILS1,instr(TAX_DETAILS1,'<VALUE>',1,1)+7,instr(TAX_DETAILS1,'</VALUE>',1,1)-(instr(TAX_DETAILS1,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE1,
substr(TAX_DETAILS1,instr(TAX_DETAILS1,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS1,'</TAXCODE>',1,1)-(instr(TAX_DETAILS1,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE1,
substr(LINE_DETAILS1,instr(LINE_DETAILS1,'<VALUE>',1,3)+7,instr(LINE_DETAILS1,'</VALUE>',1,3)-(instr(LINE_DETAILS1,'<VALUE>',1,3)+7)) quantity_line1,
(substr(LINE_DETAILS1,instr(LINE_DETAILS1,'<VALUE>',1,2)+7,instr(LINE_DETAILS1,'</VALUE>',1,2)-(instr(LINE_DETAILS1,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line1,
(substr(TAX_DETAILS1,instr(TAX_DETAILS1,'<VALUE>',1,3)+7,instr(TAX_DETAILS1,'</VALUE>',1,3)-(instr(TAX_DETAILS1,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE1,
substr(tax_details1,instr(tax_details1,'<DESCRIPTN>',1,1)+11,instr(tax_details1,'</DESCRIPTN>',1,1)-(instr(tax_details1,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION1,
substr(LINE_DETAILS2,instr(LINE_DETAILS2,'<LINENUM>',1,1)+9,instr(LINE_DETAILS2,'</LINENUM>',1,1)-(instr(LINE_DETAILS2,'<LINENUM>',1,1)+9)) LINE_NUM2,
(substr(LINE_DETAILS2,instr(LINE_DETAILS2,'<VALUE>',1,1)+7,instr(LINE_DETAILS2,'</VALUE>',1,1)-(instr(LINE_DETAILS2,'<VALUE>',1,1)+7)))*.01 LINE2_AMOUNT,
substr(LINE_DETAILS2,instr(LINE_DETAILS2,'<ITEM>',1,1)+6,instr(LINE_DETAILS2,'</ITEM>',1,1)-(instr(LINE_DETAILS2,'<ITEM>',1,1)+6)) ITEM_NAME_LINE2,
substr(LINE_DETAILS2,instr(LINE_DETAILS2,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS2,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS2,'<DESCRIPTN>',1,1)+11)) ITEM_Description2,
(substr(TAX_DETAILS2,instr(TAX_DETAILS2,'<VALUE>',1,1)+7,instr(TAX_DETAILS2,'</VALUE>',1,1)-(instr(TAX_DETAILS2,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE2,
substr(TAX_DETAILS2,instr(TAX_DETAILS2,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS2,'</TAXCODE>',1,1)-(instr(TAX_DETAILS2,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE2,
substr(LINE_DETAILS2,instr(LINE_DETAILS2,'<VALUE>',1,3)+7,instr(LINE_DETAILS2,'</VALUE>',1,3)-(instr(LINE_DETAILS2,'<VALUE>',1,3)+7)) quantity_line2,
(substr(LINE_DETAILS2,instr(LINE_DETAILS2,'<VALUE>',1,2)+7,instr(LINE_DETAILS2,'</VALUE>',1,2)-(instr(LINE_DETAILS2,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line2,
(substr(TAX_DETAILS2,instr(TAX_DETAILS2,'<VALUE>',1,3)+7,instr(TAX_DETAILS2,'</VALUE>',1,3)-(instr(TAX_DETAILS2,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE2,
substr(tax_details2,instr(tax_details2,'<DESCRIPTN>',1,1)+11,instr(tax_details2,'</DESCRIPTN>',1,1)-(instr(tax_details2,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION2,
substr(LINE_DETAILS3,instr(LINE_DETAILS3,'<LINENUM>',1,1)+9,instr(LINE_DETAILS3,'</LINENUM>',1,1)-(instr(LINE_DETAILS3,'<LINENUM>',1,1)+9))LINE_NUM3,
(substr(LINE_DETAILS3,instr(LINE_DETAILS3,'<VALUE>',1,1)+7,instr(LINE_DETAILS3,'</VALUE>',1,1)-(instr(LINE_DETAILS3,'<VALUE>',1,1)+7)))*.01 LINE3_AMOUNT,
substr(LINE_DETAILS3,instr(LINE_DETAILS3,'<ITEM>',1,1)+6,instr(LINE_DETAILS3,'</ITEM>',1,1)-(instr(LINE_DETAILS3,'<ITEM>',1,1)+6)) ITEM_NAME_LINE3,
substr(LINE_DETAILS3,instr(LINE_DETAILS3,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS3,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS3,'<DESCRIPTN>',1,1)+11)) ITEM_Description3,
(substr(TAX_DETAILS3,instr(TAX_DETAILS3,'<VALUE>',1,1)+7,instr(TAX_DETAILS3,'</VALUE>',1,1)-(instr(TAX_DETAILS3,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE3,
substr(TAX_DETAILS3,instr(TAX_DETAILS3,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS3,'</TAXCODE>',1,1)-(instr(TAX_DETAILS3,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE3,
substr(LINE_DETAILS3,instr(LINE_DETAILS3,'<VALUE>',1,3)+7,instr(LINE_DETAILS3,'</VALUE>',1,3)-(instr(LINE_DETAILS3,'<VALUE>',1,3)+7)) quantity_line3,
(substr(LINE_DETAILS3,instr(LINE_DETAILS3,'<VALUE>',1,2)+7,instr(LINE_DETAILS3,'</VALUE>',1,2)-(instr(LINE_DETAILS3,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line3,
(substr(TAX_DETAILS3,instr(TAX_DETAILS3,'<VALUE>',1,3)+7,instr(TAX_DETAILS3,'</VALUE>',1,3)-(instr(TAX_DETAILS3,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE3,
substr(tax_details3,instr(tax_details3,'<DESCRIPTN>',1,1)+11,instr(tax_details3,'</DESCRIPTN>',1,1)-(instr(tax_details3,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION3,
substr(LINE_DETAILS4,instr(LINE_DETAILS4,'<LINENUM>',1,1)+9,instr(LINE_DETAILS4,'</LINENUM>',1,1)-(instr(LINE_DETAILS4,'<LINENUM>',1,1)+9)) LINE_NUM4,
(substr(LINE_DETAILS4,instr(LINE_DETAILS4,'<VALUE>',1,1)+7,instr(LINE_DETAILS4,'</VALUE>',1,1)-(instr(LINE_DETAILS4,'<VALUE>',1,1)+7)))*.01 LINE4_AMOUNT,
substr(LINE_DETAILS4,instr(LINE_DETAILS4,'<ITEM>',1,1)+6,instr(LINE_DETAILS4,'</ITEM>',1,1)-(instr(LINE_DETAILS4,'<ITEM>',1,1)+6)) ITEM_NAME_LINE4,
substr(LINE_DETAILS4,instr(LINE_DETAILS4,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS4,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS4,'<DESCRIPTN>',1,1)+11)) ITEM_Description4,
(substr(TAX_DETAILS4,instr(TAX_DETAILS4,'<VALUE>',1,1)+7,instr(TAX_DETAILS4,'</VALUE>',1,1)-(instr(TAX_DETAILS4,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE4,
substr(TAX_DETAILS4,instr(TAX_DETAILS4,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS4,'</TAXCODE>',1,1)-(instr(TAX_DETAILS4,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE4,
substr(LINE_DETAILS4,instr(LINE_DETAILS4,'<VALUE>',1,3)+7,instr(LINE_DETAILS4,'</VALUE>',1,3)-(instr(LINE_DETAILS4,'<VALUE>',1,3)+7)) quantity_line4,
(substr(LINE_DETAILS4,instr(LINE_DETAILS4,'<VALUE>',1,2)+7,instr(LINE_DETAILS4,'</VALUE>',1,2)-(instr(LINE_DETAILS4,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line4,
(substr(TAX_DETAILS4,instr(TAX_DETAILS4,'<VALUE>',1,3)+7,instr(TAX_DETAILS4,'</VALUE>',1,3)-(instr(TAX_DETAILS4,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE4,
substr(tax_details4,instr(tax_details4,'<DESCRIPTN>',1,1)+11,instr(tax_details4,'</DESCRIPTN>',1,1)-(instr(tax_details4,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION4,
substr(LINE_DETAILS5,instr(LINE_DETAILS5,'<LINENUM>',1,1)+9,instr(LINE_DETAILS5,'</LINENUM>',1,1)-(instr(LINE_DETAILS5,'<LINENUM>',1,1)+9)) LINE_NUM5,
(substr(LINE_DETAILS5,instr(LINE_DETAILS5,'<VALUE>',1,1)+7,instr(LINE_DETAILS5,'</VALUE>',1,1)-(instr(LINE_DETAILS5,'<VALUE>',1,1)+7)))*.01 LINE5_AMOUNT,
substr(LINE_DETAILS5,instr(LINE_DETAILS5,'<ITEM>',1,1)+6,instr(LINE_DETAILS5,'</ITEM>',1,1)-(instr(LINE_DETAILS5,'<ITEM>',1,1)+6)) ITEM_NAME_LINE5,
substr(LINE_DETAILS5,instr(LINE_DETAILS5,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS5,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS5,'<DESCRIPTN>',1,1)+11)) ITEM_Description5,
(substr(TAX_DETAILS5,instr(TAX_DETAILS5,'<VALUE>',1,1)+7,instr(TAX_DETAILS5,'</VALUE>',1,1)-(instr(TAX_DETAILS5,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE5,
substr(TAX_DETAILS5,instr(TAX_DETAILS5,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS5,'</TAXCODE>',1,1)-(instr(TAX_DETAILS5,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE5,
substr(LINE_DETAILS5,instr(LINE_DETAILS5,'<VALUE>',1,3)+7,instr(LINE_DETAILS5,'</VALUE>',1,3)-(instr(LINE_DETAILS5,'<VALUE>',1,3)+7)) quantity_line5,
(substr(LINE_DETAILS5,instr(LINE_DETAILS5,'<VALUE>',1,2)+7,instr(LINE_DETAILS5,'</VALUE>',1,2)-(instr(LINE_DETAILS5,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line5,
(substr(TAX_DETAILS5,instr(TAX_DETAILS5,'<VALUE>',1,3)+7,instr(TAX_DETAILS5,'</VALUE>',1,3)-(instr(TAX_DETAILS5,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE5,
substr(tax_details5,instr(tax_details5,'<DESCRIPTN>',1,1)+11,instr(tax_details5,'</DESCRIPTN>',1,1)-(instr(tax_details5,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION5,
substr(LINE_DETAILS6,instr(LINE_DETAILS6,'<LINENUM>',1,1)+9,instr(LINE_DETAILS6,'</LINENUM>',1,1)-(instr(LINE_DETAILS6,'<LINENUM>',1,1)+9)) LINE_NUM6,
(substr(LINE_DETAILS6,instr(LINE_DETAILS6,'<VALUE>',1,1)+7,instr(LINE_DETAILS6,'</VALUE>',1,1)-(instr(LINE_DETAILS6,'<VALUE>',1,1)+7)))*.01 LINE6_AMOUNT,
substr(LINE_DETAILS6,instr(LINE_DETAILS6,'<ITEM>',1,1)+6,instr(LINE_DETAILS6,'</ITEM>',1,1)-(instr(LINE_DETAILS6,'<ITEM>',1,1)+6)) ITEM_NAME_LINE6,
substr(LINE_DETAILS6,instr(LINE_DETAILS6,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS6,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS6,'<DESCRIPTN>',1,1)+11)) ITEM_Description6,
(substr(TAX_DETAILS6,instr(TAX_DETAILS6,'<VALUE>',1,1)+7,instr(TAX_DETAILS6,'</VALUE>',1,1)-(instr(TAX_DETAILS6,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE6,
substr(TAX_DETAILS6,instr(TAX_DETAILS6,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS6,'</TAXCODE>',1,1)-(instr(TAX_DETAILS6,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE6,
substr(LINE_DETAILS6,instr(LINE_DETAILS6,'<VALUE>',1,3)+7,instr(LINE_DETAILS6,'</VALUE>',1,3)-(instr(LINE_DETAILS6,'<VALUE>',1,3)+7)) quantity_line6,
(substr(LINE_DETAILS6,instr(LINE_DETAILS6,'<VALUE>',1,2)+7,instr(LINE_DETAILS6,'</VALUE>',1,2)-(instr(LINE_DETAILS6,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line6,
(substr(TAX_DETAILS6,instr(TAX_DETAILS6,'<VALUE>',1,3)+7,instr(TAX_DETAILS6,'</VALUE>',1,3)-(instr(TAX_DETAILS6,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE6,
substr(tax_details6,instr(tax_details6,'<DESCRIPTN>',1,1)+11,instr(tax_details6,'</DESCRIPTN>',1,1)-(instr(tax_details6,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION6,
substr(LINE_DETAILS7,instr(LINE_DETAILS7,'<LINENUM>',1,1)+9,instr(LINE_DETAILS7,'</LINENUM>',1,1)-(instr(LINE_DETAILS7,'<LINENUM>',1,1)+9)) LINE_NUM7,
(substr(LINE_DETAILS7,instr(LINE_DETAILS7,'<VALUE>',1,1)+7,instr(LINE_DETAILS7,'</VALUE>',1,1)-(instr(LINE_DETAILS7,'<VALUE>',1,1)+7)) )*.01 LINE7_AMOUNT,
substr(LINE_DETAILS7,instr(LINE_DETAILS7,'<ITEM>',1,1)+6,instr(LINE_DETAILS7,'</ITEM>',1,1)-(instr(LINE_DETAILS7,'<ITEM>',1,1)+6)) ITEM_NAME_LINE7,
substr(LINE_DETAILS7,instr(LINE_DETAILS7,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS7,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS7,'<DESCRIPTN>',1,1)+11)) ITEM_Description7,
(substr(TAX_DETAILS7,instr(TAX_DETAILS7,'<VALUE>',1,1)+7,instr(TAX_DETAILS7,'</VALUE>',1,1)-(instr(TAX_DETAILS7,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE7,
substr(TAX_DETAILS7,instr(TAX_DETAILS7,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS7,'</TAXCODE>',1,1)-(instr(TAX_DETAILS7,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE7,
substr(LINE_DETAILS7,instr(LINE_DETAILS7,'<VALUE>',1,3)+7,instr(LINE_DETAILS7,'</VALUE>',1,3)-(instr(LINE_DETAILS7,'<VALUE>',1,3)+7)) quantity_line7,
(substr(LINE_DETAILS7,instr(LINE_DETAILS7,'<VALUE>',1,2)+7,instr(LINE_DETAILS7,'</VALUE>',1,2)-(instr(LINE_DETAILS7,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line7,
(substr(TAX_DETAILS7,instr(TAX_DETAILS7,'<VALUE>',1,3)+7,instr(TAX_DETAILS7,'</VALUE>',1,3)-(instr(TAX_DETAILS7,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE7,
substr(tax_details7,instr(tax_details7,'<DESCRIPTN>',1,1)+11,instr(tax_details7,'</DESCRIPTN>',1,1)-(instr(tax_details7,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION7,
substr(LINE_DETAILS8,instr(LINE_DETAILS8,'<LINENUM>',1,1)+9,instr(LINE_DETAILS8,'</LINENUM>',1,1)-(instr(LINE_DETAILS8,'<LINENUM>',1,1)+9)) LINE_NUM8,
(substr(LINE_DETAILS8,instr(LINE_DETAILS8,'<VALUE>',1,1)+7,instr(LINE_DETAILS8,'</VALUE>',1,1)-(instr(LINE_DETAILS8,'<VALUE>',1,1)+7)))*.01 LINE8_AMOUNT,
substr(LINE_DETAILS8,instr(LINE_DETAILS8,'<ITEM>',1,1)+6,instr(LINE_DETAILS8,'</ITEM>',1,1)-(instr(LINE_DETAILS8,'<ITEM>',1,1)+6)) ITEM_NAME_LINE8,
substr(LINE_DETAILS8,instr(LINE_DETAILS8,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS8,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS8,'<DESCRIPTN>',1,1)+11)) ITEM_Description8,
(substr(TAX_DETAILS8,instr(TAX_DETAILS8,'<VALUE>',1,1)+7,instr(TAX_DETAILS8,'</VALUE>',1,1)-(instr(TAX_DETAILS8,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE8,
substr(TAX_DETAILS8,instr(TAX_DETAILS8,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS8,'</TAXCODE>',1,1)-(instr(TAX_DETAILS8,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE8,
substr(LINE_DETAILS8,instr(LINE_DETAILS8,'<VALUE>',1,3)+7,instr(LINE_DETAILS8,'</VALUE>',1,3)-(instr(LINE_DETAILS8,'<VALUE>',1,3)+7)) quantity_line8,
(substr(LINE_DETAILS8,instr(LINE_DETAILS8,'<VALUE>',1,2)+7,instr(LINE_DETAILS8,'</VALUE>',1,2)-(instr(LINE_DETAILS8,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line8,
(substr(TAX_DETAILS8,instr(TAX_DETAILS8,'<VALUE>',1,3)+7,instr(TAX_DETAILS8,'</VALUE>',1,3)-(instr(TAX_DETAILS8,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE8,
substr(tax_details8,instr(tax_details8,'<DESCRIPTN>',1,1)+11,instr(tax_details8,'</DESCRIPTN>',1,1)-(instr(tax_details8,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION8,
substr(LINE_DETAILS9,instr(LINE_DETAILS9,'<LINENUM>',1,1)+9,instr(LINE_DETAILS9,'</LINENUM>',1,1)-(instr(LINE_DETAILS9,'<LINENUM>',1,1)+9)) LINE_NUM9,
(substr(LINE_DETAILS9,instr(LINE_DETAILS9,'<VALUE>',1,1)+7,instr(LINE_DETAILS9,'</VALUE>',1,1)-(instr(LINE_DETAILS9,'<VALUE>',1,1)+7)))*.01 LINE9_AMOUNT,
substr(LINE_DETAILS9,instr(LINE_DETAILS9,'<ITEM>',1,1)+6,instr(LINE_DETAILS9,'</ITEM>',1,1)-(instr(LINE_DETAILS9,'<ITEM>',1,1)+6)) ITEM_NAME_LINE9,
substr(LINE_DETAILS9,instr(LINE_DETAILS9,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS9,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS9,'<DESCRIPTN>',1,1)+11)) ITEM_Description9,
(substr(TAX_DETAILS9,instr(TAX_DETAILS9,'<VALUE>',1,1)+7,instr(TAX_DETAILS9,'</VALUE>',1,1)-(instr(TAX_DETAILS9,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE9,
substr(TAX_DETAILS9,instr(TAX_DETAILS9,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS9,'</TAXCODE>',1,1)-(instr(TAX_DETAILS9,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE9,
substr(LINE_DETAILS9,instr(LINE_DETAILS9,'<VALUE>',1,3)+7,instr(LINE_DETAILS9,'</VALUE>',1,3)-(instr(LINE_DETAILS9,'<VALUE>',1,3)+7)) quantity_line9,
(substr(LINE_DETAILS9,instr(LINE_DETAILS9,'<VALUE>',1,2)+7,instr(LINE_DETAILS9,'</VALUE>',1,2)-(instr(LINE_DETAILS9,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line9,
(substr(TAX_DETAILS9,instr(TAX_DETAILS9,'<VALUE>',1,3)+7,instr(TAX_DETAILS9,'</VALUE>',1,3)-(instr(TAX_DETAILS9,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE9,
substr(tax_details9,instr(tax_details9,'<DESCRIPTN>',1,1)+11,instr(tax_details9,'</DESCRIPTN>',1,1)-(instr(tax_details9,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION9,
substr(LINE_DETAILS10,instr(LINE_DETAILS10,'<LINENUM>',1,1)+9,instr(LINE_DETAILS10,'</LINENUM>',1,1)-(instr(LINE_DETAILS10,'<LINENUM>',1,1)+9)) LINE_NUM10,
(substr(LINE_DETAILS10,instr(LINE_DETAILS10,'<VALUE>',1,1)+7,instr(LINE_DETAILS10,'</VALUE>',1,1)-(instr(LINE_DETAILS10,'<VALUE>',1,1)+7)))*.01 LINE10_AMOUNT,
substr(LINE_DETAILS10,instr(LINE_DETAILS10,'<ITEM>',1,1)+6,instr(LINE_DETAILS10,'</ITEM>',1,1)-(instr(LINE_DETAILS10,'<ITEM>',1,1)+6)) ITEM_NAME_LINE10,
substr(LINE_DETAILS10,instr(LINE_DETAILS10,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS10,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS10,'<DESCRIPTN>',1,1)+11)) ITEM_Description10,
(substr(TAX_DETAILS10,instr(TAX_DETAILS10,'<VALUE>',1,1)+7,instr(TAX_DETAILS10,'</VALUE>',1,1)-(instr(TAX_DETAILS10,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE10,
substr(TAX_DETAILS10,instr(TAX_DETAILS10,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS10,'</TAXCODE>',1,1)-(instr(TAX_DETAILS10,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE10,
substr(LINE_DETAILS10,instr(LINE_DETAILS10,'<VALUE>',1,3)+7,instr(LINE_DETAILS10,'</VALUE>',1,3)-(instr(LINE_DETAILS10,'<VALUE>',1,3)+7)) quantity_line10,
(substr(LINE_DETAILS10,instr(LINE_DETAILS10,'<VALUE>',1,2)+7,instr(LINE_DETAILS10,'</VALUE>',1,2)-(instr(LINE_DETAILS10,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line10,
(substr(TAX_DETAILS10,instr(TAX_DETAILS10,'<VALUE>',1,3)+7,instr(TAX_DETAILS10,'</VALUE>',1,3)-(instr(TAX_DETAILS10,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE10,
substr(tax_details10,instr(tax_details10,'<DESCRIPTN>',1,1)+11,instr(tax_details10,'</DESCRIPTN>',1,1)-(instr(tax_details10,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION10,
substr(LINE_DETAILS11,instr(LINE_DETAILS11,'<LINENUM>',1,1)+9,instr(LINE_DETAILS11,'</LINENUM>',1,1)-(instr(LINE_DETAILS11,'<LINENUM>',1,1)+9)) LINE_NUM11,
(substr(LINE_DETAILS11,instr(LINE_DETAILS11,'<VALUE>',1,1)+7,instr(LINE_DETAILS11,'</VALUE>',1,1)-(instr(LINE_DETAILS11,'<VALUE>',1,1)+7)))*.01 LINE11_AMOUNT,
substr(LINE_DETAILS11,instr(LINE_DETAILS11,'<ITEM>',1,1)+6,instr(LINE_DETAILS11,'</ITEM>',1,1)-(instr(LINE_DETAILS11,'<ITEM>',1,1)+6)) ITEM_NAME_LINE11,
substr(LINE_DETAILS11,instr(LINE_DETAILS11,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS11,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS11,'<DESCRIPTN>',1,1)+11)) ITEM_Description11,
(substr(TAX_DETAILS11,instr(TAX_DETAILS11,'<VALUE>',1,1)+7,instr(TAX_DETAILS11,'</VALUE>',1,1)-(instr(TAX_DETAILS11,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE11,
substr(TAX_DETAILS11,instr(TAX_DETAILS11,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS11,'</TAXCODE>',1,1)-(instr(TAX_DETAILS11,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE11,
substr(LINE_DETAILS11,instr(LINE_DETAILS11,'<VALUE>',1,3)+7,instr(LINE_DETAILS11,'</VALUE>',1,3)-(instr(LINE_DETAILS11,'<VALUE>',1,3)+7)) quantity_line11,
(substr(LINE_DETAILS11,instr(LINE_DETAILS11,'<VALUE>',1,2)+7,instr(LINE_DETAILS11,'</VALUE>',1,2)-(instr(LINE_DETAILS11,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line11,
(substr(TAX_DETAILS11,instr(TAX_DETAILS11,'<VALUE>',1,3)+7,instr(TAX_DETAILS11,'</VALUE>',1,3)-(instr(TAX_DETAILS11,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE11,
substr(tax_details11,instr(tax_details11,'<DESCRIPTN>',1,1)+11,instr(tax_details11,'</DESCRIPTN>',1,1)-(instr(tax_details11,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION11,
substr(LINE_DETAILS12,instr(LINE_DETAILS12,'<LINENUM>',1,1)+9,instr(LINE_DETAILS12,'</LINENUM>',1,1)-(instr(LINE_DETAILS12,'<LINENUM>',1,1)+9)) LINE_NUM12,
(substr(LINE_DETAILS12,instr(LINE_DETAILS12,'<VALUE>',1,1)+7,instr(LINE_DETAILS12,'</VALUE>',1,1)-(instr(LINE_DETAILS12,'<VALUE>',1,1)+7)))*.01 LINE12_AMOUNT,
substr(LINE_DETAILS12,instr(LINE_DETAILS12,'<ITEM>',1,1)+6,instr(LINE_DETAILS12,'</ITEM>',1,1)-(instr(LINE_DETAILS12,'<ITEM>',1,1)+6)) ITEM_NAME_LINE12,
substr(LINE_DETAILS12,instr(LINE_DETAILS12,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS12,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS12,'<DESCRIPTN>',1,1)+11)) ITEM_Description12,
(substr(TAX_DETAILS12,instr(TAX_DETAILS12,'<VALUE>',1,1)+7,instr(TAX_DETAILS12,'</VALUE>',1,1)-(instr(TAX_DETAILS12,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE12,
substr(TAX_DETAILS12,instr(TAX_DETAILS12,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS12,'</TAXCODE>',1,1)-(instr(TAX_DETAILS12,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE12,
substr(LINE_DETAILS12,instr(LINE_DETAILS12,'<VALUE>',1,3)+7,instr(LINE_DETAILS12,'</VALUE>',1,3)-(instr(LINE_DETAILS12,'<VALUE>',1,3)+7)) quantity_line12,
(substr(LINE_DETAILS12,instr(LINE_DETAILS12,'<VALUE>',1,2)+7,instr(LINE_DETAILS12,'</VALUE>',1,2)-(instr(LINE_DETAILS12,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line12,
(substr(TAX_DETAILS12,instr(TAX_DETAILS12,'<VALUE>',1,3)+7,instr(TAX_DETAILS12,'</VALUE>',1,3)-(instr(TAX_DETAILS12,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE12,
substr(tax_details12,instr(tax_details12,'<DESCRIPTN>',1,1)+11,instr(tax_details12,'</DESCRIPTN>',1,1)-(instr(tax_details12,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION12,
substr(LINE_DETAILS13,instr(LINE_DETAILS13,'<LINENUM>',1,1)+9,instr(LINE_DETAILS13,'</LINENUM>',1,1)-(instr(LINE_DETAILS13,'<LINENUM>',1,1)+9)) LINE_NUM13,
(substr(LINE_DETAILS13,instr(LINE_DETAILS13,'<VALUE>',1,1)+7,instr(LINE_DETAILS13,'</VALUE>',1,1)-(instr(LINE_DETAILS13,'<VALUE>',1,1)+7)))*.01 LINE13_AMOUNT,
substr(LINE_DETAILS13,instr(LINE_DETAILS13,'<ITEM>',1,1)+6,instr(LINE_DETAILS13,'</ITEM>',1,1)-(instr(LINE_DETAILS13,'<ITEM>',1,1)+6)) ITEM_NAME_LINE13,
substr(LINE_DETAILS13,instr(LINE_DETAILS13,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS13,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS13,'<DESCRIPTN>',1,1)+11)) ITEM_Description13,
(substr(TAX_DETAILS13,instr(TAX_DETAILS13,'<VALUE>',1,1)+7,instr(TAX_DETAILS13,'</VALUE>',1,1)-(instr(TAX_DETAILS13,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE13,
substr(TAX_DETAILS13,instr(TAX_DETAILS13,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS13,'</TAXCODE>',1,1)-(instr(TAX_DETAILS13,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE13,
substr(LINE_DETAILS13,instr(LINE_DETAILS13,'<VALUE>',1,3)+7,instr(LINE_DETAILS13,'</VALUE>',1,3)-(instr(LINE_DETAILS13,'<VALUE>',1,3)+7)) quantity_line13,
(substr(LINE_DETAILS13,instr(LINE_DETAILS13,'<VALUE>',1,2)+7,instr(LINE_DETAILS13,'</VALUE>',1,2)-(instr(LINE_DETAILS13,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line13,
(substr(TAX_DETAILS13,instr(TAX_DETAILS13,'<VALUE>',1,3)+7,instr(TAX_DETAILS13,'</VALUE>',1,3)-(instr(TAX_DETAILS13,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE13,
substr(tax_details13,instr(tax_details13,'<DESCRIPTN>',1,1)+11,instr(tax_details13,'</DESCRIPTN>',1,1)-(instr(tax_details13,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION13,
substr(LINE_DETAILS14,instr(LINE_DETAILS14,'<LINENUM>',1,1)+9,instr(LINE_DETAILS14,'</LINENUM>',1,1)-(instr(LINE_DETAILS14,'<LINENUM>',1,1)+9)) LINE_NUM14,
(substr(LINE_DETAILS14,instr(LINE_DETAILS14,'<VALUE>',1,1)+7,instr(LINE_DETAILS14,'</VALUE>',1,1)-(instr(LINE_DETAILS14,'<VALUE>',1,1)+7)))*.01 LINE14_AMOUNT,
substr(LINE_DETAILS14,instr(LINE_DETAILS14,'<ITEM>',1,1)+6,instr(LINE_DETAILS14,'</ITEM>',1,1)-(instr(LINE_DETAILS14,'<ITEM>',1,1)+6)) ITEM_NAME_LINE14,
substr(LINE_DETAILS14,instr(LINE_DETAILS14,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS14,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS14,'<DESCRIPTN>',1,1)+11)) ITEM_Description14,
(substr(TAX_DETAILS14,instr(TAX_DETAILS14,'<VALUE>',1,1)+7,instr(TAX_DETAILS14,'</VALUE>',1,1)-(instr(TAX_DETAILS14,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE14,
substr(TAX_DETAILS14,instr(TAX_DETAILS14,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS14,'</TAXCODE>',1,1)-(instr(TAX_DETAILS14,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE14,
substr(LINE_DETAILS14,instr(LINE_DETAILS14,'<VALUE>',1,3)+7,instr(LINE_DETAILS14,'</VALUE>',1,3)-(instr(LINE_DETAILS14,'<VALUE>',1,3)+7)) quantity_line14,
(substr(LINE_DETAILS14,instr(LINE_DETAILS14,'<VALUE>',1,2)+7,instr(LINE_DETAILS14,'</VALUE>',1,2)-(instr(LINE_DETAILS14,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line14,
(substr(TAX_DETAILS14,instr(TAX_DETAILS14,'<VALUE>',1,3)+7,instr(TAX_DETAILS14,'</VALUE>',1,3)-(instr(TAX_DETAILS14,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE14,
substr(tax_details14,instr(tax_details14,'<DESCRIPTN>',1,1)+11,instr(tax_details14,'</DESCRIPTN>',1,1)-(instr(tax_details14,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION14,
substr(LINE_DETAILS15,instr(LINE_DETAILS15,'<LINENUM>',1,1)+9,instr(LINE_DETAILS15,'</LINENUM>',1,1)-(instr(LINE_DETAILS15,'<LINENUM>',1,1)+9)) LINE_NUM15,
(substr(LINE_DETAILS15,instr(LINE_DETAILS15,'<VALUE>',1,1)+7,instr(LINE_DETAILS15,'</VALUE>',1,1)-(instr(LINE_DETAILS15,'<VALUE>',1,1)+7)))*.01 LINE15_AMOUNT,
substr(LINE_DETAILS15,instr(LINE_DETAILS15,'<ITEM>',1,1)+6,instr(LINE_DETAILS15,'</ITEM>',1,1)-(instr(LINE_DETAILS15,'<ITEM>',1,1)+6)) ITEM_NAME_LINE15,
substr(LINE_DETAILS15,instr(LINE_DETAILS15,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS15,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS15,'<DESCRIPTN>',1,1)+11)) ITEM_Description15,
substr(LINE_DETAILS15,instr(LINE_DETAILS15,'<VALUE>',1,3)+7,instr(LINE_DETAILS15,'</VALUE>',1,3)-(instr(LINE_DETAILS15,'<VALUE>',1,3)+7)) quantity_line15,
(substr(TAX_DETAILS15,instr(TAX_DETAILS15,'<VALUE>',1,1)+7,instr(TAX_DETAILS15,'</VALUE>',1,1)-(instr(TAX_DETAILS15,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE15,
substr(TAX_DETAILS15,instr(TAX_DETAILS15,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS15,'</TAXCODE>',1,1)-(instr(TAX_DETAILS15,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE15,
(substr(LINE_DETAILS15,instr(LINE_DETAILS15,'<VALUE>',1,2)+7,instr(LINE_DETAILS15,'</VALUE>',1,2)-(instr(LINE_DETAILS15,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line15,
(substr(TAX_DETAILS15,instr(TAX_DETAILS15,'<VALUE>',1,3)+7,instr(TAX_DETAILS15,'</VALUE>',1,3)-(instr(TAX_DETAILS15,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE15,
substr(tax_details15,instr(tax_details15,'<DESCRIPTN>',1,1)+11,instr(tax_details15,'</DESCRIPTN>',1,1)-(instr(tax_details15,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION15,
substr(LINE_DETAILS16,instr(LINE_DETAILS16,'<LINENUM>',1,1)+9,instr(LINE_DETAILS16,'</LINENUM>',1,1)-(instr(LINE_DETAILS16,'<LINENUM>',1,1)+9)) LINE_NUM16,
(substr(LINE_DETAILS16,instr(LINE_DETAILS16,'<VALUE>',1,1)+7,instr(LINE_DETAILS16,'</VALUE>',1,1)-(instr(LINE_DETAILS16,'<VALUE>',1,1)+7)))*.01 LINE16_AMOUNT,
substr(LINE_DETAILS16,instr(LINE_DETAILS16,'<ITEM>',1,1)+6,instr(LINE_DETAILS16,'</ITEM>',1,1)-(instr(LINE_DETAILS16,'<ITEM>',1,1)+6)) ITEM_NAME_LINE16,
substr(LINE_DETAILS16,instr(LINE_DETAILS16,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS16,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS16,'<DESCRIPTN>',1,1)+11)) ITEM_Description16,
(substr(TAX_DETAILS16,instr(TAX_DETAILS16,'<VALUE>',1,1)+7,instr(TAX_DETAILS16,'</VALUE>',1,1)-(instr(TAX_DETAILS16,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE16,
substr(TAX_DETAILS16,instr(TAX_DETAILS16,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS16,'</TAXCODE>',1,1)-(instr(TAX_DETAILS16,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE16,
substr(LINE_DETAILS16,instr(LINE_DETAILS16,'<VALUE>',1,3)+7,instr(LINE_DETAILS16,'</VALUE>',1,3)-(instr(LINE_DETAILS16,'<VALUE>',1,3)+7))quantity_line16,
(substr(LINE_DETAILS16,instr(LINE_DETAILS16,'<VALUE>',1,2)+7,instr(LINE_DETAILS16,'</VALUE>',1,2)-(instr(LINE_DETAILS16,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line16,
(substr(TAX_DETAILS16,instr(TAX_DETAILS16,'<VALUE>',1,3)+7,instr(TAX_DETAILS16,'</VALUE>',1,3)-(instr(TAX_DETAILS16,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE16,
substr(tax_details16,instr(tax_details16,'<DESCRIPTN>',1,1)+11,instr(tax_details16,'</DESCRIPTN>',1,1)-(instr(tax_details16,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION16,
substr(LINE_DETAILS17,instr(LINE_DETAILS17,'<LINENUM>',1,1)+9,instr(LINE_DETAILS17,'</LINENUM>',1,1)-(instr(LINE_DETAILS17,'<LINENUM>',1,1)+9)) LINE_NUM17,
(substr(LINE_DETAILS17,instr(LINE_DETAILS17,'<VALUE>',1,1)+7,instr(LINE_DETAILS17,'</VALUE>',1,1)-(instr(LINE_DETAILS17,'<VALUE>',1,1)+7)))*.01 LINE17_AMOUNT,
substr(LINE_DETAILS17,instr(LINE_DETAILS17,'<ITEM>',1,1)+6,instr(LINE_DETAILS17,'</ITEM>',1,1)-(instr(LINE_DETAILS17,'<ITEM>',1,1)+6)) ITEM_NAME_LINE17,
substr(LINE_DETAILS17,instr(LINE_DETAILS17,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS17,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS17,'<DESCRIPTN>',1,1)+11)) ITEM_Description17,
(substr(TAX_DETAILS17,instr(TAX_DETAILS17,'<VALUE>',1,1)+7,instr(TAX_DETAILS17,'</VALUE>',1,1)-(instr(TAX_DETAILS17,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE17,
substr(TAX_DETAILS17,instr(TAX_DETAILS17,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS17,'</TAXCODE>',1,1)-(instr(TAX_DETAILS17,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE17,
substr(LINE_DETAILS17,instr(LINE_DETAILS17,'<VALUE>',1,3)+7,instr(LINE_DETAILS17,'</VALUE>',1,3)-(instr(LINE_DETAILS17,'<VALUE>',1,3)+7)) quantity_line17,
(substr(LINE_DETAILS17,instr(LINE_DETAILS17,'<VALUE>',1,2)+7,instr(LINE_DETAILS17,'</VALUE>',1,2)-(instr(LINE_DETAILS17,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line17,
(substr(TAX_DETAILS17,instr(TAX_DETAILS17,'<VALUE>',1,3)+7,instr(TAX_DETAILS17,'</VALUE>',1,3)-(instr(TAX_DETAILS17,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE17,
substr(tax_details17,instr(tax_details17,'<DESCRIPTN>',1,1)+11,instr(tax_details17,'</DESCRIPTN>',1,1)-(instr(tax_details17,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION17,
substr(LINE_DETAILS18,instr(LINE_DETAILS18,'<LINENUM>',1,1)+9,instr(LINE_DETAILS18,'</LINENUM>',1,1)-(instr(LINE_DETAILS18,'<LINENUM>',1,1)+9)) LINE_NUM18,
(substr(LINE_DETAILS18,instr(LINE_DETAILS18,'<VALUE>',1,1)+7,instr(LINE_DETAILS18,'</VALUE>',1,1)-(instr(LINE_DETAILS18,'<VALUE>',1,1)+7)))*.01 LINE18_AMOUNT,
substr(LINE_DETAILS18,instr(LINE_DETAILS18,'<ITEM>',1,1)+6,instr(LINE_DETAILS18,'</ITEM>',1,1)-(instr(LINE_DETAILS18,'<ITEM>',1,1)+6)) ITEM_NAME_LINE18,
substr(LINE_DETAILS18,instr(LINE_DETAILS18,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS18,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS18,'<DESCRIPTN>',1,1)+11)) ITEM_Description18,
(substr(TAX_DETAILS18,instr(TAX_DETAILS18,'<VALUE>',1,1)+7,instr(TAX_DETAILS18,'</VALUE>',1,1)-(instr(TAX_DETAILS18,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE18,
substr(TAX_DETAILS18,instr(TAX_DETAILS18,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS18,'</TAXCODE>',1,1)-(instr(TAX_DETAILS18,'<TAXCODE>',1,1)+9))TAX_CODE_LINE18,
substr(LINE_DETAILS18,instr(LINE_DETAILS18,'<VALUE>',1,3)+7,instr(LINE_DETAILS18,'</VALUE>',1,3)-(instr(LINE_DETAILS18,'<VALUE>',1,3)+7)) quantity_line18,
(substr(LINE_DETAILS18,instr(LINE_DETAILS18,'<VALUE>',1,2)+7,instr(LINE_DETAILS18,'</VALUE>',1,2)-(instr(LINE_DETAILS18,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line18,
(substr(TAX_DETAILS18,instr(TAX_DETAILS18,'<VALUE>',1,3)+7,instr(TAX_DETAILS18,'</VALUE>',1,3)-(instr(TAX_DETAILS18,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE18,
substr(tax_details18,instr(tax_details18,'<DESCRIPTN>',1,1)+11,instr(tax_details18,'</DESCRIPTN>',1,1)-(instr(tax_details18,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION18,
substr(LINE_DETAILS19,instr(LINE_DETAILS19,'<LINENUM>',1,1)+9,instr(LINE_DETAILS19,'</LINENUM>',1,1)-(instr(LINE_DETAILS19,'<LINENUM>',1,1)+9)) LINE_NUM19,
(substr(LINE_DETAILS19,instr(LINE_DETAILS19,'<VALUE>',1,1)+7,instr(LINE_DETAILS19,'</VALUE>',1,1)-(instr(LINE_DETAILS19,'<VALUE>',1,1)+7)))*.01 LINE19_AMOUNT,
substr(LINE_DETAILS19,instr(LINE_DETAILS19,'<ITEM>',1,1)+6,instr(LINE_DETAILS19,'</ITEM>',1,1)-(instr(LINE_DETAILS19,'<ITEM>',1,1)+6)) ITEM_NAME_LINE19,
substr(LINE_DETAILS19,instr(LINE_DETAILS19,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS19,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS19,'<DESCRIPTN>',1,1)+11)) ITEM_Description19,
(substr(TAX_DETAILS19,instr(TAX_DETAILS19,'<VALUE>',1,1)+7,instr(TAX_DETAILS19,'</VALUE>',1,1)-(instr(TAX_DETAILS19,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE19,
substr(TAX_DETAILS19,instr(TAX_DETAILS19,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS19,'</TAXCODE>',1,1)-(instr(TAX_DETAILS19,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE19,
substr(LINE_DETAILS19,instr(LINE_DETAILS19,'<VALUE>',1,3)+7,instr(LINE_DETAILS19,'</VALUE>',1,3)-(instr(LINE_DETAILS19,'<VALUE>',1,3)+7)) quantity_line19,
(substr(LINE_DETAILS19,instr(LINE_DETAILS19,'<VALUE>',1,2)+7,instr(LINE_DETAILS19,'</VALUE>',1,2)-(instr(LINE_DETAILS19,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line19,
(substr(TAX_DETAILS19,instr(TAX_DETAILS19,'<VALUE>',1,3)+7,instr(TAX_DETAILS19,'</VALUE>',1,3)-(instr(TAX_DETAILS19,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE19,
substr(tax_details19,instr(tax_details19,'<DESCRIPTN>',1,1)+11,instr(tax_details19,'</DESCRIPTN>',1,1)-(instr(tax_details19,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION19,
substr(LINE_DETAILS20,instr(LINE_DETAILS20,'<LINENUM>',1,1)+9,instr(LINE_DETAILS20,'</LINENUM>',1,1)-(instr(LINE_DETAILS20,'<LINENUM>',1,1)+9)) LINE_NUM20,
(substr(LINE_DETAILS20,instr(LINE_DETAILS20,'<VALUE>',1,1)+7,instr(LINE_DETAILS20,'</VALUE>',1,1)-(instr(LINE_DETAILS20,'<VALUE>',1,1)+7)))*.01 LINE20_AMOUNT,
substr(LINE_DETAILS20,instr(LINE_DETAILS20,'<ITEM>',1,1)+6,instr(LINE_DETAILS20,'</ITEM>',1,1)-(instr(LINE_DETAILS20,'<ITEM>',1,1)+6)) ITEM_NAME_LINE20,
substr(LINE_DETAILS20,instr(LINE_DETAILS20,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS20,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS20,'<DESCRIPTN>',1,1)+11)) ITEM_Description20,
(substr(TAX_DETAILS20,instr(TAX_DETAILS20,'<VALUE>',1,1)+7,instr(TAX_DETAILS20,'</VALUE>',1,1)-(instr(TAX_DETAILS20,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE20,
substr(TAX_DETAILS20,instr(TAX_DETAILS20,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS20,'</TAXCODE>',1,1)-(instr(TAX_DETAILS20,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE20,
substr(LINE_DETAILS20,instr(LINE_DETAILS20,'<VALUE>',1,3)+7,instr(LINE_DETAILS20,'</VALUE>',1,3)-(instr(LINE_DETAILS20,'<VALUE>',1,3)+7)) quantity_line20,
(substr(LINE_DETAILS20,instr(LINE_DETAILS20,'<VALUE>',1,2)+7,instr(LINE_DETAILS20,'</VALUE>',1,2)-(instr(LINE_DETAILS20,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line20,
(substr(TAX_DETAILS20,instr(TAX_DETAILS20,'<VALUE>',1,3)+7,instr(TAX_DETAILS20,'</VALUE>',1,3)-(instr(TAX_DETAILS20,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE20,
substr(tax_details20,instr(tax_details20,'<DESCRIPTN>',1,1)+11,instr(tax_details20,'</DESCRIPTN>',1,1)-(instr(tax_details20,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION20,
substr(LINE_DETAILS21,instr(LINE_DETAILS21,'<LINENUM>',1,1)+9,instr(LINE_DETAILS21,'</LINENUM>',1,1)-(instr(LINE_DETAILS21,'<LINENUM>',1,1)+9)) LINE_NUM21,
(substr(LINE_DETAILS21,instr(LINE_DETAILS21,'<VALUE>',1,1)+7,instr(LINE_DETAILS21,'</VALUE>',1,1)-(instr(LINE_DETAILS21,'<VALUE>',1,1)+7)))*.01 LINE21_AMOUNT,
substr(LINE_DETAILS21,instr(LINE_DETAILS21,'<ITEM>',1,1)+6,instr(LINE_DETAILS21,'</ITEM>',1,1)-(instr(LINE_DETAILS21,'<ITEM>',1,1)+6)) ITEM_NAME_LINE21,
substr(LINE_DETAILS21,instr(LINE_DETAILS21,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS21,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS21,'<DESCRIPTN>',1,1)+11)) ITEM_Description21,
(substr(TAX_DETAILS21,instr(TAX_DETAILS21,'<VALUE>',1,1)+7,instr(TAX_DETAILS21,'</VALUE>',1,1)-(instr(TAX_DETAILS21,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE21,
substr(TAX_DETAILS21,instr(TAX_DETAILS21,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS21,'</TAXCODE>',1,1)-(instr(TAX_DETAILS21,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE21,
substr(LINE_DETAILS21,instr(LINE_DETAILS21,'<VALUE>',1,3)+7,instr(LINE_DETAILS21,'</VALUE>',1,3)-(instr(LINE_DETAILS21,'<VALUE>',1,3)+7)) quantity_line21,
(substr(LINE_DETAILS21,instr(LINE_DETAILS21,'<VALUE>',1,2)+7,instr(LINE_DETAILS21,'</VALUE>',1,2)-(instr(LINE_DETAILS21,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line21,
(substr(TAX_DETAILS21,instr(TAX_DETAILS21,'<VALUE>',1,3)+7,instr(TAX_DETAILS21,'</VALUE>',1,3)-(instr(TAX_DETAILS21,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE21,
substr(tax_details21,instr(tax_details21,'<DESCRIPTN>',1,1)+11,instr(tax_details21,'</DESCRIPTN>',1,1)-(instr(tax_details21,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION21,
substr(LINE_DETAILS22,instr(LINE_DETAILS22,'<LINENUM>',1,1)+9,instr(LINE_DETAILS22,'</LINENUM>',1,1)-(instr(LINE_DETAILS22,'<LINENUM>',1,1)+9)) LINE_NUM22,
(substr(LINE_DETAILS22,instr(LINE_DETAILS22,'<VALUE>',1,1)+7,instr(LINE_DETAILS22,'</VALUE>',1,1)-(instr(LINE_DETAILS22,'<VALUE>',1,1)+7)))*.01 LINE22_AMOUNT,
substr(LINE_DETAILS22,instr(LINE_DETAILS22,'<ITEM>',1,1)+6,instr(LINE_DETAILS22,'</ITEM>',1,1)-(instr(LINE_DETAILS22,'<ITEM>',1,1)+6)) ITEM_NAME_LINE22,
substr(LINE_DETAILS22,instr(LINE_DETAILS22,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS22,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS22,'<DESCRIPTN>',1,1)+11)) ITEM_Description22,
(substr(TAX_DETAILS22,instr(TAX_DETAILS22,'<VALUE>',1,1)+7,instr(TAX_DETAILS22,'</VALUE>',1,1)-(instr(TAX_DETAILS22,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE22,
substr(TAX_DETAILS22,instr(TAX_DETAILS22,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS22,'</TAXCODE>',1,1)-(instr(TAX_DETAILS22,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE22,
substr(LINE_DETAILS22,instr(LINE_DETAILS22,'<VALUE>',1,3)+7,instr(LINE_DETAILS22,'</VALUE>',1,3)-(instr(LINE_DETAILS22,'<VALUE>',1,3)+7)) quantity_line22,
(substr(LINE_DETAILS22,instr(LINE_DETAILS22,'<VALUE>',1,2)+7,instr(LINE_DETAILS22,'</VALUE>',1,2)-(instr(LINE_DETAILS22,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line22,
(substr(TAX_DETAILS22,instr(TAX_DETAILS22,'<VALUE>',1,3)+7,instr(TAX_DETAILS22,'</VALUE>',1,3)-(instr(TAX_DETAILS22,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE22,
substr(tax_details22,instr(tax_details22,'<DESCRIPTN>',1,1)+11,instr(tax_details22,'</DESCRIPTN>',1,1)-(instr(tax_details22,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION22,
substr(LINE_DETAILS23,instr(LINE_DETAILS23,'<LINENUM>',1,1)+9,instr(LINE_DETAILS23,'</LINENUM>',1,1)-(instr(LINE_DETAILS23,'<LINENUM>',1,1)+9)) LINE_NUM23,
(substr(LINE_DETAILS23,instr(LINE_DETAILS23,'<VALUE>',1,1)+7,instr(LINE_DETAILS23,'</VALUE>',1,1)-(instr(LINE_DETAILS23,'<VALUE>',1,1)+7)))*.01 LINE23_AMOUNT,
substr(LINE_DETAILS23,instr(LINE_DETAILS23,'<ITEM>',1,1)+6,instr(LINE_DETAILS23,'</ITEM>',1,1)-(instr(LINE_DETAILS23,'<ITEM>',1,1)+6)) ITEM_NAME_LINE23,
substr(LINE_DETAILS23,instr(LINE_DETAILS23,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS23,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS23,'<DESCRIPTN>',1,1)+11)) ITEM_Description23,
(substr(TAX_DETAILS23,instr(TAX_DETAILS23,'<VALUE>',1,1)+7,instr(TAX_DETAILS23,'</VALUE>',1,1)-(instr(TAX_DETAILS23,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE23,
substr(TAX_DETAILS23,instr(TAX_DETAILS23,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS23,'</TAXCODE>',1,1)-(instr(TAX_DETAILS23,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE23,
substr(LINE_DETAILS23,instr(LINE_DETAILS23,'<VALUE>',1,3)+7,instr(LINE_DETAILS23,'</VALUE>',1,3)-(instr(LINE_DETAILS23,'<VALUE>',1,3)+7)) quantity_line23,
(substr(LINE_DETAILS23,instr(LINE_DETAILS23,'<VALUE>',1,2)+7,instr(LINE_DETAILS23,'</VALUE>',1,2)-(instr(LINE_DETAILS23,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line23,
(substr(TAX_DETAILS23,instr(TAX_DETAILS23,'<VALUE>',1,3)+7,instr(TAX_DETAILS23,'</VALUE>',1,3)-(instr(TAX_DETAILS23,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE23,
substr(tax_details23,instr(tax_details23,'<DESCRIPTN>',1,1)+11,instr(tax_details23,'</DESCRIPTN>',1,1)-(instr(tax_details23,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION23,
substr(LINE_DETAILS24,instr(LINE_DETAILS24,'<LINENUM>',1,1)+9,instr(LINE_DETAILS24,'</LINENUM>',1,1)-(instr(LINE_DETAILS24,'<LINENUM>',1,1)+9)) LINE_NUM24,
(substr(LINE_DETAILS24,instr(LINE_DETAILS24,'<VALUE>',1,1)+7,instr(LINE_DETAILS24,'</VALUE>',1,1)-(instr(LINE_DETAILS24,'<VALUE>',1,1)+7)))*.01 LINE24_AMOUNT,
substr(LINE_DETAILS24,instr(LINE_DETAILS24,'<ITEM>',1,1)+6,instr(LINE_DETAILS24,'</ITEM>',1,1)-(instr(LINE_DETAILS24,'<ITEM>',1,1)+6)) ITEM_NAME_LINE24,
substr(LINE_DETAILS24,instr(LINE_DETAILS24,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS24,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS24,'<DESCRIPTN>',1,1)+11)) ITEM_Description24,
(substr(TAX_DETAILS24,instr(TAX_DETAILS24,'<VALUE>',1,1)+7,instr(TAX_DETAILS24,'</VALUE>',1,1)-(instr(TAX_DETAILS24,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE24,
substr(TAX_DETAILS24,instr(TAX_DETAILS24,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS24,'</TAXCODE>',1,1)-(instr(TAX_DETAILS24,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE24,
substr(LINE_DETAILS24,instr(LINE_DETAILS24,'<VALUE>',1,3)+7,instr(LINE_DETAILS24,'</VALUE>',1,3)-(instr(LINE_DETAILS24,'<VALUE>',1,3)+7)) quantity_line24,
(substr(LINE_DETAILS24,instr(LINE_DETAILS24,'<VALUE>',1,2)+7,instr(LINE_DETAILS24,'</VALUE>',1,2)-(instr(LINE_DETAILS24,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line24,
(substr(TAX_DETAILS24,instr(TAX_DETAILS24,'<VALUE>',1,3)+7,instr(TAX_DETAILS24,'</VALUE>',1,3)-(instr(TAX_DETAILS24,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE24,
substr(tax_details24,instr(tax_details24,'<DESCRIPTN>',1,1)+11,instr(tax_details24,'</DESCRIPTN>',1,1)-(instr(tax_details24,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION24,
substr(LINE_DETAILS25,instr(LINE_DETAILS25,'<LINENUM>',1,1)+9,instr(LINE_DETAILS25,'</LINENUM>',1,1)-(instr(LINE_DETAILS25,'<LINENUM>',1,1)+9)) LINE_NUM25,
(substr(LINE_DETAILS25,instr(LINE_DETAILS25,'<VALUE>',1,1)+7,instr(LINE_DETAILS25,'</VALUE>',1,1)-(instr(LINE_DETAILS25,'<VALUE>',1,1)+7)))*.01 LINE25_AMOUNT,
substr(LINE_DETAILS25,instr(LINE_DETAILS25,'<ITEM>',1,1)+6,instr(LINE_DETAILS25,'</ITEM>',1,1)-(instr(LINE_DETAILS25,'<ITEM>',1,1)+6)) ITEM_NAME_LINE25,
substr(LINE_DETAILS25,instr(LINE_DETAILS25,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS25,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS25,'<DESCRIPTN>',1,1)+11)) ITEM_Description25,
(substr(TAX_DETAILS25,instr(TAX_DETAILS25,'<VALUE>',1,1)+7,instr(TAX_DETAILS25,'</VALUE>',1,1)-(instr(TAX_DETAILS25,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE25,
substr(TAX_DETAILS25,instr(TAX_DETAILS25,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS25,'</TAXCODE>',1,1)-(instr(TAX_DETAILS25,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE25,
substr(LINE_DETAILS25,instr(LINE_DETAILS25,'<VALUE>',1,3)+7,instr(LINE_DETAILS25,'</VALUE>',1,3)-(instr(LINE_DETAILS25,'<VALUE>',1,3)+7)) quantity_line25,
(substr(LINE_DETAILS25,instr(LINE_DETAILS25,'<VALUE>',1,2)+7,instr(LINE_DETAILS25,'</VALUE>',1,2)-(instr(LINE_DETAILS25,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line25,
(substr(TAX_DETAILS25,instr(TAX_DETAILS25,'<VALUE>',1,3)+7,instr(TAX_DETAILS25,'</VALUE>',1,3)-(instr(TAX_DETAILS25,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE25,
substr(tax_details25,instr(tax_details25,'<DESCRIPTN>',1,1)+11,instr(tax_details25,'</DESCRIPTN>',1,1)-(instr(tax_details25,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION25,
substr(LINE_DETAILS26,instr(LINE_DETAILS26,'<LINENUM>',1,1)+9,instr(LINE_DETAILS26,'</LINENUM>',1,1)-(instr(LINE_DETAILS26,'<LINENUM>',1,1)+9)) LINE_NUM26,
(substr(LINE_DETAILS26,instr(LINE_DETAILS26,'<VALUE>',1,1)+7,instr(LINE_DETAILS26,'</VALUE>',1,1)-(instr(LINE_DETAILS26,'<VALUE>',1,1)+7)))*.01 LINE26_AMOUNT,
substr(LINE_DETAILS26,instr(LINE_DETAILS26,'<ITEM>',1,1)+6,instr(LINE_DETAILS26,'</ITEM>',1,1)-(instr(LINE_DETAILS26,'<ITEM>',1,1)+6)) ITEM_NAME_LINE26,
substr(LINE_DETAILS26,instr(LINE_DETAILS26,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS26,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS26,'<DESCRIPTN>',1,1)+11)) ITEM_Description26,
(substr(TAX_DETAILS26,instr(TAX_DETAILS26,'<VALUE>',1,1)+7,instr(TAX_DETAILS26,'</VALUE>',1,1)-(instr(TAX_DETAILS26,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE26,
substr(TAX_DETAILS26,instr(TAX_DETAILS26,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS26,'</TAXCODE>',1,1)-(instr(TAX_DETAILS26,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE26,
substr(LINE_DETAILS26,instr(LINE_DETAILS26,'<VALUE>',1,3)+7,instr(LINE_DETAILS26,'</VALUE>',1,3)-(instr(LINE_DETAILS26,'<VALUE>',1,3)+7)) quantity_line26,
(substr(LINE_DETAILS26,instr(LINE_DETAILS26,'<VALUE>',1,2)+7,instr(LINE_DETAILS26,'</VALUE>',1,2)-(instr(LINE_DETAILS26,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line26,
(substr(TAX_DETAILS26,instr(TAX_DETAILS26,'<VALUE>',1,3)+7,instr(TAX_DETAILS26,'</VALUE>',1,3)-(instr(TAX_DETAILS26,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE26,
substr(tax_details26,instr(tax_details26,'<DESCRIPTN>',1,1)+11,instr(tax_details26,'</DESCRIPTN>',1,1)-(instr(tax_details26,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION26,
substr(LINE_DETAILS27,instr(LINE_DETAILS27,'<LINENUM>',1,1)+9,instr(LINE_DETAILS27,'</LINENUM>',1,1)-(instr(LINE_DETAILS27,'<LINENUM>',1,1)+9)) LINE_NUM27,
(substr(LINE_DETAILS27,instr(LINE_DETAILS27,'<VALUE>',1,1)+7,instr(LINE_DETAILS27,'</VALUE>',1,1)-(instr(LINE_DETAILS27,'<VALUE>',1,1)+7)))*.01 LINE27_AMOUNT,
substr(LINE_DETAILS27,instr(LINE_DETAILS27,'<ITEM>',1,1)+6,instr(LINE_DETAILS27,'</ITEM>',1,1)-(instr(LINE_DETAILS27,'<ITEM>',1,1)+6)) ITEM_NAME_LINE27,
substr(LINE_DETAILS27,instr(LINE_DETAILS27,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS27,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS27,'<DESCRIPTN>',1,1)+11)) ITEM_Description27,
(substr(TAX_DETAILS27,instr(TAX_DETAILS27,'<VALUE>',1,1)+7,instr(TAX_DETAILS27,'</VALUE>',1,1)-(instr(TAX_DETAILS27,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE27,
substr(TAX_DETAILS27,instr(TAX_DETAILS27,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS27,'</TAXCODE>',1,1)-(instr(TAX_DETAILS27,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE27,
substr(LINE_DETAILS27,instr(LINE_DETAILS27,'<VALUE>',1,3)+7,instr(LINE_DETAILS27,'</VALUE>',1,3)-(instr(LINE_DETAILS27,'<VALUE>',1,3)+7)) quantity_line27,
(substr(LINE_DETAILS27,instr(LINE_DETAILS27,'<VALUE>',1,2)+7,instr(LINE_DETAILS27,'</VALUE>',1,2)-(instr(LINE_DETAILS27,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line27,
(substr(TAX_DETAILS27,instr(TAX_DETAILS27,'<VALUE>',1,3)+7,instr(TAX_DETAILS27,'</VALUE>',1,3)-(instr(TAX_DETAILS27,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE27,
substr(tax_details27,instr(tax_details27,'<DESCRIPTN>',1,1)+11,instr(tax_details27,'</DESCRIPTN>',1,1)-(instr(tax_details27,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION27,
substr(LINE_DETAILS28,instr(LINE_DETAILS28,'<LINENUM>',1,1)+9,instr(LINE_DETAILS28,'</LINENUM>',1,1)-(instr(LINE_DETAILS28,'<LINENUM>',1,1)+9)) LINE_NUM28,
(substr(LINE_DETAILS28,instr(LINE_DETAILS28,'<VALUE>',1,1)+7,instr(LINE_DETAILS28,'</VALUE>',1,1)-(instr(LINE_DETAILS28,'<VALUE>',1,1)+7)))*.01 LINE28_AMOUNT,
substr(LINE_DETAILS28,instr(LINE_DETAILS28,'<ITEM>',1,1)+6,instr(LINE_DETAILS28,'</ITEM>',1,1)-(instr(LINE_DETAILS28,'<ITEM>',1,1)+6)) ITEM_NAME_LINE28,
substr(LINE_DETAILS28,instr(LINE_DETAILS28,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS28,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS28,'<DESCRIPTN>',1,1)+11)) ITEM_Description28,
(substr(TAX_DETAILS28,instr(TAX_DETAILS28,'<VALUE>',1,1)+7,instr(TAX_DETAILS28,'</VALUE>',1,1)-(instr(TAX_DETAILS28,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE28,
substr(TAX_DETAILS28,instr(TAX_DETAILS28,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS28,'</TAXCODE>',1,1)-(instr(TAX_DETAILS28,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE28,
substr(LINE_DETAILS28,instr(LINE_DETAILS28,'<VALUE>',1,3)+7,instr(LINE_DETAILS28,'</VALUE>',1,3)-(instr(LINE_DETAILS28,'<VALUE>',1,3)+7)) quantity_line28,
(substr(LINE_DETAILS28,instr(LINE_DETAILS28,'<VALUE>',1,2)+7,instr(LINE_DETAILS28,'</VALUE>',1,2)-(instr(LINE_DETAILS28,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line28,
(substr(TAX_DETAILS28,instr(TAX_DETAILS28,'<VALUE>',1,3)+7,instr(TAX_DETAILS28,'</VALUE>',1,3)-(instr(TAX_DETAILS28,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE28,
substr(tax_details28,instr(tax_details28,'<DESCRIPTN>',1,1)+11,instr(tax_details28,'</DESCRIPTN>',1,1)-(instr(tax_details28,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION28,
substr(LINE_DETAILS29,instr(LINE_DETAILS29,'<LINENUM>',1,1)+9,instr(LINE_DETAILS29,'</LINENUM>',1,1)-(instr(LINE_DETAILS29,'<LINENUM>',1,1)+9)) LINE_NUM29,
(substr(LINE_DETAILS29,instr(LINE_DETAILS29,'<VALUE>',1,1)+7,instr(LINE_DETAILS29,'</VALUE>',1,1)-(instr(LINE_DETAILS29,'<VALUE>',1,1)+7)))*.01 LINE29_AMOUNT,
substr(LINE_DETAILS29,instr(LINE_DETAILS29,'<ITEM>',1,1)+6,instr(LINE_DETAILS29,'</ITEM>',1,1)-(instr(LINE_DETAILS29,'<ITEM>',1,1)+6)) ITEM_NAME_LINE29,
substr(LINE_DETAILS29,instr(LINE_DETAILS29,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS29,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS29,'<DESCRIPTN>',1,1)+11)) ITEM_Description29,
(substr(TAX_DETAILS29,instr(TAX_DETAILS29,'<VALUE>',1,1)+7,instr(TAX_DETAILS29,'</VALUE>',1,1)-(instr(TAX_DETAILS29,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE29,
substr(TAX_DETAILS29,instr(TAX_DETAILS29,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS29,'</TAXCODE>',1,1)-(instr(TAX_DETAILS29,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE29,
substr(LINE_DETAILS29,instr(LINE_DETAILS29,'<VALUE>',1,3)+7,instr(LINE_DETAILS29,'</VALUE>',1,3)-(instr(LINE_DETAILS29,'<VALUE>',1,3)+7)) quantity_line29,
(substr(LINE_DETAILS29,instr(LINE_DETAILS29,'<VALUE>',1,2)+7,instr(LINE_DETAILS29,'</VALUE>',1,2)-(instr(LINE_DETAILS29,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line29,
(substr(TAX_DETAILS29,instr(TAX_DETAILS29,'<VALUE>',1,3)+7,instr(TAX_DETAILS29,'</VALUE>',1,3)-(instr(TAX_DETAILS29,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE29,
substr(tax_details29,instr(tax_details29,'<DESCRIPTN>',1,1)+11,instr(tax_details29,'</DESCRIPTN>',1,1)-(instr(tax_details29,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION29,
substr(LINE_DETAILS30,instr(LINE_DETAILS30,'<LINENUM>',1,1)+9,instr(LINE_DETAILS30,'</LINENUM>',1,1)-(instr(LINE_DETAILS30,'<LINENUM>',1,1)+9)) LINE_NUM30,
(substr(LINE_DETAILS30,instr(LINE_DETAILS30,'<VALUE>',1,1)+7,instr(LINE_DETAILS30,'</VALUE>',1,1)-(instr(LINE_DETAILS30,'<VALUE>',1,1)+7)))*.01 LINE30_AMOUNT,
substr(LINE_DETAILS30,instr(LINE_DETAILS30,'<ITEM>',1,1)+6,instr(LINE_DETAILS30,'</ITEM>',1,1)-(instr(LINE_DETAILS30,'<ITEM>',1,1)+6)) ITEM_NAME_LINE30,
substr(LINE_DETAILS30,instr(LINE_DETAILS30,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS30,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS30,'<DESCRIPTN>',1,1)+11)) ITEM_Description30,
(substr(TAX_DETAILS30,instr(TAX_DETAILS30,'<VALUE>',1,1)+7,instr(TAX_DETAILS30,'</VALUE>',1,1)-(instr(TAX_DETAILS30,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE30,
substr(TAX_DETAILS30,instr(TAX_DETAILS30,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS30,'</TAXCODE>',1,1)-(instr(TAX_DETAILS30,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE30,
substr(LINE_DETAILS30,instr(LINE_DETAILS30,'<VALUE>',1,3)+7,instr(LINE_DETAILS30,'</VALUE>',1,3)-(instr(LINE_DETAILS30,'<VALUE>',1,3)+7)) quantity_line30,
(substr(LINE_DETAILS30,instr(LINE_DETAILS30,'<VALUE>',1,2)+7,instr(LINE_DETAILS30,'</VALUE>',1,2)-(instr(LINE_DETAILS30,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line30,
(substr(TAX_DETAILS30,instr(TAX_DETAILS30,'<VALUE>',1,3)+7,instr(TAX_DETAILS30,'</VALUE>',1,3)-(instr(TAX_DETAILS30,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE30,
substr(tax_details30,instr(tax_details30,'<DESCRIPTN>',1,1)+11,instr(tax_details30,'</DESCRIPTN>',1,1)-(instr(tax_details30,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION30,
substr(LINE_DETAILS31,instr(LINE_DETAILS31,'<LINENUM>',1,1)+9,instr(LINE_DETAILS31,'</LINENUM>',1,1)-(instr(LINE_DETAILS31,'<LINENUM>',1,1)+9)) LINE_NUM31,
(substr(LINE_DETAILS31,instr(LINE_DETAILS31,'<VALUE>',1,1)+7,instr(LINE_DETAILS31,'</VALUE>',1,1)-(instr(LINE_DETAILS31,'<VALUE>',1,1)+7)))*.01 LINE31_AMOUNT,
substr(LINE_DETAILS31,instr(LINE_DETAILS31,'<ITEM>',1,1)+6,instr(LINE_DETAILS31,'</ITEM>',1,1)-(instr(LINE_DETAILS31,'<ITEM>',1,1)+6)) ITEM_NAME_LINE31,
substr(LINE_DETAILS31,instr(LINE_DETAILS31,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS31,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS31,'<DESCRIPTN>',1,1)+11)) ITEM_Description31,
(substr(TAX_DETAILS31,instr(TAX_DETAILS31,'<VALUE>',1,1)+7,instr(TAX_DETAILS31,'</VALUE>',1,1)-(instr(TAX_DETAILS31,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE31,
substr(TAX_DETAILS31,instr(TAX_DETAILS31,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS31,'</TAXCODE>',1,1)-(instr(TAX_DETAILS31,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE31,
substr(LINE_DETAILS31,instr(LINE_DETAILS31,'<VALUE>',1,3)+7,instr(LINE_DETAILS31,'</VALUE>',1,3)-(instr(LINE_DETAILS31,'<VALUE>',1,3)+7)) quantity_line31,
(substr(LINE_DETAILS31,instr(LINE_DETAILS31,'<VALUE>',1,2)+7,instr(LINE_DETAILS31,'</VALUE>',1,2)-(instr(LINE_DETAILS31,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line31,
(substr(TAX_DETAILS31,instr(TAX_DETAILS31,'<VALUE>',1,3)+7,instr(TAX_DETAILS31,'</VALUE>',1,3)-(instr(TAX_DETAILS31,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE31,
substr(tax_details31,instr(tax_details31,'<DESCRIPTN>',1,1)+11,instr(tax_details31,'</DESCRIPTN>',1,1)-(instr(tax_details31,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION31,
substr(LINE_DETAILS32,instr(LINE_DETAILS32,'<LINENUM>',1,1)+9,instr(LINE_DETAILS32,'</LINENUM>',1,1)-(instr(LINE_DETAILS32,'<LINENUM>',1,1)+9)) LINE_NUM32,
(substr(LINE_DETAILS32,instr(LINE_DETAILS32,'<VALUE>',1,1)+7,instr(LINE_DETAILS32,'</VALUE>',1,1)-(instr(LINE_DETAILS32,'<VALUE>',1,1)+7)))*.01 LINE32_AMOUNT,
substr(LINE_DETAILS32,instr(LINE_DETAILS32,'<ITEM>',1,1)+6,instr(LINE_DETAILS32,'</ITEM>',1,1)-(instr(LINE_DETAILS32,'<ITEM>',1,1)+6)) ITEM_NAME_LINE32,
substr(LINE_DETAILS32,instr(LINE_DETAILS32,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS32,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS32,'<DESCRIPTN>',1,1)+11)) ITEM_Description32,
(substr(TAX_DETAILS32,instr(TAX_DETAILS32,'<VALUE>',1,1)+7,instr(TAX_DETAILS32,'</VALUE>',1,1)-(instr(TAX_DETAILS32,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE32,
substr(TAX_DETAILS32,instr(TAX_DETAILS32,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS32,'</TAXCODE>',1,1)-(instr(TAX_DETAILS32,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE32,
substr(LINE_DETAILS32,instr(LINE_DETAILS32,'<VALUE>',1,3)+7,instr(LINE_DETAILS32,'</VALUE>',1,3)-(instr(LINE_DETAILS32,'<VALUE>',1,3)+7)) quantity_line32,
(substr(LINE_DETAILS32,instr(LINE_DETAILS32,'<VALUE>',1,2)+7,instr(LINE_DETAILS32,'</VALUE>',1,2)-(instr(LINE_DETAILS32,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line32,
(substr(TAX_DETAILS32,instr(TAX_DETAILS32,'<VALUE>',1,3)+7,instr(TAX_DETAILS32,'</VALUE>',1,3)-(instr(TAX_DETAILS32,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE32,
substr(tax_details32,instr(tax_details32,'<DESCRIPTN>',1,1)+11,instr(tax_details32,'</DESCRIPTN>',1,1)-(instr(tax_details32,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION32,
substr(LINE_DETAILS33,instr(LINE_DETAILS33,'<LINENUM>',1,1)+9,instr(LINE_DETAILS33,'</LINENUM>',1,1)-(instr(LINE_DETAILS33,'<LINENUM>',1,1)+9)) LINE_NUM33,
(substr(LINE_DETAILS33,instr(LINE_DETAILS33,'<VALUE>',1,1)+7,instr(LINE_DETAILS33,'</VALUE>',1,1)-(instr(LINE_DETAILS33,'<VALUE>',1,1)+7)))*.01 LINE33_AMOUNT,
substr(LINE_DETAILS33,instr(LINE_DETAILS33,'<ITEM>',1,1)+6,instr(LINE_DETAILS33,'</ITEM>',1,1)-(instr(LINE_DETAILS33,'<ITEM>',1,1)+6)) ITEM_NAME_LINE33,
substr(LINE_DETAILS33,instr(LINE_DETAILS33,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS33,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS33,'<DESCRIPTN>',1,1)+11)) ITEM_Description33,
(substr(TAX_DETAILS33,instr(TAX_DETAILS33,'<VALUE>',1,1)+7,instr(TAX_DETAILS33,'</VALUE>',1,1)-(instr(TAX_DETAILS33,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE33,
substr(TAX_DETAILS33,instr(TAX_DETAILS33,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS33,'</TAXCODE>',1,1)-(instr(TAX_DETAILS33,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE33,
substr(LINE_DETAILS33,instr(LINE_DETAILS33,'<VALUE>',1,3)+7,instr(LINE_DETAILS33,'</VALUE>',1,3)-(instr(LINE_DETAILS33,'<VALUE>',1,3)+7)) quantity_line33,
(substr(LINE_DETAILS33,instr(LINE_DETAILS33,'<VALUE>',1,2)+7,instr(LINE_DETAILS33,'</VALUE>',1,2)-(instr(LINE_DETAILS33,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line33,
(substr(TAX_DETAILS33,instr(TAX_DETAILS33,'<VALUE>',1,3)+7,instr(TAX_DETAILS33,'</VALUE>',1,3)-(instr(TAX_DETAILS33,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE33,
substr(tax_details33,instr(tax_details33,'<DESCRIPTN>',1,1)+11,instr(tax_details33,'</DESCRIPTN>',1,1)-(instr(tax_details33,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION33,
substr(LINE_DETAILS34,instr(LINE_DETAILS34,'<LINENUM>',1,1)+9,instr(LINE_DETAILS34,'</LINENUM>',1,1)-(instr(LINE_DETAILS34,'<LINENUM>',1,1)+9)) LINE_NUM34,
(substr(LINE_DETAILS34,instr(LINE_DETAILS34,'<VALUE>',1,1)+7,instr(LINE_DETAILS34,'</VALUE>',1,1)-(instr(LINE_DETAILS34,'<VALUE>',1,1)+7)))*.01 LINE34_AMOUNT,
substr(LINE_DETAILS34,instr(LINE_DETAILS34,'<ITEM>',1,1)+6,instr(LINE_DETAILS34,'</ITEM>',1,1)-(instr(LINE_DETAILS34,'<ITEM>',1,1)+6)) ITEM_NAME_LINE34,
substr(LINE_DETAILS34,instr(LINE_DETAILS34,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS34,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS34,'<DESCRIPTN>',1,1)+11)) ITEM_Description34,
(substr(TAX_DETAILS34,instr(TAX_DETAILS34,'<VALUE>',1,1)+7,instr(TAX_DETAILS34,'</VALUE>',1,1)-(instr(TAX_DETAILS34,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE34,
substr(TAX_DETAILS34,instr(TAX_DETAILS34,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS34,'</TAXCODE>',1,1)-(instr(TAX_DETAILS34,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE34,
substr(LINE_DETAILS34,instr(LINE_DETAILS34,'<VALUE>',1,3)+7,instr(LINE_DETAILS34,'</VALUE>',1,3)-(instr(LINE_DETAILS34,'<VALUE>',1,3)+7)) quantity_line34,
(substr(LINE_DETAILS34,instr(LINE_DETAILS34,'<VALUE>',1,2)+7,instr(LINE_DETAILS34,'</VALUE>',1,2)-(instr(LINE_DETAILS34,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line34,
(substr(TAX_DETAILS34,instr(TAX_DETAILS34,'<VALUE>',1,3)+7,instr(TAX_DETAILS34,'</VALUE>',1,3)-(instr(TAX_DETAILS34,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE34,
substr(tax_details34,instr(tax_details34,'<DESCRIPTN>',1,1)+11,instr(tax_details34,'</DESCRIPTN>',1,1)-(instr(tax_details34,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION34,
substr(LINE_DETAILS35,instr(LINE_DETAILS35,'<LINENUM>',1,1)+9,instr(LINE_DETAILS35,'</LINENUM>',1,1)-(instr(LINE_DETAILS35,'<LINENUM>',1,1)+9)) LINE_NUM35,
(substr(LINE_DETAILS35,instr(LINE_DETAILS35,'<VALUE>',1,1)+7,instr(LINE_DETAILS35,'</VALUE>',1,1)-(instr(LINE_DETAILS35,'<VALUE>',1,1)+7)))*.01 LINE35_AMOUNT,
substr(LINE_DETAILS35,instr(LINE_DETAILS35,'<ITEM>',1,1)+6,instr(LINE_DETAILS35,'</ITEM>',1,1)-(instr(LINE_DETAILS35,'<ITEM>',1,1)+6)) ITEM_NAME_LINE35,
substr(LINE_DETAILS35,instr(LINE_DETAILS35,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS35,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS35,'<DESCRIPTN>',1,1)+11)) ITEM_Description35,
(substr(TAX_DETAILS35,instr(TAX_DETAILS35,'<VALUE>',1,1)+7,instr(TAX_DETAILS35,'</VALUE>',1,1)-(instr(TAX_DETAILS35,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE35,
substr(TAX_DETAILS35,instr(TAX_DETAILS35,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS35,'</TAXCODE>',1,1)-(instr(TAX_DETAILS35,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE35,
substr(LINE_DETAILS35,instr(LINE_DETAILS35,'<VALUE>',1,3)+7,instr(LINE_DETAILS35,'</VALUE>',1,3)-(instr(LINE_DETAILS35,'<VALUE>',1,3)+7)) quantity_line35,
(substr(LINE_DETAILS35,instr(LINE_DETAILS35,'<VALUE>',1,2)+7,instr(LINE_DETAILS35,'</VALUE>',1,2)-(instr(LINE_DETAILS35,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line35,
(substr(TAX_DETAILS35,instr(TAX_DETAILS35,'<VALUE>',1,3)+7,instr(TAX_DETAILS35,'</VALUE>',1,3)-(instr(TAX_DETAILS35,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE35,
substr(tax_details35,instr(tax_details35,'<DESCRIPTN>',1,1)+11,instr(tax_details35,'</DESCRIPTN>',1,1)-(instr(tax_details35,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION35,
substr(LINE_DETAILS36,instr(LINE_DETAILS36,'<LINENUM>',1,1)+9,instr(LINE_DETAILS36,'</LINENUM>',1,1)-(instr(LINE_DETAILS36,'<LINENUM>',1,1)+9)) LINE_NUM36,
(substr(LINE_DETAILS36,instr(LINE_DETAILS36,'<VALUE>',1,1)+7,instr(LINE_DETAILS36,'</VALUE>',1,1)-(instr(LINE_DETAILS36,'<VALUE>',1,1)+7)))*.01 LINE36_AMOUNT,
substr(LINE_DETAILS36,instr(LINE_DETAILS36,'<ITEM>',1,1)+6,instr(LINE_DETAILS36,'</ITEM>',1,1)-(instr(LINE_DETAILS36,'<ITEM>',1,1)+6)) ITEM_NAME_LINE36,
substr(LINE_DETAILS36,instr(LINE_DETAILS36,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS36,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS36,'<DESCRIPTN>',1,1)+11)) ITEM_Description36,
(substr(TAX_DETAILS36,instr(TAX_DETAILS36,'<VALUE>',1,1)+7,instr(TAX_DETAILS36,'</VALUE>',1,1)-(instr(TAX_DETAILS36,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE36,
substr(TAX_DETAILS36,instr(TAX_DETAILS36,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS36,'</TAXCODE>',1,1)-(instr(TAX_DETAILS36,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE36,
substr(LINE_DETAILS36,instr(LINE_DETAILS36,'<VALUE>',1,3)+7,instr(LINE_DETAILS36,'</VALUE>',1,3)-(instr(LINE_DETAILS36,'<VALUE>',1,3)+7)) quantity_line36,
(substr(LINE_DETAILS36,instr(LINE_DETAILS36,'<VALUE>',1,2)+7,instr(LINE_DETAILS36,'</VALUE>',1,2)-(instr(LINE_DETAILS36,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line36,
(substr(TAX_DETAILS36,instr(TAX_DETAILS36,'<VALUE>',1,3)+7,instr(TAX_DETAILS36,'</VALUE>',1,3)-(instr(TAX_DETAILS36,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE36,
substr(tax_details36,instr(tax_details36,'<DESCRIPTN>',1,1)+11,instr(tax_details36,'</DESCRIPTN>',1,1)-(instr(tax_details36,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION36,
substr(LINE_DETAILS37,instr(LINE_DETAILS37,'<LINENUM>',1,1)+9,instr(LINE_DETAILS37,'</LINENUM>',1,1)-(instr(LINE_DETAILS37,'<LINENUM>',1,1)+9)) LINE_NUM37,
(substr(LINE_DETAILS37,instr(LINE_DETAILS37,'<VALUE>',1,1)+7,instr(LINE_DETAILS37,'</VALUE>',1,1)-(instr(LINE_DETAILS37,'<VALUE>',1,1)+7)))*.01 LINE37_AMOUNT,
substr(LINE_DETAILS37,instr(LINE_DETAILS37,'<ITEM>',1,1)+6,instr(LINE_DETAILS37,'</ITEM>',1,1)-(instr(LINE_DETAILS37,'<ITEM>',1,1)+6)) ITEM_NAME_LINE37,
substr(LINE_DETAILS37,instr(LINE_DETAILS37,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS37,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS37,'<DESCRIPTN>',1,1)+11)) ITEM_Description37,
(substr(TAX_DETAILS37,instr(TAX_DETAILS37,'<VALUE>',1,1)+7,instr(TAX_DETAILS37,'</VALUE>',1,1)-(instr(TAX_DETAILS37,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE37,
substr(TAX_DETAILS37,instr(TAX_DETAILS37,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS37,'</TAXCODE>',1,1)-(instr(TAX_DETAILS37,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE37,
substr(LINE_DETAILS37,instr(LINE_DETAILS37,'<VALUE>',1,3)+7,instr(LINE_DETAILS37,'</VALUE>',1,3)-(instr(LINE_DETAILS37,'<VALUE>',1,3)+7)) quantity_line37,
(substr(LINE_DETAILS37,instr(LINE_DETAILS37,'<VALUE>',1,2)+7,instr(LINE_DETAILS37,'</VALUE>',1,2)-(instr(LINE_DETAILS37,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line37,
(substr(TAX_DETAILS37,instr(TAX_DETAILS37,'<VALUE>',1,3)+7,instr(TAX_DETAILS37,'</VALUE>',1,3)-(instr(TAX_DETAILS37,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE37,
substr(tax_details37,instr(tax_details37,'<DESCRIPTN>',1,1)+11,instr(tax_details37,'</DESCRIPTN>',1,1)-(instr(tax_details37,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION37,
substr(LINE_DETAILS38,instr(LINE_DETAILS38,'<LINENUM>',1,1)+9,instr(LINE_DETAILS38,'</LINENUM>',1,1)-(instr(LINE_DETAILS38,'<LINENUM>',1,1)+9)) LINE_NUM38,
(substr(LINE_DETAILS38,instr(LINE_DETAILS38,'<VALUE>',1,1)+7,instr(LINE_DETAILS38,'</VALUE>',1,1)-(instr(LINE_DETAILS38,'<VALUE>',1,1)+7)))*.01 LINE38_AMOUNT,
substr(LINE_DETAILS38,instr(LINE_DETAILS38,'<ITEM>',1,1)+6,instr(LINE_DETAILS38,'</ITEM>',1,1)-(instr(LINE_DETAILS38,'<ITEM>',1,1)+6)) ITEM_NAME_LINE38,
substr(LINE_DETAILS38,instr(LINE_DETAILS38,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS38,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS38,'<DESCRIPTN>',1,1)+11)) ITEM_Description38,
(substr(TAX_DETAILS38,instr(TAX_DETAILS38,'<VALUE>',1,1)+7,instr(TAX_DETAILS38,'</VALUE>',1,1)-(instr(TAX_DETAILS38,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE38,
substr(TAX_DETAILS38,instr(TAX_DETAILS38,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS38,'</TAXCODE>',1,1)-(instr(TAX_DETAILS38,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE38,
substr(LINE_DETAILS38,instr(LINE_DETAILS38,'<VALUE>',1,3)+7,instr(LINE_DETAILS38,'</VALUE>',1,3)-(instr(LINE_DETAILS38,'<VALUE>',1,3)+7)) quantity_line38,
(substr(LINE_DETAILS38,instr(LINE_DETAILS38,'<VALUE>',1,2)+7,instr(LINE_DETAILS38,'</VALUE>',1,2)-(instr(LINE_DETAILS38,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line38,
(substr(TAX_DETAILS38,instr(TAX_DETAILS38,'<VALUE>',1,3)+7,instr(TAX_DETAILS38,'</VALUE>',1,3)-(instr(TAX_DETAILS38,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE38,
substr(tax_details38,instr(tax_details38,'<DESCRIPTN>',1,1)+11,instr(tax_details38,'</DESCRIPTN>',1,1)-(instr(tax_details38,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION38,
substr(LINE_DETAILS39,instr(LINE_DETAILS39,'<LINENUM>',1,1)+9,instr(LINE_DETAILS39,'</LINENUM>',1,1)-(instr(LINE_DETAILS39,'<LINENUM>',1,1)+9)) LINE_NUM39,
(substr(LINE_DETAILS39,instr(LINE_DETAILS39,'<VALUE>',1,1)+7,instr(LINE_DETAILS39,'</VALUE>',1,1)-(instr(LINE_DETAILS39,'<VALUE>',1,1)+7)))*.01 LINE39_AMOUNT,
substr(LINE_DETAILS39,instr(LINE_DETAILS39,'<ITEM>',1,1)+6,instr(LINE_DETAILS39,'</ITEM>',1,1)-(instr(LINE_DETAILS39,'<ITEM>',1,1)+6)) ITEM_NAME_LINE39,
substr(LINE_DETAILS39,instr(LINE_DETAILS39,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS39,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS39,'<DESCRIPTN>',1,1)+11)) ITEM_Description39,
(substr(TAX_DETAILS39,instr(TAX_DETAILS39,'<VALUE>',1,1)+7,instr(TAX_DETAILS39,'</VALUE>',1,1)-(instr(TAX_DETAILS39,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE39,
substr(TAX_DETAILS39,instr(TAX_DETAILS39,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS39,'</TAXCODE>',1,1)-(instr(TAX_DETAILS39,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE39,
substr(LINE_DETAILS39,instr(LINE_DETAILS39,'<VALUE>',1,3)+7,instr(LINE_DETAILS39,'</VALUE>',1,3)-(instr(LINE_DETAILS39,'<VALUE>',1,3)+7)) quantity_line39,
(substr(LINE_DETAILS39,instr(LINE_DETAILS39,'<VALUE>',1,2)+7,instr(LINE_DETAILS39,'</VALUE>',1,2)-(instr(LINE_DETAILS39,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line39,
(substr(TAX_DETAILS39,instr(TAX_DETAILS39,'<VALUE>',1,3)+7,instr(TAX_DETAILS39,'</VALUE>',1,3)-(instr(TAX_DETAILS39,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE39,
substr(tax_details39,instr(tax_details39,'<DESCRIPTN>',1,1)+11,instr(tax_details39,'</DESCRIPTN>',1,1)-(instr(tax_details39,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION39,
substr(LINE_DETAILS40,instr(LINE_DETAILS40,'<LINENUM>',1,1)+9,instr(LINE_DETAILS40,'</LINENUM>',1,1)-(instr(LINE_DETAILS40,'<LINENUM>',1,1)+9)) LINE_NUM40,
(substr(LINE_DETAILS40,instr(LINE_DETAILS40,'<VALUE>',1,1)+7,instr(LINE_DETAILS40,'</VALUE>',1,1)-(instr(LINE_DETAILS40,'<VALUE>',1,1)+7)))*.01 LINE40_AMOUNT,
substr(LINE_DETAILS40,instr(LINE_DETAILS40,'<ITEM>',1,1)+6,instr(LINE_DETAILS40,'</ITEM>',1,1)-(instr(LINE_DETAILS40,'<ITEM>',1,1)+6)) ITEM_NAME_LINE40,
substr(LINE_DETAILS40,instr(LINE_DETAILS40,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS40,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS40,'<DESCRIPTN>',1,1)+11)) ITEM_Description40,
(substr(TAX_DETAILS40,instr(TAX_DETAILS40,'<VALUE>',1,1)+7,instr(TAX_DETAILS40,'</VALUE>',1,1)-(instr(TAX_DETAILS40,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE40,
substr(TAX_DETAILS40,instr(TAX_DETAILS40,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS40,'</TAXCODE>',1,1)-(instr(TAX_DETAILS40,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE40,
substr(LINE_DETAILS40,instr(LINE_DETAILS40,'<VALUE>',1,3)+7,instr(LINE_DETAILS40,'</VALUE>',1,3)-(instr(LINE_DETAILS40,'<VALUE>',1,3)+7)) quantity_line40,
(substr(LINE_DETAILS40,instr(LINE_DETAILS40,'<VALUE>',1,2)+7,instr(LINE_DETAILS40,'</VALUE>',1,2)-(instr(LINE_DETAILS40,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line40,
(substr(TAX_DETAILS40,instr(TAX_DETAILS40,'<VALUE>',1,3)+7,instr(TAX_DETAILS40,'</VALUE>',1,3)-(instr(TAX_DETAILS40,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE40,
substr(tax_details40,instr(tax_details40,'<DESCRIPTN>',1,1)+11,instr(tax_details40,'</DESCRIPTN>',1,1)-(instr(tax_details40,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION40,
substr(LINE_DETAILS41,instr(LINE_DETAILS41,'<LINENUM>',1,1)+9,instr(LINE_DETAILS41,'</LINENUM>',1,1)-(instr(LINE_DETAILS41,'<LINENUM>',1,1)+9)) LINE_NUM41,
(substr(LINE_DETAILS41,instr(LINE_DETAILS41,'<VALUE>',1,1)+7,instr(LINE_DETAILS41,'</VALUE>',1,1)-(instr(LINE_DETAILS41,'<VALUE>',1,1)+7)))*.01 LINE41_AMOUNT,
substr(LINE_DETAILS41,instr(LINE_DETAILS41,'<ITEM>',1,1)+6,instr(LINE_DETAILS41,'</ITEM>',1,1)-(instr(LINE_DETAILS41,'<ITEM>',1,1)+6)) ITEM_NAME_LINE41,
substr(LINE_DETAILS41,instr(LINE_DETAILS41,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS41,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS41,'<DESCRIPTN>',1,1)+11)) ITEM_Description41,
(substr(TAX_DETAILS41,instr(TAX_DETAILS41,'<VALUE>',1,1)+7,instr(TAX_DETAILS41,'</VALUE>',1,1)-(instr(TAX_DETAILS41,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE41,
substr(TAX_DETAILS41,instr(TAX_DETAILS41,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS41,'</TAXCODE>',1,1)-(instr(TAX_DETAILS41,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE41,
substr(LINE_DETAILS41,instr(LINE_DETAILS41,'<VALUE>',1,3)+7,instr(LINE_DETAILS41,'</VALUE>',1,3)-(instr(LINE_DETAILS41,'<VALUE>',1,3)+7)) quantity_line41,
(substr(LINE_DETAILS41,instr(LINE_DETAILS41,'<VALUE>',1,2)+7,instr(LINE_DETAILS41,'</VALUE>',1,2)-(instr(LINE_DETAILS41,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line41,
(substr(TAX_DETAILS41,instr(TAX_DETAILS41,'<VALUE>',1,3)+7,instr(TAX_DETAILS41,'</VALUE>',1,3)-(instr(TAX_DETAILS41,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE41,
substr(tax_details41,instr(tax_details41,'<DESCRIPTN>',1,1)+11,instr(tax_details41,'</DESCRIPTN>',1,1)-(instr(tax_details41,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION41,
substr(LINE_DETAILS42,instr(LINE_DETAILS42,'<LINENUM>',1,1)+9,instr(LINE_DETAILS42,'</LINENUM>',1,1)-(instr(LINE_DETAILS42,'<LINENUM>',1,1)+9)) LINE_NUM42,
(substr(LINE_DETAILS42,instr(LINE_DETAILS42,'<VALUE>',1,1)+7,instr(LINE_DETAILS42,'</VALUE>',1,1)-(instr(LINE_DETAILS42,'<VALUE>',1,1)+7)))*.01 LINE42_AMOUNT,
substr(LINE_DETAILS42,instr(LINE_DETAILS42,'<ITEM>',1,1)+6,instr(LINE_DETAILS42,'</ITEM>',1,1)-(instr(LINE_DETAILS42,'<ITEM>',1,1)+6)) ITEM_NAME_LINE42,
substr(LINE_DETAILS42,instr(LINE_DETAILS42,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS42,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS42,'<DESCRIPTN>',1,1)+11)) ITEM_Description42,
(substr(TAX_DETAILS42,instr(TAX_DETAILS42,'<VALUE>',1,1)+7,instr(TAX_DETAILS42,'</VALUE>',1,1)-(instr(TAX_DETAILS42,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE42,
substr(TAX_DETAILS42,instr(TAX_DETAILS42,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS42,'</TAXCODE>',1,1)-(instr(TAX_DETAILS42,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE42,
substr(LINE_DETAILS42,instr(LINE_DETAILS42,'<VALUE>',1,3)+7,instr(LINE_DETAILS42,'</VALUE>',1,3)-(instr(LINE_DETAILS42,'<VALUE>',1,3)+7)) quantity_line42,
(substr(LINE_DETAILS42,instr(LINE_DETAILS42,'<VALUE>',1,2)+7,instr(LINE_DETAILS42,'</VALUE>',1,2)-(instr(LINE_DETAILS42,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line42,
(substr(TAX_DETAILS42,instr(TAX_DETAILS42,'<VALUE>',1,3)+7,instr(TAX_DETAILS42,'</VALUE>',1,3)-(instr(TAX_DETAILS42,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE42,
substr(tax_details42,instr(tax_details42,'<DESCRIPTN>',1,1)+11,instr(tax_details42,'</DESCRIPTN>',1,1)-(instr(tax_details42,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION42,
substr(LINE_DETAILS43,instr(LINE_DETAILS43,'<LINENUM>',1,1)+9,instr(LINE_DETAILS43,'</LINENUM>',1,1)-(instr(LINE_DETAILS43,'<LINENUM>',1,1)+9)) LINE_NUM43,
(substr(LINE_DETAILS43,instr(LINE_DETAILS43,'<VALUE>',1,1)+7,instr(LINE_DETAILS43,'</VALUE>',1,1)-(instr(LINE_DETAILS43,'<VALUE>',1,1)+7)))*.01 LINE43_AMOUNT,
substr(LINE_DETAILS43,instr(LINE_DETAILS43,'<ITEM>',1,1)+6,instr(LINE_DETAILS43,'</ITEM>',1,1)-(instr(LINE_DETAILS43,'<ITEM>',1,1)+6)) ITEM_NAME_LINE43,
substr(LINE_DETAILS43,instr(LINE_DETAILS43,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS43,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS43,'<DESCRIPTN>',1,1)+11)) ITEM_Description43,
(substr(TAX_DETAILS43,instr(TAX_DETAILS43,'<VALUE>',1,1)+7,instr(TAX_DETAILS43,'</VALUE>',1,1)-(instr(TAX_DETAILS43,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE43,
substr(TAX_DETAILS43,instr(TAX_DETAILS43,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS43,'</TAXCODE>',1,1)-(instr(TAX_DETAILS43,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE43,
substr(LINE_DETAILS43,instr(LINE_DETAILS43,'<VALUE>',1,3)+7,instr(LINE_DETAILS43,'</VALUE>',1,3)-(instr(LINE_DETAILS43,'<VALUE>',1,3)+7)) quantity_line43,
(substr(LINE_DETAILS43,instr(LINE_DETAILS43,'<VALUE>',1,2)+7,instr(LINE_DETAILS43,'</VALUE>',1,2)-(instr(LINE_DETAILS43,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line43,
(substr(TAX_DETAILS43,instr(TAX_DETAILS43,'<VALUE>',1,3)+7,instr(TAX_DETAILS43,'</VALUE>',1,3)-(instr(TAX_DETAILS43,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE43,
substr(tax_details43,instr(tax_details43,'<DESCRIPTN>',1,1)+11,instr(tax_details43,'</DESCRIPTN>',1,1)-(instr(tax_details43,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION43,
substr(LINE_DETAILS44,instr(LINE_DETAILS44,'<LINENUM>',1,1)+9,instr(LINE_DETAILS44,'</LINENUM>',1,1)-(instr(LINE_DETAILS44,'<LINENUM>',1,1)+9)) LINE_NUM44,
(substr(LINE_DETAILS44,instr(LINE_DETAILS44,'<VALUE>',1,1)+7,instr(LINE_DETAILS44,'</VALUE>',1,1)-(instr(LINE_DETAILS44,'<VALUE>',1,1)+7)))*.01 LINE44_AMOUNT,
substr(LINE_DETAILS44,instr(LINE_DETAILS44,'<ITEM>',1,1)+6,instr(LINE_DETAILS44,'</ITEM>',1,1)-(instr(LINE_DETAILS44,'<ITEM>',1,1)+6)) ITEM_NAME_LINE44,
substr(LINE_DETAILS44,instr(LINE_DETAILS44,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS44,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS44,'<DESCRIPTN>',1,1)+11)) ITEM_Description44,
(substr(TAX_DETAILS44,instr(TAX_DETAILS44,'<VALUE>',1,1)+7,instr(TAX_DETAILS44,'</VALUE>',1,1)-(instr(TAX_DETAILS44,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE44,
substr(TAX_DETAILS44,instr(TAX_DETAILS44,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS44,'</TAXCODE>',1,1)-(instr(TAX_DETAILS44,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE44,
substr(LINE_DETAILS44,instr(LINE_DETAILS44,'<VALUE>',1,3)+7,instr(LINE_DETAILS44,'</VALUE>',1,3)-(instr(LINE_DETAILS44,'<VALUE>',1,3)+7)) quantity_line44,
(substr(LINE_DETAILS44,instr(LINE_DETAILS44,'<VALUE>',1,2)+7,instr(LINE_DETAILS44,'</VALUE>',1,2)-(instr(LINE_DETAILS44,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line44,
(substr(TAX_DETAILS44,instr(TAX_DETAILS44,'<VALUE>',1,3)+7,instr(TAX_DETAILS44,'</VALUE>',1,3)-(instr(TAX_DETAILS44,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE44,
substr(tax_details44,instr(tax_details44,'<DESCRIPTN>',1,1)+11,instr(tax_details44,'</DESCRIPTN>',1,1)-(instr(tax_details44,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION44,
substr(LINE_DETAILS45,instr(LINE_DETAILS45,'<LINENUM>',1,1)+9,instr(LINE_DETAILS45,'</LINENUM>',1,1)-(instr(LINE_DETAILS45,'<LINENUM>',1,1)+9)) LINE_NUM45,
(substr(LINE_DETAILS45,instr(LINE_DETAILS45,'<VALUE>',1,1)+7,instr(LINE_DETAILS45,'</VALUE>',1,1)-(instr(LINE_DETAILS45,'<VALUE>',1,1)+7)))*.01 LINE45_AMOUNT,
substr(LINE_DETAILS45,instr(LINE_DETAILS45,'<ITEM>',1,1)+6,instr(LINE_DETAILS45,'</ITEM>',1,1)-(instr(LINE_DETAILS45,'<ITEM>',1,1)+6)) ITEM_NAME_LINE45,
substr(LINE_DETAILS45,instr(LINE_DETAILS45,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS45,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS45,'<DESCRIPTN>',1,1)+11)) ITEM_Description45,
(substr(TAX_DETAILS45,instr(TAX_DETAILS45,'<VALUE>',1,1)+7,instr(TAX_DETAILS45,'</VALUE>',1,1)-(instr(TAX_DETAILS45,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE45,
substr(TAX_DETAILS45,instr(TAX_DETAILS45,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS45,'</TAXCODE>',1,1)-(instr(TAX_DETAILS45,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE45,
substr(LINE_DETAILS45,instr(LINE_DETAILS45,'<VALUE>',1,3)+7,instr(LINE_DETAILS45,'</VALUE>',1,3)-(instr(LINE_DETAILS45,'<VALUE>',1,3)+7)) quantity_line45,
(substr(LINE_DETAILS45,instr(LINE_DETAILS45,'<VALUE>',1,2)+7,instr(LINE_DETAILS45,'</VALUE>',1,2)-(instr(LINE_DETAILS45,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line45,
(substr(TAX_DETAILS45,instr(TAX_DETAILS45,'<VALUE>',1,3)+7,instr(TAX_DETAILS45,'</VALUE>',1,3)-(instr(TAX_DETAILS45,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE45,
substr(tax_details45,instr(tax_details45,'<DESCRIPTN>',1,1)+11,instr(tax_details45,'</DESCRIPTN>',1,1)-(instr(tax_details45,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION45,
substr(LINE_DETAILS46,instr(LINE_DETAILS46,'<LINENUM>',1,1)+9,instr(LINE_DETAILS46,'</LINENUM>',1,1)-(instr(LINE_DETAILS46,'<LINENUM>',1,1)+9)) LINE_NUM46,
(substr(LINE_DETAILS46,instr(LINE_DETAILS46,'<VALUE>',1,1)+7,instr(LINE_DETAILS46,'</VALUE>',1,1)-(instr(LINE_DETAILS46,'<VALUE>',1,1)+7)))*.01 LINE46_AMOUNT,
substr(LINE_DETAILS46,instr(LINE_DETAILS46,'<ITEM>',1,1)+6,instr(LINE_DETAILS46,'</ITEM>',1,1)-(instr(LINE_DETAILS46,'<ITEM>',1,1)+6)) ITEM_NAME_LINE46,
substr(LINE_DETAILS46,instr(LINE_DETAILS46,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS46,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS46,'<DESCRIPTN>',1,1)+11)) ITEM_Description46,
(substr(TAX_DETAILS46,instr(TAX_DETAILS46,'<VALUE>',1,1)+7,instr(TAX_DETAILS46,'</VALUE>',1,1)-(instr(TAX_DETAILS46,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE46,
substr(TAX_DETAILS46,instr(TAX_DETAILS46,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS46,'</TAXCODE>',1,1)-(instr(TAX_DETAILS46,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE46,
substr(LINE_DETAILS46,instr(LINE_DETAILS46,'<VALUE>',1,3)+7,instr(LINE_DETAILS46,'</VALUE>',1,3)-(instr(LINE_DETAILS46,'<VALUE>',1,3)+7)) quantity_line46,
(substr(LINE_DETAILS46,instr(LINE_DETAILS46,'<VALUE>',1,2)+7,instr(LINE_DETAILS46,'</VALUE>',1,2)-(instr(LINE_DETAILS46,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line46,
(substr(TAX_DETAILS46,instr(TAX_DETAILS46,'<VALUE>',1,3)+7,instr(TAX_DETAILS46,'</VALUE>',1,3)-(instr(TAX_DETAILS46,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE46,
substr(tax_details46,instr(tax_details46,'<DESCRIPTN>',1,1)+11,instr(tax_details46,'</DESCRIPTN>',1,1)-(instr(tax_details46,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION46,
substr(LINE_DETAILS47,instr(LINE_DETAILS47,'<LINENUM>',1,1)+9,instr(LINE_DETAILS47,'</LINENUM>',1,1)-(instr(LINE_DETAILS47,'<LINENUM>',1,1)+9)) LINE_NUM47,
(substr(LINE_DETAILS47,instr(LINE_DETAILS47,'<VALUE>',1,1)+7,instr(LINE_DETAILS47,'</VALUE>',1,1)-(instr(LINE_DETAILS47,'<VALUE>',1,1)+7)))*.01 LINE47_AMOUNT,
substr(LINE_DETAILS47,instr(LINE_DETAILS47,'<ITEM>',1,1)+6,instr(LINE_DETAILS47,'</ITEM>',1,1)-(instr(LINE_DETAILS47,'<ITEM>',1,1)+6)) ITEM_NAME_LINE47,
substr(LINE_DETAILS47,instr(LINE_DETAILS47,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS47,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS47,'<DESCRIPTN>',1,1)+11)) ITEM_Description47,
(substr(TAX_DETAILS47,instr(TAX_DETAILS47,'<VALUE>',1,1)+7,instr(TAX_DETAILS47,'</VALUE>',1,1)-(instr(TAX_DETAILS47,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE47,
substr(TAX_DETAILS47,instr(TAX_DETAILS47,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS47,'</TAXCODE>',1,1)-(instr(TAX_DETAILS47,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE47,
substr(LINE_DETAILS47,instr(LINE_DETAILS47,'<VALUE>',1,3)+7,instr(LINE_DETAILS47,'</VALUE>',1,3)-(instr(LINE_DETAILS47,'<VALUE>',1,3)+7)) quantity_line47,
(substr(LINE_DETAILS47,instr(LINE_DETAILS47,'<VALUE>',1,2)+7,instr(LINE_DETAILS47,'</VALUE>',1,2)-(instr(LINE_DETAILS47,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line47,
(substr(TAX_DETAILS47,instr(TAX_DETAILS47,'<VALUE>',1,3)+7,instr(TAX_DETAILS47,'</VALUE>',1,3)-(instr(TAX_DETAILS47,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE47,
substr(tax_details47,instr(tax_details47,'<DESCRIPTN>',1,1)+11,instr(tax_details47,'</DESCRIPTN>',1,1)-(instr(tax_details47,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION47,
substr(LINE_DETAILS48,instr(LINE_DETAILS48,'<LINENUM>',1,1)+9,instr(LINE_DETAILS48,'</LINENUM>',1,1)-(instr(LINE_DETAILS48,'<LINENUM>',1,1)+9)) LINE_NUM48,
(substr(LINE_DETAILS48,instr(LINE_DETAILS48,'<VALUE>',1,1)+7,instr(LINE_DETAILS48,'</VALUE>',1,1)-(instr(LINE_DETAILS48,'<VALUE>',1,1)+7)))*.01 LINE48_AMOUNT,
substr(LINE_DETAILS48,instr(LINE_DETAILS48,'<ITEM>',1,1)+6,instr(LINE_DETAILS48,'</ITEM>',1,1)-(instr(LINE_DETAILS48,'<ITEM>',1,1)+6)) ITEM_NAME_LINE48,
substr(LINE_DETAILS48,instr(LINE_DETAILS48,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS48,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS48,'<DESCRIPTN>',1,1)+11)) ITEM_Description48,
(substr(TAX_DETAILS48,instr(TAX_DETAILS48,'<VALUE>',1,1)+7,instr(TAX_DETAILS48,'</VALUE>',1,1)-(instr(TAX_DETAILS48,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE48,
substr(TAX_DETAILS48,instr(TAX_DETAILS48,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS48,'</TAXCODE>',1,1)-(instr(TAX_DETAILS48,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE48,
substr(LINE_DETAILS48,instr(LINE_DETAILS48,'<VALUE>',1,3)+7,instr(LINE_DETAILS48,'</VALUE>',1,3)-(instr(LINE_DETAILS48,'<VALUE>',1,3)+7)) quantity_line48,
(substr(LINE_DETAILS48,instr(LINE_DETAILS48,'<VALUE>',1,2)+7,instr(LINE_DETAILS48,'</VALUE>',1,2)-(instr(LINE_DETAILS48,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line48,
(substr(TAX_DETAILS48,instr(TAX_DETAILS48,'<VALUE>',1,3)+7,instr(TAX_DETAILS48,'</VALUE>',1,3)-(instr(TAX_DETAILS48,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE48,
substr(tax_details48,instr(tax_details48,'<DESCRIPTN>',1,1)+11,instr(tax_details48,'</DESCRIPTN>',1,1)-(instr(tax_details48,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION48,
substr(LINE_DETAILS49,instr(LINE_DETAILS49,'<LINENUM>',1,1)+9,instr(LINE_DETAILS49,'</LINENUM>',1,1)-(instr(LINE_DETAILS49,'<LINENUM>',1,1)+9)) LINE_NUM49,
(substr(LINE_DETAILS49,instr(LINE_DETAILS49,'<VALUE>',1,1)+7,instr(LINE_DETAILS49,'</VALUE>',1,1)-(instr(LINE_DETAILS49,'<VALUE>',1,1)+7)))*.01 LINE49_AMOUNT,
substr(LINE_DETAILS49,instr(LINE_DETAILS49,'<ITEM>',1,1)+6,instr(LINE_DETAILS49,'</ITEM>',1,1)-(instr(LINE_DETAILS49,'<ITEM>',1,1)+6)) ITEM_NAME_LINE49,
substr(LINE_DETAILS49,instr(LINE_DETAILS49,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS49,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS49,'<DESCRIPTN>',1,1)+11)) ITEM_Description49,
(substr(TAX_DETAILS49,instr(TAX_DETAILS49,'<VALUE>',1,1)+7,instr(TAX_DETAILS49,'</VALUE>',1,1)-(instr(TAX_DETAILS49,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE49,
substr(TAX_DETAILS49,instr(TAX_DETAILS49,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS49,'</TAXCODE>',1,1)-(instr(TAX_DETAILS49,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE49,
substr(LINE_DETAILS49,instr(LINE_DETAILS49,'<VALUE>',1,3)+7,instr(LINE_DETAILS49,'</VALUE>',1,3)-(instr(LINE_DETAILS49,'<VALUE>',1,3)+7)) quantity_line49,
(substr(LINE_DETAILS49,instr(LINE_DETAILS49,'<VALUE>',1,2)+7,instr(LINE_DETAILS49,'</VALUE>',1,2)-(instr(LINE_DETAILS49,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line49,
(substr(TAX_DETAILS49,instr(TAX_DETAILS49,'<VALUE>',1,3)+7,instr(TAX_DETAILS49,'</VALUE>',1,3)-(instr(TAX_DETAILS49,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE49,
substr(tax_details49,instr(tax_details49,'<DESCRIPTN>',1,1)+11,instr(tax_details49,'</DESCRIPTN>',1,1)-(instr(tax_details49,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION49,
substr(LINE_DETAILS50,instr(LINE_DETAILS50,'<LINENUM>',1,1)+9,instr(LINE_DETAILS50,'</LINENUM>',1,1)-(instr(LINE_DETAILS50,'<LINENUM>',1,1)+9)) LINE_NUM50,
(substr(LINE_DETAILS50,instr(LINE_DETAILS50,'<VALUE>',1,1)+7,instr(LINE_DETAILS50,'</VALUE>',1,1)-(instr(LINE_DETAILS50,'<VALUE>',1,1)+7)))*.01 LINE50_AMOUNT,
substr(LINE_DETAILS50,instr(LINE_DETAILS50,'<ITEM>',1,1)+6,instr(LINE_DETAILS50,'</ITEM>',1,1)-(instr(LINE_DETAILS50,'<ITEM>',1,1)+6)) ITEM_NAME_LINE50,
substr(LINE_DETAILS50,instr(LINE_DETAILS50,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS50,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS50,'<DESCRIPTN>',1,1)+11)) ITEM_Description50,
(substr(TAX_DETAILS50,instr(TAX_DETAILS50,'<VALUE>',1,1)+7,instr(TAX_DETAILS50,'</VALUE>',1,1)-(instr(TAX_DETAILS50,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE50,
substr(TAX_DETAILS50,instr(TAX_DETAILS50,'<TAXCODE>',1,1)+9,instr(TAX_DETAILS50,'</TAXCODE>',1,1)-(instr(TAX_DETAILS50,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE50,
substr(LINE_DETAILS50,instr(LINE_DETAILS50,'<VALUE>',1,3)+7,instr(LINE_DETAILS50,'</VALUE>',1,3)-(instr(LINE_DETAILS50,'<VALUE>',1,3)+7)) quantity_line50,
(substr(LINE_DETAILS50,instr(LINE_DETAILS50,'<VALUE>',1,2)+7,instr(LINE_DETAILS50,'</VALUE>',1,2)-(instr(LINE_DETAILS50,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line50,
(substr(TAX_DETAILS50,instr(TAX_DETAILS50,'<VALUE>',1,3)+7,instr(TAX_DETAILS50,'</VALUE>',1,3)-(instr(TAX_DETAILS50,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE50,
substr(tax_details50,instr(tax_details50,'<DESCRIPTN>',1,1)+11,instr(tax_details50,'</DESCRIPTN>',1,1)-(instr(tax_details50,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION50,
substr(LINE_DETAILS51,instr(LINE_DETAILS51,'<LINENUM>',1,1)+9,instr(LINE_DETAILS51,'</LINENUM>',1,1)-(instr(LINE_DETAILS51,'<LINENUM>',1,1)+9)) LINE_NUM51,
(substr(LINE_DETAILS51,instr(LINE_DETAILS51,'<VALUE>',1,1)+7,instr(LINE_DETAILS51,'</VALUE>',1,1)-(instr(LINE_DETAILS51,'<VALUE>',1,1)+7)))*.01 LINE51_AMOUNT,
substr(LINE_DETAILS51,instr(LINE_DETAILS51,'<ITEM>',1,1)+6,instr(LINE_DETAILS51,'</ITEM>',1,1)-(instr(LINE_DETAILS51,'<ITEM>',1,1)+6)) ITEM_NAME_LINE51,
substr(LINE_DETAILS51,instr(LINE_DETAILS51,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS51,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS51,'<DESCRIPTN>',1,1)+11)) ITEM_Description51,
(substr(tax_details51,instr(tax_details51,'<VALUE>',1,1)+7,instr(tax_details51,'</VALUE>',1,1)-(instr(tax_details51,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE51,
substr(tax_details51,instr(tax_details51,'<TAXCODE>',1,1)+9,instr(tax_details51,'</TAXCODE>',1,1)-(instr(tax_details51,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE51,
substr(LINE_DETAILS51,instr(LINE_DETAILS51,'<VALUE>',1,3)+7,instr(LINE_DETAILS51,'</VALUE>',1,3)-(instr(LINE_DETAILS51,'<VALUE>',1,3)+7)) quantity_line51,
(substr(LINE_DETAILS51,instr(LINE_DETAILS51,'<VALUE>',1,2)+7,instr(LINE_DETAILS51,'</VALUE>',1,2)-(instr(LINE_DETAILS51,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line51,
(substr(tax_details51,instr(tax_details51,'<VALUE>',1,3)+7,instr(tax_details51,'</VALUE>',1,3)-(instr(tax_details51,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE51,
substr(tax_details51,instr(tax_details51,'<DESCRIPTN>',1,1)+11,instr(tax_details51,'</DESCRIPTN>',1,1)-(instr(tax_details51,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION51,
substr(LINE_DETAILS52,instr(LINE_DETAILS52,'<LINENUM>',1,1)+9,instr(LINE_DETAILS52,'</LINENUM>',1,1)-(instr(LINE_DETAILS52,'<LINENUM>',1,1)+9)) LINE_NUM52,
(substr(LINE_DETAILS52,instr(LINE_DETAILS52,'<VALUE>',1,1)+7,instr(LINE_DETAILS52,'</VALUE>',1,1)-(instr(LINE_DETAILS52,'<VALUE>',1,1)+7)))*.01 LINE52_AMOUNT,
substr(LINE_DETAILS52,instr(LINE_DETAILS52,'<ITEM>',1,1)+6,instr(LINE_DETAILS52,'</ITEM>',1,1)-(instr(LINE_DETAILS52,'<ITEM>',1,1)+6)) ITEM_NAME_LINE52,
substr(LINE_DETAILS52,instr(LINE_DETAILS52,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS52,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS52,'<DESCRIPTN>',1,1)+11)) ITEM_Description52,
(substr(tax_details52,instr(tax_details52,'<VALUE>',1,1)+7,instr(tax_details52,'</VALUE>',1,1)-(instr(tax_details52,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE52,
substr(tax_details52,instr(tax_details52,'<TAXCODE>',1,1)+9,instr(tax_details52,'</TAXCODE>',1,1)-(instr(tax_details52,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE52,
substr(LINE_DETAILS52,instr(LINE_DETAILS52,'<VALUE>',1,3)+7,instr(LINE_DETAILS52,'</VALUE>',1,3)-(instr(LINE_DETAILS52,'<VALUE>',1,3)+7)) quantity_line52,
(substr(LINE_DETAILS52,instr(LINE_DETAILS52,'<VALUE>',1,2)+7,instr(LINE_DETAILS52,'</VALUE>',1,2)-(instr(LINE_DETAILS52,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line52,
(substr(tax_details52,instr(tax_details52,'<VALUE>',1,3)+7,instr(tax_details52,'</VALUE>',1,3)-(instr(tax_details52,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE52,
substr(tax_details52,instr(tax_details52,'<DESCRIPTN>',1,1)+11,instr(tax_details52,'</DESCRIPTN>',1,1)-(instr(tax_details52,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION52,
substr(LINE_DETAILS53,instr(LINE_DETAILS53,'<LINENUM>',1,1)+9,instr(LINE_DETAILS53,'</LINENUM>',1,1)-(instr(LINE_DETAILS53,'<LINENUM>',1,1)+9)) LINE_NUM53,
(substr(LINE_DETAILS53,instr(LINE_DETAILS53,'<VALUE>',1,1)+7,instr(LINE_DETAILS53,'</VALUE>',1,1)-(instr(LINE_DETAILS53,'<VALUE>',1,1)+7)))*.01 LINE53_AMOUNT,
substr(LINE_DETAILS53,instr(LINE_DETAILS53,'<ITEM>',1,1)+6,instr(LINE_DETAILS53,'</ITEM>',1,1)-(instr(LINE_DETAILS53,'<ITEM>',1,1)+6)) ITEM_NAME_LINE53,
substr(LINE_DETAILS53,instr(LINE_DETAILS53,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS53,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS53,'<DESCRIPTN>',1,1)+11)) ITEM_Description53,
(substr(tax_details53,instr(tax_details53,'<VALUE>',1,1)+7,instr(tax_details53,'</VALUE>',1,1)-(instr(tax_details53,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE53,
substr(tax_details53,instr(tax_details53,'<TAXCODE>',1,1)+9,instr(tax_details53,'</TAXCODE>',1,1)-(instr(tax_details53,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE53,
substr(LINE_DETAILS53,instr(LINE_DETAILS53,'<VALUE>',1,3)+7,instr(LINE_DETAILS53,'</VALUE>',1,3)-(instr(LINE_DETAILS53,'<VALUE>',1,3)+7)) quantity_line53,
(substr(LINE_DETAILS53,instr(LINE_DETAILS53,'<VALUE>',1,2)+7,instr(LINE_DETAILS53,'</VALUE>',1,2)-(instr(LINE_DETAILS53,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line53,
(substr(tax_details53,instr(tax_details53,'<VALUE>',1,3)+7,instr(tax_details53,'</VALUE>',1,3)-(instr(tax_details53,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE53,
substr(tax_details53,instr(tax_details53,'<DESCRIPTN>',1,1)+11,instr(tax_details53,'</DESCRIPTN>',1,1)-(instr(tax_details53,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION53,
substr(LINE_DETAILS54,instr(LINE_DETAILS54,'<LINENUM>',1,1)+9,instr(LINE_DETAILS54,'</LINENUM>',1,1)-(instr(LINE_DETAILS54,'<LINENUM>',1,1)+9)) LINE_NUM54,
(substr(LINE_DETAILS54,instr(LINE_DETAILS54,'<VALUE>',1,1)+7,instr(LINE_DETAILS54,'</VALUE>',1,1)-(instr(LINE_DETAILS54,'<VALUE>',1,1)+7)))*.01 LINE54_AMOUNT,
substr(LINE_DETAILS54,instr(LINE_DETAILS54,'<ITEM>',1,1)+6,instr(LINE_DETAILS54,'</ITEM>',1,1)-(instr(LINE_DETAILS54,'<ITEM>',1,1)+6)) ITEM_NAME_LINE54,
substr(LINE_DETAILS54,instr(LINE_DETAILS54,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS54,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS54,'<DESCRIPTN>',1,1)+11)) ITEM_Description54,
(substr(tax_details54,instr(tax_details54,'<VALUE>',1,1)+7,instr(tax_details54,'</VALUE>',1,1)-(instr(tax_details54,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE54,
substr(tax_details54,instr(tax_details54,'<TAXCODE>',1,1)+9,instr(tax_details54,'</TAXCODE>',1,1)-(instr(tax_details54,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE54,
substr(LINE_DETAILS54,instr(LINE_DETAILS54,'<VALUE>',1,3)+7,instr(LINE_DETAILS54,'</VALUE>',1,3)-(instr(LINE_DETAILS54,'<VALUE>',1,3)+7)) quantity_line54,
(substr(LINE_DETAILS54,instr(LINE_DETAILS54,'<VALUE>',1,2)+7,instr(LINE_DETAILS54,'</VALUE>',1,2)-(instr(LINE_DETAILS54,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line54,
(substr(tax_details54,instr(tax_details54,'<VALUE>',1,3)+7,instr(tax_details54,'</VALUE>',1,3)-(instr(tax_details54,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE54,
substr(tax_details54,instr(tax_details54,'<DESCRIPTN>',1,1)+11,instr(tax_details54,'</DESCRIPTN>',1,1)-(instr(tax_details54,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION54,
substr(LINE_DETAILS55,instr(LINE_DETAILS55,'<LINENUM>',1,1)+9,instr(LINE_DETAILS55,'</LINENUM>',1,1)-(instr(LINE_DETAILS55,'<LINENUM>',1,1)+9)) LINE_NUM55,
(substr(LINE_DETAILS55,instr(LINE_DETAILS55,'<VALUE>',1,1)+7,instr(LINE_DETAILS55,'</VALUE>',1,1)-(instr(LINE_DETAILS55,'<VALUE>',1,1)+7)))*.01 LINE55_AMOUNT,
substr(LINE_DETAILS55,instr(LINE_DETAILS55,'<ITEM>',1,1)+6,instr(LINE_DETAILS55,'</ITEM>',1,1)-(instr(LINE_DETAILS55,'<ITEM>',1,1)+6)) ITEM_NAME_LINE55,
substr(LINE_DETAILS55,instr(LINE_DETAILS55,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS55,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS55,'<DESCRIPTN>',1,1)+11)) ITEM_Description55,
(substr(tax_details55,instr(tax_details55,'<VALUE>',1,1)+7,instr(tax_details55,'</VALUE>',1,1)-(instr(tax_details55,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE55,
substr(tax_details55,instr(tax_details55,'<TAXCODE>',1,1)+9,instr(tax_details55,'</TAXCODE>',1,1)-(instr(tax_details55,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE55,
substr(LINE_DETAILS55,instr(LINE_DETAILS55,'<VALUE>',1,3)+7,instr(LINE_DETAILS55,'</VALUE>',1,3)-(instr(LINE_DETAILS55,'<VALUE>',1,3)+7)) quantity_line55,
(substr(LINE_DETAILS55,instr(LINE_DETAILS55,'<VALUE>',1,2)+7,instr(LINE_DETAILS55,'</VALUE>',1,2)-(instr(LINE_DETAILS55,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line55,
(substr(tax_details55,instr(tax_details55,'<VALUE>',1,3)+7,instr(tax_details55,'</VALUE>',1,3)-(instr(tax_details55,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE55,
substr(tax_details55,instr(tax_details55,'<DESCRIPTN>',1,1)+11,instr(tax_details55,'</DESCRIPTN>',1,1)-(instr(tax_details55,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION55,
substr(LINE_DETAILS56,instr(LINE_DETAILS56,'<LINENUM>',1,1)+9,instr(LINE_DETAILS56,'</LINENUM>',1,1)-(instr(LINE_DETAILS56,'<LINENUM>',1,1)+9)) LINE_NUM56,
(substr(LINE_DETAILS56,instr(LINE_DETAILS56,'<VALUE>',1,1)+7,instr(LINE_DETAILS56,'</VALUE>',1,1)-(instr(LINE_DETAILS56,'<VALUE>',1,1)+7)))*.01 LINE56_AMOUNT,
substr(LINE_DETAILS56,instr(LINE_DETAILS56,'<ITEM>',1,1)+6,instr(LINE_DETAILS56,'</ITEM>',1,1)-(instr(LINE_DETAILS56,'<ITEM>',1,1)+6)) ITEM_NAME_LINE56,
substr(LINE_DETAILS56,instr(LINE_DETAILS56,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS56,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS56,'<DESCRIPTN>',1,1)+11)) ITEM_Description56,
(substr(tax_details56,instr(tax_details56,'<VALUE>',1,1)+7,instr(tax_details56,'</VALUE>',1,1)-(instr(tax_details56,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE56,
substr(tax_details56,instr(tax_details56,'<TAXCODE>',1,1)+9,instr(tax_details56,'</TAXCODE>',1,1)-(instr(tax_details56,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE56,
substr(LINE_DETAILS56,instr(LINE_DETAILS56,'<VALUE>',1,3)+7,instr(LINE_DETAILS56,'</VALUE>',1,3)-(instr(LINE_DETAILS56,'<VALUE>',1,3)+7)) quantity_line56,
(substr(LINE_DETAILS56,instr(LINE_DETAILS56,'<VALUE>',1,2)+7,instr(LINE_DETAILS56,'</VALUE>',1,2)-(instr(LINE_DETAILS56,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line56,
(substr(tax_details56,instr(tax_details56,'<VALUE>',1,3)+7,instr(tax_details56,'</VALUE>',1,3)-(instr(tax_details56,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE56,
substr(tax_details56,instr(tax_details56,'<DESCRIPTN>',1,1)+11,instr(tax_details56,'</DESCRIPTN>',1,1)-(instr(tax_details56,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION56,
substr(LINE_DETAILS57,instr(LINE_DETAILS57,'<LINENUM>',1,1)+9,instr(LINE_DETAILS57,'</LINENUM>',1,1)-(instr(LINE_DETAILS57,'<LINENUM>',1,1)+9)) LINE_NUM57,
(substr(LINE_DETAILS57,instr(LINE_DETAILS57,'<VALUE>',1,1)+7,instr(LINE_DETAILS57,'</VALUE>',1,1)-(instr(LINE_DETAILS57,'<VALUE>',1,1)+7)))*.01 LINE57_AMOUNT,
substr(LINE_DETAILS57,instr(LINE_DETAILS57,'<ITEM>',1,1)+6,instr(LINE_DETAILS57,'</ITEM>',1,1)-(instr(LINE_DETAILS57,'<ITEM>',1,1)+6)) ITEM_NAME_LINE57,
substr(LINE_DETAILS57,instr(LINE_DETAILS57,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS57,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS57,'<DESCRIPTN>',1,1)+11)) ITEM_Description57,
(substr(tax_details57,instr(tax_details57,'<VALUE>',1,1)+7,instr(tax_details57,'</VALUE>',1,1)-(instr(tax_details57,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE57,
substr(tax_details57,instr(tax_details57,'<TAXCODE>',1,1)+9,instr(tax_details57,'</TAXCODE>',1,1)-(instr(tax_details57,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE57,
substr(LINE_DETAILS57,instr(LINE_DETAILS57,'<VALUE>',1,3)+7,instr(LINE_DETAILS57,'</VALUE>',1,3)-(instr(LINE_DETAILS57,'<VALUE>',1,3)+7)) quantity_line57,
(substr(LINE_DETAILS57,instr(LINE_DETAILS57,'<VALUE>',1,2)+7,instr(LINE_DETAILS57,'</VALUE>',1,2)-(instr(LINE_DETAILS57,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line57,
(substr(tax_details57,instr(tax_details57,'<VALUE>',1,3)+7,instr(tax_details57,'</VALUE>',1,3)-(instr(tax_details57,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE57,
substr(tax_details57,instr(tax_details57,'<DESCRIPTN>',1,1)+11,instr(tax_details57,'</DESCRIPTN>',1,1)-(instr(tax_details57,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION57,
substr(LINE_DETAILS58,instr(LINE_DETAILS58,'<LINENUM>',1,1)+9,instr(LINE_DETAILS58,'</LINENUM>',1,1)-(instr(LINE_DETAILS58,'<LINENUM>',1,1)+9)) LINE_NUM58,
(substr(LINE_DETAILS58,instr(LINE_DETAILS58,'<VALUE>',1,1)+7,instr(LINE_DETAILS58,'</VALUE>',1,1)-(instr(LINE_DETAILS58,'<VALUE>',1,1)+7)))*.01 LINE58_AMOUNT,
substr(LINE_DETAILS58,instr(LINE_DETAILS58,'<ITEM>',1,1)+6,instr(LINE_DETAILS58,'</ITEM>',1,1)-(instr(LINE_DETAILS58,'<ITEM>',1,1)+6)) ITEM_NAME_LINE58,
substr(LINE_DETAILS58,instr(LINE_DETAILS58,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS58,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS58,'<DESCRIPTN>',1,1)+11)) ITEM_Description58,
(substr(tax_details58,instr(tax_details58,'<VALUE>',1,1)+7,instr(tax_details58,'</VALUE>',1,1)-(instr(tax_details58,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE58,
substr(tax_details58,instr(tax_details58,'<TAXCODE>',1,1)+9,instr(tax_details58,'</TAXCODE>',1,1)-(instr(tax_details58,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE58,
substr(LINE_DETAILS58,instr(LINE_DETAILS58,'<VALUE>',1,3)+7,instr(LINE_DETAILS58,'</VALUE>',1,3)-(instr(LINE_DETAILS58,'<VALUE>',1,3)+7)) quantity_line58,
(substr(LINE_DETAILS58,instr(LINE_DETAILS58,'<VALUE>',1,2)+7,instr(LINE_DETAILS58,'</VALUE>',1,2)-(instr(LINE_DETAILS58,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line58,
(substr(tax_details58,instr(tax_details58,'<VALUE>',1,3)+7,instr(tax_details58,'</VALUE>',1,3)-(instr(tax_details58,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE58,
substr(tax_details58,instr(tax_details58,'<DESCRIPTN>',1,1)+11,instr(tax_details58,'</DESCRIPTN>',1,1)-(instr(tax_details58,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION58,
substr(LINE_DETAILS59,instr(LINE_DETAILS59,'<LINENUM>',1,1)+9,instr(LINE_DETAILS59,'</LINENUM>',1,1)-(instr(LINE_DETAILS59,'<LINENUM>',1,1)+9)) LINE_NUM59,
(substr(LINE_DETAILS59,instr(LINE_DETAILS59,'<VALUE>',1,1)+7,instr(LINE_DETAILS59,'</VALUE>',1,1)-(instr(LINE_DETAILS59,'<VALUE>',1,1)+7)))*.01 LINE59_AMOUNT,
substr(LINE_DETAILS59,instr(LINE_DETAILS59,'<ITEM>',1,1)+6,instr(LINE_DETAILS59,'</ITEM>',1,1)-(instr(LINE_DETAILS59,'<ITEM>',1,1)+6)) ITEM_NAME_LINE59,
substr(LINE_DETAILS59,instr(LINE_DETAILS59,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS59,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS59,'<DESCRIPTN>',1,1)+11)) ITEM_Description59,
(substr(tax_details59,instr(tax_details59,'<VALUE>',1,1)+7,instr(tax_details59,'</VALUE>',1,1)-(instr(tax_details59,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE59,
substr(tax_details59,instr(tax_details59,'<TAXCODE>',1,1)+9,instr(tax_details59,'</TAXCODE>',1,1)-(instr(tax_details59,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE59,
substr(LINE_DETAILS59,instr(LINE_DETAILS59,'<VALUE>',1,3)+7,instr(LINE_DETAILS59,'</VALUE>',1,3)-(instr(LINE_DETAILS59,'<VALUE>',1,3)+7)) quantity_line59,
(substr(LINE_DETAILS59,instr(LINE_DETAILS59,'<VALUE>',1,2)+7,instr(LINE_DETAILS59,'</VALUE>',1,2)-(instr(LINE_DETAILS59,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line59,
(substr(tax_details59,instr(tax_details59,'<VALUE>',1,3)+7,instr(tax_details59,'</VALUE>',1,3)-(instr(tax_details59,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE59,
substr(tax_details59,instr(tax_details59,'<DESCRIPTN>',1,1)+11,instr(tax_details59,'</DESCRIPTN>',1,1)-(instr(tax_details59,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION59,
substr(LINE_DETAILS60,instr(LINE_DETAILS60,'<LINENUM>',1,1)+9,instr(LINE_DETAILS60,'</LINENUM>',1,1)-(instr(LINE_DETAILS60,'<LINENUM>',1,1)+9)) LINE_NUM60,
(substr(LINE_DETAILS60,instr(LINE_DETAILS60,'<VALUE>',1,1)+7,instr(LINE_DETAILS60,'</VALUE>',1,1)-(instr(LINE_DETAILS60,'<VALUE>',1,1)+7)))*.01 LINE60_AMOUNT,
substr(LINE_DETAILS60,instr(LINE_DETAILS60,'<ITEM>',1,1)+6,instr(LINE_DETAILS60,'</ITEM>',1,1)-(instr(LINE_DETAILS60,'<ITEM>',1,1)+6)) ITEM_NAME_LINE60,
substr(LINE_DETAILS60,instr(LINE_DETAILS60,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS60,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS60,'<DESCRIPTN>',1,1)+11)) ITEM_Description60,
(substr(tax_details60,instr(tax_details60,'<VALUE>',1,1)+7,instr(tax_details60,'</VALUE>',1,1)-(instr(tax_details60,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE60,
substr(tax_details60,instr(tax_details60,'<TAXCODE>',1,1)+9,instr(tax_details60,'</TAXCODE>',1,1)-(instr(tax_details60,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE60,
substr(LINE_DETAILS60,instr(LINE_DETAILS60,'<VALUE>',1,3)+7,instr(LINE_DETAILS60,'</VALUE>',1,3)-(instr(LINE_DETAILS60,'<VALUE>',1,3)+7)) quantity_line60,
(substr(LINE_DETAILS60,instr(LINE_DETAILS60,'<VALUE>',1,2)+7,instr(LINE_DETAILS60,'</VALUE>',1,2)-(instr(LINE_DETAILS60,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line60,
(substr(tax_details60,instr(tax_details60,'<VALUE>',1,3)+7,instr(tax_details60,'</VALUE>',1,3)-(instr(tax_details60,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE60,
substr(tax_details60,instr(tax_details60,'<DESCRIPTN>',1,1)+11,instr(tax_details60,'</DESCRIPTN>',1,1)-(instr(tax_details60,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION60,
substr(LINE_DETAILS61,instr(LINE_DETAILS61,'<LINENUM>',1,1)+9,instr(LINE_DETAILS61,'</LINENUM>',1,1)-(instr(LINE_DETAILS61,'<LINENUM>',1,1)+9)) LINE_NUM61,
(substr(LINE_DETAILS61,instr(LINE_DETAILS61,'<VALUE>',1,1)+7,instr(LINE_DETAILS61,'</VALUE>',1,1)-(instr(LINE_DETAILS61,'<VALUE>',1,1)+7)))*.01 LINE61_AMOUNT,
substr(LINE_DETAILS61,instr(LINE_DETAILS61,'<ITEM>',1,1)+6,instr(LINE_DETAILS61,'</ITEM>',1,1)-(instr(LINE_DETAILS61,'<ITEM>',1,1)+6)) ITEM_NAME_LINE61,
substr(LINE_DETAILS61,instr(LINE_DETAILS61,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS61,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS61,'<DESCRIPTN>',1,1)+11)) ITEM_Description61,
(substr(tax_details61,instr(tax_details61,'<VALUE>',1,1)+7,instr(tax_details61,'</VALUE>',1,1)-(instr(tax_details61,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE61,
substr(tax_details61,instr(tax_details61,'<TAXCODE>',1,1)+9,instr(tax_details61,'</TAXCODE>',1,1)-(instr(tax_details61,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE61,
substr(LINE_DETAILS61,instr(LINE_DETAILS61,'<VALUE>',1,3)+7,instr(LINE_DETAILS61,'</VALUE>',1,3)-(instr(LINE_DETAILS61,'<VALUE>',1,3)+7)) quantity_line61,
(substr(LINE_DETAILS61,instr(LINE_DETAILS61,'<VALUE>',1,2)+7,instr(LINE_DETAILS61,'</VALUE>',1,2)-(instr(LINE_DETAILS61,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line61,
(substr(tax_details61,instr(tax_details61,'<VALUE>',1,3)+7,instr(tax_details61,'</VALUE>',1,3)-(instr(tax_details61,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE61,
substr(tax_details61,instr(tax_details61,'<DESCRIPTN>',1,1)+11,instr(tax_details61,'</DESCRIPTN>',1,1)-(instr(tax_details61,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION61,
substr(LINE_DETAILS62,instr(LINE_DETAILS62,'<LINENUM>',1,1)+9,instr(LINE_DETAILS62,'</LINENUM>',1,1)-(instr(LINE_DETAILS62,'<LINENUM>',1,1)+9)) LINE_NUM62,
(substr(LINE_DETAILS62,instr(LINE_DETAILS62,'<VALUE>',1,1)+7,instr(LINE_DETAILS62,'</VALUE>',1,1)-(instr(LINE_DETAILS62,'<VALUE>',1,1)+7)))*.01 LINE62_AMOUNT,
substr(LINE_DETAILS62,instr(LINE_DETAILS62,'<ITEM>',1,1)+6,instr(LINE_DETAILS62,'</ITEM>',1,1)-(instr(LINE_DETAILS62,'<ITEM>',1,1)+6)) ITEM_NAME_LINE62,
substr(LINE_DETAILS62,instr(LINE_DETAILS62,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS62,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS62,'<DESCRIPTN>',1,1)+11)) ITEM_Description62,
(substr(tax_details62,instr(tax_details62,'<VALUE>',1,1)+7,instr(tax_details62,'</VALUE>',1,1)-(instr(tax_details62,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE62,
substr(tax_details62,instr(tax_details62,'<TAXCODE>',1,1)+9,instr(tax_details62,'</TAXCODE>',1,1)-(instr(tax_details62,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE62,
substr(LINE_DETAILS62,instr(LINE_DETAILS62,'<VALUE>',1,3)+7,instr(LINE_DETAILS62,'</VALUE>',1,3)-(instr(LINE_DETAILS62,'<VALUE>',1,3)+7)) quantity_line62,
(substr(LINE_DETAILS62,instr(LINE_DETAILS62,'<VALUE>',1,2)+7,instr(LINE_DETAILS62,'</VALUE>',1,2)-(instr(LINE_DETAILS62,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line62,
(substr(tax_details62,instr(tax_details62,'<VALUE>',1,3)+7,instr(tax_details62,'</VALUE>',1,3)-(instr(tax_details62,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE62,
substr(tax_details62,instr(tax_details62,'<DESCRIPTN>',1,1)+11,instr(tax_details62,'</DESCRIPTN>',1,1)-(instr(tax_details62,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION62,
substr(LINE_DETAILS63,instr(LINE_DETAILS63,'<LINENUM>',1,1)+9,instr(LINE_DETAILS63,'</LINENUM>',1,1)-(instr(LINE_DETAILS63,'<LINENUM>',1,1)+9)) LINE_NUM63,
(substr(LINE_DETAILS63,instr(LINE_DETAILS63,'<VALUE>',1,1)+7,instr(LINE_DETAILS63,'</VALUE>',1,1)-(instr(LINE_DETAILS63,'<VALUE>',1,1)+7)))*.01 LINE63_AMOUNT,
substr(LINE_DETAILS63,instr(LINE_DETAILS63,'<ITEM>',1,1)+6,instr(LINE_DETAILS63,'</ITEM>',1,1)-(instr(LINE_DETAILS63,'<ITEM>',1,1)+6)) ITEM_NAME_LINE63,
substr(LINE_DETAILS63,instr(LINE_DETAILS63,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS63,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS63,'<DESCRIPTN>',1,1)+11)) ITEM_Description63,
(substr(tax_details63,instr(tax_details63,'<VALUE>',1,1)+7,instr(tax_details63,'</VALUE>',1,1)-(instr(tax_details63,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE63,
substr(tax_details63,instr(tax_details63,'<TAXCODE>',1,1)+9,instr(tax_details63,'</TAXCODE>',1,1)-(instr(tax_details63,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE63,
substr(LINE_DETAILS63,instr(LINE_DETAILS63,'<VALUE>',1,3)+7,instr(LINE_DETAILS63,'</VALUE>',1,3)-(instr(LINE_DETAILS63,'<VALUE>',1,3)+7)) quantity_line63,
(substr(LINE_DETAILS63,instr(LINE_DETAILS63,'<VALUE>',1,2)+7,instr(LINE_DETAILS63,'</VALUE>',1,2)-(instr(LINE_DETAILS63,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line63,
(substr(tax_details63,instr(tax_details63,'<VALUE>',1,3)+7,instr(tax_details63,'</VALUE>',1,3)-(instr(tax_details63,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE63,
substr(tax_details63,instr(tax_details63,'<DESCRIPTN>',1,1)+11,instr(tax_details63,'</DESCRIPTN>',1,1)-(instr(tax_details63,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION63,
substr(LINE_DETAILS64,instr(LINE_DETAILS64,'<LINENUM>',1,1)+9,instr(LINE_DETAILS64,'</LINENUM>',1,1)-(instr(LINE_DETAILS64,'<LINENUM>',1,1)+9)) LINE_NUM64,
(substr(LINE_DETAILS64,instr(LINE_DETAILS64,'<VALUE>',1,1)+7,instr(LINE_DETAILS64,'</VALUE>',1,1)-(instr(LINE_DETAILS64,'<VALUE>',1,1)+7)))*.01 LINE64_AMOUNT,
substr(LINE_DETAILS64,instr(LINE_DETAILS64,'<ITEM>',1,1)+6,instr(LINE_DETAILS64,'</ITEM>',1,1)-(instr(LINE_DETAILS64,'<ITEM>',1,1)+6)) ITEM_NAME_LINE64,
substr(LINE_DETAILS64,instr(LINE_DETAILS64,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS64,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS64,'<DESCRIPTN>',1,1)+11)) ITEM_Description64,
(substr(tax_details64,instr(tax_details64,'<VALUE>',1,1)+7,instr(tax_details64,'</VALUE>',1,1)-(instr(tax_details64,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE64,
substr(tax_details64,instr(tax_details64,'<TAXCODE>',1,1)+9,instr(tax_details64,'</TAXCODE>',1,1)-(instr(tax_details64,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE64,
substr(LINE_DETAILS64,instr(LINE_DETAILS64,'<VALUE>',1,3)+7,instr(LINE_DETAILS64,'</VALUE>',1,3)-(instr(LINE_DETAILS64,'<VALUE>',1,3)+7)) quantity_line64,
(substr(LINE_DETAILS64,instr(LINE_DETAILS64,'<VALUE>',1,2)+7,instr(LINE_DETAILS64,'</VALUE>',1,2)-(instr(LINE_DETAILS64,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line64,
(substr(tax_details64,instr(tax_details64,'<VALUE>',1,3)+7,instr(tax_details64,'</VALUE>',1,3)-(instr(tax_details64,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE64,
substr(tax_details64,instr(tax_details64,'<DESCRIPTN>',1,1)+11,instr(tax_details64,'</DESCRIPTN>',1,1)-(instr(tax_details64,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION64,
substr(LINE_DETAILS65,instr(LINE_DETAILS65,'<LINENUM>',1,1)+9,instr(LINE_DETAILS65,'</LINENUM>',1,1)-(instr(LINE_DETAILS65,'<LINENUM>',1,1)+9)) LINE_NUM65,
(substr(LINE_DETAILS65,instr(LINE_DETAILS65,'<VALUE>',1,1)+7,instr(LINE_DETAILS65,'</VALUE>',1,1)-(instr(LINE_DETAILS65,'<VALUE>',1,1)+7)))*.01 LINE65_AMOUNT,
substr(LINE_DETAILS65,instr(LINE_DETAILS65,'<ITEM>',1,1)+6,instr(LINE_DETAILS65,'</ITEM>',1,1)-(instr(LINE_DETAILS65,'<ITEM>',1,1)+6)) ITEM_NAME_LINE65,
substr(LINE_DETAILS65,instr(LINE_DETAILS65,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS65,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS65,'<DESCRIPTN>',1,1)+11)) ITEM_Description65,
(substr(tax_details65,instr(tax_details65,'<VALUE>',1,1)+7,instr(tax_details65,'</VALUE>',1,1)-(instr(tax_details65,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE65,
substr(tax_details65,instr(tax_details65,'<TAXCODE>',1,1)+9,instr(tax_details65,'</TAXCODE>',1,1)-(instr(tax_details65,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE65,
substr(LINE_DETAILS65,instr(LINE_DETAILS65,'<VALUE>',1,3)+7,instr(LINE_DETAILS65,'</VALUE>',1,3)-(instr(LINE_DETAILS65,'<VALUE>',1,3)+7)) quantity_line65,
(substr(LINE_DETAILS65,instr(LINE_DETAILS65,'<VALUE>',1,2)+7,instr(LINE_DETAILS65,'</VALUE>',1,2)-(instr(LINE_DETAILS65,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line65,
(substr(tax_details65,instr(tax_details65,'<VALUE>',1,3)+7,instr(tax_details65,'</VALUE>',1,3)-(instr(tax_details65,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE65,
substr(tax_details65,instr(tax_details65,'<DESCRIPTN>',1,1)+11,instr(tax_details65,'</DESCRIPTN>',1,1)-(instr(tax_details65,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION65,
substr(LINE_DETAILS66,instr(LINE_DETAILS66,'<LINENUM>',1,1)+9,instr(LINE_DETAILS66,'</LINENUM>',1,1)-(instr(LINE_DETAILS66,'<LINENUM>',1,1)+9)) LINE_NUM66,
(substr(LINE_DETAILS66,instr(LINE_DETAILS66,'<VALUE>',1,1)+7,instr(LINE_DETAILS66,'</VALUE>',1,1)-(instr(LINE_DETAILS66,'<VALUE>',1,1)+7)))*.01 LINE66_AMOUNT,
substr(LINE_DETAILS66,instr(LINE_DETAILS66,'<ITEM>',1,1)+6,instr(LINE_DETAILS66,'</ITEM>',1,1)-(instr(LINE_DETAILS66,'<ITEM>',1,1)+6)) ITEM_NAME_LINE66,
substr(LINE_DETAILS66,instr(LINE_DETAILS66,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS66,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS66,'<DESCRIPTN>',1,1)+11)) ITEM_Description66,
(substr(tax_details66,instr(tax_details66,'<VALUE>',1,1)+7,instr(tax_details66,'</VALUE>',1,1)-(instr(tax_details66,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE66,
substr(tax_details66,instr(tax_details66,'<TAXCODE>',1,1)+9,instr(tax_details66,'</TAXCODE>',1,1)-(instr(tax_details66,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE66,
substr(LINE_DETAILS66,instr(LINE_DETAILS66,'<VALUE>',1,3)+7,instr(LINE_DETAILS66,'</VALUE>',1,3)-(instr(LINE_DETAILS66,'<VALUE>',1,3)+7)) quantity_line66,
(substr(LINE_DETAILS66,instr(LINE_DETAILS66,'<VALUE>',1,2)+7,instr(LINE_DETAILS66,'</VALUE>',1,2)-(instr(LINE_DETAILS66,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line66,
(substr(tax_details66,instr(tax_details66,'<VALUE>',1,3)+7,instr(tax_details66,'</VALUE>',1,3)-(instr(tax_details66,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE66,
substr(tax_details66,instr(tax_details66,'<DESCRIPTN>',1,1)+11,instr(tax_details66,'</DESCRIPTN>',1,1)-(instr(tax_details66,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION66,
substr(LINE_DETAILS67,instr(LINE_DETAILS67,'<LINENUM>',1,1)+9,instr(LINE_DETAILS67,'</LINENUM>',1,1)-(instr(LINE_DETAILS67,'<LINENUM>',1,1)+9)) LINE_NUM67,
(substr(LINE_DETAILS67,instr(LINE_DETAILS67,'<VALUE>',1,1)+7,instr(LINE_DETAILS67,'</VALUE>',1,1)-(instr(LINE_DETAILS67,'<VALUE>',1,1)+7)))*.01 LINE67_AMOUNT,
substr(LINE_DETAILS67,instr(LINE_DETAILS67,'<ITEM>',1,1)+6,instr(LINE_DETAILS67,'</ITEM>',1,1)-(instr(LINE_DETAILS67,'<ITEM>',1,1)+6)) ITEM_NAME_LINE67,
substr(LINE_DETAILS67,instr(LINE_DETAILS67,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS67,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS67,'<DESCRIPTN>',1,1)+11)) ITEM_Description67,
(substr(tax_details67,instr(tax_details67,'<VALUE>',1,1)+7,instr(tax_details67,'</VALUE>',1,1)-(instr(tax_details67,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE67,
substr(tax_details67,instr(tax_details67,'<TAXCODE>',1,1)+9,instr(tax_details67,'</TAXCODE>',1,1)-(instr(tax_details67,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE67,
substr(LINE_DETAILS67,instr(LINE_DETAILS67,'<VALUE>',1,3)+7,instr(LINE_DETAILS67,'</VALUE>',1,3)-(instr(LINE_DETAILS67,'<VALUE>',1,3)+7)) quantity_line67,
(substr(LINE_DETAILS67,instr(LINE_DETAILS67,'<VALUE>',1,2)+7,instr(LINE_DETAILS67,'</VALUE>',1,2)-(instr(LINE_DETAILS67,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line67,
(substr(tax_details67,instr(tax_details67,'<VALUE>',1,3)+7,instr(tax_details67,'</VALUE>',1,3)-(instr(tax_details67,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE67,
substr(tax_details67,instr(tax_details67,'<DESCRIPTN>',1,1)+11,instr(tax_details67,'</DESCRIPTN>',1,1)-(instr(tax_details67,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION67,
substr(LINE_DETAILS68,instr(LINE_DETAILS68,'<LINENUM>',1,1)+9,instr(LINE_DETAILS68,'</LINENUM>',1,1)-(instr(LINE_DETAILS68,'<LINENUM>',1,1)+9)) LINE_NUM68,
(substr(LINE_DETAILS68,instr(LINE_DETAILS68,'<VALUE>',1,1)+7,instr(LINE_DETAILS68,'</VALUE>',1,1)-(instr(LINE_DETAILS68,'<VALUE>',1,1)+7)))*.01 LINE68_AMOUNT,
substr(LINE_DETAILS68,instr(LINE_DETAILS68,'<ITEM>',1,1)+6,instr(LINE_DETAILS68,'</ITEM>',1,1)-(instr(LINE_DETAILS68,'<ITEM>',1,1)+6)) ITEM_NAME_LINE68,
substr(LINE_DETAILS68,instr(LINE_DETAILS68,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS68,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS68,'<DESCRIPTN>',1,1)+11)) ITEM_Description68,
(substr(tax_details68,instr(tax_details68,'<VALUE>',1,1)+7,instr(tax_details68,'</VALUE>',1,1)-(instr(tax_details68,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE68,
substr(tax_details68,instr(tax_details68,'<TAXCODE>',1,1)+9,instr(tax_details68,'</TAXCODE>',1,1)-(instr(tax_details68,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE68,
substr(LINE_DETAILS68,instr(LINE_DETAILS68,'<VALUE>',1,3)+7,instr(LINE_DETAILS68,'</VALUE>',1,3)-(instr(LINE_DETAILS68,'<VALUE>',1,3)+7)) quantity_line68,
(substr(LINE_DETAILS68,instr(LINE_DETAILS68,'<VALUE>',1,2)+7,instr(LINE_DETAILS68,'</VALUE>',1,2)-(instr(LINE_DETAILS68,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line68,
(substr(tax_details68,instr(tax_details68,'<VALUE>',1,3)+7,instr(tax_details68,'</VALUE>',1,3)-(instr(tax_details68,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE68,
substr(tax_details68,instr(tax_details68,'<DESCRIPTN>',1,1)+11,instr(tax_details68,'</DESCRIPTN>',1,1)-(instr(tax_details68,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION68,
substr(LINE_DETAILS69,instr(LINE_DETAILS69,'<LINENUM>',1,1)+9,instr(LINE_DETAILS69,'</LINENUM>',1,1)-(instr(LINE_DETAILS69,'<LINENUM>',1,1)+9)) LINE_NUM69,
(substr(LINE_DETAILS69,instr(LINE_DETAILS69,'<VALUE>',1,1)+7,instr(LINE_DETAILS69,'</VALUE>',1,1)-(instr(LINE_DETAILS69,'<VALUE>',1,1)+7)))*.01 LINE69_AMOUNT,
substr(LINE_DETAILS69,instr(LINE_DETAILS69,'<ITEM>',1,1)+6,instr(LINE_DETAILS69,'</ITEM>',1,1)-(instr(LINE_DETAILS69,'<ITEM>',1,1)+6)) ITEM_NAME_LINE69,
substr(LINE_DETAILS69,instr(LINE_DETAILS69,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS69,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS69,'<DESCRIPTN>',1,1)+11)) ITEM_Description69,
(substr(tax_details69,instr(tax_details69,'<VALUE>',1,1)+7,instr(tax_details69,'</VALUE>',1,1)-(instr(tax_details69,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE69,
substr(tax_details69,instr(tax_details69,'<TAXCODE>',1,1)+9,instr(tax_details69,'</TAXCODE>',1,1)-(instr(tax_details69,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE69,
substr(LINE_DETAILS69,instr(LINE_DETAILS69,'<VALUE>',1,3)+7,instr(LINE_DETAILS69,'</VALUE>',1,3)-(instr(LINE_DETAILS69,'<VALUE>',1,3)+7)) quantity_line69,
(substr(LINE_DETAILS69,instr(LINE_DETAILS69,'<VALUE>',1,2)+7,instr(LINE_DETAILS69,'</VALUE>',1,2)-(instr(LINE_DETAILS69,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line69,
(substr(tax_details69,instr(tax_details69,'<VALUE>',1,3)+7,instr(tax_details69,'</VALUE>',1,3)-(instr(tax_details69,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE69,
substr(tax_details69,instr(tax_details69,'<DESCRIPTN>',1,1)+11,instr(tax_details69,'</DESCRIPTN>',1,1)-(instr(tax_details69,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION69,
substr(LINE_DETAILS70,instr(LINE_DETAILS70,'<LINENUM>',1,1)+9,instr(LINE_DETAILS70,'</LINENUM>',1,1)-(instr(LINE_DETAILS70,'<LINENUM>',1,1)+9)) LINE_NUM70,
(substr(LINE_DETAILS70,instr(LINE_DETAILS70,'<VALUE>',1,1)+7,instr(LINE_DETAILS70,'</VALUE>',1,1)-(instr(LINE_DETAILS70,'<VALUE>',1,1)+7)))*.01 LINE70_AMOUNT,
substr(LINE_DETAILS70,instr(LINE_DETAILS70,'<ITEM>',1,1)+6,instr(LINE_DETAILS70,'</ITEM>',1,1)-(instr(LINE_DETAILS70,'<ITEM>',1,1)+6)) ITEM_NAME_LINE70,
substr(LINE_DETAILS70,instr(LINE_DETAILS70,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS70,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS70,'<DESCRIPTN>',1,1)+11)) ITEM_Description70,
(substr(tax_details70,instr(tax_details70,'<VALUE>',1,1)+7,instr(tax_details70,'</VALUE>',1,1)-(instr(tax_details70,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE70,
substr(tax_details70,instr(tax_details70,'<TAXCODE>',1,1)+9,instr(tax_details70,'</TAXCODE>',1,1)-(instr(tax_details70,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE70,
substr(LINE_DETAILS70,instr(LINE_DETAILS70,'<VALUE>',1,3)+7,instr(LINE_DETAILS70,'</VALUE>',1,3)-(instr(LINE_DETAILS70,'<VALUE>',1,3)+7)) quantity_line70,
(substr(LINE_DETAILS70,instr(LINE_DETAILS70,'<VALUE>',1,2)+7,instr(LINE_DETAILS70,'</VALUE>',1,2)-(instr(LINE_DETAILS70,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line70,
(substr(tax_details70,instr(tax_details70,'<VALUE>',1,3)+7,instr(tax_details70,'</VALUE>',1,3)-(instr(tax_details70,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE70,
substr(tax_details70,instr(tax_details70,'<DESCRIPTN>',1,1)+11,instr(tax_details70,'</DESCRIPTN>',1,1)-(instr(tax_details70,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION70,
substr(LINE_DETAILS71,instr(LINE_DETAILS71,'<LINENUM>',1,1)+9,instr(LINE_DETAILS71,'</LINENUM>',1,1)-(instr(LINE_DETAILS71,'<LINENUM>',1,1)+9)) LINE_NUM71,
(substr(LINE_DETAILS71,instr(LINE_DETAILS71,'<VALUE>',1,1)+7,instr(LINE_DETAILS71,'</VALUE>',1,1)-(instr(LINE_DETAILS71,'<VALUE>',1,1)+7)))*.01 LINE71_AMOUNT,
substr(LINE_DETAILS71,instr(LINE_DETAILS71,'<ITEM>',1,1)+6,instr(LINE_DETAILS71,'</ITEM>',1,1)-(instr(LINE_DETAILS71,'<ITEM>',1,1)+6)) ITEM_NAME_LINE71,
substr(LINE_DETAILS71,instr(LINE_DETAILS71,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS71,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS71,'<DESCRIPTN>',1,1)+11)) ITEM_Description71,
(substr(tax_details71,instr(tax_details71,'<VALUE>',1,1)+7,instr(tax_details71,'</VALUE>',1,1)-(instr(tax_details71,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE71,
substr(tax_details71,instr(tax_details71,'<TAXCODE>',1,1)+9,instr(tax_details71,'</TAXCODE>',1,1)-(instr(tax_details71,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE71,
substr(LINE_DETAILS71,instr(LINE_DETAILS71,'<VALUE>',1,3)+7,instr(LINE_DETAILS71,'</VALUE>',1,3)-(instr(LINE_DETAILS71,'<VALUE>',1,3)+7)) quantity_line71,
(substr(LINE_DETAILS71,instr(LINE_DETAILS71,'<VALUE>',1,2)+7,instr(LINE_DETAILS71,'</VALUE>',1,2)-(instr(LINE_DETAILS71,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line71,
(substr(tax_details71,instr(tax_details71,'<VALUE>',1,3)+7,instr(tax_details71,'</VALUE>',1,3)-(instr(tax_details71,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE71,
substr(tax_details71,instr(tax_details71,'<DESCRIPTN>',1,1)+11,instr(tax_details71,'</DESCRIPTN>',1,1)-(instr(tax_details71,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION71,
substr(LINE_DETAILS72,instr(LINE_DETAILS72,'<LINENUM>',1,1)+9,instr(LINE_DETAILS72,'</LINENUM>',1,1)-(instr(LINE_DETAILS72,'<LINENUM>',1,1)+9)) LINE_NUM72,
(substr(LINE_DETAILS72,instr(LINE_DETAILS72,'<VALUE>',1,1)+7,instr(LINE_DETAILS72,'</VALUE>',1,1)-(instr(LINE_DETAILS72,'<VALUE>',1,1)+7)))*.01 LINE72_AMOUNT,
substr(LINE_DETAILS72,instr(LINE_DETAILS72,'<ITEM>',1,1)+6,instr(LINE_DETAILS72,'</ITEM>',1,1)-(instr(LINE_DETAILS72,'<ITEM>',1,1)+6)) ITEM_NAME_LINE72,
substr(LINE_DETAILS72,instr(LINE_DETAILS72,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS72,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS72,'<DESCRIPTN>',1,1)+11)) ITEM_Description72,
(substr(tax_details72,instr(tax_details72,'<VALUE>',1,1)+7,instr(tax_details72,'</VALUE>',1,1)-(instr(tax_details72,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE72,
substr(tax_details72,instr(tax_details72,'<TAXCODE>',1,1)+9,instr(tax_details72,'</TAXCODE>',1,1)-(instr(tax_details72,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE72,
substr(LINE_DETAILS72,instr(LINE_DETAILS72,'<VALUE>',1,3)+7,instr(LINE_DETAILS72,'</VALUE>',1,3)-(instr(LINE_DETAILS72,'<VALUE>',1,3)+7)) quantity_line72,
(substr(LINE_DETAILS72,instr(LINE_DETAILS72,'<VALUE>',1,2)+7,instr(LINE_DETAILS72,'</VALUE>',1,2)-(instr(LINE_DETAILS72,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line72,
(substr(tax_details72,instr(tax_details72,'<VALUE>',1,3)+7,instr(tax_details72,'</VALUE>',1,3)-(instr(tax_details72,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE72,
substr(tax_details72,instr(tax_details72,'<DESCRIPTN>',1,1)+11,instr(tax_details72,'</DESCRIPTN>',1,1)-(instr(tax_details72,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION72,
substr(LINE_DETAILS73,instr(LINE_DETAILS73,'<LINENUM>',1,1)+9,instr(LINE_DETAILS73,'</LINENUM>',1,1)-(instr(LINE_DETAILS73,'<LINENUM>',1,1)+9)) LINE_NUM73,
(substr(LINE_DETAILS73,instr(LINE_DETAILS73,'<VALUE>',1,1)+7,instr(LINE_DETAILS73,'</VALUE>',1,1)-(instr(LINE_DETAILS73,'<VALUE>',1,1)+7)))*.01 LINE73_AMOUNT,
substr(LINE_DETAILS73,instr(LINE_DETAILS73,'<ITEM>',1,1)+6,instr(LINE_DETAILS73,'</ITEM>',1,1)-(instr(LINE_DETAILS73,'<ITEM>',1,1)+6)) ITEM_NAME_LINE73,
substr(LINE_DETAILS73,instr(LINE_DETAILS73,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS73,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS73,'<DESCRIPTN>',1,1)+11)) ITEM_Description73,
(substr(tax_details73,instr(tax_details73,'<VALUE>',1,1)+7,instr(tax_details73,'</VALUE>',1,1)-(instr(tax_details73,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE73,
substr(tax_details73,instr(tax_details73,'<TAXCODE>',1,1)+9,instr(tax_details73,'</TAXCODE>',1,1)-(instr(tax_details73,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE73,
substr(LINE_DETAILS73,instr(LINE_DETAILS73,'<VALUE>',1,3)+7,instr(LINE_DETAILS73,'</VALUE>',1,3)-(instr(LINE_DETAILS73,'<VALUE>',1,3)+7)) quantity_line73,
(substr(LINE_DETAILS73,instr(LINE_DETAILS73,'<VALUE>',1,2)+7,instr(LINE_DETAILS73,'</VALUE>',1,2)-(instr(LINE_DETAILS73,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line73,
(substr(tax_details73,instr(tax_details73,'<VALUE>',1,3)+7,instr(tax_details73,'</VALUE>',1,3)-(instr(tax_details73,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE73,
substr(tax_details73,instr(tax_details73,'<DESCRIPTN>',1,1)+11,instr(tax_details73,'</DESCRIPTN>',1,1)-(instr(tax_details73,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION73,
substr(LINE_DETAILS74,instr(LINE_DETAILS74,'<LINENUM>',1,1)+9,instr(LINE_DETAILS74,'</LINENUM>',1,1)-(instr(LINE_DETAILS74,'<LINENUM>',1,1)+9)) LINE_NUM74,
(substr(LINE_DETAILS74,instr(LINE_DETAILS74,'<VALUE>',1,1)+7,instr(LINE_DETAILS74,'</VALUE>',1,1)-(instr(LINE_DETAILS74,'<VALUE>',1,1)+7)))*.01 LINE74_AMOUNT,
substr(LINE_DETAILS74,instr(LINE_DETAILS74,'<ITEM>',1,1)+6,instr(LINE_DETAILS74,'</ITEM>',1,1)-(instr(LINE_DETAILS74,'<ITEM>',1,1)+6)) ITEM_NAME_LINE74,
substr(LINE_DETAILS74,instr(LINE_DETAILS74,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS74,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS74,'<DESCRIPTN>',1,1)+11)) ITEM_Description74,
(substr(tax_details74,instr(tax_details74,'<VALUE>',1,1)+7,instr(tax_details74,'</VALUE>',1,1)-(instr(tax_details74,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE74,
substr(tax_details74,instr(tax_details74,'<TAXCODE>',1,1)+9,instr(tax_details74,'</TAXCODE>',1,1)-(instr(tax_details74,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE74,
substr(LINE_DETAILS74,instr(LINE_DETAILS74,'<VALUE>',1,3)+7,instr(LINE_DETAILS74,'</VALUE>',1,3)-(instr(LINE_DETAILS74,'<VALUE>',1,3)+7)) quantity_line74,
(substr(LINE_DETAILS74,instr(LINE_DETAILS74,'<VALUE>',1,2)+7,instr(LINE_DETAILS74,'</VALUE>',1,2)-(instr(LINE_DETAILS74,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line74,
(substr(tax_details74,instr(tax_details74,'<VALUE>',1,3)+7,instr(tax_details74,'</VALUE>',1,3)-(instr(tax_details74,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE74,
substr(tax_details74,instr(tax_details74,'<DESCRIPTN>',1,1)+11,instr(tax_details74,'</DESCRIPTN>',1,1)-(instr(tax_details74,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION74,
substr(LINE_DETAILS75,instr(LINE_DETAILS75,'<LINENUM>',1,1)+9,instr(LINE_DETAILS75,'</LINENUM>',1,1)-(instr(LINE_DETAILS75,'<LINENUM>',1,1)+9)) LINE_NUM75,
(substr(LINE_DETAILS75,instr(LINE_DETAILS75,'<VALUE>',1,1)+7,instr(LINE_DETAILS75,'</VALUE>',1,1)-(instr(LINE_DETAILS75,'<VALUE>',1,1)+7)))*.01 LINE75_AMOUNT,
substr(LINE_DETAILS75,instr(LINE_DETAILS75,'<ITEM>',1,1)+6,instr(LINE_DETAILS75,'</ITEM>',1,1)-(instr(LINE_DETAILS75,'<ITEM>',1,1)+6)) ITEM_NAME_LINE75,
substr(LINE_DETAILS75,instr(LINE_DETAILS75,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS75,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS75,'<DESCRIPTN>',1,1)+11)) ITEM_Description75,
(substr(tax_details75,instr(tax_details75,'<VALUE>',1,1)+7,instr(tax_details75,'</VALUE>',1,1)-(instr(tax_details75,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE75,
substr(tax_details75,instr(tax_details75,'<TAXCODE>',1,1)+9,instr(tax_details75,'</TAXCODE>',1,1)-(instr(tax_details75,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE75,
substr(LINE_DETAILS75,instr(LINE_DETAILS75,'<VALUE>',1,3)+7,instr(LINE_DETAILS75,'</VALUE>',1,3)-(instr(LINE_DETAILS75,'<VALUE>',1,3)+7)) quantity_line75,
(substr(LINE_DETAILS75,instr(LINE_DETAILS75,'<VALUE>',1,2)+7,instr(LINE_DETAILS75,'</VALUE>',1,2)-(instr(LINE_DETAILS75,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line75,
(substr(tax_details75,instr(tax_details75,'<VALUE>',1,3)+7,instr(tax_details75,'</VALUE>',1,3)-(instr(tax_details75,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE75,
substr(tax_details75,instr(tax_details75,'<DESCRIPTN>',1,1)+11,instr(tax_details75,'</DESCRIPTN>',1,1)-(instr(tax_details75,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION75,
substr(LINE_DETAILS76,instr(LINE_DETAILS76,'<LINENUM>',1,1)+9,instr(LINE_DETAILS76,'</LINENUM>',1,1)-(instr(LINE_DETAILS76,'<LINENUM>',1,1)+9)) LINE_NUM76,
(substr(LINE_DETAILS76,instr(LINE_DETAILS76,'<VALUE>',1,1)+7,instr(LINE_DETAILS76,'</VALUE>',1,1)-(instr(LINE_DETAILS76,'<VALUE>',1,1)+7)))*.01 LINE76_AMOUNT,
substr(LINE_DETAILS76,instr(LINE_DETAILS76,'<ITEM>',1,1)+6,instr(LINE_DETAILS76,'</ITEM>',1,1)-(instr(LINE_DETAILS76,'<ITEM>',1,1)+6)) ITEM_NAME_LINE76,
substr(LINE_DETAILS76,instr(LINE_DETAILS76,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS76,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS76,'<DESCRIPTN>',1,1)+11)) ITEM_Description76,
(substr(tax_details76,instr(tax_details76,'<VALUE>',1,1)+7,instr(tax_details76,'</VALUE>',1,1)-(instr(tax_details76,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE76,
substr(tax_details76,instr(tax_details76,'<TAXCODE>',1,1)+9,instr(tax_details76,'</TAXCODE>',1,1)-(instr(tax_details76,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE76,
substr(LINE_DETAILS76,instr(LINE_DETAILS76,'<VALUE>',1,3)+7,instr(LINE_DETAILS76,'</VALUE>',1,3)-(instr(LINE_DETAILS76,'<VALUE>',1,3)+7)) quantity_line76,
(substr(LINE_DETAILS76,instr(LINE_DETAILS76,'<VALUE>',1,2)+7,instr(LINE_DETAILS76,'</VALUE>',1,2)-(instr(LINE_DETAILS76,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line76,
(substr(tax_details76,instr(tax_details76,'<VALUE>',1,3)+7,instr(tax_details76,'</VALUE>',1,3)-(instr(tax_details76,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE76,
substr(tax_details76,instr(tax_details76,'<DESCRIPTN>',1,1)+11,instr(tax_details76,'</DESCRIPTN>',1,1)-(instr(tax_details76,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION76,
substr(LINE_DETAILS77,instr(LINE_DETAILS77,'<LINENUM>',1,1)+9,instr(LINE_DETAILS77,'</LINENUM>',1,1)-(instr(LINE_DETAILS77,'<LINENUM>',1,1)+9)) LINE_NUM77,
(substr(LINE_DETAILS77,instr(LINE_DETAILS77,'<VALUE>',1,1)+7,instr(LINE_DETAILS77,'</VALUE>',1,1)-(instr(LINE_DETAILS77,'<VALUE>',1,1)+7)))*.01 LINE77_AMOUNT,
substr(LINE_DETAILS77,instr(LINE_DETAILS77,'<ITEM>',1,1)+6,instr(LINE_DETAILS77,'</ITEM>',1,1)-(instr(LINE_DETAILS77,'<ITEM>',1,1)+6)) ITEM_NAME_LINE77,
substr(LINE_DETAILS77,instr(LINE_DETAILS77,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS77,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS77,'<DESCRIPTN>',1,1)+11)) ITEM_Description77,
(substr(tax_details77,instr(tax_details77,'<VALUE>',1,1)+7,instr(tax_details77,'</VALUE>',1,1)-(instr(tax_details77,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE77,
substr(tax_details77,instr(tax_details77,'<TAXCODE>',1,1)+9,instr(tax_details77,'</TAXCODE>',1,1)-(instr(tax_details77,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE77,
substr(LINE_DETAILS77,instr(LINE_DETAILS77,'<VALUE>',1,3)+7,instr(LINE_DETAILS77,'</VALUE>',1,3)-(instr(LINE_DETAILS77,'<VALUE>',1,3)+7)) quantity_line77,
(substr(LINE_DETAILS77,instr(LINE_DETAILS77,'<VALUE>',1,2)+7,instr(LINE_DETAILS77,'</VALUE>',1,2)-(instr(LINE_DETAILS77,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line77,
(substr(tax_details77,instr(tax_details77,'<VALUE>',1,3)+7,instr(tax_details77,'</VALUE>',1,3)-(instr(tax_details77,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE77,
substr(tax_details77,instr(tax_details77,'<DESCRIPTN>',1,1)+11,instr(tax_details77,'</DESCRIPTN>',1,1)-(instr(tax_details77,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION77,
substr(LINE_DETAILS78,instr(LINE_DETAILS78,'<LINENUM>',1,1)+9,instr(LINE_DETAILS78,'</LINENUM>',1,1)-(instr(LINE_DETAILS78,'<LINENUM>',1,1)+9)) LINE_NUM78,
(substr(LINE_DETAILS78,instr(LINE_DETAILS78,'<VALUE>',1,1)+7,instr(LINE_DETAILS78,'</VALUE>',1,1)-(instr(LINE_DETAILS78,'<VALUE>',1,1)+7)))*.01 LINE78_AMOUNT,
substr(LINE_DETAILS78,instr(LINE_DETAILS78,'<ITEM>',1,1)+6,instr(LINE_DETAILS78,'</ITEM>',1,1)-(instr(LINE_DETAILS78,'<ITEM>',1,1)+6)) ITEM_NAME_LINE78,
substr(LINE_DETAILS78,instr(LINE_DETAILS78,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS78,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS78,'<DESCRIPTN>',1,1)+11)) ITEM_Description78,
(substr(tax_details78,instr(tax_details78,'<VALUE>',1,1)+7,instr(tax_details78,'</VALUE>',1,1)-(instr(tax_details78,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE78,
substr(tax_details78,instr(tax_details78,'<TAXCODE>',1,1)+9,instr(tax_details78,'</TAXCODE>',1,1)-(instr(tax_details78,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE78,
substr(LINE_DETAILS78,instr(LINE_DETAILS78,'<VALUE>',1,3)+7,instr(LINE_DETAILS78,'</VALUE>',1,3)-(instr(LINE_DETAILS78,'<VALUE>',1,3)+7)) quantity_line78,
(substr(LINE_DETAILS78,instr(LINE_DETAILS78,'<VALUE>',1,2)+7,instr(LINE_DETAILS78,'</VALUE>',1,2)-(instr(LINE_DETAILS78,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line78,
(substr(tax_details78,instr(tax_details78,'<VALUE>',1,3)+7,instr(tax_details78,'</VALUE>',1,3)-(instr(tax_details78,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE78,
substr(tax_details78,instr(tax_details78,'<DESCRIPTN>',1,1)+11,instr(tax_details78,'</DESCRIPTN>',1,1)-(instr(tax_details78,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION78,
substr(LINE_DETAILS79,instr(LINE_DETAILS79,'<LINENUM>',1,1)+9,instr(LINE_DETAILS79,'</LINENUM>',1,1)-(instr(LINE_DETAILS79,'<LINENUM>',1,1)+9)) LINE_NUM79,
(substr(LINE_DETAILS79,instr(LINE_DETAILS79,'<VALUE>',1,1)+7,instr(LINE_DETAILS79,'</VALUE>',1,1)-(instr(LINE_DETAILS79,'<VALUE>',1,1)+7)))*.01 LINE79_AMOUNT,
substr(LINE_DETAILS79,instr(LINE_DETAILS79,'<ITEM>',1,1)+6,instr(LINE_DETAILS79,'</ITEM>',1,1)-(instr(LINE_DETAILS79,'<ITEM>',1,1)+6)) ITEM_NAME_LINE79,
substr(LINE_DETAILS79,instr(LINE_DETAILS79,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS79,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS79,'<DESCRIPTN>',1,1)+11)) ITEM_Description79,
(substr(tax_details79,instr(tax_details79,'<VALUE>',1,1)+7,instr(tax_details79,'</VALUE>',1,1)-(instr(tax_details79,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE79,
substr(tax_details79,instr(tax_details79,'<TAXCODE>',1,1)+9,instr(tax_details79,'</TAXCODE>',1,1)-(instr(tax_details79,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE79,
substr(LINE_DETAILS79,instr(LINE_DETAILS79,'<VALUE>',1,3)+7,instr(LINE_DETAILS79,'</VALUE>',1,3)-(instr(LINE_DETAILS79,'<VALUE>',1,3)+7)) quantity_line79,
(substr(LINE_DETAILS79,instr(LINE_DETAILS79,'<VALUE>',1,2)+7,instr(LINE_DETAILS79,'</VALUE>',1,2)-(instr(LINE_DETAILS79,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line79,
(substr(tax_details79,instr(tax_details79,'<VALUE>',1,3)+7,instr(tax_details79,'</VALUE>',1,3)-(instr(tax_details79,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE79,
substr(tax_details79,instr(tax_details79,'<DESCRIPTN>',1,1)+11,instr(tax_details79,'</DESCRIPTN>',1,1)-(instr(tax_details79,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION79,
substr(LINE_DETAILS80,instr(LINE_DETAILS80,'<LINENUM>',1,1)+9,instr(LINE_DETAILS80,'</LINENUM>',1,1)-(instr(LINE_DETAILS80,'<LINENUM>',1,1)+9)) LINE_NUM80,
(substr(LINE_DETAILS80,instr(LINE_DETAILS80,'<VALUE>',1,1)+7,instr(LINE_DETAILS80,'</VALUE>',1,1)-(instr(LINE_DETAILS80,'<VALUE>',1,1)+7)))*.01 LINE80_AMOUNT,
substr(LINE_DETAILS80,instr(LINE_DETAILS80,'<ITEM>',1,1)+6,instr(LINE_DETAILS80,'</ITEM>',1,1)-(instr(LINE_DETAILS80,'<ITEM>',1,1)+6)) ITEM_NAME_LINE80,
substr(LINE_DETAILS80,instr(LINE_DETAILS80,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS80,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS80,'<DESCRIPTN>',1,1)+11)) ITEM_Description80,
(substr(tax_details80,instr(tax_details80,'<VALUE>',1,1)+7,instr(tax_details80,'</VALUE>',1,1)-(instr(tax_details80,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE80,
substr(tax_details80,instr(tax_details80,'<TAXCODE>',1,1)+9,instr(tax_details80,'</TAXCODE>',1,1)-(instr(tax_details80,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE80,
substr(LINE_DETAILS80,instr(LINE_DETAILS80,'<VALUE>',1,3)+7,instr(LINE_DETAILS80,'</VALUE>',1,3)-(instr(LINE_DETAILS80,'<VALUE>',1,3)+7)) quantity_line80,
(substr(LINE_DETAILS80,instr(LINE_DETAILS80,'<VALUE>',1,2)+7,instr(LINE_DETAILS80,'</VALUE>',1,2)-(instr(LINE_DETAILS80,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line80,
(substr(tax_details80,instr(tax_details80,'<VALUE>',1,3)+7,instr(tax_details80,'</VALUE>',1,3)-(instr(tax_details80,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE80,
substr(tax_details80,instr(tax_details80,'<DESCRIPTN>',1,1)+11,instr(tax_details80,'</DESCRIPTN>',1,1)-(instr(tax_details80,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION80,
substr(LINE_DETAILS81,instr(LINE_DETAILS81,'<LINENUM>',1,1)+9,instr(LINE_DETAILS81,'</LINENUM>',1,1)-(instr(LINE_DETAILS81,'<LINENUM>',1,1)+9)) LINE_NUM81,
(substr(LINE_DETAILS81,instr(LINE_DETAILS81,'<VALUE>',1,1)+7,instr(LINE_DETAILS81,'</VALUE>',1,1)-(instr(LINE_DETAILS81,'<VALUE>',1,1)+7)))*.01 LINE81_AMOUNT,
substr(LINE_DETAILS81,instr(LINE_DETAILS81,'<ITEM>',1,1)+6,instr(LINE_DETAILS81,'</ITEM>',1,1)-(instr(LINE_DETAILS81,'<ITEM>',1,1)+6)) ITEM_NAME_LINE81,
substr(LINE_DETAILS81,instr(LINE_DETAILS81,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS81,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS81,'<DESCRIPTN>',1,1)+11)) ITEM_Description81,
(substr(tax_details81,instr(tax_details81,'<VALUE>',1,1)+7,instr(tax_details81,'</VALUE>',1,1)-(instr(tax_details81,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE81,
substr(tax_details81,instr(tax_details81,'<TAXCODE>',1,1)+9,instr(tax_details81,'</TAXCODE>',1,1)-(instr(tax_details81,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE81,
substr(LINE_DETAILS81,instr(LINE_DETAILS81,'<VALUE>',1,3)+7,instr(LINE_DETAILS81,'</VALUE>',1,3)-(instr(LINE_DETAILS81,'<VALUE>',1,3)+7)) quantity_line81,
(substr(LINE_DETAILS81,instr(LINE_DETAILS81,'<VALUE>',1,2)+7,instr(LINE_DETAILS81,'</VALUE>',1,2)-(instr(LINE_DETAILS81,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line81,
(substr(tax_details81,instr(tax_details81,'<VALUE>',1,3)+7,instr(tax_details81,'</VALUE>',1,3)-(instr(tax_details81,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE81,
substr(tax_details81,instr(tax_details81,'<DESCRIPTN>',1,1)+11,instr(tax_details81,'</DESCRIPTN>',1,1)-(instr(tax_details81,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION81,
substr(LINE_DETAILS82,instr(LINE_DETAILS82,'<LINENUM>',1,1)+9,instr(LINE_DETAILS82,'</LINENUM>',1,1)-(instr(LINE_DETAILS82,'<LINENUM>',1,1)+9)) LINE_NUM82,
(substr(LINE_DETAILS82,instr(LINE_DETAILS82,'<VALUE>',1,1)+7,instr(LINE_DETAILS82,'</VALUE>',1,1)-(instr(LINE_DETAILS82,'<VALUE>',1,1)+7)))*.01 LINE82_AMOUNT,
substr(LINE_DETAILS82,instr(LINE_DETAILS82,'<ITEM>',1,1)+6,instr(LINE_DETAILS82,'</ITEM>',1,1)-(instr(LINE_DETAILS82,'<ITEM>',1,1)+6)) ITEM_NAME_LINE82,
substr(LINE_DETAILS82,instr(LINE_DETAILS82,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS82,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS82,'<DESCRIPTN>',1,1)+11)) ITEM_Description82,
(substr(tax_details82,instr(tax_details82,'<VALUE>',1,1)+7,instr(tax_details82,'</VALUE>',1,1)-(instr(tax_details82,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE82,
substr(tax_details82,instr(tax_details82,'<TAXCODE>',1,1)+9,instr(tax_details82,'</TAXCODE>',1,1)-(instr(tax_details82,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE82,
substr(LINE_DETAILS82,instr(LINE_DETAILS82,'<VALUE>',1,3)+7,instr(LINE_DETAILS82,'</VALUE>',1,3)-(instr(LINE_DETAILS82,'<VALUE>',1,3)+7)) quantity_line82,
(substr(LINE_DETAILS82,instr(LINE_DETAILS82,'<VALUE>',1,2)+7,instr(LINE_DETAILS82,'</VALUE>',1,2)-(instr(LINE_DETAILS82,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line82,
(substr(tax_details82,instr(tax_details82,'<VALUE>',1,3)+7,instr(tax_details82,'</VALUE>',1,3)-(instr(tax_details82,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE82,
substr(tax_details82,instr(tax_details82,'<DESCRIPTN>',1,1)+11,instr(tax_details82,'</DESCRIPTN>',1,1)-(instr(tax_details82,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION82,
substr(LINE_DETAILS83,instr(LINE_DETAILS83,'<LINENUM>',1,1)+9,instr(LINE_DETAILS83,'</LINENUM>',1,1)-(instr(LINE_DETAILS83,'<LINENUM>',1,1)+9)) LINE_NUM83,
(substr(LINE_DETAILS83,instr(LINE_DETAILS83,'<VALUE>',1,1)+7,instr(LINE_DETAILS83,'</VALUE>',1,1)-(instr(LINE_DETAILS83,'<VALUE>',1,1)+7)))*.01 LINE83_AMOUNT,
substr(LINE_DETAILS83,instr(LINE_DETAILS83,'<ITEM>',1,1)+6,instr(LINE_DETAILS83,'</ITEM>',1,1)-(instr(LINE_DETAILS83,'<ITEM>',1,1)+6)) ITEM_NAME_LINE83,
substr(LINE_DETAILS83,instr(LINE_DETAILS83,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS83,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS83,'<DESCRIPTN>',1,1)+11)) ITEM_Description83,
(substr(tax_details83,instr(tax_details83,'<VALUE>',1,1)+7,instr(tax_details83,'</VALUE>',1,1)-(instr(tax_details83,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE83,
substr(tax_details83,instr(tax_details83,'<TAXCODE>',1,1)+9,instr(tax_details83,'</TAXCODE>',1,1)-(instr(tax_details83,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE83,
substr(LINE_DETAILS83,instr(LINE_DETAILS83,'<VALUE>',1,3)+7,instr(LINE_DETAILS83,'</VALUE>',1,3)-(instr(LINE_DETAILS83,'<VALUE>',1,3)+7)) quantity_line83,
(substr(LINE_DETAILS83,instr(LINE_DETAILS83,'<VALUE>',1,2)+7,instr(LINE_DETAILS83,'</VALUE>',1,2)-(instr(LINE_DETAILS83,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line83,
(substr(tax_details83,instr(tax_details83,'<VALUE>',1,3)+7,instr(tax_details83,'</VALUE>',1,3)-(instr(tax_details83,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE83,
substr(tax_details83,instr(tax_details83,'<DESCRIPTN>',1,1)+11,instr(tax_details83,'</DESCRIPTN>',1,1)-(instr(tax_details83,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION83,
substr(LINE_DETAILS84,instr(LINE_DETAILS84,'<LINENUM>',1,1)+9,instr(LINE_DETAILS84,'</LINENUM>',1,1)-(instr(LINE_DETAILS84,'<LINENUM>',1,1)+9)) LINE_NUM84,
(substr(LINE_DETAILS84,instr(LINE_DETAILS84,'<VALUE>',1,1)+7,instr(LINE_DETAILS84,'</VALUE>',1,1)-(instr(LINE_DETAILS84,'<VALUE>',1,1)+7)))*.01 LINE84_AMOUNT,
substr(LINE_DETAILS84,instr(LINE_DETAILS84,'<ITEM>',1,1)+6,instr(LINE_DETAILS84,'</ITEM>',1,1)-(instr(LINE_DETAILS84,'<ITEM>',1,1)+6)) ITEM_NAME_LINE84,
substr(LINE_DETAILS84,instr(LINE_DETAILS84,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS84,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS84,'<DESCRIPTN>',1,1)+11)) ITEM_Description84,
(substr(tax_details84,instr(tax_details84,'<VALUE>',1,1)+7,instr(tax_details84,'</VALUE>',1,1)-(instr(tax_details84,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE84,
substr(tax_details84,instr(tax_details84,'<TAXCODE>',1,1)+9,instr(tax_details84,'</TAXCODE>',1,1)-(instr(tax_details84,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE84,
substr(LINE_DETAILS84,instr(LINE_DETAILS84,'<VALUE>',1,3)+7,instr(LINE_DETAILS84,'</VALUE>',1,3)-(instr(LINE_DETAILS84,'<VALUE>',1,3)+7)) quantity_line84,
(substr(LINE_DETAILS84,instr(LINE_DETAILS84,'<VALUE>',1,2)+7,instr(LINE_DETAILS84,'</VALUE>',1,2)-(instr(LINE_DETAILS84,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line84,
(substr(tax_details84,instr(tax_details84,'<VALUE>',1,3)+7,instr(tax_details84,'</VALUE>',1,3)-(instr(tax_details84,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE84,
substr(tax_details84,instr(tax_details84,'<DESCRIPTN>',1,1)+11,instr(tax_details84,'</DESCRIPTN>',1,1)-(instr(tax_details84,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION84,
substr(LINE_DETAILS85,instr(LINE_DETAILS85,'<LINENUM>',1,1)+9,instr(LINE_DETAILS85,'</LINENUM>',1,1)-(instr(LINE_DETAILS85,'<LINENUM>',1,1)+9)) LINE_NUM85,
(substr(LINE_DETAILS85,instr(LINE_DETAILS85,'<VALUE>',1,1)+7,instr(LINE_DETAILS85,'</VALUE>',1,1)-(instr(LINE_DETAILS85,'<VALUE>',1,1)+7)))*.01 LINE85_AMOUNT,
substr(LINE_DETAILS85,instr(LINE_DETAILS85,'<ITEM>',1,1)+6,instr(LINE_DETAILS85,'</ITEM>',1,1)-(instr(LINE_DETAILS85,'<ITEM>',1,1)+6)) ITEM_NAME_LINE85,
substr(LINE_DETAILS85,instr(LINE_DETAILS85,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS85,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS85,'<DESCRIPTN>',1,1)+11)) ITEM_Description85,
(substr(tax_details85,instr(tax_details85,'<VALUE>',1,1)+7,instr(tax_details85,'</VALUE>',1,1)-(instr(tax_details85,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE85,
substr(tax_details85,instr(tax_details85,'<TAXCODE>',1,1)+9,instr(tax_details85,'</TAXCODE>',1,1)-(instr(tax_details85,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE85,
substr(LINE_DETAILS85,instr(LINE_DETAILS85,'<VALUE>',1,3)+7,instr(LINE_DETAILS85,'</VALUE>',1,3)-(instr(LINE_DETAILS85,'<VALUE>',1,3)+7)) quantity_line85,
(substr(LINE_DETAILS85,instr(LINE_DETAILS85,'<VALUE>',1,2)+7,instr(LINE_DETAILS85,'</VALUE>',1,2)-(instr(LINE_DETAILS85,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line85,
(substr(tax_details85,instr(tax_details85,'<VALUE>',1,3)+7,instr(tax_details85,'</VALUE>',1,3)-(instr(tax_details85,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE85,
substr(tax_details85,instr(tax_details85,'<DESCRIPTN>',1,1)+11,instr(tax_details85,'</DESCRIPTN>',1,1)-(instr(tax_details85,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION85,
substr(LINE_DETAILS86,instr(LINE_DETAILS86,'<LINENUM>',1,1)+9,instr(LINE_DETAILS86,'</LINENUM>',1,1)-(instr(LINE_DETAILS86,'<LINENUM>',1,1)+9)) LINE_NUM86,
(substr(LINE_DETAILS86,instr(LINE_DETAILS86,'<VALUE>',1,1)+7,instr(LINE_DETAILS86,'</VALUE>',1,1)-(instr(LINE_DETAILS86,'<VALUE>',1,1)+7)))*.01 LINE86_AMOUNT,
substr(LINE_DETAILS86,instr(LINE_DETAILS86,'<ITEM>',1,1)+6,instr(LINE_DETAILS86,'</ITEM>',1,1)-(instr(LINE_DETAILS86,'<ITEM>',1,1)+6)) ITEM_NAME_LINE86,
substr(LINE_DETAILS86,instr(LINE_DETAILS86,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS86,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS86,'<DESCRIPTN>',1,1)+11)) ITEM_Description86,
(substr(tax_details86,instr(tax_details86,'<VALUE>',1,1)+7,instr(tax_details86,'</VALUE>',1,1)-(instr(tax_details86,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE86,
substr(tax_details86,instr(tax_details86,'<TAXCODE>',1,1)+9,instr(tax_details86,'</TAXCODE>',1,1)-(instr(tax_details86,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE86,
substr(LINE_DETAILS86,instr(LINE_DETAILS86,'<VALUE>',1,3)+7,instr(LINE_DETAILS86,'</VALUE>',1,3)-(instr(LINE_DETAILS86,'<VALUE>',1,3)+7)) quantity_line86,
(substr(LINE_DETAILS86,instr(LINE_DETAILS86,'<VALUE>',1,2)+7,instr(LINE_DETAILS86,'</VALUE>',1,2)-(instr(LINE_DETAILS86,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line86,
(substr(tax_details86,instr(tax_details86,'<VALUE>',1,3)+7,instr(tax_details86,'</VALUE>',1,3)-(instr(tax_details86,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE86,
substr(tax_details86,instr(tax_details86,'<DESCRIPTN>',1,1)+11,instr(tax_details86,'</DESCRIPTN>',1,1)-(instr(tax_details86,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION86,
substr(LINE_DETAILS87,instr(LINE_DETAILS87,'<LINENUM>',1,1)+9,instr(LINE_DETAILS87,'</LINENUM>',1,1)-(instr(LINE_DETAILS87,'<LINENUM>',1,1)+9)) LINE_NUM87,
(substr(LINE_DETAILS87,instr(LINE_DETAILS87,'<VALUE>',1,1)+7,instr(LINE_DETAILS87,'</VALUE>',1,1)-(instr(LINE_DETAILS87,'<VALUE>',1,1)+7)))*.01 LINE87_AMOUNT,
substr(LINE_DETAILS87,instr(LINE_DETAILS87,'<ITEM>',1,1)+6,instr(LINE_DETAILS87,'</ITEM>',1,1)-(instr(LINE_DETAILS87,'<ITEM>',1,1)+6)) ITEM_NAME_LINE87,
substr(LINE_DETAILS87,instr(LINE_DETAILS87,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS87,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS87,'<DESCRIPTN>',1,1)+11)) ITEM_Description87,
(substr(tax_details87,instr(tax_details87,'<VALUE>',1,1)+7,instr(tax_details87,'</VALUE>',1,1)-(instr(tax_details87,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE87,
substr(tax_details87,instr(tax_details87,'<TAXCODE>',1,1)+9,instr(tax_details87,'</TAXCODE>',1,1)-(instr(tax_details87,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE87,
substr(LINE_DETAILS87,instr(LINE_DETAILS87,'<VALUE>',1,3)+7,instr(LINE_DETAILS87,'</VALUE>',1,3)-(instr(LINE_DETAILS87,'<VALUE>',1,3)+7)) quantity_line87,
(substr(LINE_DETAILS87,instr(LINE_DETAILS87,'<VALUE>',1,2)+7,instr(LINE_DETAILS87,'</VALUE>',1,2)-(instr(LINE_DETAILS87,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line87,
(substr(tax_details87,instr(tax_details87,'<VALUE>',1,3)+7,instr(tax_details87,'</VALUE>',1,3)-(instr(tax_details87,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE87,
substr(tax_details87,instr(tax_details87,'<DESCRIPTN>',1,1)+11,instr(tax_details87,'</DESCRIPTN>',1,1)-(instr(tax_details87,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION87,
substr(LINE_DETAILS88,instr(LINE_DETAILS88,'<LINENUM>',1,1)+9,instr(LINE_DETAILS88,'</LINENUM>',1,1)-(instr(LINE_DETAILS88,'<LINENUM>',1,1)+9)) LINE_NUM88,
(substr(LINE_DETAILS88,instr(LINE_DETAILS88,'<VALUE>',1,1)+7,instr(LINE_DETAILS88,'</VALUE>',1,1)-(instr(LINE_DETAILS88,'<VALUE>',1,1)+7)))*.01 LINE88_AMOUNT,
substr(LINE_DETAILS88,instr(LINE_DETAILS88,'<ITEM>',1,1)+6,instr(LINE_DETAILS88,'</ITEM>',1,1)-(instr(LINE_DETAILS88,'<ITEM>',1,1)+6)) ITEM_NAME_LINE88,
substr(LINE_DETAILS88,instr(LINE_DETAILS88,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS88,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS88,'<DESCRIPTN>',1,1)+11)) ITEM_Description88,
(substr(tax_details88,instr(tax_details88,'<VALUE>',1,1)+7,instr(tax_details88,'</VALUE>',1,1)-(instr(tax_details88,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE88,
substr(tax_details88,instr(tax_details88,'<TAXCODE>',1,1)+9,instr(tax_details88,'</TAXCODE>',1,1)-(instr(tax_details88,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE88,
substr(LINE_DETAILS88,instr(LINE_DETAILS88,'<VALUE>',1,3)+7,instr(LINE_DETAILS88,'</VALUE>',1,3)-(instr(LINE_DETAILS88,'<VALUE>',1,3)+7)) quantity_line88,
(substr(LINE_DETAILS88,instr(LINE_DETAILS88,'<VALUE>',1,2)+7,instr(LINE_DETAILS88,'</VALUE>',1,2)-(instr(LINE_DETAILS88,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line88,
(substr(tax_details88,instr(tax_details88,'<VALUE>',1,3)+7,instr(tax_details88,'</VALUE>',1,3)-(instr(tax_details88,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE88,
substr(tax_details88,instr(tax_details88,'<DESCRIPTN>',1,1)+11,instr(tax_details88,'</DESCRIPTN>',1,1)-(instr(tax_details88,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION88,
substr(LINE_DETAILS89,instr(LINE_DETAILS89,'<LINENUM>',1,1)+9,instr(LINE_DETAILS89,'</LINENUM>',1,1)-(instr(LINE_DETAILS89,'<LINENUM>',1,1)+9)) LINE_NUM89,
(substr(LINE_DETAILS89,instr(LINE_DETAILS89,'<VALUE>',1,1)+7,instr(LINE_DETAILS89,'</VALUE>',1,1)-(instr(LINE_DETAILS89,'<VALUE>',1,1)+7)))*.01 LINE89_AMOUNT,
substr(LINE_DETAILS89,instr(LINE_DETAILS89,'<ITEM>',1,1)+6,instr(LINE_DETAILS89,'</ITEM>',1,1)-(instr(LINE_DETAILS89,'<ITEM>',1,1)+6)) ITEM_NAME_LINE89,
substr(LINE_DETAILS89,instr(LINE_DETAILS89,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS89,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS89,'<DESCRIPTN>',1,1)+11)) ITEM_Description89,
(substr(tax_details89,instr(tax_details89,'<VALUE>',1,1)+7,instr(tax_details89,'</VALUE>',1,1)-(instr(tax_details89,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE89,
substr(tax_details89,instr(tax_details89,'<TAXCODE>',1,1)+9,instr(tax_details89,'</TAXCODE>',1,1)-(instr(tax_details89,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE89,
substr(LINE_DETAILS89,instr(LINE_DETAILS89,'<VALUE>',1,3)+7,instr(LINE_DETAILS89,'</VALUE>',1,3)-(instr(LINE_DETAILS89,'<VALUE>',1,3)+7)) quantity_line89,
(substr(LINE_DETAILS89,instr(LINE_DETAILS89,'<VALUE>',1,2)+7,instr(LINE_DETAILS89,'</VALUE>',1,2)-(instr(LINE_DETAILS89,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line89,
(substr(tax_details89,instr(tax_details89,'<VALUE>',1,3)+7,instr(tax_details89,'</VALUE>',1,3)-(instr(tax_details89,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE89,
substr(tax_details89,instr(tax_details89,'<DESCRIPTN>',1,1)+11,instr(tax_details89,'</DESCRIPTN>',1,1)-(instr(tax_details89,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION89,
substr(LINE_DETAILS90,instr(LINE_DETAILS90,'<LINENUM>',1,1)+9,instr(LINE_DETAILS90,'</LINENUM>',1,1)-(instr(LINE_DETAILS90,'<LINENUM>',1,1)+9)) LINE_NUM90,
(substr(LINE_DETAILS90,instr(LINE_DETAILS90,'<VALUE>',1,1)+7,instr(LINE_DETAILS90,'</VALUE>',1,1)-(instr(LINE_DETAILS90,'<VALUE>',1,1)+7)))*.01 LINE90_AMOUNT,
substr(LINE_DETAILS90,instr(LINE_DETAILS90,'<ITEM>',1,1)+6,instr(LINE_DETAILS90,'</ITEM>',1,1)-(instr(LINE_DETAILS90,'<ITEM>',1,1)+6)) ITEM_NAME_LINE90,
substr(LINE_DETAILS90,instr(LINE_DETAILS90,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS90,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS90,'<DESCRIPTN>',1,1)+11)) ITEM_Description90,
(substr(tax_details90,instr(tax_details90,'<VALUE>',1,1)+7,instr(tax_details90,'</VALUE>',1,1)-(instr(tax_details90,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE90,
substr(tax_details90,instr(tax_details90,'<TAXCODE>',1,1)+9,instr(tax_details90,'</TAXCODE>',1,1)-(instr(tax_details90,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE90,
substr(LINE_DETAILS90,instr(LINE_DETAILS90,'<VALUE>',1,3)+7,instr(LINE_DETAILS90,'</VALUE>',1,3)-(instr(LINE_DETAILS90,'<VALUE>',1,3)+7)) quantity_line90,
(substr(LINE_DETAILS90,instr(LINE_DETAILS90,'<VALUE>',1,2)+7,instr(LINE_DETAILS90,'</VALUE>',1,2)-(instr(LINE_DETAILS90,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line90,
(substr(tax_details90,instr(tax_details90,'<VALUE>',1,3)+7,instr(tax_details90,'</VALUE>',1,3)-(instr(tax_details90,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE90,
substr(tax_details90,instr(tax_details90,'<DESCRIPTN>',1,1)+11,instr(tax_details90,'</DESCRIPTN>',1,1)-(instr(tax_details90,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION90,
substr(LINE_DETAILS91,instr(LINE_DETAILS91,'<LINENUM>',1,1)+9,instr(LINE_DETAILS91,'</LINENUM>',1,1)-(instr(LINE_DETAILS91,'<LINENUM>',1,1)+9)) LINE_NUM91,
(substr(LINE_DETAILS91,instr(LINE_DETAILS91,'<VALUE>',1,1)+7,instr(LINE_DETAILS91,'</VALUE>',1,1)-(instr(LINE_DETAILS91,'<VALUE>',1,1)+7)))*.01 LINE91_AMOUNT,
substr(LINE_DETAILS91,instr(LINE_DETAILS91,'<ITEM>',1,1)+6,instr(LINE_DETAILS91,'</ITEM>',1,1)-(instr(LINE_DETAILS91,'<ITEM>',1,1)+6)) ITEM_NAME_LINE91,
substr(LINE_DETAILS91,instr(LINE_DETAILS91,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS91,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS91,'<DESCRIPTN>',1,1)+11)) ITEM_Description91,
(substr(tax_details91,instr(tax_details91,'<VALUE>',1,1)+7,instr(tax_details91,'</VALUE>',1,1)-(instr(tax_details91,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE91,
substr(tax_details91,instr(tax_details91,'<TAXCODE>',1,1)+9,instr(tax_details91,'</TAXCODE>',1,1)-(instr(tax_details91,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE91,
substr(LINE_DETAILS91,instr(LINE_DETAILS91,'<VALUE>',1,3)+7,instr(LINE_DETAILS91,'</VALUE>',1,3)-(instr(LINE_DETAILS91,'<VALUE>',1,3)+7)) quantity_line91,
(substr(LINE_DETAILS91,instr(LINE_DETAILS91,'<VALUE>',1,2)+7,instr(LINE_DETAILS91,'</VALUE>',1,2)-(instr(LINE_DETAILS91,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line91,
(substr(tax_details91,instr(tax_details91,'<VALUE>',1,3)+7,instr(tax_details91,'</VALUE>',1,3)-(instr(tax_details91,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE91,
substr(tax_details91,instr(tax_details91,'<DESCRIPTN>',1,1)+11,instr(tax_details91,'</DESCRIPTN>',1,1)-(instr(tax_details91,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION91,
substr(LINE_DETAILS92,instr(LINE_DETAILS92,'<LINENUM>',1,1)+9,instr(LINE_DETAILS92,'</LINENUM>',1,1)-(instr(LINE_DETAILS92,'<LINENUM>',1,1)+9)) LINE_NUM92,
(substr(LINE_DETAILS92,instr(LINE_DETAILS92,'<VALUE>',1,1)+7,instr(LINE_DETAILS92,'</VALUE>',1,1)-(instr(LINE_DETAILS92,'<VALUE>',1,1)+7)))*.01 LINE92_AMOUNT,
substr(LINE_DETAILS92,instr(LINE_DETAILS92,'<ITEM>',1,1)+6,instr(LINE_DETAILS92,'</ITEM>',1,1)-(instr(LINE_DETAILS92,'<ITEM>',1,1)+6)) ITEM_NAME_LINE92,
substr(LINE_DETAILS92,instr(LINE_DETAILS92,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS92,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS92,'<DESCRIPTN>',1,1)+11)) ITEM_Description92,
(substr(tax_details92,instr(tax_details92,'<VALUE>',1,1)+7,instr(tax_details92,'</VALUE>',1,1)-(instr(tax_details92,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE92,
substr(tax_details92,instr(tax_details92,'<TAXCODE>',1,1)+9,instr(tax_details92,'</TAXCODE>',1,1)-(instr(tax_details92,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE92,
substr(LINE_DETAILS92,instr(LINE_DETAILS92,'<VALUE>',1,3)+7,instr(LINE_DETAILS92,'</VALUE>',1,3)-(instr(LINE_DETAILS92,'<VALUE>',1,3)+7)) quantity_line92,
(substr(LINE_DETAILS92,instr(LINE_DETAILS92,'<VALUE>',1,2)+7,instr(LINE_DETAILS92,'</VALUE>',1,2)-(instr(LINE_DETAILS92,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line92,
(substr(tax_details92,instr(tax_details92,'<VALUE>',1,3)+7,instr(tax_details92,'</VALUE>',1,3)-(instr(tax_details92,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE92,
substr(tax_details92,instr(tax_details92,'<DESCRIPTN>',1,1)+11,instr(tax_details92,'</DESCRIPTN>',1,1)-(instr(tax_details92,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION92,
substr(LINE_DETAILS93,instr(LINE_DETAILS93,'<LINENUM>',1,1)+9,instr(LINE_DETAILS93,'</LINENUM>',1,1)-(instr(LINE_DETAILS93,'<LINENUM>',1,1)+9)) LINE_NUM93,
(substr(LINE_DETAILS93,instr(LINE_DETAILS93,'<VALUE>',1,1)+7,instr(LINE_DETAILS93,'</VALUE>',1,1)-(instr(LINE_DETAILS93,'<VALUE>',1,1)+7)))*.01 LINE93_AMOUNT,
substr(LINE_DETAILS93,instr(LINE_DETAILS93,'<ITEM>',1,1)+6,instr(LINE_DETAILS93,'</ITEM>',1,1)-(instr(LINE_DETAILS93,'<ITEM>',1,1)+6)) ITEM_NAME_LINE93,
substr(LINE_DETAILS93,instr(LINE_DETAILS93,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS93,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS93,'<DESCRIPTN>',1,1)+11)) ITEM_Description93,
(substr(tax_details93,instr(tax_details93,'<VALUE>',1,1)+7,instr(tax_details93,'</VALUE>',1,1)-(instr(tax_details93,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE93,
substr(tax_details93,instr(tax_details93,'<TAXCODE>',1,1)+9,instr(tax_details93,'</TAXCODE>',1,1)-(instr(tax_details93,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE93,
substr(LINE_DETAILS93,instr(LINE_DETAILS93,'<VALUE>',1,3)+7,instr(LINE_DETAILS93,'</VALUE>',1,3)-(instr(LINE_DETAILS93,'<VALUE>',1,3)+7)) quantity_line93,
(substr(LINE_DETAILS93,instr(LINE_DETAILS93,'<VALUE>',1,2)+7,instr(LINE_DETAILS93,'</VALUE>',1,2)-(instr(LINE_DETAILS93,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line93,
(substr(tax_details93,instr(tax_details93,'<VALUE>',1,3)+7,instr(tax_details93,'</VALUE>',1,3)-(instr(tax_details93,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE93,
substr(tax_details93,instr(tax_details93,'<DESCRIPTN>',1,1)+11,instr(tax_details93,'</DESCRIPTN>',1,1)-(instr(tax_details93,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION93,
substr(LINE_DETAILS94,instr(LINE_DETAILS94,'<LINENUM>',1,1)+9,instr(LINE_DETAILS94,'</LINENUM>',1,1)-(instr(LINE_DETAILS94,'<LINENUM>',1,1)+9)) LINE_NUM94,
(substr(LINE_DETAILS94,instr(LINE_DETAILS94,'<VALUE>',1,1)+7,instr(LINE_DETAILS94,'</VALUE>',1,1)-(instr(LINE_DETAILS94,'<VALUE>',1,1)+7)))*.01 LINE94_AMOUNT,
substr(LINE_DETAILS94,instr(LINE_DETAILS94,'<ITEM>',1,1)+6,instr(LINE_DETAILS94,'</ITEM>',1,1)-(instr(LINE_DETAILS94,'<ITEM>',1,1)+6)) ITEM_NAME_LINE94,
substr(LINE_DETAILS94,instr(LINE_DETAILS94,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS94,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS94,'<DESCRIPTN>',1,1)+11)) ITEM_Description94,
(substr(tax_details94,instr(tax_details94,'<VALUE>',1,1)+7,instr(tax_details94,'</VALUE>',1,1)-(instr(tax_details94,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE94,
substr(tax_details94,instr(tax_details94,'<TAXCODE>',1,1)+9,instr(tax_details94,'</TAXCODE>',1,1)-(instr(tax_details94,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE94,
substr(LINE_DETAILS94,instr(LINE_DETAILS94,'<VALUE>',1,3)+7,instr(LINE_DETAILS94,'</VALUE>',1,3)-(instr(LINE_DETAILS94,'<VALUE>',1,3)+7)) quantity_line94,
(substr(LINE_DETAILS94,instr(LINE_DETAILS94,'<VALUE>',1,2)+7,instr(LINE_DETAILS94,'</VALUE>',1,2)-(instr(LINE_DETAILS94,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line94,
(substr(tax_details94,instr(tax_details94,'<VALUE>',1,3)+7,instr(tax_details94,'</VALUE>',1,3)-(instr(tax_details94,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE94,
substr(tax_details94,instr(tax_details94,'<DESCRIPTN>',1,1)+11,instr(tax_details94,'</DESCRIPTN>',1,1)-(instr(tax_details94,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION94,
substr(LINE_DETAILS95,instr(LINE_DETAILS95,'<LINENUM>',1,1)+9,instr(LINE_DETAILS95,'</LINENUM>',1,1)-(instr(LINE_DETAILS95,'<LINENUM>',1,1)+9)) LINE_NUM95,
(substr(LINE_DETAILS95,instr(LINE_DETAILS95,'<VALUE>',1,1)+7,instr(LINE_DETAILS95,'</VALUE>',1,1)-(instr(LINE_DETAILS95,'<VALUE>',1,1)+7)))*.01 LINE95_AMOUNT,
substr(LINE_DETAILS95,instr(LINE_DETAILS95,'<ITEM>',1,1)+6,instr(LINE_DETAILS95,'</ITEM>',1,1)-(instr(LINE_DETAILS95,'<ITEM>',1,1)+6)) ITEM_NAME_LINE95,
substr(LINE_DETAILS95,instr(LINE_DETAILS95,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS95,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS95,'<DESCRIPTN>',1,1)+11)) ITEM_Description95,
(substr(tax_details95,instr(tax_details95,'<VALUE>',1,1)+7,instr(tax_details95,'</VALUE>',1,1)-(instr(tax_details95,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE95,
substr(tax_details95,instr(tax_details95,'<TAXCODE>',1,1)+9,instr(tax_details95,'</TAXCODE>',1,1)-(instr(tax_details95,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE95,
substr(LINE_DETAILS95,instr(LINE_DETAILS95,'<VALUE>',1,3)+7,instr(LINE_DETAILS95,'</VALUE>',1,3)-(instr(LINE_DETAILS95,'<VALUE>',1,3)+7)) quantity_line95,
(substr(LINE_DETAILS95,instr(LINE_DETAILS95,'<VALUE>',1,2)+7,instr(LINE_DETAILS95,'</VALUE>',1,2)-(instr(LINE_DETAILS95,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line95,
(substr(tax_details95,instr(tax_details95,'<VALUE>',1,3)+7,instr(tax_details95,'</VALUE>',1,3)-(instr(tax_details95,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE95,
substr(tax_details95,instr(tax_details95,'<DESCRIPTN>',1,1)+11,instr(tax_details95,'</DESCRIPTN>',1,1)-(instr(tax_details95,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION95,
substr(LINE_DETAILS96,instr(LINE_DETAILS96,'<LINENUM>',1,1)+9,instr(LINE_DETAILS96,'</LINENUM>',1,1)-(instr(LINE_DETAILS96,'<LINENUM>',1,1)+9)) LINE_NUM96,
(substr(LINE_DETAILS96,instr(LINE_DETAILS96,'<VALUE>',1,1)+7,instr(LINE_DETAILS96,'</VALUE>',1,1)-(instr(LINE_DETAILS96,'<VALUE>',1,1)+7)))*.01 LINE96_AMOUNT,
substr(LINE_DETAILS96,instr(LINE_DETAILS96,'<ITEM>',1,1)+6,instr(LINE_DETAILS96,'</ITEM>',1,1)-(instr(LINE_DETAILS96,'<ITEM>',1,1)+6)) ITEM_NAME_LINE96,
substr(LINE_DETAILS96,instr(LINE_DETAILS96,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS96,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS96,'<DESCRIPTN>',1,1)+11)) ITEM_Description96,
(substr(tax_details96,instr(tax_details96,'<VALUE>',1,1)+7,instr(tax_details96,'</VALUE>',1,1)-(instr(tax_details96,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE96,
substr(tax_details96,instr(tax_details96,'<TAXCODE>',1,1)+9,instr(tax_details96,'</TAXCODE>',1,1)-(instr(tax_details96,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE96,
substr(LINE_DETAILS96,instr(LINE_DETAILS96,'<VALUE>',1,3)+7,instr(LINE_DETAILS96,'</VALUE>',1,3)-(instr(LINE_DETAILS96,'<VALUE>',1,3)+7)) quantity_line96,
(substr(LINE_DETAILS96,instr(LINE_DETAILS96,'<VALUE>',1,2)+7,instr(LINE_DETAILS96,'</VALUE>',1,2)-(instr(LINE_DETAILS96,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line96,
(substr(tax_details96,instr(tax_details96,'<VALUE>',1,3)+7,instr(tax_details96,'</VALUE>',1,3)-(instr(tax_details96,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE96,
substr(tax_details96,instr(tax_details96,'<DESCRIPTN>',1,1)+11,instr(tax_details96,'</DESCRIPTN>',1,1)-(instr(tax_details96,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION96,
substr(LINE_DETAILS97,instr(LINE_DETAILS97,'<LINENUM>',1,1)+9,instr(LINE_DETAILS97,'</LINENUM>',1,1)-(instr(LINE_DETAILS97,'<LINENUM>',1,1)+9)) LINE_NUM97,
(substr(LINE_DETAILS97,instr(LINE_DETAILS97,'<VALUE>',1,1)+7,instr(LINE_DETAILS97,'</VALUE>',1,1)-(instr(LINE_DETAILS97,'<VALUE>',1,1)+7)))*.01 LINE97_AMOUNT,
substr(LINE_DETAILS97,instr(LINE_DETAILS97,'<ITEM>',1,1)+6,instr(LINE_DETAILS97,'</ITEM>',1,1)-(instr(LINE_DETAILS97,'<ITEM>',1,1)+6)) ITEM_NAME_LINE97,
substr(LINE_DETAILS97,instr(LINE_DETAILS97,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS97,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS97,'<DESCRIPTN>',1,1)+11)) ITEM_Description97,
(substr(tax_details97,instr(tax_details97,'<VALUE>',1,1)+7,instr(tax_details97,'</VALUE>',1,1)-(instr(tax_details97,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE97,
substr(tax_details97,instr(tax_details97,'<TAXCODE>',1,1)+9,instr(tax_details97,'</TAXCODE>',1,1)-(instr(tax_details97,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE97,
substr(LINE_DETAILS97,instr(LINE_DETAILS97,'<VALUE>',1,3)+7,instr(LINE_DETAILS97,'</VALUE>',1,3)-(instr(LINE_DETAILS97,'<VALUE>',1,3)+7)) quantity_line97,
(substr(LINE_DETAILS97,instr(LINE_DETAILS97,'<VALUE>',1,2)+7,instr(LINE_DETAILS97,'</VALUE>',1,2)-(instr(LINE_DETAILS97,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line97,
(substr(tax_details97,instr(tax_details97,'<VALUE>',1,3)+7,instr(tax_details97,'</VALUE>',1,3)-(instr(tax_details97,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE97,
substr(tax_details97,instr(tax_details97,'<DESCRIPTN>',1,1)+11,instr(tax_details97,'</DESCRIPTN>',1,1)-(instr(tax_details97,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION97,
substr(LINE_DETAILS98,instr(LINE_DETAILS98,'<LINENUM>',1,1)+9,instr(LINE_DETAILS98,'</LINENUM>',1,1)-(instr(LINE_DETAILS98,'<LINENUM>',1,1)+9)) LINE_NUM98,
(substr(LINE_DETAILS98,instr(LINE_DETAILS98,'<VALUE>',1,1)+7,instr(LINE_DETAILS98,'</VALUE>',1,1)-(instr(LINE_DETAILS98,'<VALUE>',1,1)+7)))*.01 LINE98_AMOUNT,
substr(LINE_DETAILS98,instr(LINE_DETAILS98,'<ITEM>',1,1)+6,instr(LINE_DETAILS98,'</ITEM>',1,1)-(instr(LINE_DETAILS98,'<ITEM>',1,1)+6)) ITEM_NAME_LINE98,
substr(LINE_DETAILS98,instr(LINE_DETAILS98,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS98,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS98,'<DESCRIPTN>',1,1)+11)) ITEM_Description98,
(substr(tax_details98,instr(tax_details98,'<VALUE>',1,1)+7,instr(tax_details98,'</VALUE>',1,1)-(instr(tax_details98,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE98,
substr(tax_details98,instr(tax_details98,'<TAXCODE>',1,1)+9,instr(tax_details98,'</TAXCODE>',1,1)-(instr(tax_details98,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE98,
substr(LINE_DETAILS98,instr(LINE_DETAILS98,'<VALUE>',1,3)+7,instr(LINE_DETAILS98,'</VALUE>',1,3)-(instr(LINE_DETAILS98,'<VALUE>',1,3)+7)) quantity_line98,
(substr(LINE_DETAILS98,instr(LINE_DETAILS98,'<VALUE>',1,2)+7,instr(LINE_DETAILS98,'</VALUE>',1,2)-(instr(LINE_DETAILS98,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line98,
(substr(tax_details98,instr(tax_details98,'<VALUE>',1,3)+7,instr(tax_details98,'</VALUE>',1,3)-(instr(tax_details98,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE98,
substr(tax_details98,instr(tax_details98,'<DESCRIPTN>',1,1)+11,instr(tax_details98,'</DESCRIPTN>',1,1)-(instr(tax_details98,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION98,
substr(LINE_DETAILS99,instr(LINE_DETAILS99,'<LINENUM>',1,1)+9,instr(LINE_DETAILS99,'</LINENUM>',1,1)-(instr(LINE_DETAILS99,'<LINENUM>',1,1)+9)) LINE_NUM99,
(substr(LINE_DETAILS99,instr(LINE_DETAILS99,'<VALUE>',1,1)+7,instr(LINE_DETAILS99,'</VALUE>',1,1)-(instr(LINE_DETAILS99,'<VALUE>',1,1)+7)))*.01 LINE99_AMOUNT,
substr(LINE_DETAILS99,instr(LINE_DETAILS99,'<ITEM>',1,1)+6,instr(LINE_DETAILS99,'</ITEM>',1,1)-(instr(LINE_DETAILS99,'<ITEM>',1,1)+6)) ITEM_NAME_LINE99,
substr(LINE_DETAILS99,instr(LINE_DETAILS99,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS99,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS99,'<DESCRIPTN>',1,1)+11)) ITEM_Description99,
(substr(tax_details99,instr(tax_details99,'<VALUE>',1,1)+7,instr(tax_details99,'</VALUE>',1,1)-(instr(tax_details99,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE99,
substr(tax_details99,instr(tax_details99,'<TAXCODE>',1,1)+9,instr(tax_details99,'</TAXCODE>',1,1)-(instr(tax_details99,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE99,
substr(LINE_DETAILS99,instr(LINE_DETAILS99,'<VALUE>',1,3)+7,instr(LINE_DETAILS99,'</VALUE>',1,3)-(instr(LINE_DETAILS99,'<VALUE>',1,3)+7)) quantity_line99,
(substr(LINE_DETAILS99,instr(LINE_DETAILS99,'<VALUE>',1,2)+7,instr(LINE_DETAILS99,'</VALUE>',1,2)-(instr(LINE_DETAILS99,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line99,
(substr(tax_details99,instr(tax_details99,'<VALUE>',1,3)+7,instr(tax_details99,'</VALUE>',1,3)-(instr(tax_details99,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE99,
substr(tax_details99,instr(tax_details99,'<DESCRIPTN>',1,1)+11,instr(tax_details99,'</DESCRIPTN>',1,1)-(instr(tax_details99,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION99,
substr(LINE_DETAILS100,instr(LINE_DETAILS100,'<LINENUM>',1,1)+9,instr(LINE_DETAILS100,'</LINENUM>',1,1)-(instr(LINE_DETAILS100,'<LINENUM>',1,1)+9)) LINE_NUM100,
(substr(LINE_DETAILS100,instr(LINE_DETAILS100,'<VALUE>',1,1)+7,instr(LINE_DETAILS100,'</VALUE>',1,1)-(instr(LINE_DETAILS100,'<VALUE>',1,1)+7)))*.01 LINE100_AMOUNT,
substr(LINE_DETAILS100,instr(LINE_DETAILS100,'<ITEM>',1,1)+6,instr(LINE_DETAILS100,'</ITEM>',1,1)-(instr(LINE_DETAILS100,'<ITEM>',1,1)+6)) ITEM_NAME_LINE100,
substr(LINE_DETAILS100,instr(LINE_DETAILS100,'<DESCRIPTN>',1,1)+11,instr(LINE_DETAILS100,'</DESCRIPTN>',1,1)-(instr(LINE_DETAILS100,'<DESCRIPTN>',1,1)+11)) ITEM_Description100,
(substr(tax_details100,instr(tax_details100,'<VALUE>',1,1)+7,instr(tax_details100,'</VALUE>',1,1)-(instr(tax_details100,'<VALUE>',1,1)+7)))*.01 TAX_AMOUNT_LINE100,
substr(tax_details100,instr(tax_details100,'<TAXCODE>',1,1)+9,instr(tax_details100,'</TAXCODE>',1,1)-(instr(tax_details100,'<TAXCODE>',1,1)+9)) TAX_CODE_LINE100,
substr(LINE_DETAILS100,instr(LINE_DETAILS100,'<VALUE>',1,3)+7,instr(LINE_DETAILS100,'</VALUE>',1,3)-(instr(LINE_DETAILS100,'<VALUE>',1,3)+7)) quantity_line100,
(substr(LINE_DETAILS100,instr(LINE_DETAILS100,'<VALUE>',1,2)+7,instr(LINE_DETAILS100,'</VALUE>',1,2)-(instr(LINE_DETAILS100,'<VALUE>',1,2)+7)))*.01 Unit_Price_Line100,
(substr(tax_details100,instr(tax_details100,'<VALUE>',1,3)+7,instr(tax_details100,'</VALUE>',1,3)-(instr(tax_details100,'<VALUE>',1,3)+7))*.1) TAX_PERCENTAGE100,
substr(tax_details100,instr(tax_details100,'<DESCRIPTN>',1,1)+11,instr(tax_details100,'</DESCRIPTN>',1,1)-(instr(tax_details100,'<DESCRIPTN>',1,1)+11)) VAT_DESCRIPTION100
From CCS_XML_Staging_Table;
--------------------------------------------------------------------------------------------------------------------------------------------
cursor p1 is
select
dbms_lob.substr(substr(payload,instr(payload,'<PARTNER>',1,1),(instr(payload,'</PARTNER>',1,1)-instr(payload,'<PARTNER>',1,1)))) Supplier_Details,
dbms_lob.substr(substr(payload,instr(payload,'<PARTNER>',1,2),(instr(payload,'</PARTNER>',1,2)-instr(payload,'<PARTNER>',1,2)))) SOLD_TO_DETAILS,
dbms_lob.substr(substr(payload,instr(payload,'<PARTNER>',1,4),(instr(payload,'</PARTNER>',1,4)-instr(payload,'<PARTNER>',1,4)))) BILL_TO_DETAILS,
dbms_lob.substr(substr(payload,instr(payload,'<DOCUMENTID>',1)+12,instr(payload,'</DOCUMENTID>',1)-(instr(payload,'<DOCUMENTID>',1)+12))) INVOICE_NUMBER
from ecx_doclogs where transaction_type='INVOICE'AND
dbms_lob.substr(substr(payload,instr(payload,'<DOCUMENTID>',1)+12,instr(payload,'</DOCUMENTID>',1)-(instr(payload,'<DOCUMENTID>',1)+12)))=T_Invoice  AND
dbms_lob.substr(substr(payload,instr(payload,'<NAME index="1">',1)+16,instr(payload,'</NAME>',1)-(instr(payload,'<NAME index="1">',1)+16)))=T_Supplier;
------------------------------------------------------------------------------------------------------------------------------------
cursor p3 is
select invoice_number,
substr(Supplier_details,instr(Supplier_details,'<NAME index="1">',1,1)+16,instr(Supplier_details,'</NAME>',1,1)-(instr(Supplier_details,'<NAME index="1">',1,1)+16)) SUPPLIER_NAME,
substr(Supplier_details,instr(Supplier_details,'<ADDRLINE index="1">',1,1)+20,instr(Supplier_details,'</ADDRLINE>',1,1)-(instr(Supplier_details,'<ADDRLINE index="1">',1,1)+20)) SUPPLIER_ADDRESS,
substr(Supplier_details,instr(Supplier_details,'<CITY>',1,1)+6,instr(Supplier_details,'</CITY>',1,1)-(instr(Supplier_details,'<CITY>',1,1)+6)) SUPPLIER_CITY,
substr(Supplier_details,instr(Supplier_details,'<COUNTRY>',1,1)+9,instr(Supplier_details,'</COUNTRY>',1,1)-(instr(Supplier_details,'<COUNTRY>',1,1)+9)) SUPPLIER_COUNTRY,
substr(Supplier_details,instr(Supplier_details,'<STATEPROVN>',1,1)+12,instr(Supplier_details,'</STATEPROVN>',1,1)-(instr(Supplier_details,'<STATEPROVN>',1,1)+12)) SUPPLIER_STATE_PROVINCE,
substr(Supplier_details,instr(Supplier_details,'<POSTALCODE>',1,1)+12,instr(Supplier_details,'</POSTALCODE>',1,1)-(instr(Supplier_details,'<POSTALCODE>',1,1)+12)) SUPPLIER_POSTAL_CODE,
substr(Supplier_details,instr(Supplier_details,'<NAME index="1">',1,2)+16,instr(Supplier_details,'</NAME>',1,2)-(instr(Supplier_details,'<NAME index="1">',1,2)+16)) SUPPLIER_CONTACT_NAME,
substr(Supplier_details,instr(Supplier_details,'<EMAIL>',1,1)+7,instr(Supplier_details,'</EMAIL>',1,1)-(instr(Supplier_details,'<EMAIL>',1,1)+7)) SUPPLIER_EMAIL_ADDRESS,
substr(sold_to_details,instr(sold_to_details,'<DUNSNUMBER>',1,1)+12,instr(sold_to_details,'</DUNSNUMBER>',1,1)-(instr(sold_to_details,'<DUNSNUMBER>',1,1)+12)) SOLD_TO_DUNS_NUMBER,
substr(sold_to_details,instr(sold_to_details,'<NAME index="1">',1,1)+16,instr(sold_to_details,'</NAME>',1,1)-(instr(sold_to_details,'<NAME index="1">',1,1)+16)) SOLD_TO_NAME,
substr(sold_to_details,instr(sold_to_details,'<EMAIL>',1,1)+7,instr(sold_to_details,'</EMAIL>',1,1)-(instr(sold_to_details,'<EMAIL>',1,1)+7)) SOLD_TO_EMAIL,
substr(sold_to_details,instr(sold_to_details,'<TELEPHONE index="1">',1,1)+21,instr(sold_to_details,'</TELEPHONE>',1,1)-(instr(sold_to_details,'<TELEPHONE index="1">',1,1)+21)) SOLD_TO_TELEPHONE_INDEX,
substr(bill_to_details,instr(bill_to_details,'<NAME index="1">',1,1)+16,instr(bill_to_details,'</NAME>',1,1)-(instr(bill_to_details,'<NAME index="1">',1,1)+16)) BILL_TO_NAME,
substr(bill_to_details,instr(bill_to_details,'<ADDRLINE index="1">',1,1)+20,instr(bill_to_details,'</ADDRLINE>',1,1)-(instr(bill_to_details,'<ADDRLINE index="1">',1,1)+20)) BILL_TO_ADDRESS1,
substr(bill_to_details,instr(bill_to_details,'<ADDRLINE index="2">',1,1)+20,instr(bill_to_details,'</ADDRLINE>',1,2)-(instr(bill_to_details,'<ADDRLINE index="2">',1,1)+20)) BILL_TO_ADDRESS2,
substr(bill_to_details,instr(bill_to_details,'<CITY>',1,1)+6,instr(bill_to_details,'</CITY>',1,1)-(instr(bill_to_details,'<CITY>',1,1)+6)) BILL_TO_CITY,
substr(bill_to_details,instr(bill_to_details,'<POSTALCODE>',1,1)+12,instr(bill_to_details,'</POSTALCODE>',1,1)-(instr(bill_to_details,'<POSTALCODE>',1,1)+12)) BILL_TO_POSTAL_CODE,
substr(bill_to_details,instr(bill_to_details,'<TELEPHONE index="1">',1,1)+21,instr(bill_to_details,'</TELEPHONE>',1,1)-(instr(bill_to_details,'<TELEPHONE index="1">',1,1)+21)) BILL_TO_TELEPHONE
from CCS_XML_SEPERATE_HEADER_TABLE;
-----------------------------------------------------------------------------------------------------------------------------------
begin
delete from CCS_XML_Header_Table;
delete from CCS_XML_Staging_Table;
delete from CCS_XML_LINE_TAX_TABLE;
delete from CCS_XML_SEPERATE_HEADER_TABLE;
delete from CCS_XML_DETAIL_TABLE;
-----------------------------------------------------------------------------------------------------------------------------
for p2 in p1
loop
insert into CCS_XML_SEPERATE_HEADER_TABLE values(p2.Supplier_Details,p2.SOLD_TO_DETAILS,p2.BILL_TO_DETAILS,p2.INVOICE_NUMBER);
end loop;
----------------------------------------------------------------------------------------------------------------------------
for p4 in p3
loop
insert into CCS_XML_DETAIL_TABLE values(p4.invoice_number,p4.SUPPLIER_NAME,p4.supplier_address,p4.SUPPLIER_CITY,p4.SUPPLIER_COUNTRY,
p4.SUPPLIER_STATE_PROVINCE,p4.SUPPLIER_POSTAL_CODE,p4.SUPPLIER_CONTACT_NAME,p4.SUPPLIER_EMAIL_ADDRESS,
p4.SOLD_TO_DUNS_NUMBER,p4.SOLD_TO_NAME,p4.SOLD_TO_EMAIL,p4.SOLD_TO_TELEPHONE_INDEX,p4.BILL_TO_NAME,
p4.BILL_TO_ADDRESS1,p4.BILL_TO_ADDRESS2,p4.BILL_TO_CITY,p4.BILL_TO_POSTAL_CODE,p4.BILL_TO_TELEPHONE);
end loop;
----------------------------------------------------------------------------------------------------------------------------
for c2 in c1
loop
insert into CCS_XML_Header_Table Values(c2.SENDER_LOGIVALID,c2.SENDER_COMPONENT,c2.SENDER_TASK,c2.REFERENCE_ID,c2.INVOICE_CREATION_DATE,c2.CONFIRMATION_IDENTIFIER,c2.INVOICE_NUMBER,c2.INVOICE_DESCRIPTION,
c2.Document_type,c2.Reason_Code,c2.Supplier_Name,c2.PARTNER_TYPE,c2.DUNS_NUMBER_SUPPLIER,c2.PARTNER_ID_SUPPLIER,c2.TAX_ID_SUPPLIER,
c2.SUPPLIER_ADDRESS1,
c2.SUPPLIER_CITY,c2.SUPPLIER_COUNTRY,c2.SUPPLIER_POSTAL_CODE,c2.SUPPLIER_STATE_PROVINCE,c2.SUPPLIER_TELEPHONE_INDEX,c2.SUPPLIER_CONTACT_NAME,
c2.SUPPLIER_EMAIL_ADDRESS,c2.SUPPLIER_CONT_TELEPHONE,c2.SOLD_TO_NAME,c2.SOLD_TO_PARTNER_TYPE,c2.SOLD_TO_DUNS_NUMBER,c2.SOLD_TO_CONTACT_EMAIL,
c2.SOLD_TO_TELEPHONE_INDEX,c2.REMIT_TO_NAME,c2.REMIT_TO_PARTNER_TYPE,c2.REMIT_TO_DUNS_NUMBER,c2.REMIT_TO_TAXID,c2.REMIT_TO_CONTACT_NAME,
c2.REMIT_TO_CONTACT_EMAIL,c2.BILL_TO_NAME,c2.BILL_TO_PARTNERTYPE,c2.BILL_TO_DUNSNUMBER,c2.BILL_TO_PARTNERID,c2.DOCUMENT_REFERENCE_TYPE,
c2.DOCUMENT_NUMBER,c2.BILL_TO_ADDRESS1,c2.BILL_TO_ADDRESS2,c2.BILL_TO_CITY,c2.BILL_TO_POSTAL_CODE,c2.BILL_TO_FAX,c2.CURRENCY);
end loop;
--------------------------------------------------------------------------------------------------------------------
for c4 in c3
loop
insert into CCS_XML_Staging_Table values(c4.INVOICE_NUMBER,c4.LINE_DETAILS1,c4.LINE_DETAILS2,c4.LINE_DETAILS3,c4.LINE_DETAILS4,c4.LINE_DETAILS5,
c4.LINE_DETAILS6,c4.LINE_DETAILS7,c4.LINE_DETAILS8,c4.LINE_DETAILS9,c4.LINE_DETAILS10,c4.LINE_DETAILS11,c4.LINE_DETAILS12,c4.LINE_DETAILS13,
c4.LINE_DETAILS14,c4.LINE_DETAILS15,c4.LINE_DETAILS16,c4.LINE_DETAILS17,c4.LINE_DETAILS18,c4.LINE_DETAILS19,c4.LINE_DETAILS20,c4.LINE_DETAILS21,
c4.LINE_DETAILS22,c4.LINE_DETAILS23,c4.LINE_DETAILS24,c4.LINE_DETAILS25,c4.LINE_DETAILS26,c4.LINE_DETAILS27,c4.LINE_DETAILS28,c4.LINE_DETAILS29,
c4.LINE_DETAILS30,c4.LINE_DETAILS31,c4.LINE_DETAILS32,c4.LINE_DETAILS33,c4.LINE_DETAILS34,c4.LINE_DETAILS35,c4.LINE_DETAILS36,c4.LINE_DETAILS37,
c4.LINE_DETAILS38,c4.LINE_DETAILS39,c4.LINE_DETAILS40,c4.LINE_DETAILS41,c4.LINE_DETAILS42,c4.LINE_DETAILS43,c4.LINE_DETAILS44,c4.LINE_DETAILS45,
c4.LINE_DETAILS46,c4.LINE_DETAILS47,c4.LINE_DETAILS48,c4.LINE_DETAILS49,c4.LINE_DETAILS50,c4.LINE_DETAILS51,c4.LINE_DETAILS52,c4.LINE_DETAILS53,
c4.LINE_DETAILS54,c4.LINE_DETAILS55,c4.LINE_DETAILS56,c4.LINE_DETAILS57,c4.LINE_DETAILS58,c4.LINE_DETAILS59,c4.LINE_DETAILS60,c4.LINE_DETAILS61,
c4.LINE_DETAILS62,c4.LINE_DETAILS63,c4.LINE_DETAILS64,c4.LINE_DETAILS65,c4.LINE_DETAILS66,c4.LINE_DETAILS67,c4.LINE_DETAILS68,c4.LINE_DETAILS69,
c4.LINE_DETAILS70,c4.LINE_DETAILS71,c4.LINE_DETAILS72,c4.LINE_DETAILS73,c4.LINE_DETAILS74,c4.LINE_DETAILS75,c4.LINE_DETAILS76,c4.LINE_DETAILS77,
c4.LINE_DETAILS78,c4.LINE_DETAILS79,c4.LINE_DETAILS80,c4.LINE_DETAILS81,c4.LINE_DETAILS82,c4.LINE_DETAILS83,c4.LINE_DETAILS84,c4.LINE_DETAILS85,
c4.LINE_DETAILS86,c4.LINE_DETAILS87,c4.LINE_DETAILS88,c4.LINE_DETAILS89,c4.LINE_DETAILS90,c4.LINE_DETAILS91,c4.LINE_DETAILS92,c4.LINE_DETAILS93,
c4.LINE_DETAILS94,c4.LINE_DETAILS95,c4.LINE_DETAILS96,c4.LINE_DETAILS97,c4.LINE_DETAILS98,c4.LINE_DETAILS99,c4.LINE_DETAILS100,c4.TAX_DETAILS1,
c4.TAX_DETAILS2,c4.TAX_DETAILS3,c4.TAX_DETAILS4,c4.TAX_DETAILS5,c4.TAX_DETAILS6,c4.TAX_DETAILS7,c4.TAX_DETAILS8,c4.TAX_DETAILS9,c4.TAX_DETAILS10,
c4.TAX_DETAILS11,c4.TAX_DETAILS12,c4.TAX_DETAILS13,c4.TAX_DETAILS14,c4.TAX_DETAILS15,c4.TAX_DETAILS16,c4.TAX_DETAILS17,c4.TAX_DETAILS18,c4.TAX_DETAILS19,
c4.TAX_DETAILS20,c4.TAX_DETAILS21,c4.TAX_DETAILS22,c4.TAX_DETAILS23,c4.TAX_DETAILS24,c4.TAX_DETAILS25,c4.TAX_DETAILS26,c4.TAX_DETAILS27,c4.TAX_DETAILS28,
c4.TAX_DETAILS29,c4.TAX_DETAILS30,c4.TAX_DETAILS31,c4.TAX_DETAILS32,c4.TAX_DETAILS33,c4.TAX_DETAILS34,c4.TAX_DETAILS35,c4.TAX_DETAILS36,c4.TAX_DETAILS37,
c4.TAX_DETAILS38,c4.TAX_DETAILS39,c4.TAX_DETAILS40,c4.TAX_DETAILS41,c4.TAX_DETAILS42,c4.TAX_DETAILS43,c4.TAX_DETAILS44,c4.TAX_DETAILS45,
c4.TAX_DETAILS46,c4.TAX_DETAILS47,c4.TAX_DETAILS48,c4.TAX_DETAILS49,c4.TAX_DETAILS50,c4.TAX_DETAILS51,c4.TAX_DETAILS52,c4.TAX_DETAILS53,
c4.TAX_DETAILS54,c4.TAX_DETAILS55,c4.TAX_DETAILS56,c4.TAX_DETAILS57,c4.TAX_DETAILS58,c4.TAX_DETAILS59,c4.TAX_DETAILS60,c4.TAX_DETAILS61,
c4.TAX_DETAILS62,c4.TAX_DETAILS63,c4.TAX_DETAILS64,c4.TAX_DETAILS65,c4.TAX_DETAILS66,c4.TAX_DETAILS67,c4.TAX_DETAILS68,c4.TAX_DETAILS69,
c4.TAX_DETAILS70,c4.TAX_DETAILS71,c4.TAX_DETAILS72,c4.TAX_DETAILS73,c4.TAX_DETAILS74,c4.TAX_DETAILS75,c4.TAX_DETAILS76,c4.TAX_DETAILS77,
c4.TAX_DETAILS78,c4.TAX_DETAILS79,c4.TAX_DETAILS80,c4.TAX_DETAILS81,c4.TAX_DETAILS82,c4.TAX_DETAILS83,c4.TAX_DETAILS84,c4.TAX_DETAILS85,
c4.TAX_DETAILS86,c4.TAX_DETAILS87,c4.TAX_DETAILS88,c4.TAX_DETAILS89,c4.TAX_DETAILS90,c4.TAX_DETAILS91,c4.TAX_DETAILS92,c4.TAX_DETAILS93,
c4.TAX_DETAILS94,c4.TAX_DETAILS95,c4.TAX_DETAILS96,c4.TAX_DETAILS97,c4.TAX_DETAILS98,c4.TAX_DETAILS99,c4.TAX_DETAILS100);
end loop;
---------------------------------------------------------------------------------------------------------------------------
for c6 in c5
loop
-----------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num1 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num1,c6.line1_amount,c6.item_name_line1,c6.item_description1,c6.tax_amount_line1,c6.tax_code_line1,c6.quantity_line1,c6.unit_price_line1,c6.tax_percentage1,c6.VAT_DESCRIPTION1);
end if;
-----------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num2 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num2,c6.line2_amount,c6.item_name_line2,c6.item_description2,c6.tax_amount_line2,c6.tax_code_line2,c6.quantity_line2,c6.unit_price_line2,c6.tax_percentage2,c6.VAT_DESCRIPTION2);
end if;
-----------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num3 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num3,c6.line3_amount,c6.item_name_line3,c6.item_description3,c6.tax_amount_line3,c6.tax_code_line3,c6.quantity_line3,c6.unit_price_line3,c6.tax_percentage3,c6.VAT_DESCRIPTION3);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num4 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num4,c6.line4_amount,c6.item_name_line4,c6.item_description4,c6.tax_amount_line4,c6.tax_code_line4,c6.quantity_line4,c6.unit_price_line4,c6.tax_percentage4,c6.VAT_DESCRIPTION4);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num5 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num5,c6.line5_amount,c6.item_name_line5,c6.item_description5,c6.tax_amount_line5,c6.tax_code_line5,c6.quantity_line5,c6.unit_price_line5,c6.tax_percentage5,c6.VAT_DESCRIPTION5);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num6 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num6,c6.line6_amount,c6.item_name_line6,c6.item_description6,c6.tax_amount_line6,c6.tax_code_line6,c6.quantity_line6,c6.unit_price_line6,c6.tax_percentage6,c6.VAT_DESCRIPTION6);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num7 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num7,c6.line7_amount,c6.item_name_line7,c6.item_description7,c6.tax_amount_line7,c6.tax_code_line7,c6.quantity_line7,c6.unit_price_line7,c6.tax_percentage7,c6.VAT_DESCRIPTION7);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num8 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num8,c6.line8_amount,c6.item_name_line8,c6.item_description8,c6.tax_amount_line8,c6.tax_code_line8,c6.quantity_line8,c6.unit_price_line8,c6.tax_percentage8,c6.VAT_DESCRIPTION8);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num9 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num9,c6.line9_amount,c6.item_name_line9,c6.item_description9,c6.tax_amount_line9,c6.tax_code_line9,c6.quantity_line9,c6.unit_price_line9,c6.tax_percentage9,c6.VAT_DESCRIPTION9);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num10 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num10,c6.line10_amount,c6.item_name_line10,c6.item_description10,c6.tax_amount_line10,c6.tax_code_line10,c6.quantity_line10,c6.unit_price_line10,c6.tax_percentage10,c6.VAT_DESCRIPTION10);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num11 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num11,c6.line11_amount,c6.item_name_line11,c6.item_description11,c6.tax_amount_line11,c6.tax_code_line11,c6.quantity_line11,c6.unit_price_line11,c6.tax_percentage11,c6.VAT_DESCRIPTION11);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num12 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num12,c6.line12_amount,c6.item_name_line12,c6.item_description12,c6.tax_amount_line12,c6.tax_code_line12,c6.quantity_line12,c6.unit_price_line12,c6.tax_percentage12,c6.VAT_DESCRIPTION12);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num13 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num13,c6.line13_amount,c6.item_name_line13,c6.item_description13,c6.tax_amount_line13,c6.tax_code_line13,c6.quantity_line13,c6.unit_price_line13,c6.tax_percentage13,c6.VAT_DESCRIPTION13);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num14 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num14,c6.line14_amount,c6.item_name_line14,c6.item_description14,c6.tax_amount_line14,c6.tax_code_line14,c6.quantity_line14,c6.unit_price_line14,c6.tax_percentage14,c6.VAT_DESCRIPTION14);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num15 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num15,c6.line15_amount,c6.item_name_line15,c6.item_description15,c6.tax_amount_line15,c6.tax_code_line15,c6.quantity_line15,c6.unit_price_line15,c6.tax_percentage15,c6.VAT_DESCRIPTION15);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num16 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num16,c6.line16_amount,c6.item_name_line16,c6.item_description16,c6.tax_amount_line16,c6.tax_code_line16,c6.quantity_line16,c6.unit_price_line16,c6.tax_percentage16,c6.VAT_DESCRIPTION16);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num17 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num17,c6.line17_amount,c6.item_name_line17,c6.item_description17,c6.tax_amount_line17,c6.tax_code_line17,c6.quantity_line17,c6.unit_price_line17,c6.tax_percentage17,c6.VAT_DESCRIPTION17);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num18 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num18,c6.line18_amount,c6.item_name_line18,c6.item_description18,c6.tax_amount_line18,c6.tax_code_line18,c6.quantity_line18,c6.unit_price_line18,c6.tax_percentage18,c6.VAT_DESCRIPTION18);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num19 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num19,c6.line19_amount,c6.item_name_line19,c6.item_description19,c6.tax_amount_line19,c6.tax_code_line19,c6.quantity_line19,c6.unit_price_line19,c6.tax_percentage19,c6.VAT_DESCRIPTION19);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num20 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num20,c6.line20_amount,c6.item_name_line20,c6.item_description20,c6.tax_amount_line20,c6.tax_code_line20,c6.quantity_line20,c6.unit_price_line20,c6.tax_percentage20,c6.VAT_DESCRIPTION20);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num21 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num21,c6.line21_amount,c6.item_name_line21,c6.item_description21,c6.tax_amount_line21,c6.tax_code_line21,c6.quantity_line21,c6.unit_price_line21,c6.tax_percentage21,c6.VAT_DESCRIPTION21);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num22 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num22,c6.line22_amount,c6.item_name_line22,c6.item_description22,c6.tax_amount_line22,c6.tax_code_line22,c6.quantity_line22,c6.unit_price_line22,c6.tax_percentage22,c6.VAT_DESCRIPTION22);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num23 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num23,c6.line23_amount,c6.item_name_line23,c6.item_description23,c6.tax_amount_line23,c6.tax_code_line23,c6.quantity_line23,c6.unit_price_line23,c6.tax_percentage23,c6.VAT_DESCRIPTION23);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num24 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num24,c6.line24_amount,c6.item_name_line24,c6.item_description24,c6.tax_amount_line24,c6.tax_code_line24,c6.quantity_line24,c6.unit_price_line24,c6.tax_percentage24,c6.VAT_DESCRIPTION24);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num25 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num25,c6.line25_amount,c6.item_name_line25,c6.item_description25,c6.tax_amount_line25,c6.tax_code_line25,c6.quantity_line25,c6.unit_price_line25,c6.tax_percentage25,c6.VAT_DESCRIPTION25);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num26 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num26,c6.line26_amount,c6.item_name_line26,c6.item_description26,c6.tax_amount_line26,c6.tax_code_line26,c6.quantity_line26,c6.unit_price_line26,c6.tax_percentage26,c6.VAT_DESCRIPTION26);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num27 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num27,c6.line27_amount,c6.item_name_line27,c6.item_description27,c6.tax_amount_line27,c6.tax_code_line27,c6.quantity_line27,c6.unit_price_line27,c6.tax_percentage27,c6.VAT_DESCRIPTION27);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num28 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num28,c6.line28_amount,c6.item_name_line28,c6.item_description28,c6.tax_amount_line28,c6.tax_code_line28,c6.quantity_line28,c6.unit_price_line28,c6.tax_percentage28,c6.VAT_DESCRIPTION28);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num29 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num29,c6.line29_amount,c6.item_name_line29,c6.item_description29,c6.tax_amount_line29,c6.tax_code_line29,c6.quantity_line29,c6.unit_price_line29,c6.tax_percentage29,c6.VAT_DESCRIPTION29);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num30 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num30,c6.line30_amount,c6.item_name_line30,c6.item_description30,c6.tax_amount_line30,c6.tax_code_line30,c6.quantity_line30,c6.unit_price_line30,c6.tax_percentage30,c6.VAT_DESCRIPTION30);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num31 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num31,c6.line31_amount,c6.item_name_line31,c6.item_description31,c6.tax_amount_line31,c6.tax_code_line31,c6.quantity_line31,c6.unit_price_line31,c6.tax_percentage31,c6.VAT_DESCRIPTION31);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num32 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num32,c6.line32_amount,c6.item_name_line32,c6.item_description32,c6.tax_amount_line32,c6.tax_code_line32,c6.quantity_line32,c6.unit_price_line32,c6.tax_percentage32,c6.VAT_DESCRIPTION32);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num33 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num33,c6.line33_amount,c6.item_name_line33,c6.item_description33,c6.tax_amount_line33,c6.tax_code_line33,c6.quantity_line33,c6.unit_price_line33,c6.tax_percentage33,c6.VAT_DESCRIPTION33);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num34 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num34,c6.line34_amount,c6.item_name_line34,c6.item_description34,c6.tax_amount_line34,c6.tax_code_line34,c6.quantity_line34,c6.unit_price_line34,c6.tax_percentage34,c6.VAT_DESCRIPTION34);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num35 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num35,c6.line35_amount,c6.item_name_line35,c6.item_description35,c6.tax_amount_line35,c6.tax_code_line35,c6.quantity_line35,c6.unit_price_line35,c6.tax_percentage35,c6.VAT_DESCRIPTION35);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num36 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num36,c6.line36_amount,c6.item_name_line36,c6.item_description36,c6.tax_amount_line36,c6.tax_code_line36,c6.quantity_line36,c6.unit_price_line36,c6.tax_percentage36,c6.VAT_DESCRIPTION36);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num37 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num37,c6.line37_amount,c6.item_name_line37,c6.item_description37,c6.tax_amount_line37,c6.tax_code_line37,c6.quantity_line37,c6.unit_price_line37,c6.tax_percentage37,c6.VAT_DESCRIPTION37);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num38 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num38,c6.line38_amount,c6.item_name_line38,c6.item_description38,c6.tax_amount_line38,c6.tax_code_line38,c6.quantity_line38,c6.unit_price_line38,c6.tax_percentage38,c6.VAT_DESCRIPTION38);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num39 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num39,c6.line39_amount,c6.item_name_line39,c6.item_description39,c6.tax_amount_line39,c6.tax_code_line39,c6.quantity_line39,c6.unit_price_line39,c6.tax_percentage39,c6.VAT_DESCRIPTION39);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num40 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num40,c6.line40_amount,c6.item_name_line40,c6.item_description40,c6.tax_amount_line40,c6.tax_code_line40,c6.quantity_line40,c6.unit_price_line40,c6.tax_percentage40,c6.VAT_DESCRIPTION40);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num41 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num41,c6.line41_amount,c6.item_name_line41,c6.item_description41,c6.tax_amount_line41,c6.tax_code_line41,c6.quantity_line41,c6.unit_price_line41,c6.tax_percentage41,c6.VAT_DESCRIPTION41);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num42 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num42,c6.line42_amount,c6.item_name_line42,c6.item_description42,c6.tax_amount_line42,c6.tax_code_line42,c6.quantity_line42,c6.unit_price_line42,c6.tax_percentage42,c6.VAT_DESCRIPTION42);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num43 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num43,c6.line43_amount,c6.item_name_line43,c6.item_description43,c6.tax_amount_line43,c6.tax_code_line43,c6.quantity_line43,c6.unit_price_line43,c6.tax_percentage43,c6.VAT_DESCRIPTION43);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num44 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num44,c6.line44_amount,c6.item_name_line44,c6.item_description44,c6.tax_amount_line44,c6.tax_code_line44,c6.quantity_line44,c6.unit_price_line44,c6.tax_percentage44,c6.VAT_DESCRIPTION44);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num45 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num45,c6.line45_amount,c6.item_name_line45,c6.item_description45,c6.tax_amount_line45,c6.tax_code_line45,c6.quantity_line45,c6.unit_price_line45,c6.tax_percentage45,c6.VAT_DESCRIPTION45);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num46 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num46,c6.line46_amount,c6.item_name_line46,c6.item_description46,c6.tax_amount_line46,c6.tax_code_line46,c6.quantity_line46,c6.unit_price_line46,c6.tax_percentage46,c6.VAT_DESCRIPTION46);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num47 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num47,c6.line47_amount,c6.item_name_line47,c6.item_description47,c6.tax_amount_line47,c6.tax_code_line47,c6.quantity_line47,c6.unit_price_line47,c6.tax_percentage47,c6.VAT_DESCRIPTION47);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num48 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num48,c6.line48_amount,c6.item_name_line48,c6.item_description48,c6.tax_amount_line48,c6.tax_code_line48,c6.quantity_line48,c6.unit_price_line48,c6.tax_percentage48,c6.VAT_DESCRIPTION48);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num49 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num49,c6.line49_amount,c6.item_name_line49,c6.item_description49,c6.tax_amount_line49,c6.tax_code_line49,c6.quantity_line49,c6.unit_price_line49,c6.tax_percentage49,c6.VAT_DESCRIPTION49);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num50 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num50,c6.line50_amount,c6.item_name_line50,c6.item_description50,c6.tax_amount_line50,c6.tax_code_line50,c6.quantity_line50,c6.unit_price_line50,c6.tax_percentage50,c6.VAT_DESCRIPTION50);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num51 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num51,c6.line51_amount,c6.item_name_line51,c6.item_description51,c6.tax_amount_line51,c6.tax_code_line51,c6.quantity_line51,c6.unit_price_line51,c6.tax_percentage51,c6.VAT_DESCRIPTION51);
end if;
--------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num52 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num52,c6.line52_amount,c6.item_name_line52,c6.item_description52,c6.tax_amount_line52,c6.tax_code_line52,c6.quantity_line52,c6.unit_price_line52,c6.tax_percentage52,c6.VAT_DESCRIPTION52);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num53 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num53,c6.line53_amount,c6.item_name_line53,c6.item_description53,c6.tax_amount_line53,c6.tax_code_line53,c6.quantity_line53,c6.unit_price_line53,c6.tax_percentage53,c6.VAT_DESCRIPTION53);
end if;
----------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num54 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num54,c6.line54_amount,c6.item_name_line54,c6.item_description54,c6.tax_amount_line54,c6.tax_code_line54,c6.quantity_line54,c6.unit_price_line54,c6.tax_percentage54,c6.VAT_DESCRIPTION54);
end if;
-----------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num55 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num55,c6.line55_amount,c6.item_name_line55,c6.item_description55,c6.tax_amount_line55,c6.tax_code_line55,c6.quantity_line55,c6.unit_price_line55,c6.tax_percentage55,c6.VAT_DESCRIPTION55);
end if;
-------------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num56 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num56,c6.line56_amount,c6.item_name_line56,c6.item_description56,c6.tax_amount_line56,c6.tax_code_line56,c6.quantity_line56,c6.unit_price_line56,c6.tax_percentage56,c6.VAT_DESCRIPTION56);
end if;
----------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num57 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num57,c6.line57_amount,c6.item_name_line57,c6.item_description57,c6.tax_amount_line57,c6.tax_code_line57,c6.quantity_line57,c6.unit_price_line57,c6.tax_percentage57,c6.VAT_DESCRIPTION57);
end if;
----------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num58 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num58,c6.line58_amount,c6.item_name_line58,c6.item_description58,c6.tax_amount_line58,c6.tax_code_line58,c6.quantity_line58,c6.unit_price_line58,c6.tax_percentage58,c6.VAT_DESCRIPTION58);
end if;
-----------------------------------------------------------------------------------------------------------------------------------
if c6.line_num59 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num59,c6.line59_amount,c6.item_name_line59,c6.item_description59,c6.tax_amount_line59,c6.tax_code_line59,c6.quantity_line59,c6.unit_price_line59,c6.tax_percentage59,c6.VAT_DESCRIPTION59);
end if;
----------------------------------------------------------------------------------------------------------------------------------
if c6.line_num60 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num60,c6.line60_amount,c6.item_name_line60,c6.item_description60,c6.tax_amount_line60,c6.tax_code_line60,c6.quantity_line60,c6.unit_price_line60,c6.tax_percentage60,c6.VAT_DESCRIPTION60);
end if;
----------------------------------------------------------------------------------------------------------------------------------
if c6.line_num61 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num61,c6.line61_amount,c6.item_name_line61,c6.item_description61,c6.tax_amount_line61,c6.tax_code_line61,c6.quantity_line61,c6.unit_price_line61,c6.tax_percentage61,c6.VAT_DESCRIPTION61);
end if;
----------------------------------------------------------------------------------------------------------------------------------
if c6.line_num62 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num62,c6.line62_amount,c6.item_name_line62,c6.item_description62,c6.tax_amount_line62,c6.tax_code_line62,c6.quantity_line62,c6.unit_price_line62,c6.tax_percentage62,c6.VAT_DESCRIPTION62);
end if;
----------------------------------------------------------------------------------------------------------------------------------
if c6.line_num63 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE
values(c6.invoice_number,c6.line_num63,c6.line63_amount,c6.item_name_line63,c6.item_description63,c6.tax_amount_line63,c6.tax_code_line63,c6.quantity_line63,c6.unit_price_line63,c6.tax_percentage63,c6.VAT_DESCRIPTION63);
end if;
-----------------------------------------------------------------------------------------------------------------------------------
if c6.line_num64 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num64,c6.line64_amount,c6.item_name_line64,c6.item_description64,c6.tax_amount_line64,c6.tax_code_line64,c6.quantity_line64,c6.unit_price_line64,c6.tax_percentage64,c6.VAT_DESCRIPTION64);
end if;
-------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num65 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num65,c6.line65_amount,c6.item_name_line65,c6.item_description65,c6.tax_amount_line65,c6.tax_code_line65,c6.quantity_line65,c6.unit_price_line65,c6.tax_percentage65,c6.VAT_DESCRIPTION65);
end if;
--------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num66 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num66,c6.line66_amount,c6.item_name_line66,c6.item_description66,c6.tax_amount_line66,c6.tax_code_line66,c6.quantity_line66,c6.unit_price_line66,c6.tax_percentage66,c6.VAT_DESCRIPTION66);
end if;
--------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num67 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num67,c6.line67_amount,c6.item_name_line67,c6.item_description67,c6.tax_amount_line67,c6.tax_code_line67,c6.quantity_line67,c6.unit_price_line67,c6.tax_percentage67,c6.VAT_DESCRIPTION67);
end if;
---------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num68 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num68,c6.line68_amount,c6.item_name_line68,c6.item_description68,c6.tax_amount_line68,c6.tax_code_line68,c6.quantity_line68,c6.unit_price_line68,c6.tax_percentage68,c6.VAT_DESCRIPTION68);
end if;
-------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num69 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num69,c6.line69_amount,c6.item_name_line69,c6.item_description69,c6.tax_amount_line69,c6.tax_code_line69,c6.quantity_line69,c6.unit_price_line69,c6.tax_percentage69,c6.VAT_DESCRIPTION69);
end if;
---------------------------------------------------------------------------------------------------------------------------------
if c6.line_num70 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num70,c6.line70_amount,c6.item_name_line70,c6.item_description70,c6.tax_amount_line70,c6.tax_code_line70,c6.quantity_line70,c6.unit_price_line70,c6.tax_percentage70,c6.VAT_DESCRIPTION70);
end if;
---------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num71 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num71,c6.line71_amount,c6.item_name_line71,c6.item_description71,c6.tax_amount_line71,c6.tax_code_line71,c6.quantity_line71,c6.unit_price_line71,c6.tax_percentage71,c6.VAT_DESCRIPTION71);
end if;
----------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num72 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num72,c6.line72_amount,c6.item_name_line72,c6.item_description72,c6.tax_amount_line72,c6.tax_code_line72,c6.quantity_line72,c6.unit_price_line72,c6.tax_percentage72,c6.VAT_DESCRIPTION72);
end if;
----------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num73 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num73,c6.line73_amount,c6.item_name_line73,c6.item_description73,c6.tax_amount_line73,c6.tax_code_line73,c6.quantity_line73,c6.unit_price_line73,c6.tax_percentage73,c6.VAT_DESCRIPTION73);
end if;
---------------------------------------------------------------------------------------------------------------------------------------
if c6.line_num74 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num74,c6.line74_amount,c6.item_name_line74,c6.item_description74,c6.tax_amount_line74,c6.tax_code_line74,c6.quantity_line74,c6.unit_price_line74,c6.tax_percentage74,c6.VAT_DESCRIPTION74);
end if;
---------------------------------------------------------------------------------------------------------------------------------
if c6.line_num75 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num75,c6.line75_amount,c6.item_name_line75,c6.item_description75,c6.tax_amount_line75,c6.tax_code_line75,c6.quantity_line75,c6.unit_price_line75,c6.tax_percentage75,c6.VAT_DESCRIPTION75);
end if;
---------------------------------------------------------------------------------------------------------------------------------
if c6.line_num76 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num76,c6.line76_amount,c6.item_name_line76,c6.item_description76,c6.tax_amount_line76,c6.tax_code_line76,c6.quantity_line76,c6.unit_price_line76,c6.tax_percentage76,c6.VAT_DESCRIPTION76);
end if;
----------------------------------------------------------------------------------------------------------------------------------
if c6.line_num77 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num77,c6.line77_amount,c6.item_name_line77,c6.item_description77,c6.tax_amount_line77,c6.tax_code_line77,c6.quantity_line77,c6.unit_price_line77,c6.tax_percentage77,c6.VAT_DESCRIPTION77);
end if;
-----------------------------------------------------------------------------------------------------------------------------
if c6.line_num78 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num78,c6.line78_amount,c6.item_name_line78,c6.item_description78,c6.tax_amount_line78,c6.tax_code_line78,c6.quantity_line78,c6.unit_price_line78,c6.tax_percentage78,c6.VAT_DESCRIPTION78);
end if;
--------------------------------------------------------------------------------------------------------------------------------
if c6.line_num79 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num79,c6.line79_amount,c6.item_name_line79,c6.item_description79,c6.tax_amount_line79,c6.tax_code_line79,c6.quantity_line79,c6.unit_price_line79,c6.tax_percentage79,c6.VAT_DESCRIPTION79);
end if;
-------------------------------------------------------------------------------------------------------------------------------
if c6.line_num80 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num80,c6.line80_amount,c6.item_name_line80,c6.item_description80,c6.tax_amount_line80,c6.tax_code_line80,c6.quantity_line80,c6.unit_price_line80,c6.tax_percentage80,c6.VAT_DESCRIPTION80);
end if;
----------------------------------------------------------------------------------------------------------------------------
if c6.line_num81 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num81,c6.line81_amount,c6.item_name_line81,c6.item_description81,c6.tax_amount_line81,c6.tax_code_line81,c6.quantity_line81,c6.unit_price_line81,c6.tax_percentage81,c6.VAT_DESCRIPTION81);
end if;
----------------------------------------------------------------------------------------------------------------------------
if c6.line_num82 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num82,c6.line82_amount,c6.item_name_line82,c6.item_description82,c6.tax_amount_line82,c6.tax_code_line82,c6.quantity_line82,c6.unit_price_line82,c6.tax_percentage82,c6.VAT_DESCRIPTION82);
end if;
--------------------------------------------------------------------------------------------------------------------------
if c6.line_num83 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num83,c6.line83_amount,c6.item_name_line83,c6.item_description83,c6.tax_amount_line83,c6.tax_code_line83,c6.quantity_line83,c6.unit_price_line83,c6.tax_percentage83,c6.VAT_DESCRIPTION83);
end if;
----------------------------------------------------------------------------------------------------------------------
if c6.line_num84 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num84,c6.line84_amount,c6.item_name_line84,c6.item_description84,c6.tax_amount_line84,c6.tax_code_line84,c6.quantity_line84,c6.unit_price_line84,c6.tax_percentage84,c6.VAT_DESCRIPTION84);
end if;
----------------------------------------------------------------------------------------------------------------------
if c6.line_num85 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num85,c6.line85_amount,c6.item_name_line85,c6.item_description85,c6.tax_amount_line85,c6.tax_code_line85,c6.quantity_line85,c6.unit_price_line85,c6.tax_percentage85,c6.VAT_DESCRIPTION85);
end if;
-----------------------------------------------------------------------------------------------------------------------
if c6.line_num86 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num86,c6.line86_amount,c6.item_name_line86,c6.item_description86,c6.tax_amount_line86,c6.tax_code_line86,c6.quantity_line86,c6.unit_price_line86,c6.tax_percentage86,c6.VAT_DESCRIPTION86);
end if;
----------------------------------------------------------------------------------------------------------------------------
if c6.line_num87 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num87,c6.line87_amount,c6.item_name_line87,c6.item_description87,c6.tax_amount_line87,c6.tax_code_line87,c6.quantity_line87,c6.unit_price_line87,c6.tax_percentage87,c6.VAT_DESCRIPTION87);
end if;
---------------------------------------------------------------------------------------------------------------------------------
if c6.line_num88 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num88,c6.line88_amount,c6.item_name_line88,c6.item_description88,c6.tax_amount_line88,c6.tax_code_line88,c6.quantity_line88,c6.unit_price_line88,c6.tax_percentage88,c6.VAT_DESCRIPTION88);
end if;
-------------------------------------------------------------------------------------------------------------------------------
if c6.line_num89 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num89,c6.line89_amount,c6.item_name_line89,c6.item_description89,c6.tax_amount_line89,c6.tax_code_line89,c6.quantity_line89,c6.unit_price_line89,c6.tax_percentage89,c6.VAT_DESCRIPTION89);
end if;
---------------------------------------------------------------------------------------------------------------------------------
if c6.line_num90 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num90,c6.line90_amount,c6.item_name_line90,c6.item_description90,c6.tax_amount_line90,c6.tax_code_line90,c6.quantity_line90,c6.unit_price_line90,c6.tax_percentage90,c6.VAT_DESCRIPTION90);
end if;
---------------------------------------------------------------------------------------------------------------------------------
if c6.line_num91 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num91,c6.line91_amount,c6.item_name_line91,c6.item_description91,c6.tax_amount_line91,c6.tax_code_line91,c6.quantity_line91,c6.unit_price_line91,c6.tax_percentage91,c6.VAT_DESCRIPTION91);
end if;
---------------------------------------------------------------------------------------------------------------------------------
if c6.line_num92 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num92,c6.line92_amount,c6.item_name_line92,c6.item_description92,c6.tax_amount_line92,c6.tax_code_line92,c6.quantity_line92,c6.unit_price_line92,c6.tax_percentage92,c6.VAT_DESCRIPTION92);
end if;
--------------------------------------------------------------------------------------------------------------------------------
if c6.line_num93 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num93,c6.line93_amount,c6.item_name_line93,c6.item_description93,c6.tax_amount_line93,c6.tax_code_line93,c6.quantity_line93,c6.unit_price_line93,c6.tax_percentage93,c6.VAT_DESCRIPTION93);
end if;
---------------------------------------------------------------------------------------------------------------------------------
if c6.line_num94 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num94,c6.line94_amount,c6.item_name_line94,c6.item_description94,c6.tax_amount_line94,c6.tax_code_line94,c6.quantity_line94,c6.unit_price_line94,c6.tax_percentage94,c6.VAT_DESCRIPTION94);
end if;
----------------------------------------------------------------------------------------------------------------------------------
if c6.line_num95 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num95,c6.line95_amount,c6.item_name_line95,c6.item_description95,c6.tax_amount_line95,c6.tax_code_line95,c6.quantity_line95,c6.unit_price_line95,c6.tax_percentage95,c6.VAT_DESCRIPTION95);
end if;
---------------------------------------------------------------------------------------------------------------------------------
if c6.line_num96 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num96,c6.line96_amount,c6.item_name_line96,c6.item_description96,c6.tax_amount_line96,c6.tax_code_line96,c6.quantity_line96,c6.unit_price_line96,c6.tax_percentage96,c6.VAT_DESCRIPTION96);
end if;
-------------------------------------------------------------------------------------------------------------------------------
if c6.line_num97 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num97,c6.line97_amount,c6.item_name_line97,c6.item_description97,c6.tax_amount_line97,c6.tax_code_line97,c6.quantity_line97,c6.unit_price_line97,c6.tax_percentage97,c6.VAT_DESCRIPTION97);
end if;
-----------------------------------------------------------------------------------------------------------------------------
if c6.line_num98 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num98,c6.line98_amount,c6.item_name_line98,c6.item_description98,c6.tax_amount_line98,c6.tax_code_line98,c6.quantity_line98,c6.unit_price_line98,c6.tax_percentage98,c6.VAT_DESCRIPTION98);
end if;
----------------------------------------------------------------------------------------------------------------------------
if c6.line_num99 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num99,c6.line99_amount,c6.item_name_line99,c6.item_description99,c6.tax_amount_line99,c6.tax_code_line99,c6.quantity_line99,c6.unit_price_line99,c6.tax_percentage99,c6.VAT_DESCRIPTION99);
end if;
---------------------------------------------------------------------------------------------------------------------------
if c6.line_num100 IS NOT NULL then
insert into CCS_XML_LINE_TAX_TABLE values(c6.invoice_number,c6.line_num100,c6.line100_amount,c6.item_name_line100,c6.item_description100,c6.tax_amount_line100,c6.tax_code_line100,c6.quantity_line100,c6.unit_price_line100,c6.tax_percentage100,c6.VAT_DESCRIPTION100);
end if;
------------------------------------------------------------------------------------------------------------------------------------------
end loop;
---------------------------------------------------------------------------------------------------------------------------
end CCS_XML_INSERT_INTO_TABLE;
end CCS_XML_INSERT_TABLE;
