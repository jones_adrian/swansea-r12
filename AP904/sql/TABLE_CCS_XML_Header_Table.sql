create table CCS_XML_Header_Table
AS 
select dbms_lob.substr(substr(payload,instr(payload,'<LOGICALID>',1)+11,instr(payload,'</LOGICALID>',1)-(instr(payload,'<LOGICALID>',1)+11))) SENDER_LOGIVALID,
dbms_lob.substr(substr(payload,instr(payload,'<COMPONENT>',1)+11,instr(payload,'</COMPONENT>',1)-(instr(payload,'<COMPONENT>',1)+11))) SENDER_COMPONENT,
dbms_lob.substr(substr(payload,instr(payload,'<TASK>',1)+6,instr(payload,'</TASK>',1)-(instr(payload,'<TASK>',1)+6))) SENDER_TASK,
dbms_lob.substr(substr(payload,instr(payload,'<REFERENCEID>',1)+13,instr(payload,'</REFERENCEID>',1)-(instr(payload,'<REFERENCEID>',1)+13))) REFERENCE_ID,
to_date(dbms_lob.substr(substr(payload,instr(payload,'<YEAR>',1)+6,instr(payload,'</YEAR>',1)-(instr(payload,'<YEAR>',1)+6)))||'-'||dbms_lob.substr(substr(payload,instr(payload,'<MONTH>',1)+7,instr(payload,'</MONTH>',1)-(instr(payload,'<MONTH>',1)+7)))||'-'||dbms_lob.substr(substr(payload,instr(payload,'<DAY>',1)+5,instr(payload,'</DAY>',1)-(instr(payload,'<DAY>',1)+5))),'YYYY-MM-DD') INVOICE_CREATION_DATE,
dbms_lob.substr(substr(payload,instr(payload,'<CONFIRMATION>',1)+14,instr(payload,'</CONFIRMATION>',1)-(instr(payload,'<CONFIRMATION>',1)+14))) CONFIRMATION_IDENTIFIER,
dbms_lob.substr(substr(payload,instr(payload,'<DOCUMENTID>',1)+12,instr(payload,'</DOCUMENTID>',1)-(instr(payload,'<DOCUMENTID>',1)+12))) INVOICE_NUMBER,
dbms_lob.substr(substr(payload,instr(payload,'<DESCRIPTN>',1)+11,instr(payload,'</DESCRIPTN>',1)-(instr(payload,'<DESCRIPTN>',1)+11))) INVOICE_DESCRIPTION,
dbms_lob.substr(substr(payload,instr(payload,'<DOCTYPE>',1)+9,instr(payload,'</DOCTYPE>',1)-(instr(payload,'<DOCTYPE>',1)+9))) Document_type,
dbms_lob.substr(substr(payload,instr(payload,'<REASONCODE>',1)+12,instr(payload,'</REASONCODE>',1)-(instr(payload,'<REASONCODE>',1)+12))) Reason_Code,
dbms_lob.substr(substr(payload,instr(payload,'<NAME index="1">',1)+16,instr(payload,'</NAME>',1)-(instr(payload,'<NAME index="1">',1)+16))) Supplier_Name,
dbms_lob.substr(substr(payload,instr(payload,'<PARTNRTYPE>',1)+12,instr(payload,'</PARTNRTYPE>',1)-(instr(payload,'<PARTNRTYPE>',1)+12))) PARTNER_TYPE,
dbms_lob.substr(substr(payload,instr(payload,'<DUNSNUMBER>',1)+12,instr(payload,'</DUNSNUMBER>',1)-(instr(payload,'<DUNSNUMBER>',1)+12))) DUNS_NUMBER_SUPPLIER,
dbms_lob.substr(substr(payload,instr(payload,'<PARTNRIDX>',1)+11,instr(payload,'</PARTNRIDX>',1)-(instr(payload,'<PARTNRIDX>',1)+11))) PARTNER_ID_SUPPLIER,
dbms_lob.substr(substr(payload,instr(payload,'<TAXID>',1)+7,instr(payload,'</TAXID>',1)-(instr(payload,'<TAXID>',1)+7))) TAX_ID_SUPPLIER,
dbms_lob.substr(substr(payload,instr(payload,'<ADDRLINE index="1">',1)+20,instr(payload,'</ADDRLINE>',1)-(instr(payload,'<ADDRLINE index="1">',1)+20))) SUPPLIER_ADDRESS1,
dbms_lob.substr(substr(payload,instr(payload,'<CITY>',1)+6,instr(payload,'</CITY>',1)-(instr(payload,'<CITY>',1)+6))) SUPPLIER_CITY,
dbms_lob.substr(substr(payload,instr(payload,'<COUNTRY>',1)+9,instr(payload,'</COUNTRY>',1)-(instr(payload,'<COUNTRY>',1)+9))) SUPPLIER_COUNTRY,
dbms_lob.substr(substr(payload,instr(payload,'<POSTALCODE>',1)+12,instr(payload,'</POSTALCODE>',1)-(instr(payload,'<POSTALCODE>',1)+12))) SUPPLIER_POSTAL_CODE,
dbms_lob.substr(substr(payload,instr(payload,'<STATEPROVN>',1)+12,instr(payload,'</STATEPROVN>',1)-(instr(payload,'<STATEPROVN>',1)+12))) SUPPLIER_STATE_PROVINCE,
dbms_lob.substr(substr(payload,instr(payload,'<TELEPHONE index="1">',1)+21,instr(payload,'</TELEPHONE>',1)-(instr(payload,'<TELEPHONE index="1">',1)+21))) SUPPLIER_TELEPHONE_INDEX,
dbms_lob.substr(substr(payload,instr(payload,'<NAME index="1">',1,2)+16,instr(payload,'</NAME>',1,2)-(instr(payload,'<NAME index="1">',1,2)+16))) SUPPLIER_CONTACT_NAME,
dbms_lob.substr(substr(payload,instr(payload,'<EMAIL>',1,1)+7,instr(payload,'</EMAIL>',1,1)-(instr(payload,'<EMAIL>',1,1)+7))) SUPPLIER_EMAIL_ADDRESS,
dbms_lob.substr(substr(payload,instr(payload,'<TELEPHONE index="1">',1,2)+21,instr(payload,'</TELEPHONE>',1,2)-(instr(payload,'<TELEPHONE index="1">',1,2)+21)))  SUPPLIER_CONT_TELEPHONE,
dbms_lob.substr(substr(payload,instr(payload,'<NAME index="1">',1,3)+16,instr(payload,'</NAME>',1,3)-(instr(payload,'<NAME index="1">',1,3)+16))) SOLD_TO_NAME,
dbms_lob.substr(substr(payload,instr(payload,'<PARTNRTYPE>',1,2)+12,instr(payload,'</PARTNRTYPE>',1,2)-(instr(payload,'<PARTNRTYPE>',1,2)+12))) SOLD_TO_PARTNER_TYPE,
dbms_lob.substr(substr(payload,instr(payload,'<DUNSNUMBER>',1,2)+12,instr(payload,'</DUNSNUMBER>',1,2)-(instr(payload,'<DUNSNUMBER>',1,2)+12))) SOLD_TO_DUNS_NUMBER,
dbms_lob.substr(substr(payload,instr(payload,'<EMAIL>',1,2)+7,instr(payload,'</EMAIL>',1,2)-(instr(payload,'<EMAIL>',1,2)+7))) SOLD_TO_CONTACT_EMAIL,
dbms_lob.substr(substr(payload,instr(payload,'<TELEPHONE index="1">',1,3)+21,instr(payload,'</TELEPHONE>',1,3)-(instr(payload,'<TELEPHONE index="1">',1,3)+21))) SOLD_TO_TELEPHONE_INDEX,
dbms_lob.substr(substr(payload,instr(payload,'<NAME index="1">',1,4)+16,instr(payload,'</NAME>',1,4)-(instr(payload,'<NAME index="1">',1,4)+16))) REMIT_TO_NAME,
dbms_lob.substr(substr(payload,instr(payload,'<PARTNRTYPE>',1,3)+12,instr(payload,'</PARTNRTYPE>',1,3)-(instr(payload,'<PARTNRTYPE>',1,3)+12))) REMIT_TO_PARTNER_TYPE,
dbms_lob.substr(substr(payload,instr(payload,'<DUNSNUMBER>',1,3)+12,instr(payload,'</DUNSNUMBER>',1,3)-(instr(payload,'<DUNSNUMBER>',1,3)+12))) REMIT_TO_DUNS_NUMBER,
dbms_lob.substr(substr(payload,instr(payload,'<TAXID>',1,2)+7,instr(payload,'</TAXID>',1,2)-(instr(payload,'<TAXID>',1,2)+7))) REMIT_TO_TAXID,
dbms_lob.substr(substr(payload,instr(payload,'<NAME index="1">',1,5)+16,instr(payload,'</NAME>',1,5)-(instr(payload,'<NAME index="1">',1,5)+16))) REMIT_TO_CONTACT_NAME,
dbms_lob.substr(substr(payload,instr(payload,'<EMAIL>',1,3)+7,instr(payload,'</EMAIL>',1,3)-(instr(payload,'<EMAIL>',1,3)+7))) REMIT_TO_CONTACT_EMAIL,
dbms_lob.substr(substr(payload,instr(payload,'<NAME index="1">',1,6)+16,instr(payload,'</NAME>',1,6)-(instr(payload,'<NAME index="1">',1,6)+16))) BILL_TO_NAME,
dbms_lob.substr(substr(payload,instr(payload,'<PARTNRTYPE>',1,4)+12,instr(payload,'</PARTNRTYPE>',1,4)-(instr(payload,'<PARTNRTYPE>',1,4)+12))) BILL_TO_PARTNERTYPE,
dbms_lob.substr(substr(payload,instr(payload,'<DUNSNUMBER>',1,4)+12,instr(payload,'</DUNSNUMBER>',1,4)-(instr(payload,'<DUNSNUMBER>',1,4)+12))) BILL_TO_DUNSNUMBER,
dbms_lob.substr(substr(payload,instr(payload,'<PARTNRIDX>',1,2)+11,instr(payload,'</PARTNRIDX>',1,2)-(instr(payload,'<PARTNRIDX>',1,2)+11))) BILL_TO_PARTNERID,
dbms_lob.substr(substr(payload,instr(payload,'<DOCTYPE>',1,2)+9,instr(payload,'</DOCTYPE>',1,2)-(instr(payload,'<DOCTYPE>',1,2)+9))) DOCUMENT_REFERENCE_TYPE,
dbms_lob.substr(substr(payload,instr(payload,'<DOCUMENTID>',1,3)+12,instr(payload,'</DOCUMENTID>',1,3)-(instr(payload,'<DOCUMENTID>',1,3)+12))) DOCUMENT_NUMBER,
dbms_lob.substr(substr(payload,instr(payload,'<ADDRLINE index="1">',1,3)+20,(instr(payload,'</ADDRLINE>',1,5)-(instr(payload,'<ADDRLINE index="1">',1,3)+20)))) BILL_TO_ADDRESS1,
dbms_lob.substr(substr(payload,instr(payload,'<ADDRLINE index="2">',1,3)+20,(instr(payload,'</ADDRLINE>',1,6)-(instr(payload,'<ADDRLINE index="2">',1,3)+20)))) BILL_TO_ADDRESS2,
dbms_lob.substr(substr(payload,instr(payload,'<CITY>',1,3)+6,(instr(payload,'</CITY>',1,3)-(instr(payload,'<CITY>',1,3)+6)))) BILL_TO_CITY,
dbms_lob.substr(substr(payload,instr(payload,'<POSTALCODE>',1,3)+12,(instr(payload,'</POSTALCODE>',1,3)-(instr(payload,'<POSTALCODE>',1,3)+12)))) BILL_TO_POSTAL_CODE,
dbms_lob.substr(substr(payload,instr(payload,'<FAX index="1">',1,1)+15,(instr(payload,'</FAX>',1,1)-(instr(payload,'<FAX index="1">',1,1)+15)))) BILL_TO_FAX,
dbms_lob.substr(substr(payload,instr(payload,'<CURRENCY>',1,1)+10,(instr(payload,'</CURRENCY>',1,1)-(instr(payload,'<CURRENCY>',1,1)+10)))) CURRENCY
FROM ecx_doclogs where transaction_type='INVOICE'
and 1=2
/












