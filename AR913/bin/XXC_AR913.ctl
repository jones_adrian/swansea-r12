--*************************************************************************************************
--     File Name        --  XXC_AR913.ctl 		                               	       
--     Author Name      --  Richard Chapman
--     Description      --  This Control file is used to load the data from Social Services Sales
--                          Ledger data file into staging table XXC_INV_SSSL_STAGING.            
-- Version History
-- $Log$
---------------------------------------------------------------------------------------------------
-- Change History
---------------------------------------------------------------------------------------------------
-- Author           Version No  Modidifed Date    Description
---------------------------------------------------------------------------------------------------
-- Richard Chapman  1.0         19/08/2014        Initial Creation
--*************************************************************************************************

LOAD DATA
APPEND
INTO TABLE xxc.xxc_inv_sssl_staging
FIELDS TERMINATED BY '|' 
optionally enclosed by '"'
TRAILING NULLCOLS
(   lob_cust_ref             "RTRIM(:lob_cust_ref)",
    amount                   "RTRIM(:amount)",
    gl_code1                 "RTRIM(:gl_code1)",
    gl_code2                 "RTRIM(:gl_code2)",
    gl_code3                 "RTRIM(:gl_code3)",
    gl_code4                 "RTRIM(:gl_code4)",
    gl_code5                 "RTRIM(:gl_code5)",
    gl_code6                 "RTRIM(:gl_code6)",
    gl_code7                 "RTRIM(:gl_code7)",
    third_party              "RTRIM(:third_party)",
    description              "RTRIM(:description)",
    created_by               CONSTANT '-1',
    creation_date            SYSDATE,
    last_updated_by          CONSTANT '-1',
    last_update_date         SYSDATE,
    status_flag              CONSTANT 'N',
    comments                 "RTRIM(:comments)")