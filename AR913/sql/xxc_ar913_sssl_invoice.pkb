CREATE OR REPLACE PACKAGE BODY xxc_ar913_sssl_invoice
IS
/*******************************************************************************
* $Id$
*
* $Source$
*
* Module         : xxc_ar913_sssl_invoice - 
*
* Module Type    : PL/SQL Package Specification
*
* Original Author: Richard Chapaman
*
* Description    : Invoice Interface from Social Services Sales Ledger into AR. 
*                  This package is used to validate and inserts data into the 
*                  interface tables. Based on xxc_p_066a_ar_open_invoice.
*
* Version History:
*
* 12-Aug-14	Richard Chapman CRQ000000007180 - initial version.
* 05-Jan-15	Richard Chapman Amend cut-off date to 15th; add Third Party to 5th
*                           unique flexfield; invoice line count. 
*
*******************************************************************************/

  -------------------------------------------------------------------------------
  -- Local Variables
  -------------------------------------------------------------------------------
  l_cust_account_id         NUMBER;
  l_lob_cust_ref            VARCHAR2(20);
  l_bill_addr_ref           VARCHAR2(20);
  l_bill_contact_ref        VARCHAR2(20);
  l_bill_party_type         VARCHAR2(30); --Artifact 2138407
  l_ship_addr_ref           VARCHAR2(20);
  l_ship_cust_ref           VARCHAR2(20);
  l_orcl_cust_ref           VARCHAR2(20) := 0;
  l_line_amount             NUMBER      ;
  l_line_tax                NUMBER      ;
  l_line_desc               VARCHAR2(240);
  l_source_name             VARCHAR2(30);
  l_sales_ord_nmbr          VARCHAR2(50);
  l_sales_ord_date          DATE;
  l_inv_line_nmbr           NUMBER;
  l_comments                VARCHAR2(500);
  l_payment_method          VARCHAR2(15);
  l_payment_term            VARCHAR2(30);
  l_tax_code                VARCHAR2(20);
  l_org_id                  NUMBER;
  l_set_of_books_id         NUMBER;
  l_cust_trx_name           VARCHAR2(20);
  l_cust_trx_id             NUMBER;
  l_entity_code             VARCHAR2(2) ;
  l_service_code            VARCHAR2(3) ;
  l_cost_cntr_code          VARCHAR2(5) ;
  l_natural_acc_code        VARCHAR2(6) ;
  l_sub_analysis_code       VARCHAR2(5) ;
  l_project_code            VARCHAR2(6) ;
  l_spare_code              VARCHAR2(5) ;
  l_rec_processed           NUMBER                                := 0;
  l_rec_total               NUMBER                                := 0;
  l_rec_error               NUMBER                                := 0;
  l_line_quantity           NUMBER;
  l_err_msg                 VARCHAR2 (240);
  l_header_cat              VARCHAR2(30);
  l_sales_person_no         VARCHAR2(5);
  l_uniqid                  VARCHAR2(240);
  l_internal_notes          VARCHAR2(240);
  l_line_count              NUMBER                                := 0;
  l_store_cust_ref          VARCHAR2(20)                          := 'NONE';
  l_store_third_party       VARCHAR2(240)                         := 'NONE';
  l_prev_inv_error          VARCHAR2(1)                           := 'N';
  l_trx_date                DATE;
  
  --------------------------------------------------------------------------------
  -- Local Constant Declaration
  --------------------------------------------------------------------------------
  lc_currency_code           VARCHAR2(3)                           := 'GBP' ;
  lc_conversion_rate         NUMBER                                := '1'   ;
  lc_conversion_type         VARCHAR2(30)                          := 'User';
  lc_account_class           VARCHAR2(3)                           := 'REV' ;
  lc_line_type               VARCHAR2(20)                          := 'LINE';
  lc_line_quantity           NUMBER                                := '1'   ;
  lc_line_unit_measure       VARCHAR2(25)                          := 'Each';
  lc_amt_incld_tax           VARCHAR2(1)                           := 'N'   ;
  --lc_ll_source               fnd_flex_values_vl.flex_value%TYPE    := 'Lifeline';
  --lc_tr_source               fnd_flex_values_vl.flex_value%TYPE    := 'Trade Waste';
  --lc_hw_source               fnd_flex_values_vl.flex_value%TYPE    := 'Highways';
  lc_sssl_source             fnd_flex_values_vl.flex_value%TYPE    := 'SSSL';
  lc_payment_term            VARCHAR2(15)                          := 'Immediate';
  --lc_payment_method          VARCHAR2(30)                          := 'Direct Debit';
  --lc_payment_term_1          VARCHAR2(15)                          := '5 Monthly';
  --lc_flex_value_set_name_1   fnd_flex_value_sets.flex_value_set_name%TYPE
  --                            := 'CCS_AR_INVOICE_GL_MAP';
  lc_flex_value_set_name_2   fnd_flex_value_sets.flex_value_set_name%TYPE
                              := 'CCS_VAT_CODES';
  lc_slcr_percent_split      NUMBER                                := 100;
  lc_slcr_type_name          VARCHAR2(30)                          := 'Quota Sales Credit';
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- Local Procedure: get_sssl_addr_ref
-- Description    : This Procedure gets the Bill to Address Reference,
--                  Oracle customer Ref and Ship to Address ref
--                  for given LoB Customer Reference.
--
-- Parameters     :
-- Name                       IN/OUT   Description
-- --------------------       -------  -----------------------------------------
-- p_lob_cust_ref               IN       LoB customer Ref.
-- p_bill_to_ref                OUT      Bill address ref
-- p_orcl_cust_ref              OUT      Bill customer ref
-- p_ship_to_ref                OUT      Ship address ref
--------------------------------------------------------------------------------
PROCEDURE get_sssl_addr_ref (p_lob_cust_ref  IN VARCHAR2,
  p_bill_to_ref   OUT VARCHAR2,
  p_orcl_cust_ref OUT VARCHAR2,
  p_ship_to_ref   OUT VARCHAR2)
IS
  l_f_bill_addr_ref   hz_party_sites.orig_system_reference%type;
  l_f_orcl_cust_ref   hz_cust_accounts.orig_system_reference%type;
  l_f_ship_addr_ref   hz_party_sites.orig_system_reference%type;

BEGIN
  -------------------------------------------------------------------------------
  -- This PL/SQL block gets the Bill To address reference and
  -- Oracle reference from database for SSSL
  -------------------------------------------------------------------------------
  --Replaced to_char(to_number((p_lob_cust_ref)) in the place of p_lob_cust_ref by subba
  BEGIN
    SELECT hcs.orig_system_reference,
      hca.orig_system_reference
    INTO l_f_bill_addr_ref,
      l_f_orcl_cust_ref
    FROM hz_party_sites hps,
      hz_cust_accounts_all hca,
      hz_cust_acct_sites_all hcs,
      hz_cust_site_uses_all hcu,
      --ra_site_uses_all rsu /*Commented by rakesh task id 796*/
      hz_cust_site_uses_all rsu /*Added by rakesh task id 796*/
    WHERE hcs.attribute12 = TO_CHAR(TO_NUMBER(p_lob_cust_ref))
    AND   hps.status = 'A'
    AND   hps.party_site_id = hcs.party_site_id
    AND   hcs.status = 'A'
    AND   hcs.cust_account_id = hca.cust_account_id
    AND   hca.status = 'A'
    AND   hcs.cust_acct_site_id = hcu.cust_acct_site_id
    AND   hcu.status = 'A'
    AND   rsu.site_use_id = hcu.site_use_id
    AND   rsu.site_use_code = 'BILL_TO';

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      l_err_msg := 'E008 Bill TO Address Ref does not exist';
      RAISE fnd_api.g_exc_error;
    WHEN TOO_MANY_ROWS THEN
      l_err_msg := 'E009 Multiple Bill To Address Ref. found';
      RAISE fnd_api.g_exc_error;
    WHEN OTHERS THEN
      l_err_msg := 'E010 Error in getting BILL TO address; SQLERRM='||SQLERRM;
      RAISE fnd_api.g_exc_error;
  END;

  BEGIN
    -------------------------------------------------------------------------------
    -- This PL/SQL block gets the Ship To address reference.
    -- Replaced to_char(to_number((p_lob_cust_ref)) in the place of p_lob_cust_ref by subba
    -------------------------------------------------------------------------------
    SELECT hcs.orig_system_reference
    INTO   l_f_ship_addr_ref
    FROM   hz_party_sites hps,
      hz_cust_accounts hca,
      hz_cust_acct_sites_all hcs,
      hz_cust_site_uses_all hcu
    WHERE  hcs.attribute12 = TO_CHAR(TO_NUMBER(p_lob_cust_ref))
    AND    hps.party_site_id = hcs.party_site_id
    AND    hcs.status = 'A'
    AND    hcs.cust_account_id = hca.cust_account_id
    AND    hca.status = 'A'
    AND    hcs.cust_acct_site_id = hcu.cust_acct_site_id
    AND    hcu.status = 'A'
    AND    hcu.site_use_code = 'SHIP_TO';
    
    l_ship_cust_ref := l_f_orcl_cust_ref;
    
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      l_f_ship_addr_ref := NULL;
      l_ship_cust_ref := NULL;
    WHEN TOO_MANY_ROWS THEN
      l_err_msg := 'E011 Multiple Ship To Address Ref. found';
      RAISE fnd_api.g_exc_error;
    WHEN OTHERS THEN
      l_err_msg := 'E012 Error in getting SHIP TO address; SQLERRM='||SQLERRM;
      RAISE fnd_api.g_exc_error;
  END;
  
  p_bill_to_ref   := l_f_bill_addr_ref;
  p_orcl_cust_ref := l_f_orcl_cust_ref;

  IF (NVL(l_f_ship_addr_ref,'XX') = l_f_bill_addr_ref) THEN
    p_ship_to_ref := NULL;
    l_ship_cust_ref := NULL;
  ELSIF (NVL(l_f_ship_addr_ref,'XX') = 'XX') THEN
    l_ship_cust_ref := NULL;
  ELSE
    p_ship_to_ref := l_f_ship_addr_ref;
  END IF;
END get_sssl_addr_ref;
--------------------------------------------------------------------------------
      
--------------------------------------------------------------------------------
-- Local Procedure: VALIDATE_GL_ACCOUNT
--
-- Description    : This Procedure validates the GL Code Combinations
--
-- Parameters     :
-- Name                       IN/OUT   Description
-- --------------------       -------  -----------------------------------------
-- p_entity_code                 IN     Entity Code
-- p_service_code                IN     Service Code
-- p_cost_cntr_code              IN     Cost Centre Code
-- p_natural_acc_code            IN     Natural Account Code
-- p_sub_analysis_code           IN     Sub Analysis Code
-- p_project_code                IN     Project Code
-- p_spare_code                  IN     Spare Code
--------------------------------------------------------------------------------
PROCEDURE validate_gl_account (p_entity_code       IN VARCHAR2,
  p_service_code      IN VARCHAR2,
  p_cost_cntr_code    IN VARCHAR2,
  p_natural_acc_code  IN VARCHAR2,
  p_sub_analysis_code IN VARCHAR2,
  p_project_code      IN VARCHAR2,
  p_spare_code        IN VARCHAR2)
IS
  l_f_entity_code           gl_code_combinations.segment1%TYPE;
  l_f_service_code          gl_code_combinations.segment2%TYPE;
  l_f_cost_cntr_code        gl_code_combinations.segment3%TYPE;
  l_f_natural_acc_code      gl_code_combinations.segment4%TYPE;
  l_f_sub_analysis_code     gl_code_combinations.segment5%TYPE;
  l_f_project_code          gl_code_combinations.segment6%TYPE;
  l_f_spare_code            gl_code_combinations.segment7%TYPE;
  
BEGIN
  SELECT gcc.segment1,
    gcc.segment2,
    gcc.segment3,
    gcc.segment4,
    gcc.segment5,
    gcc.segment6,
    gcc.segment7
  INTO   l_f_entity_code,
    l_f_service_code,
    l_f_cost_cntr_code,
    l_f_natural_acc_code,
    l_f_sub_analysis_code,
    l_f_project_code,
    l_f_spare_code
  FROM   gl_code_combinations gcc
  WHERE  gcc.segment1 = p_entity_code
  AND    gcc.segment2 = p_service_code
  AND    gcc.segment3 = p_cost_cntr_code
  AND    gcc.segment4 = p_natural_acc_code
  AND    gcc.segment5 = p_sub_analysis_code
  AND    gcc.segment6 = p_project_code
  AND    gcc.segment7 = p_spare_code;
  
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    l_err_msg := 'E013 GL Account not Found: '||p_entity_code||'.'||p_service_code||'.'||p_cost_cntr_code||'.'
    ||p_natural_acc_code||'.'||p_sub_analysis_code||'.'||p_project_code||'.'||p_spare_code;
    RAISE fnd_api.g_exc_error;
  WHEN OTHERS THEN
    l_err_msg := 'E014 Error in getting Account information: '||p_entity_code||'.'||p_service_code||'.'||p_cost_cntr_code||'.'
    ||p_natural_acc_code||'.'||p_sub_analysis_code||'.'||p_project_code||'.'||p_spare_code||'; SQLERRM='||SQLERRM;
    RAISE fnd_api.g_exc_error;
END validate_gl_account;
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- Local Function: GET_BILL_CONTACT
-- Description   : This function gets the Bill Contact Reference
--                 for given Bill address ref.
--
-- Parameters:
-- Name                       IN/OUT   Description
-- --------------------       -------  -----------------------------------------
-- p_bill_add_ref               IN       Bill address ref
--------------------------------------------------------------------------------
FUNCTION get_bill_contact (p_bill_add_ref IN VARCHAR2)
RETURN VARCHAR2
IS
  l_f_contact_ref   hz_parties.orig_system_reference%type;
  l_f_party_type    hz_parties.party_type%type; --Artifact 2138407 
  
BEGIN
  --Artf2543572 comment out:
  --SELECT  rcs.orig_system_reference
  --INTO    l_f_contact_ref
  ----FROM    ra_contacts rcs, /*cOMMENTED BY RAKESH TASK ID 796*/
  --FROM    hz_parties rcs, /*Added BY RAKESH TASK ID 796*/
  --        hz_party_sites hps
  --WHERE   rcs.party_id = hps.party_id
  --AND     hps.orig_system_reference = p_bill_add_ref;

  --Artf2543572 new query - find most-recently-updated active contact:
  SELECT  hcr.orig_system_reference
  INTO    l_f_contact_ref
  FROM    ar.hz_cust_acct_sites_all hcs,
    ar.hz_cust_account_roles hcr
  WHERE   hcs.cust_acct_site_id = hcr.cust_acct_site_id
  AND     hcr.status = 'A'
  AND     hcs.orig_system_reference = p_bill_add_ref
  AND     rownum = 1
  AND     hcr.last_update_date =
    (select max(hcr.last_update_date)
    from    hz_cust_account_roles hcr
    WHERE   hcs.cust_acct_site_id = hcr.cust_acct_site_id
    AND     hcr.status = 'A');

  --Artf2543572 new message:
  --FND_FILE.PUT_LINE (FND_FILE.LOG, '  Contact found OK for '||l_lob_cust_ref||
  --': hz_cust_acct_sites_all.orig_system_reference='||p_bill_add_ref||
  --' hz_cust_account_roles.orig_system_reference='||l_f_contact_ref);

  RETURN  l_f_contact_ref;

EXCEPTION
  WHEN NO_DATA_FOUND THEN
    RETURN NULL;
  WHEN TOO_MANY_ROWS THEN
    l_err_msg := 'E015 Multiple Contact Ref found for p_bill_add_ref='||p_bill_add_ref;
    RAISE fnd_api.g_exc_error;
  WHEN OTHERS THEN
    l_err_msg := 'E016 Error in getting BILL TO address for p_bill_add_ref='||p_bill_add_ref||'; SQLERRM='||SQLERRM;
    RAISE fnd_api.g_exc_error;
END get_bill_contact;
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- Local Function: GET_TRX_DATE
-- Description   : This function derives the invoice date, which will become the
--                 basis for payment: customers with a bank account will pay by 
--                 direct debit on the 1st of the next month, unless the direct
--                 debit run has been done, in which case the 1st of the month 
--                 after. Invoices for non-bank account customers will become
--                 payable immediately. 
--
-- Parameters    :
-- Name                       IN/OUT   Description
-- --------------------       -------  -----------------------------------------
-- p_lob_cust_ref               IN       LoB customer Ref.
--------------------------------------------------------------------------------
FUNCTION get_trx_date (p_lob_cust_ref IN VARCHAR2)
RETURN DATE
IS
  l_trx_date        DATE;
  l_bank_account    VARCHAR2(20);
  
BEGIN
  SELECT DISTINCT a.bank_account_num
  INTO l_bank_account
  FROM hz_cust_acct_sites_all ast,
  hz_cust_site_uses_all su,
  iby_external_payers_all pay,
  iby_pmt_instr_uses_all i,
  iby_ext_bank_accounts a
  WHERE ast.status = 'A'
  AND ast.attribute12 = p_lob_cust_ref
  AND ast.cust_acct_site_id = su.cust_acct_site_id
  AND su.status = 'A'
  AND su.site_use_id = pay.acct_site_use_id
  AND pay.ext_payer_id = i.ext_pmt_party_id
  AND i.payment_flow = 'FUNDS_CAPTURE'
  AND i.payment_function = 'CUSTOMER_PAYMENT'
  AND SYSDATE between NVL(i.start_date,SYSDATE) AND NVL(i.end_date,SYSDATE)
  AND i.instrument_id = a.ext_bank_account_id(+)
  AND SYSDATE BETWEEN NVL(a.start_date,SYSDATE) AND NVL(a.end_date,SYSDATE);

  IF TO_NUMBER(TO_CHAR(SYSDATE,'DD')) > 15 THEN
    l_trx_date := TRUNC(LAST_DAY(ADD_MONTHS(SYSDATE,1)) + 1);
  ELSE  
    l_trx_date := TRUNC(LAST_DAY(SYSDATE) + 1);
  END IF;      
  RETURN l_trx_date;

EXCEPTION
  WHEN NO_DATA_FOUND THEN
    l_trx_date := TRUNC(SYSDATE);
    RETURN l_trx_date;
  WHEN TOO_MANY_ROWS THEN
    l_err_msg := 'E021 Multiple bank accounts found for p_lob_cust_ref='||p_lob_cust_ref;
    RAISE fnd_api.g_exc_error;
  WHEN OTHERS THEN
    l_err_msg := 'E022 Error in getting bank accounts for p_lob_cust_ref='||p_lob_cust_ref||'; SQLERRM='||SQLERRM;
    RAISE fnd_api.g_exc_error;
END get_trx_date;
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- Local Procedure: INSERT_DATA_IFACE_TABLE
-- Description    : This Procedure insert the validated data into the interface 
--                  tables.
--------------------------------------------------------------------------------
PROCEDURE insert_data_iface_table
IS
--------------------------------------------------------------------------------
-- Inserting data into RA_INTERFACE_LINES_ALL Tables
--------------------------------------------------------------------------------
BEGIN
  --------------------------------------------------------------------------
  -- Modified by Ashish Lampuse on 16-Oct-2006.
  -- As per Change Request by Suzanne Boydell,
  -- Column HEADER_ATTRIBUTE_CATEGORY has been commented out as per the
  -- confirmation from Suzanne Boydell.
  --------------------------------------------------------------------------
  INSERT INTO ra_interface_lines_all
    (amount,
    amount_includes_tax_flag,
    batch_source_name,
    --                     comments,
    conversion_rate,
    conversion_type,
    currency_code,
    cust_trx_type_name,
    description,
    --                     header_attribute_category,
    interface_line_attribute1,
    interface_line_attribute2,
    interface_line_attribute3,
    interface_line_attribute4,
    interface_line_attribute5,
    interface_line_context,
    line_type,
    org_id,
    orig_system_bill_address_ref,
    orig_system_bill_contact_ref,
    orig_system_bill_customer_ref,
    orig_system_ship_address_ref,
    orig_system_ship_customer_ref,
    primary_salesrep_number,
    quantity,
    receipt_method_name,
    sales_order,
    set_of_books_id,
    tax_code,
    term_name,
    uom_name,
    unit_selling_price,
    billing_date,
    trx_date,
    creation_date,
    last_update_date,
    internal_notes)
  VALUES
    (l_line_amount,
    lc_amt_incld_tax,
    l_source_name,
    --                     l_comments,
    lc_conversion_rate,
    lc_conversion_type,
    lc_currency_code,
    l_cust_trx_name,
    l_line_desc,
    --                     l_header_cat,
    l_inv_line_nmbr,
    l_lob_cust_ref,
    l_sales_ord_nmbr,
    l_sales_ord_date,
    l_uniqid,
    l_source_name,
    lc_line_type,
    l_org_id,
    l_bill_addr_ref,
    l_bill_contact_ref,
    l_orcl_cust_ref,
    l_ship_addr_ref,
    l_ship_cust_ref,
    l_sales_person_no,
    l_line_quantity,
    l_payment_method,
    l_sales_ord_nmbr,
    l_set_of_books_id,
    l_tax_code,
    l_payment_term,
    lc_line_unit_measure,
    l_line_amount,
    NULL,
    l_trx_date,
    SYSDATE,
    SYSDATE,
    l_internal_notes);

  ----------------------------------------------------------------------
  -- Inserting data into RA_INTERFACE_DISTRIBUTIONS_ALL
  ----------------------------------------------------------------------
  INSERT INTO ra_interface_distributions_all
    (account_class,
    amount,
    interface_line_attribute1,
    interface_line_attribute2,
    interface_line_attribute3,
    interface_line_attribute4,
    interface_line_attribute5,
    interface_line_context,
    segment1,
    segment2,
    segment3,
    segment4,
    segment5,
    segment6,
    segment7,
    org_id,     --R12 amendment artifact 2137768,2138407,2138419
    creation_date,
    last_update_date)        
  VALUES      
    (lc_account_class,
    l_line_amount,
    l_inv_line_nmbr,
    l_lob_cust_ref,
    l_sales_ord_nmbr,
    l_sales_ord_date,
    l_uniqid,
    l_source_name,
    l_entity_code,
    l_service_code,
    l_cost_cntr_code,
    l_natural_acc_code,
    l_sub_analysis_code,
    l_project_code,
    l_spare_code,
    l_org_id,     --R12 amendment artifact 2137768,2138407,2138419
    SYSDATE,
    SYSDATE);

  ----------------------------------------------------------------------
  -- Inserting data into RA_INTERFACE_SALESCREDITS_ALL
  ----------------------------------------------------------------------
  INSERT INTO ra_interface_salescredits_all
    (interface_line_attribute1,
    interface_line_attribute2,
    interface_line_attribute3,
    interface_line_attribute4,
    interface_line_attribute5,
    interface_line_context,
    org_id,
    sales_credit_percent_split,
    sales_credit_type_name,
    salesrep_number,
    creation_date,
    last_update_date)
  VALUES
    (l_inv_line_nmbr,
    l_lob_cust_ref,
    l_sales_ord_nmbr,
    l_sales_ord_date,
    l_uniqid,
    l_source_name,
    l_org_id,
    lc_slcr_percent_split,
    lc_slcr_type_name,
    l_sales_person_no,
    SYSDATE,
    SYSDATE);

EXCEPTION
  WHEN OTHERS THEN
    l_err_msg := 'E020 Error while inserting data into Interface Table; SQLERRM='
    ||SQLERRM||'; STATEMENT='||dbms_utility.format_error_backtrace();
    RAISE fnd_api.g_exc_error;
END insert_data_iface_table;
--------------------------------------------------------------------------------

-------------------------------------------------------------------------------
PROCEDURE xxc_main(errbuf OUT VARCHAR2, retcode OUT VARCHAR2) AS
-------------------------------------------------------------------------------
-- Procedure      : XXC_MAIN
--
-- Description    : This Procedure is used to validate the invoices from Social
--                  Services Sales Ledger. It calls different functions and 
--                  procedures to validate the data and insert it into the 
--                  interface tables.
--
-- Parameters     :
-- Name                      IN/OUT   Description
-- --------------------      -------  -----------------------------------------
-- errbuf                    OUT      Error Message
-- retcode                   OUT      Error Code
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Cursor Declaration
-------------------------------------------------------------------------------
CURSOR open_sssl_inv_cur IS
  SELECT ROWID,
    uniqid,
    lob_cust_ref,
    amount,
    gl_code1,
    gl_code2, 
    gl_code3,
    gl_code4,
    gl_code5,
    gl_code6,                   
    gl_code7,
    third_party,
    description
  FROM xxc.xxc_inv_sssl_staging
  WHERE (status_flag = 'E' OR status_flag = 'N')
  --FOR UPDATE OF status_flag, comments
  ORDER BY lob_cust_ref, third_party;

BEGIN
  FND_FILE.PUT_LINE (FND_FILE.LOG,
  '*** City and County of Swansea SSSL Invoice Interface into AR Report ***');
  
  --l_uniqid := TO_CHAR(SYSDATE,'YYYY-MM-DD HH24:MI:SS'); --Used in unique attr5
  
  --------------------------------------------------------------------------------
  -- Get the Set Of Book ID
  --------------------------------------------------------------------------------
  BEGIN
    SELECT set_of_books_id, org_id
    INTO   l_set_of_books_id, l_org_id
    FROM   ar_system_parameters_all
    WHERE  org_id = fnd_profile.VALUE ('ORG_ID');

  EXCEPTION
    WHEN OTHERS THEN
      l_err_msg := 'E001 Error in getting the Organization_Id; SQLERRM='||SQLERRM;
      RAISE fnd_api.g_exc_error;
  END;

  --------------------------------------------------------------------------------
  -- Get Transaction Type
  --------------------------------------------------------------------------------
  BEGIN
    SELECT rct.name,
      rct.cust_trx_type_id
    INTO l_cust_trx_name,
      l_cust_trx_id
    FROM   ra_cust_trx_types rct
    WHERE  rct.name = 'SSSL'
    AND    rct.type = 'INV'
    AND    rct.org_id = l_org_id;

  EXCEPTION
    WHEN no_data_found THEN
      l_err_msg := 'E002 Invalid Transaction type';
      RAISE fnd_api.g_exc_error;
    WHEN others THEN
      l_err_msg := 'E003 Error in getting Transaction Type; SQLERRM='||SQLERRM;
      RAISE fnd_api.g_exc_error;
  END;

  --------------------------------------------------------------------------------
  -- Opening the Cursor for Validation
  --------------------------------------------------------------------------------
  l_rec_processed := 0;
  l_rec_error     := 0;
  l_err_msg       := ' ';
  FOR open_sssl_inv_rec IN open_sssl_inv_cur 
  LOOP
    l_rec_total := l_rec_total + 1;
    BEGIN 
      l_comments           := open_sssl_inv_rec.description;
      l_lob_cust_ref       := open_sssl_inv_rec.lob_cust_ref;
      l_sales_ord_nmbr     := 'SSSL';
      l_source_name        := 'SSSL';
      l_sales_ord_date     := SYSDATE;
      l_header_cat         := lc_sssl_source;
      l_payment_method     := null; -- ***does this default from the customer? ***
      l_line_desc          := open_sssl_inv_rec.description;
      l_tax_code           := 'OUTSIDE SCOPE'; --get_tax_code(l_line_tax);
      l_line_quantity      := lc_line_quantity;
      l_internal_notes     := open_sssl_inv_rec.third_party;
      --l_uniqid             := open_sssl_inv_rec.uniqid;
      l_uniqid             := l_internal_notes;            
      l_payment_term       := null; -- ***does this default from the customer? ***         
      l_payment_term       := lc_payment_term;
      l_trx_date           := get_trx_date(l_lob_cust_ref);      

      IF l_lob_cust_ref <> l_store_cust_ref OR l_internal_notes <> l_store_third_party THEN
        --Change of invoice: the Autoinvoice will group by customer and third party, so if there has been any error
        --on any line for this putative invoice, don't process any of the lines:
        l_store_cust_ref     := l_lob_cust_ref;
        l_store_third_party  := l_internal_notes;
        l_line_count         := 0;
        IF l_prev_inv_error = 'Y' THEN
          ROLLBACK;
        ELSE
          COMMIT;
        END IF;
        l_prev_inv_error     := 'N';
      END IF;

      l_line_count         := l_line_count + 1;    
      l_inv_line_nmbr      := TO_CHAR(l_line_count);
      
      l_line_amount        := open_sssl_inv_rec.amount;
      IF l_line_amount <= 0 THEN
        l_err_msg := 'E023 Amount not positive: '||l_line_amount;
        RAISE fnd_api.g_exc_error;
      END IF;      
 
      get_sssl_addr_ref (l_lob_cust_ref, l_bill_addr_ref, l_orcl_cust_ref, l_ship_addr_ref);
      l_bill_contact_ref   := get_bill_contact(l_bill_addr_ref);      
      
      l_entity_code        := open_sssl_inv_rec.gl_code1;
      l_service_code       := open_sssl_inv_rec.gl_code2;
      l_cost_cntr_code     := open_sssl_inv_rec.gl_code3;
      l_natural_acc_code   := open_sssl_inv_rec.gl_code4;
      l_sub_analysis_code  := open_sssl_inv_rec.gl_code5;
      l_project_code       := open_sssl_inv_rec.gl_code6;
      l_spare_code         := open_sssl_inv_rec.gl_code7;
      validate_gl_account(l_entity_code,
        l_service_code,
        l_cost_cntr_code,
        l_natural_acc_code,
        l_sub_analysis_code,
        l_project_code,
        l_spare_code);      

      l_sales_person_no    := l_cost_cntr_code;

      --***INSERT INTO INTERFACE TABLES:***  
      insert_data_iface_table;

      UPDATE xxc.xxc_inv_sssl_staging
      SET status_flag = 'P',
        last_update_date = sysdate,
        last_updated_by  = fnd_global.user_id
      --WHERE CURRENT OF open_sssl_inv_cur;
      WHERE ROWID = open_sssl_inv_rec.ROWID;

      l_rec_processed := l_rec_processed+1;

    EXCEPTION
      WHEN fnd_api.g_exc_error THEN
        l_prev_inv_error := 'Y';
        retcode := 1;
        errbuf  := 'Error occurred while validating the Invoice data';
        IF l_rec_error = 0 THEN
          FND_FILE.PUT_LINE (FND_FILE.LOG, ' ');        
          FND_FILE.PUT_LINE (FND_FILE.LOG, 'E004 Error in the following Invoice');
          FND_FILE.PUT_LINE (FND_FILE.LOG, 'Source - Invoice Number - LoB Customer Ref - Error Description');
          FND_FILE.PUT_LINE (FND_FILE.LOG, '--------------------------------------------------------------');
        END IF;
        l_rec_error := l_rec_error + 1;
        FND_FILE.PUT_LINE (FND_FILE.LOG, RPAD(l_source_name,6)
        || ' - ' || RPAD(l_sales_ord_nmbr,14)
        || ' - ' || RPAD(l_lob_cust_ref,16)
        || ' - ' || l_err_msg);
        --Due to invoice grouping, this update will actually get ROLLBACKed:
        UPDATE xxc.xxc_inv_sssl_staging
        SET status_flag = 'E',
          comments = l_err_msg,
          last_update_date = sysdate,
          last_updated_by  = fnd_global.user_id
        --WHERE CURRENT OF open_sssl_inv_cur;
        WHERE ROWID = open_sssl_inv_rec.ROWID;

      WHEN OTHERS THEN
        l_prev_inv_error := 'Y';      
        retcode := 1;
        errbuf  := 'Error occurred while validating the Invoice data';
        --l_err_msg := 'SQLERRM='||SQLERRM;
        IF l_rec_error = 0 THEN
          FND_FILE.PUT_LINE (FND_FILE.LOG, ' ');              
          FND_FILE.PUT_LINE (FND_FILE.LOG, 'E005 Error in the following Invoice');
          FND_FILE.PUT_LINE (FND_FILE.LOG, 'Source - Invoice Number - LoB Customer Ref - Error Description');
          FND_FILE.PUT_LINE (FND_FILE.LOG, '--------------------------------------------------------------');
        END IF;
        l_rec_error := l_rec_error + 1;
        FND_FILE.PUT_LINE (FND_FILE.LOG, l_source_name
        || ' - ' || l_sales_ord_nmbr
        || ' - ' || l_lob_cust_ref
        || ' - MSG=' || l_err_msg ||'; SQLERRM='||SQLERRM||'; STATEMENT='||dbms_utility.format_error_backtrace());
        --Due to invoice grouping, this update will actually get ROLLBACKed:
        UPDATE xxc.xxc_inv_sssl_staging
        SET status_flag = 'E',
          comments = l_err_msg,
          last_update_date = sysdate,
          last_updated_by  = fnd_global.user_id
        --WHERE CURRENT OF open_sssl_inv_cur;
        WHERE ROWID = open_sssl_inv_rec.ROWID;
    END;
    --COMMIT;
  END LOOP;

  FND_FILE.PUT_LINE (FND_FILE.LOG, ' ');      
  IF l_rec_processed = 0 THEN
    FND_FILE.PUT_LINE (FND_FILE.LOG,'E006 No record Passed the Validation Rules');
    --RAISE fnd_api.g_exc_error;
    retcode := 1;
    errbuf  := 'Error occurred while validating the Invoice data';
  END IF;
  FND_FILE.PUT_LINE (FND_FILE.LOG, 'Total Number of Records                           = '|| l_rec_total);
  FND_FILE.PUT_LINE (FND_FILE.LOG, 'Total Invoice Lines not in error                  = '|| l_rec_processed);
  FND_FILE.PUT_LINE (FND_FILE.LOG, 'Number of Invoices Lines in error                 = '|| l_rec_error);
  IF l_prev_inv_error = 'Y' THEN
    --If there was an error on the last putative invoice of the run:
    ROLLBACK;
  ELSE
    COMMIT;
  END IF;    

  DELETE FROM xxc.xxc_inv_sssl_staging 
  WHERE  status_flag = 'P';
  COMMIT;

EXCEPTION
  WHEN fnd_api.g_exc_error THEN
    ROLLBACK;
    retcode := 1;
    errbuf  := 'Error occurred while validating the Invoice data';
    FND_FILE.PUT_LINE (FND_FILE.LOG,l_err_msg);
  WHEN OTHERS THEN
    ROLLBACK;
    retcode := 2;
    errbuf  := 'Error occurred while validating the Invoice data';
    FND_FILE.PUT_LINE (FND_FILE.LOG,'E007: '||l_err_msg||'; SQLERRM='||SQLERRM||'; STATEMENT='||dbms_utility.format_error_backtrace());
END xxc_main;
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
END xxc_ar913_sssl_invoice;