create or replace
package body XXC_AP_COST_CEN_PRJ_NO
as
FUNCTION XXC_COST_CENTRE_FUNC (P_SEGMENT IN VARCHAR2)
RETURN CHAR
IS
V_TEMP VARCHAR2(1000);
begin
SELECT NVL((vl.flex_value || ' - ' || vl.description),'') Cost_Centre_description
       into v_temp
       FROM   fnd_flex_values_vl vl
       ,      fnd_flex_value_sets fndvset 
       WHERE  vl.flex_value_set_id = fndvset.flex_value_set_id 
       AND    FNDVSET.FLEX_VALUE_SET_NAME = 'CCS_COST_CENTRE'
      AND     vl.flex_value NOT IN ( SELECT Distinct vl.flex_value
                                    FROM   fnd_flex_values_vl vl
                                    ,      fnd_flex_value_sets fndvset 
                                    WHERE  vl.flex_value_set_id = fndvset.flex_value_set_id 
                                    AND    fndvset.flex_value_set_name = 'CCS_COST_CENTRE'
                                    AND   ((vl.flex_value Between '46881' AND '46883')
                                                      OR (VL.FLEX_VALUE BETWEEN '46701' AND '46835' )
                                                          OR (vl.flex_value between '46851' AND '46872'))) 
       AND    vl.enabled_flag = 'Y'
       AND    NVL(VL.END_DATE_ACTIVE, SYSDATE) >= SYSDATE
       and vl.flex_value = nvl(P_SEGMENT,vl.flex_value);
     return v_temp;
Exception
	WHEN OTHERS THEN 
  RETURN	NULL;
--raise_application_error(-20001,'An error was encountered - '||SQLCODE||' -ERROR- '||SQLERRM);
end XXC_COST_CENTRE_FUNC;

FUNCTION XXC_PRJOECT_NO_FUNC (P_SEGMENT IN VARCHAR2)
RETURN CHAR
is
v_temp varchar2(1000);
begin
  SELECT nvl((vl.flex_value || ' - ' || vl.description),'') project_description
      into v_temp
       FROM   fnd_flex_values_vl vl
       ,      fnd_flex_value_sets fndvset 
       WHERE  vl.flex_value_set_id = fndvset.flex_value_set_id 
       AND    fndvset.flex_value_set_name = 'CCS_PROJECT'
       AND    vl.enabled_flag = 'Y'
       AND    NVL(VL.END_DATE_ACTIVE, SYSDATE) >= SYSDATE
       and vl.flex_value = nvl(P_SEGMENT,vl.flex_value);
       return v_temp;
              
exception when others then
 	RETURN null;
end XXC_PRJOECT_NO_FUNC;
END XXC_AP_COST_CEN_PRJ_NO;