create or replace
package body xxc_ap_tax_info
as
function xxc_tax_amt(p_invoice_id in number , p_line_num in number)
return number
IS
v_temp varchar2(1000);
begin
SELECT 
tax_amt
into v_temp
FROM ZX_LINES
WHERE TRX_ID = p_invoice_id 
AND TRX_LINE_NUMBER = p_line_num
and COMPOUNDING_TAX_MISS_FLAG='N';
return v_temp;
EXCEPTION
WHEN NO_DATA_FOUND THEN
return  0; 
when others then
return 0;
end xxc_tax_amt ;

function XXC_TAX_NAME(p_invoice_id in number , p_line_num in number)
return char
IS
v_temp varchar2(1000);
begin
SELECT 
tax
into v_temp
FROM ZX_LINES
WHERE TRX_ID = p_invoice_id 
AND TRX_LINE_NUMBER = p_line_num
and COMPOUNDING_TAX_MISS_FLAG='N';
return v_temp;
EXCEPTION
WHEN NO_DATA_FOUND THEN
return  null; 
when others then
RETURN NULL;
end XXC_TAX_NAME ;

END XXC_AP_TAX_INFO;