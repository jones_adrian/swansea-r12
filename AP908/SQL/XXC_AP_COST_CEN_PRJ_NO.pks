create or replace
PACKAGE  XXC_AP_COST_CEN_PRJ_NO
AS
/*************************************************************
$Header$
Original Author: Datta D
Module Type    : SQL*Plus
Description    : The package contains two functions which retrieves the cost centre
				and project number information.
Modification History:
$Log$
06/03/2014     Datta D  Initial Creation
**************************************************************/
FUNCTION XXC_COST_CENTRE_FUNC (P_SEGMENT IN VARCHAR2)
RETURN CHAR;
FUNCTION XXC_PRJOECT_NO_FUNC (P_SEGMENT IN VARCHAR2)
RETURN CHAR;
END XXC_AP_COST_CEN_PRJ_NO;