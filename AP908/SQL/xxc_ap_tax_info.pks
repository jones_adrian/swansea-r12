create or replace
package  xxc_ap_tax_info
as
/*************************************************************
$header$
original author: datta d
module type    : sql*plus
description    : the package contains two functions which retrieves the cost centre
				and project number information.
modification history:
$log$
07/08/2014     datta d  initial creation
**************************************************************/
function xxc_tax_amt (p_invoice_id in number , p_line_num in number)
return number;

FUNCTION XXC_TAX_NAME (P_INVOICE_ID IN NUMBER , P_LINE_NUM IN NUMBER)
return char; 

end xxc_ap_tax_info;