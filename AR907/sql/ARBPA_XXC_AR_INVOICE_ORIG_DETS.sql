CREATE OR REPLACE VIEW ARBPA_XXC_AR_INVOICE_ORIG_DETS (TRX_NUMBER, CONTEXT_VALUE, 
CONTACT_NAME, CONTACT_DEPT, CONTACT_TEL, SPECIAL_INSTRUCTIONS) 
AS
SELECT   DISTINCT
            TRX.TRX_NUMBER,
            TRX.ATTRIBUTE_CATEGORY,
            INITCAP (P.TITLE) || ' ' || P.FIRST_NAME || ' ' || P.LAST_NAME,
            O.LEVEL2_NAME,
            '01792' || ' ' || TRX.ATTRIBUTE3,
               'Any enquiry regarding this account should be addressed to '
            || INITCAP (P.TITLE)
            || ' '
            || P.FIRST_NAME
            || ' '
            || P.LAST_NAME
            || ', '
            || O.LEVEL2_NAME
            || ' telephone '
            || '01792'
            || ' '
            || TRX.ATTRIBUTE3
     FROM   AR.RA_CUSTOMER_TRX_ALL TRX,
            APPS.PER_ALL_PEOPLE_F P,
            APPS.XXC_ORG_DETAILS_MV O
    WHERE   TO_CHAR (P.PERSON_ID) = TRX.ATTRIBUTE1
            AND TO_CHAR (O.ORG_ID) = TRX.ATTRIBUTE2
            AND NVL(P.EFFECTIVE_END_DATE,SYSDATE) >= SYSDATE; --INC000000210881