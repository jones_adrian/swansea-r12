Create table bak_events as select * from xla_events ev
where EVENT_TYPE_CODE = 'ADJ_CREATE'
and application_id = 222
and event_status_code <> 'P'
and not exists
(select 'x' from AR_ADJUSTMENTS_ALL adj
where ev.event_id = adj.event_id
and posting_control_id = -3 );