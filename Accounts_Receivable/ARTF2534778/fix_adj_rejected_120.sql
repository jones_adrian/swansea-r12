/*============================================================================+
$Header: fix_adj_rejected_120.sql 120.0 2008/09/16 11:12:11 samara noship $
============================================================================+*/

REM +================================================================================+
REM | File Name: fix_adj_rejected_120.sql     			                    |
REM |										    |
REM | Root Cause : 6888581                                                          |
REM |                                                                               |
REM | Issue: When the AR adjustment is rejected, then the event associated to this  |
REM |        adjustment exist with 'I' status. [Incomplete status].                 | 
REM |        As a result, this event is listed in 'sub ledger exception report.     |  
REM |        and customer is not able to close the period                           |
REM |                                                                               |
REM | Fix Approach:								    |
REM |     1) Update the event_status_code to 'N', since the event is not accountable|
REM |                                                                               |
REM | USAGE                                                                         |
REM |      Supply the following when prompted:                                      |
REM |      1) The script should first be run with read_only_mode = Y to generate    |
REM |	      the listing of corrupted events detail. As a second step the          |
REM |         user should run this script with read_only_mode = N to fix the data.  |
REM |      2) l_start_gl_date (REQUIRED)                                            |
REM |      3) l_end_gl_date (REQUIRED)                                              |
REM |      4) l_org_id (REQUIRED)                                                |
REM |                               						    |
REM *================================================================================*/

spool fix_adj_rejected_120.out
set serveroutput on size 1000000;
set verify off;
set lines 500;

declare

l_org_id	   NUMBER := &enter_org_id;
l_read_only_mode   varchar2(1) := '&read_only_mode'||'';
l_start_gl_date    date   := to_date('&start_gl_date','DD-MON-YYYY');
l_end_gl_date      date   := to_date('&end_gl_date','DD-MON-YYYY');
l_adj_id 	 number := &enter_adjustment_id;
l_bug_number	   number := 6888581;  

cursor get_rejected_incomp_adj is
select adj.adjustment_id, adj.gl_date, adj.event_id
from ar_adjustments adj, xla_events xe
where adj.gl_date between l_start_gl_date and l_end_gl_date
and   adj.posting_control_id = -3
and   adj.status = 'R'
and   adj.postable = 'N'
and   xe.application_id = 222
and   xe.event_id = adj.event_id
and   xe.event_status_code = 'I'
order by adj.adjustment_id, adj.event_id;

PROCEDURE backup_table_xe is
l_create_bk_table varchar2(500);
BEGIN
l_create_bk_table := 'create table xla_events_bk_'||l_bug_number||'  as
                      select * from xla_events
                      where 1=2';
EXECUTE IMMEDIATE l_create_bk_table;
EXCEPTION
When others then
  IF sqlcode = -955 then
  null;
  ELSE
   raise;
  END IF;
END backup_table_xe;

PROCEDURE insert_into_backup_xe(l_event_id number) IS
l_insert_events  varchar2(500);
BEGIN
l_insert_events := 'insert into xla_events_bk_'||l_bug_number||
                '( select * from xla_events
                   where event_id = '||l_event_id||')';
EXECUTE IMMEDIATE l_insert_events;
END;

PROCEDURE debug(s varchar2) is 
BEGIN
  dbms_output.put_line(s);
END debug;

FUNCTION print_spaces(n IN number) RETURN Varchar2 IS
   l_return_string varchar2(100);
Begin
   select substr('                                                   ',1,n)
   into l_return_String
   from dual;
     return(l_return_String);
End print_spaces;

begin

   mo_global.init('AR');
   mo_global.set_policy_context('S',l_org_id);

	If  nvl(upper(l_read_only_mode),'Y') = 'N' then
		backup_table_xe;
	End if;

	debug('                                                                                                         ');
	debug('Adjustment Id    '||' '||'GL Date      '||' '||'Event Id           ');
	debug('================='||' '||'============='||' '||'===================');
	debug('                                                                                                         ');

	For rec in get_rejected_incomp_adj
	Loop

	  If  nvl(upper(l_read_only_mode),'Y') = 'N' then

		   insert_into_backup_xe(rec.event_id);

                    update xla_events
                    set    EVENT_STATUS_CODE = 'N',
                           PROCESS_STATUS_CODE = 'U'
                    where event_id = rec.event_id
                    and application_id = 222;

	  End If;


		  debug(rec.adjustment_id||
		        print_spaces(18-length(rec.adjustment_id))||		  
		        rec.gl_date||
		        print_spaces(14-length(rec.gl_date))||
		        rec.event_id||
		        print_spaces(20-length(rec.event_id))
		        );
	
	End Loop;

Exception 
 when others then
    debug ('Exception : '); 
    ROLLBACK;
    RAISE;
End; 
/

spool off