CREATE OR REPLACE PACKAGE APPS.XXC_AR912_installment_letter AUTHID CURRENT_USER
AS
   /* *****************************************************************************
   * Module Type   : PL/SQL
   * Module Name   : xxc.xxc_mod_rp_014_inst_letter_pkg.pks
   * Description   :
   * Run Env.      : SQL*Plus
   *
   * Function Name                  Description
   * AfterReport                    This function will be executed after completion
   *                                of report. This will check the remit advice
   *                                delivery method. If delivery method is EMAIL
   *                                then it will submit the concurrent request for
   *                                bursting program.
   *
   * MOD REG ID :                   MOD_RP_014
   *
   * Known Bugs and Restrictions:   None.
   *
   * History
   * =======
   * Version Name          Date           Description of Change
   * ------- ------------- -----------    -------------------------------------------
   * 0.1     Sameer Bhajan 17-Jul-13      Initial Creation.
   * 0.2     R Chapman     03-Sep-14      Add invno from and to as parameters
   * ****************************************************************************/

   ----------------------------------------------------------------------------
-- Function   : afterreport
-- Description : This function will be invoked automattically after
--               completion of the report. This will submit
--               the concurrent request for bursting program.
----------------------------------------------------------------------------
   p_cust_num_from          VARCHAR2 (30);
   p_cust_num_to            VARCHAR2 (30);
   p_profile_class          VARCHAR2 (30);
   p_direct_debit_flag      VARCHAR2 (1);
   p_instalment_date_from   DATE;
   p_instalment_date_to     DATE;
   p_instalment_invno_from  VARCHAR2 (16);
   p_instalment_invno_to    VARCHAR2 (16);
   g_counter                NUMBER        := 0;

   FUNCTION get_email_id (p_party_site_id IN NUMBER)
      RETURN NUMBER;

   FUNCTION afterreport
      RETURN BOOLEAN;
END XXC_AR912_installment_letter;