CREATE OR REPLACE PACKAGE APPS.XXC_AR912_email_sub AUTHID CURRENT_USER
AS
   /*******************************************************************************

   * Module Type   : PL/SQL
   * Module Name   : xxc_mod_util_001_email_sub_pkg
   * Description   : This package is used for Email Subject changes
   * Run Env.      : SQL*Plus

   * Function name : Description
   * get_org_id    : This fucntion is used to derive ORG id for AME setups.
   *
   *
   * Calling Module: None
   * Modules Called: None
   * Module Number : MOD_UTIL_001

   * Known Bugs and Restrictions: none
   * History
   * =======
   *
   * Version Name              Date         Description of Change
   * ------- -------------     -----------  -------------------------------------------
   * 0.1     Prerak K.         26-Mar-2013  Initial Creation.
   /********************************************************************************/
   /* Not needed RC
   PROCEDURE get_sub_email_lookup_value (
      p_org_id              IN       NUMBER,
      p_business_group_id   IN       NUMBER,
      p_prefix_value        OUT      VARCHAR2,
      p_suffix_value        OUT      VARCHAR2
   );

   PROCEDURE get_sub_email_lookup_soa (
      p_transaction_name   IN       VARCHAR2,
      p_prefix_value       OUT      VARCHAR2,
      p_suffix_value       OUT      VARCHAR2
   );

   PROCEDURE set_subject_attribute (
      p_itemtype    IN       VARCHAR2,
      p_itemkey     IN       VARCHAR2,
      p_actid       IN       NUMBER,
      x_funcmode    IN       VARCHAR2,
      x_resultout   IN OUT   VARCHAR2
   );
   */
   
   FUNCTION get_sub_email_prefix_value (
      p_org_id              IN   NUMBER,
      p_business_group_id   IN   NUMBER
   )
      RETURN VARCHAR2;

   FUNCTION get_sub_email_suffix_value (
      p_org_id              IN   NUMBER,
      p_business_group_id   IN   NUMBER
   )
      RETURN VARCHAR2;
   FUNCTION get_concat_address(
   p_addr2                   IN VARCHAR2,
   p_addr3                  IN VARCHAR2,
   p_addr4                  IN VARCHAR2,
   p_zipcode                IN VARCHAR2
   )
   RETURN  VARCHAR2;
END XXC_AR912_email_sub;
