CREATE OR REPLACE PACKAGE BODY APPS.XXC_AR912_installment_letter
AS
   /* *****************************************************************************
   * Module Type   : PL/SQL
   * Module Name   : xxc.xxc_mod_rp_014_inst_letter_pkg.pkb
   * Description   :
   * Run Env.      : SQL*Plus
   *
   * Function Name                  Description
   * AfterReport                    This function will be executed after completion
   *                                of report. This will check the remit advice
   *                                delivery method. If delivery method is EMAIL
   *                                then it will submit the concurrent request for
   *                                bursting program.
   *
   *
   * MOD REG ID :                    MOD_RP_014
   *
   * Known Bugs and Restrictions:    None.
   *
   * History
   * =======
   * Version Name          Date           Description of Change
   * ------- ------------- -----------    -------------------------------------------
   * 0.1     Sameer Bhajan 17-Jul-13      Initial Creation.
   * ****************************************************************************/

   /**************************
     | PACKAGE BODY
   ***************************/
   --g_counter   NUMBER := 0;

   FUNCTION get_email_id (p_party_site_id IN NUMBER)
      RETURN NUMBER
   IS
      CURSOR get_email (cp_party_site_id NUMBER)
      IS
         SELECT hcp.email_address
           FROM apps.hz_contact_points hcp, applsys.fnd_lookup_values al
          WHERE hcp.contact_point_type = 'EMAIL'
            AND hcp.status = 'A'
            AND al.view_application_id(+) = 222
            AND al.LANGUAGE(+) = USERENV ('LANG')
            AND al.lookup_type(+) = 'CONTACT_POINT_PURPOSE'
            AND hcp.contact_point_purpose = al.lookup_code(+)
            AND hcp.owner_table_name = 'HZ_PARTY_SITES'
            AND hcp.contact_point_purpose = 'INVOICE'
            AND hcp.owner_table_id = cp_party_site_id;

      l_email_address   VARCHAR2 (150) := NULL;
   BEGIN
      IF g_counter = 0
      THEN
         OPEN get_email (p_party_site_id);

         FETCH get_email
          INTO l_email_address;

         CLOSE get_email;

         IF l_email_address IS NOT NULL
         THEN
            g_counter := 1;
         END IF;
      END IF;

      RETURN g_counter;
   EXCEPTION
      WHEN OTHERS
      THEN
         apps.fnd_file.put_line (apps.fnd_file.LOG, 'Exception');
         RETURN g_counter;
   END get_email_id;

   ----------------------------------------------------------------------------
-- Start of AfterReport Function.
----------------------------------------------------------------------------
----------------------------------------------------------------------------
-- Function   : afterreport
-- Description : This function will be invoked automattically after
--               completion of the report. This will check the remit advice
--               delivery method. If delivery method is EMAIL then it will submit
--               the concurrent request for bursting program.
----------------------------------------------------------------------------
   FUNCTION afterreport
      RETURN BOOLEAN
   IS
      l_request_id   NUMBER (15);
      l_req_id       NUMBER (15);
   BEGIN
      --Submit the bursting program
      apps.fnd_file.put_line (apps.fnd_file.LOG, 'After Report');  
      apps.fnd_file.put_line (apps.fnd_file.LOG, 'g_counter='||g_counter);

      IF g_counter = 1
      THEN
         l_request_id := fnd_global.conc_request_id;
         apps.fnd_file.put_line (apps.fnd_file.LOG,
                                 'l_request_id ->' || l_request_id
                                );
-------------------------------------------------------------------------
-- email bursting program
-------------------------------------------------------------------------
         l_req_id :=
            fnd_request.submit_request (application      => 'XDO',
                                        program          => 'XDOBURSTREP',
                                        description      => NULL,
                                        start_time       => NULL,
                                        sub_request      => FALSE,
                                        argument1        => 'Y',
                                        argument2        => l_request_id,
                                        argument3        => 'Y'
                                       );
         apps.fnd_file.put_line (fnd_file.LOG, 'in bursting prog');
         apps.fnd_file.put_line (apps.fnd_file.LOG,
                                    'Reqest Submitted - Request Id -> '
                                 || l_req_id
                                );
      END IF;

      g_counter := 0;
      RETURN TRUE;
   EXCEPTION
      WHEN OTHERS
      THEN
         apps.fnd_file.put_line (apps.fnd_file.LOG, 'Exception');
         RETURN FALSE;
   END afterreport;
----------------------------------------------------------------------------
-- End of AfterReport Function.
----------------------------------------------------------------------------
END XXC_AR912_installment_letter;