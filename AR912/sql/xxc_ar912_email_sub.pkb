CREATE OR REPLACE PACKAGE BODY APPS.XXC_AR912_email_sub
AS
  /*******************************************************************************

  * Module Type   : PL/SQL
  * Module Name   : xxc_mod_util_001_email_sub_pkg
  * Description   : This package is used for Email Subject changes
  * Run Env.      : SQL*Plus

  * Function name : Description
  * get_org_id    : This fucntion is used to derive ORG id for AME setups.
  *
  *
  * Calling Module: None
  * Modules Called: None
  * Module Number : MOD_UTIL_001

  * Known Bugs and Restrictions: none
  * History
  * =======
  *
  * Version Name              Date         Description of Change
  * ------- -------------     -----------  -------------------------------------------
  * 0.1     Prerak K.         26-Mar-2013  Initial Creation.
  --------------------------------------------------------------------------------*/
  /**************************
    | PACKAGE BODY
  ***************************/

  --------------------------------------------------------------------------------
-- Function     :  get_bus_group
-- Description  :  get_bus_group  fucntion is used to derive business group for AME setups.

  -- Parameters:
-- Parm Name          I/O   Description
-- -----------------  ----  ----------------------------------------------------
--  N/A               N/A   ---
-- -----------------  ----  ----------------------------------------------------
  /* Not needed RC
  PROCEDURE get_sub_email_lookup_value (
    p_org_id                         IN       NUMBER,
    p_business_group_id              IN       NUMBER,
    p_prefix_value                   OUT      VARCHAR2,
    p_suffix_value                   OUT      VARCHAR2
  )
  IS
    l_count                                 NUMBER;
    l_suffix_value                          VARCHAR2 (240);
    l_prefix_value                          VARCHAR2 (240);

    CURSOR get_suffix_value_cur (
      cp_org_bg_id                              NUMBER
    )
    IS
      SELECT description
      FROM   applsys.fnd_lookup_values
      WHERE  lookup_type = 'XXC_MOD_UTIL_001_EMAIL_SUBJECT'
      AND    lookup_code = cp_org_bg_id
      AND    enabled_flag = 'Y'
      AND    NVL (end_date_active, SYSDATE + 1) > SYSDATE
      AND    UPPER (tag) = 'S';

    CURSOR get_prefix_value_cur (
      cp_org_bg_id                              NUMBER
    )
    IS
      SELECT description
      FROM   applsys.fnd_lookup_values
      WHERE  lookup_type = 'XXC_MOD_UTIL_001_EMAIL_SUBJECT'
      AND    lookup_code = cp_org_bg_id
      AND    enabled_flag = 'Y'
      AND    NVL (end_date_active, SYSDATE + 1) > SYSDATE
      AND    UPPER (tag) = 'P';

    CURSOR get_org_id_cnt_cur (
      cp_org_bg_id                              NUMBER
    )
    IS
      SELECT COUNT (1)
      FROM   applsys.fnd_lookup_values
      WHERE  lookup_type = 'XXC_MOD_UTIL_001_EMAIL_SUBJECT'
      AND    lookup_code = cp_org_bg_id
      AND    enabled_flag = 'Y'
      AND    NVL (end_date_active, SYSDATE + 1) > SYSDATE
      AND    UPPER (tag) IN ('S', 'P');
  BEGIN
    OPEN get_org_id_cnt_cur (p_org_id);

    FETCH get_org_id_cnt_cur
    INTO  l_count;

    CLOSE get_org_id_cnt_cur;

    IF     l_count = 1
       AND p_org_id IS NOT NULL
    THEN
      OPEN get_suffix_value_cur (p_org_id);

      FETCH get_suffix_value_cur
      INTO  l_suffix_value;

      CLOSE get_suffix_value_cur;

      OPEN get_prefix_value_cur (p_org_id);

      FETCH get_prefix_value_cur
      INTO  l_prefix_value;

      CLOSE get_prefix_value_cur;
    ELSE
      OPEN get_suffix_value_cur (p_business_group_id);

      FETCH get_suffix_value_cur
      INTO  l_suffix_value;

      CLOSE get_suffix_value_cur;

      OPEN get_prefix_value_cur (p_business_group_id);

      FETCH get_prefix_value_cur
      INTO  l_prefix_value;

      CLOSE get_prefix_value_cur;
    END IF;

    p_prefix_value                  := l_prefix_value;
    p_suffix_value                  := l_suffix_value;
  EXCEPTION
    WHEN OTHERS
    THEN
      p_prefix_value                  := NULL;
      p_suffix_value                  := NULL;
  END get_sub_email_lookup_value;

  PROCEDURE get_sub_email_lookup_soa (
    p_transaction_name               IN       VARCHAR2,
    p_prefix_value                   OUT      VARCHAR2,
    p_suffix_value                   OUT      VARCHAR2
  )
  IS
    l_org_id                                NUMBER;
    l_bus_id                                NUMBER;
    l_suffix_value                          VARCHAR2 (240);
    l_prefix_value                          VARCHAR2 (240);
  */  

/* ***THIS***   CURSOR get_business_group_id_cur (
      cp_transaction_name                       VARCHAR2
    )
    IS
      SELECT business_group_id
      FROM   xxc_interface_files
      WHERE  transaction_name = cp_transaction_name; */
  --BEGIN
/* ***THIS***   OPEN get_business_group_id_cur (p_transaction_name);

    FETCH get_business_group_id_cur
    INTO  l_bus_id;

    CLOSE get_business_group_id_cur; */
    /*
	l_bus_id := 101; --***THIS***
    get_sub_email_lookup_value (NULL,
                                l_bus_id,
                                l_prefix_value,
                                l_suffix_value
                               );
    p_prefix_value                  := l_prefix_value;
    p_suffix_value                  := l_suffix_value;
  EXCEPTION
    WHEN OTHERS
    THEN
      p_prefix_value                  := NULL;
      p_suffix_value                  := NULL;
  END get_sub_email_lookup_soa;

  PROCEDURE set_subject_attribute (
    p_itemtype                       IN       VARCHAR2,
    p_itemkey                        IN       VARCHAR2,
    p_actid                          IN       NUMBER,
    x_funcmode                       IN       VARCHAR2,
    x_resultout                      IN OUT   VARCHAR2
  )
  IS
    l_suffix_value                          VARCHAR2 (240);
    l_prefix_value                          VARCHAR2 (240);
  BEGIN
/* ***THIS***   IF (x_funcmode <> apps.wf_engine.eng_run)
    THEN
      x_resultout                     := wf_engine.eng_null;
      RETURN;
    END IF; */

/*  ***THIS***  IF (x_funcmode = 'RUN')
    THEN
      get_sub_email_lookup_value (fnd_profile.VALUE ('ORG_ID'),
                                  fnd_profile.VALUE ('PER_BUSINESS_GROUP_ID'),
                                  l_prefix_value,
                                  l_suffix_value
                                 );
      apps.wf_engine.setitemattrtext (itemtype                          => p_itemtype,
                                 itemkey                           => p_itemkey,
                                 aname                             => 'XXC_PREFIX_VALUE',
                                 avalue                            => l_prefix_value
                                );
      apps.wf_engine.setitemattrtext (itemtype                          => p_itemtype,
                                 itemkey                           => p_itemkey,
                                 aname                             => 'XXC_SUFFIX_VALUE',
                                 avalue                            => l_suffix_value
                                );
      x_resultout                     := 'COMPLETE:'; 
    END IF; */
  --  x_resultout                     := 'COMPLETE:'; 
  --END set_subject_attribute;

  FUNCTION get_sub_email_prefix_value (
    p_org_id                         IN       NUMBER,
    p_business_group_id              IN       NUMBER
  )
    RETURN VARCHAR2
  IS
    l_count                                 NUMBER;
    l_suffix_value                          VARCHAR2 (240);
    l_prefix_value                          VARCHAR2 (240);

    CURSOR get_prefix_value_cur (
      cp_org_bg_id                              NUMBER
    )
    IS
      SELECT description
      FROM   applsys.fnd_lookup_values
      WHERE  lookup_type = 'XXC_AR912_EMAIL_SUBJECT'
      AND    lookup_code = cp_org_bg_id
      AND    enabled_flag = 'Y'
      AND    NVL (end_date_active, SYSDATE + 1) > SYSDATE
      AND    UPPER (tag) = 'P';

    CURSOR get_org_id_cnt_cur (
      cp_org_bg_id                              NUMBER
    )
    IS
      SELECT COUNT (1)
      FROM   applsys.fnd_lookup_values
      WHERE  lookup_type = 'XXC_AR912_EMAIL_SUBJECT'
      AND    lookup_code = cp_org_bg_id
      AND    enabled_flag = 'Y'
      AND    NVL (end_date_active, SYSDATE + 1) > SYSDATE
      AND    UPPER (tag) IN ('S', 'P');
  BEGIN
    OPEN get_org_id_cnt_cur (p_org_id);

    FETCH get_org_id_cnt_cur
    INTO  l_count;

    CLOSE get_org_id_cnt_cur;

    IF     l_count = 1
       AND p_org_id IS NOT NULL
    THEN
      OPEN get_prefix_value_cur (p_org_id);

      FETCH get_prefix_value_cur
      INTO  l_prefix_value;

      CLOSE get_prefix_value_cur;
    ELSE
      OPEN get_prefix_value_cur (p_business_group_id);

      FETCH get_prefix_value_cur
      INTO  l_prefix_value;

      CLOSE get_prefix_value_cur;
    END IF;

    RETURN l_prefix_value;
  EXCEPTION
    WHEN OTHERS
    THEN
      RETURN NULL;
  END get_sub_email_prefix_value;

  FUNCTION get_sub_email_suffix_value (
    p_org_id                         IN       NUMBER,
    p_business_group_id              IN       NUMBER
  )
    RETURN VARCHAR2
  IS
    l_count                                 NUMBER;
    l_suffix_value                          VARCHAR2 (240);
    l_prefix_value                          VARCHAR2 (240);
--***THIS*** applsys in two cursors:
    CURSOR get_suffix_value_cur (
      cp_org_bg_id                              NUMBER
    )
    IS
      SELECT description
      FROM   applsys.fnd_lookup_values
      WHERE  lookup_type = 'XXC_AR912_EMAIL_SUBJECT'
      AND    lookup_code = cp_org_bg_id
      AND    enabled_flag = 'Y'
      AND    NVL (end_date_active, SYSDATE + 1) > SYSDATE
      AND    UPPER (tag) = 'S';

    CURSOR get_org_id_cnt_cur (
      cp_org_bg_id                              NUMBER
    )
    IS
      SELECT COUNT (1)
      FROM   applsys.fnd_lookup_values
      WHERE  lookup_type = 'XXC_AR912_EMAIL_SUBJECT'
      AND    lookup_code = cp_org_bg_id
      AND    enabled_flag = 'Y'
      AND    NVL (end_date_active, SYSDATE + 1) > SYSDATE
      AND    UPPER (tag) = 'S';
  BEGIN
    OPEN get_org_id_cnt_cur (p_org_id);

    FETCH get_org_id_cnt_cur
    INTO  l_count;

    CLOSE get_org_id_cnt_cur;

    IF     l_count = 1
       AND p_org_id IS NOT NULL
    THEN
      OPEN get_suffix_value_cur (p_org_id);

      FETCH get_suffix_value_cur
      INTO  l_suffix_value;

      CLOSE get_suffix_value_cur;
    ELSE
      OPEN get_suffix_value_cur (p_business_group_id);

      FETCH get_suffix_value_cur
      INTO  l_suffix_value;

      CLOSE get_suffix_value_cur;
    END IF;

    RETURN l_suffix_value;
  EXCEPTION
    WHEN OTHERS
    THEN
      RETURN NULL;
  END get_sub_email_suffix_value;

  FUNCTION get_concat_address (
    p_addr2                          IN       VARCHAR2,
    p_addr3                          IN       VARCHAR2,
    p_addr4                          IN       VARCHAR2,
    p_zipcode                        IN       VARCHAR2
  )
    RETURN VARCHAR2
  IS
    l_addr2                                 VARCHAR2 (240) := NULL;
    l_addr3                                 VARCHAR2 (240) := NULL;
    l_addr4                                 VARCHAR2 (240) := NULL;
    l_zipcode                               VARCHAR2 (240) := NULL;
  BEGIN
    IF p_addr2 IS NOT NULL
    THEN
      l_addr2                         := p_addr2 || '|';
    ELSE
      l_addr2                         := '';
    END IF;

    IF p_addr3 IS NOT NULL
    THEN
      l_addr3                         := p_addr3 || '|';
    ELSE
      l_addr3                         := '';
    END IF;

    IF p_addr4 IS NOT NULL
    THEN
      l_addr4                         := p_addr4 || '|';
    ELSE
      l_addr4                         := '';
    END IF;

    IF p_zipcode IS NOT NULL
    THEN
      l_zipcode                       := p_zipcode || '|';
    ELSE
      l_zipcode                       := '';
    END IF;

    RETURN l_addr2 || l_addr3 || l_addr4 || l_zipcode;
  EXCEPTION
    WHEN OTHERS
    THEN
      RETURN NULL;

  END get_concat_address;
END XXC_AR912_email_sub;