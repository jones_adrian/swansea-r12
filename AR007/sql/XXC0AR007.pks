CREATE OR REPLACE
PACKAGE xxc0ar007_print_copy_trx
AS
/*************************************************************
$Header$

Original Author: Yousuf Husaini

Module Type    : SQL*Plus

Description    : This package will extract all Copy Invoices from Account
                 Receivables where print option is 'PRI' along with the customer details.


Modification History:
$Log$

21/08/2006     Yousuf Husaini    Initial Creation
**************************************************************/
    FUNCTION LINE_COUNT ( 
      p_input_string VARCHAR2)   RETURN PLS_INTEGER;
   FUNCTION WORD_WRAP (
      p_input_string VARCHAR2,
      p_column_width NUMBER DEFAULT 40,
      p_max_input_length NUMBER DEFAULT 240)RETURN VARCHAR2;
   PROCEDURE xxc_main (
      p_errbuf                    OUT      VARCHAR2,
      p_retcode                   OUT      NUMBER,
      p_cust_trx_class            IN       VARCHAR2,
      p_cust_trx_type_id          IN       NUMBER,
      p_trx_number_low            IN       VARCHAR2,
      p_trx_number_high           IN       VARCHAR2,
      p_dates_low                 IN       DATE,
      p_dates_high                IN       DATE,
      p_customer_class_code       IN       VARCHAR2,
      p_customer_id               IN       NUMBER,
      p_installment_number        IN       NUMBER,
      p_open_invoice              IN       VARCHAR2,
      p_tax_registration_number   IN       NUMBER,
      p_from_dunning              IN       VARCHAR2
   );
END xxc0ar007_print_copy_trx;
