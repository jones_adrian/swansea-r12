CREATE OR REPLACE
PACKAGE BODY xxc0ar007_print_copy_trx
AS
/*************************************************************
$Header$
Original Author: Yousuf Husaini
Module Type    : SQL*Plus
Description    : This package will extract all Copy Invoices from Account
                 Receivables where print option is 'PRI' along with the customer details.
Modification History:
$Log$
21/08/2006     Yousuf Husaini     Initial Creation
19/1/2007      subbarayudu        added rctl.line_number and order by clause in the cursor query
**************************************************************/
FUNCTION      LINE_COUNT (
  p_input_string VARCHAR2)
RETURN PLS_INTEGER AS
  CR CONSTANT VARCHAR2(1) := CHR(10);
  l_line_count PLS_INTEGER := 0;
  l_loop_count PLS_INTEGER := 0;
  l_currpos    PLS_INTEGER;
  l_startpos   PLS_INTEGER := 1;
BEGIN
  IF p_input_string IS NOT NULL THEN
    LOOP
      l_loop_count := l_loop_count + 1;
      l_currpos := INSTR(p_input_string, CR, l_startpos);
      IF l_currpos > 0 THEN
        l_startpos := l_currpos + 1;
      ELSE
        EXIT;
      END IF;
    END LOOP;
    l_line_count := l_loop_count;
  ELSE
    l_line_count := 0;
  END IF;
  RETURN l_line_count;
END LINE_COUNT;
FUNCTION      WORD_WRAP (
  p_input_string VARCHAR2,
  p_column_width NUMBER DEFAULT 40,
  p_max_input_length NUMBER DEFAULT 240)
RETURN VARCHAR2 AS
  CR CONSTANT VARCHAR2(1) := CHR(10);
  SP CONSTANT VARCHAR2(1) := ' ';
  l_input_string VARCHAR2(4001) := SUBSTR(REPLACE(p_input_string,CR,SP)
                                   ,1,p_max_input_length);
  l_output_string VARCHAR2(4001);
  l_tmp_string    VARCHAR2(4001);
  l_input_string_length PLS_INTEGER;
  l_startpos            PLS_INTEGER := 1;
  l_endpos              PLS_INTEGER := 0;
  l_wrappos             PLS_INTEGER;
  l_loop_count          PLS_INTEGER := 0;
BEGIN
  l_input_string_length := LENGTH(l_input_string);
  --dbms_output.put_line(l_input_string||' '||l_input_string_length);
  IF l_input_string_length <= p_column_width THEN
    l_output_string := l_input_string;
  ELSE
    LOOP
      l_loop_count := l_loop_count + 1;
      EXIT WHEN l_startpos >= l_input_string_length;
      --dbms_output.put_line('l_startpos='||l_startpos||' p_column_width='||p_column_width);
      l_tmp_string := SUBSTRB(l_input_string, l_startpos, p_column_width);
      IF LENGTH(l_tmp_string) = p_column_width THEN
        l_wrappos := INSTR(l_tmp_string,SP,-1);
        IF l_wrappos = 0 THEN l_wrappos := p_column_width; END IF;
        l_tmp_string := SUBSTRB(l_tmp_string,1,l_wrappos);
        --dbms_output.put_line('l_tmp_string='||l_tmp_string);
        l_endpos := l_endpos + l_wrappos;
        l_startpos := l_endpos + 1;
        l_output_string := l_output_string || l_tmp_string ||CHR(10);
      ELSE
        l_output_string := l_output_string || l_tmp_string;
        EXIT;
      END IF;     
      --dbms_output.put_line('l_output_string='||l_output_string);
    END LOOP;
  END IF;
  RETURN l_output_string;
END WORD_WRAP;
   PROCEDURE xxc_main (
      p_errbuf                    OUT      VARCHAR2,
      p_retcode                   OUT      NUMBER,
      p_cust_trx_class            IN       VARCHAR2,
      p_cust_trx_type_id          IN       NUMBER,
      p_trx_number_low            IN       VARCHAR2,
      p_trx_number_high           IN       VARCHAR2,
      p_dates_low                 IN       DATE,
      p_dates_high                IN       DATE,
      p_customer_class_code       IN       VARCHAR2,
      p_customer_id               IN       NUMBER,
      p_installment_number        IN       NUMBER,
      p_open_invoice              IN       VARCHAR2,
      p_tax_registration_number   IN       NUMBER,
      p_from_dunning              IN       VARCHAR2
   )
   IS
-- ----------------------------------------------------------------------------
-- Procedure: XXC_MAIN
--
-- Description: This procedure extracts all Copy Invoices from Account
--              Receivables along with the customer details.
--
-- Parameters:
-- Name                       IN/OUT   Description
-- --------------------       -------  ---------------------------------------------
--
-- p_errbuf                    OUT      Error Message
-- p_retcode                   OUT      Error Code
-- p_cust_trx_class            IN       Customer Transaction Class
-- p_cust_trx_type_id          IN       Customer Transaction Type Id
-- p_trx_number_low            IN       Customer Transaction Number Low
-- p_trx_number_high           IN       Customer Transaction Number High
-- p_dates_low                 IN       Print Date Low
-- p_dates_high                IN       Print Date High
-- p_customer_class_code       IN       Customer Class Code
-- p_customer_id               IN       Customer Id
-- p_installment_number        IN       Installment Number
-- p_open_invoice              IN       Open Invoice
-- p_tax_registration_number   IN       Tax Registration Number
-- p_from_dunning              IN       From Dunning will decide if this program is called
--                                      from Dunning Letter Program or 
--                                      Print Copy Invoices Program
-- ----------------------------------------------------------------------------
---------------------------------------------
-- Variable Declaration
---------------------------------------------
      l_data                      VARCHAR2 (4000);
      l_file_handle               UTL_FILE.file_type;
      l_output_dir                VARCHAR2 (100)
                                   := fnd_profile.VALUE ('XXC_COPY_INVOICES');
      l_output_file               VARCHAR2 (100);
      l_request_id                NUMBER;
      l_total_succeed_rec         NUMBER                                 := 0;
      l_total_error_rec           NUMBER                                 := 0;
      l_csr_rec_count             NUMBER                                 := 0;
      l_errcode                   NUMBER;
      l_errdesc                   VARCHAR2 (250);
      l_printq_profile            VARCHAR2 (250);
      l_request_id_file           NUMBER;
      l_file_location             VARCHAR2 (250);
      l_max_cust_trx_line_id      ra_customer_trx_lines_all.customer_trx_line_id%TYPE;
      l_freight_extended_amount   ra_customer_trx_lines_all.extended_amount%TYPE;
      l_cust_trx_class            VARCHAR2(10);--sunil kalal
      l_ship_to                   VARCHAR2(10) ;--sunil kalal
      l_transaction_no1            ra_customer_trx_all.trx_number%TYPE     :=0;
      l_transaction_no2            ra_customer_trx_all.trx_number%TYPE     :=0;
      l_transaction_count         NUMBER                                   :=0;
      l_data_transaction_count    NUMBER                                   :=0;
      l_data_transaction_no1      NUMBER                                   :=0;
      l_data_transaction_no2      NUMBER                                   :=0;
      l_customer_title            VARCHAR2(3000);
      l_lines                     VARCHAR2(10);
      l_end                       VARCHAR2(10);
      l_end_tag                   VARCHAR2(10);
      l_end_tag1                  VARCHAR2(10);
      l_count_end_tag             NUMBER                                    :=0;
      l_page_invoice_lines        NUMBER 
                                  :=fnd_profile.VALUE('XXC_PAGE1_INVOICE_LINES');
      l_page_overflow_lines       NUMBER
                                  :=fnd_profile.VALUE('XXC_OVERFLOW_LINES');
     l_inv_comments               NUMBER 
                                  :=fnd_profile.VALUE('XXC_INV_COMMENTS');
     l_comments_word_wrap         VARCHAR2(3000);
     l_comments_from_function     VARCHAR2(3000);
     CR                           CONSTANT VARCHAR2(1) := CHR(10);
     SP                           CONSTANT VARCHAR2(1) := ' ';
     l_lines_available            NUMBER                                    :=0;
     l_lines_used                 NUMBER                                    :=0;
     l_total_lines_used           NUMBER                                    :=0;
     l_description_word_wrap      VARCHAR2(3000);
     l_description_lines          NUMBER;
     l_page_tag                   VARCHAR2(10);
     l_page_tag1                  VARCHAR2(10);
     l_last_end_tag               VARCHAR2(10);                                   
     l_last_count                 NUMBER :=0;
     l_overflow                   VARCHAR2(1) :='N';
     
---------------------------------------------------------------------------------------------------
-- Table Declaration for all Copy Invoices.
---------------------------------------------------------------------------------------------------
      TYPE l_copy_invoices_rec IS RECORD (
         rec_cust_trx_class_tag       VARCHAR2(10),
         rec_transaction_number       ra_customer_trx_all.trx_number%TYPE,
         rec_reference_field          ra_customer_trx_all.customer_reference%TYPE,
         rec_transaction_date         ra_customer_trx_all.trx_date%TYPE,
         rec_due_date                 ra_customer_trx_all.term_due_date%TYPE,
         rec_bill_to_customer_title   hz_parties.person_pre_name_adjunct%TYPE,
         rec_bill_to_customer_name    hz_parties.party_name%TYPE,
         rec_bill_to_contact          VARCHAR2 (200),
         rec_bill_to_address_line_1   hz_locations.address1%TYPE,
         rec_bill_to_address_line_2   hz_locations.address2%TYPE,
         rec_bill_to_address_line_3   hz_locations.address3%TYPE,
         rec_bill_to_town_city        hz_locations.city%TYPE,
         rec_bill_to_county           hz_locations.county%TYPE,
         rec_bill_to_postal_code      hz_locations.postal_code%TYPE,
         rec_ship_to_tag              VARCHAR2(10),
         rec_ship_to_customer_name    hz_parties.party_name%TYPE,
         rec_ship_to_contact          VARCHAR2 (200),
         rec_ship_to_address_line_1   hz_locations.address1%TYPE,
         rec_ship_to_address_line_2   hz_locations.address2%TYPE,
         rec_ship_to_address_line_3   hz_locations.address3%TYPE,
         rec_ship_to_town_city        hz_locations.city%TYPE,
         rec_ship_to_county           hz_locations.county%TYPE,
         rec_ship_to_postal_code      hz_locations.postal_code%TYPE,
         rec_line_description         ra_customer_trx_lines_all.description%TYPE,
         rec_line_extended_amount     ra_customer_trx_lines_all.extended_amount%TYPE,
         rec_tax_extended_amount      ra_customer_trx_lines_all.extended_amount%TYPE,
         rec_tax_rate                 ar_vat_tax.tax_code%TYPE,
         rec_lines_total              ar_payment_schedules_all.amount_line_items_original%TYPE,
         rec_tax_total                ar_payment_schedules_all.tax_original%TYPE,
         rec_transaction_total        ar_payment_schedules_all.amount_due_original%TYPE,
         rec_originators_name         ra_customer_trx_all.attribute1%TYPE,
         rec_originators_dept         ra_customer_trx_all.attribute2%TYPE,
         rec_originators_extension    ra_customer_trx_all.attribute3%TYPE,
         rec_original_trx_number      ra_customer_trx_all.trx_number%TYPE,
         rec_comments                 ra_customer_trx_all.internal_notes%TYPE,
         rec_lines_tag                VARCHAR2(10),
         rec_page_tag                 VARCHAR2(10),
         rec_end_tag                  VARCHAR2(10)
      );
      TYPE l_copy_invoices_table_type IS TABLE OF l_copy_invoices_rec
         INDEX BY BINARY_INTEGER;
      l_copy_invoices_tab         l_copy_invoices_table_type;
   BEGIN
      l_request_id := fnd_global.conc_request_id;
      l_output_file :=
            'XXC0AR007_COPY_INVOICES'
         || '_'
         || TO_CHAR (SYSDATE, 'YYYYMMDDHHMISS')
         || '.dat';
--      l_output_dir :=   '/u01/app/EGDEV2/db/10.1.0/appsutil/outbound/EGDEV2_eggdbs01';
      
      FOR l_csr_rec IN
         (SELECT a.customer_trx_id transaction_id,
                 abs(rctl.extended_amount) freight_amount,
                 rctl.customer_trx_line_id cust_trx_line_id,
                 rctl.line_type trx_line_type, TYPES.TYPE trx_class,
                 TYPES.NAME transaction_type, 
                  a.trx_number transaction_number,
                  rctl.line_number   line_number,
                 DECODE (a.purchase_order,
                         '', a.customer_reference,
                         '/' || a.purchase_order
                        ) reference_field,
                 a.trx_date transaction_date, v.term_due_date due_date,
                 party.person_pre_name_adjunct bill_to_customer_title,
                 v.rac_bill_to_customer_name bill_to_customer_name,
                 v.raco_bill_to_contact_name bill_to_contact,
                 v.raa_bill_to_address1 bill_to_customer_address_line1,
                 v.raa_bill_to_address2 bill_to_customer_address_line2,
                 v.raa_bill_to_address3_db bill_to_customer_address_line3,
                 v.raa_bill_to_city bill_to_town_city,
                 v.raa_bill_to_state bill_to_county,
                 v.raa_bill_to_postal_code bill_to_postal_code,
                 v.rac_ship_to_customer_name ship_to_customer_name,
                 v.raco_ship_to_contact_name ship_to_contact,
                 v.raa_ship_to_address1 ship_to_customer_address_line1,
                 v.raa_ship_to_address2 ship_to_customer_address_line2,
                 v.raa_ship_to_address3_db ship_to_customer_address_line3,
                 v.raa_ship_to_city ship_to_town_city,
                 v.raa_ship_to_state ship_to_county,
                 v.raa_ship_to_postal_code ship_to_postal_code,
                 DECODE
                    (TYPES.TYPE,
                     'INV', DECODE (UPPER (rctl.line_type),
                                    'FREIGHT', DECODE
                                                   (TYPES.NAME,
                                                    'County Supplies', ool.ordered_item
                                                     || rctl.description
                                                     || abs(rctl.quantity_invoiced)
                                                     || '@'
                                                     || abs(rctl.unit_selling_price),
                                                    rctl.description
                                                   ),
                                    DECODE (TYPES.NAME,
                                            'County Supplies', ool.ordered_item
                                             || rctl.description
                                             || abs(rctl.quantity_invoiced)
                                             || '@'
                                             || abs(rctl.unit_selling_price),
                                            rctl.description
                                           )
                                   ),
                     rctl.description
                    ) line_description,
                 DECODE (rctl.line_type,
                         'LINE', abs(rctl.extended_amount),
                         'FREIGHT', abs(rctl.extended_amount),
                         NULL
                        ) line_extended_amount,
                  abs(rctl.extended_amount) tax_extended_amount,
                  DECODE (rctl.line_type,
                         'FREIGHT',NULL,
                         avt.tax_code
                          ) tax_rate,
                 abs(aps.lines_total) lines_total,
                 abs(aps.tax_total) tax_total,
                 abs(aps.transaction_total) transaction_total,
                 a.attribute1 originators_name, a.attribute2 originators_dept,
                 a.attribute3 originator_telephone_extension,
                 a.trx_number original_transaction_number,
                 a.internal_notes comments
            FROM ra_customer_trx_lines rctl,
                 ra_customer_trx a,
                 ra_customer_trx_partial_v v,
                 hz_cust_accounts b,
                 hz_parties party,
                 hz_cust_site_uses u_bill,
                 hz_cust_acct_sites a_bill,
                 hz_party_sites party_site,
                 hz_locations loc,
                 ra_cust_trx_types TYPES,
                 ra_terms_lines tl,
                 ra_terms t,
                 ar_payment_schedules p,
                 ar_adjustments com_adj,
                 ra_cust_trx_line_gl_dist rec,
                 ar_lookups l_types,
                 ar_system_parameters asp,
                 ar_vat_tax avt,
                 oe_order_lines ool,
                 (SELECT   customer_trx_id,
                           SUM (amount_line_items_original) lines_total,
                           SUM (tax_original) tax_total,
                           SUM (amount_due_original) transaction_total
                      FROM ar_payment_schedules_all
                  GROUP BY customer_trx_id) aps
           WHERE rctl.customer_trx_id = a.customer_trx_id
            -- AND a.trx_number in ('10000000','50000005','50000003')  --Sunil Kalal
             AND rctl.line_type<>'TAX'
             AND a.customer_trx_id = aps.customer_trx_id(+)
             AND a.trx_number = v.trx_number
             AND a.bill_to_customer_id = b.cust_account_id
             AND rec.customer_trx_id = a.customer_trx_id
             AND rctl.vat_tax_id=avt.vat_tax_id
             AND rctl.set_of_books_id=avt.set_of_books_id
             AND rec.latest_rec_flag = 'Y'
             AND rec.account_class = 'REC'
             AND p.payment_schedule_id + DECODE (p.CLASS, 'INV', 0, '') = com_adj.payment_schedule_id(+)
             AND com_adj.subsequent_trx_id IS NULL
             AND 'C' = com_adj.adjustment_type(+)
             AND a.complete_flag = 'Y'
             AND a.cust_trx_type_id = TYPES.cust_trx_type_id
             AND l_types.lookup_type = 'INV/CM/ADJ'
             AND a.printing_option IN ('PRI', 'REP')
             AND TYPES.TYPE IN ('INV', 'CM')
             AND l_types.lookup_code =
                                 DECODE (TYPES.TYPE,
                                         'DEP', 'INV',
                                         TYPES.TYPE
                                        )
             AND NVL (p.terms_sequence_number, NVL (tl.sequence_num, 0)) =
                       NVL (tl.sequence_num, NVL (p.terms_sequence_number, 0))
             AND DECODE (p.payment_schedule_id,
                         '', 0,
                         NVL (t.printing_lead_days, 0)
                        ) = 0
             AND a.bill_to_site_use_id = u_bill.site_use_id
             AND u_bill.cust_acct_site_id = a_bill.cust_acct_site_id
             AND a_bill.party_site_id = party_site.party_site_id
             AND b.party_id = party.party_id
             AND loc.location_id = party_site.location_id
             AND NVL (loc.LANGUAGE, 'US') = 'US'
             AND NVL (p.amount_due_remaining, 0) <> 0
             AND a.term_id = tl.term_id(+)
             AND a.term_id = t.term_id(+)
             AND a.customer_trx_id = p.customer_trx_id(+)
             AND a.org_id = asp.org_id
             AND rctl.customer_trx_line_id = ool.reference_customer_trx_line_id(+)
             AND (TYPES.TYPE = p_cust_trx_class OR p_cust_trx_class IS NULL)
             AND (   TYPES.cust_trx_type_id = p_cust_trx_type_id
                  OR p_cust_trx_type_id IS NULL
                 )
          -- AND a.trx_number BETWEEN NVL (p_trx_number_low, 0)--COMMENTED BY SUNIL KALAL
           --                       AND NVL (p_trx_number_high,--AS PER SUGGESTIONS FROM NAVIN KAMAT
           --                                99999999999999999999--AND KIRAN KUMAR FOR THE SAKE OF 
           --     DUNNING LETTER PROGRAMME
              AND (
            (
             (P_FROM_DUNNING = 'N') and (a.trx_number BETWEEN NVL (p_trx_number_low, 0) AND NVL (p_trx_number_high,99999999999999999999))
            )
          OR
            (P_FROM_DUNNING = 'Y' and a.trx_number in (select trx_number from XXC_AR901_COPY_INVOICES)) 
                 )      --ADDED BY SUNIL KALAL AS SUGGESTED BY NAVI KAMAT AND KIRAN KUMAR ON 17 OCT 2006          
                       -- FOR THE SAKE OF DUNNING LETTER PROGRAMME 
             AND (   b.customer_class_code = p_customer_class_code
                  OR p_customer_class_code IS NULL
                 )
             AND (b.cust_account_id = p_customer_id OR p_customer_id IS NULL
                 )
             AND (   TRUNC (a.trx_date) BETWEEN TRUNC (p_dates_low)
                                            AND TRUNC (p_dates_high)
                  OR (p_dates_low IS NULL AND p_dates_low IS NULL)
                 )
             AND (   tl.sequence_num = p_installment_number
                  OR p_installment_number IS NULL
                 )
             AND DECODE (p_open_invoice,
                         'Y', DECODE (   a.previous_customer_trx_id
                                      || a.initial_customer_trx_id,
                                      '', NVL (p.amount_due_remaining, 1),
                                      1
                                     ),
                         1
                        ) <> 0
             AND (   asp.tax_registration_number = p_tax_registration_number
                  OR p_tax_registration_number IS NULL
                 )
          UNION
          SELECT a.customer_trx_id transaction_id,
                 rctl.extended_amount freight_amount,
                 rctl.customer_trx_line_id cust_trx_line_id,
                 rctl.line_type trx_line_type, TYPES.TYPE trx_class,
                 TYPES.NAME transaction_type, 
                 a.trx_number transaction_number,
                 rctl.line_number  line_number,
                 DECODE (a.purchase_order,
                         '', a.customer_reference,
                         '/' || a.purchase_order
                        ) reference_field,
                 a.trx_date transaction_date, v.term_due_date due_date,
                 party.person_pre_name_adjunct bill_to_customer_title,
                 v.rac_bill_to_customer_name bill_to_customer_name,
                 v.raco_bill_to_contact_name bill_to_contact,
                 v.raa_bill_to_address1 bill_to_customer_address_line1,
                 v.raa_bill_to_address2 bill_to_customer_address_line2,
                 v.raa_bill_to_address3_db bill_to_customer_address_line3,
                 v.raa_bill_to_city bill_to_town_city,
                 v.raa_bill_to_state bill_to_county,
                 v.raa_bill_to_postal_code bill_to_postal_code,
                 v.rac_ship_to_customer_name ship_to_customer_name,
                 v.raco_ship_to_contact_name ship_to_contact,
                 v.raa_ship_to_address1 ship_to_customer_address_line1,
                 v.raa_ship_to_address2 ship_to_customer_address_line2,
                 v.raa_ship_to_address3_db ship_to_customer_address_line3,
                 v.raa_ship_to_city ship_to_town_city,
                 v.raa_ship_to_state ship_to_county,
                 v.raa_ship_to_postal_code ship_to_postal_code,
                 DECODE
                    (TYPES.TYPE,
                     'INV', DECODE (UPPER (rctl.line_type),
                                    'FREIGHT', DECODE
                                                   (TYPES.NAME,
                                                    'County Supplies', ool.ordered_item
                                                     || rctl.description
                                                     || abs(rctl.quantity_invoiced)
                                                     || '@'
                                                     || abs(rctl.unit_selling_price),
                                                    rctl.description
                                                   ),
                                    DECODE (TYPES.NAME,
                                            'County Supplies', ool.ordered_item
                                             || rctl.description
                                             || abs(rctl.quantity_invoiced)
                                             || '@'
                                             || abs(rctl.unit_selling_price),
                                            rctl.description
                                           )
                                   ),
                     rctl.description
                    ) line_description,
                 DECODE (rctl.line_type,
                         'LINE', abs(rctl.extended_amount),
                         'FREIGHT', abs(rctl.extended_amount),
                         NULL
                        ) line_extended_amount,
                  abs(rctl.extended_amount) tax_extended_amount,
                  DECODE (rctl.line_type,
                         'FREIGHT',NULL,
                         avt.tax_code
                          ) tax_rate,
                 abs(aps.lines_total) lines_total,
                 abs(aps.tax_total) tax_total,
                 abs(aps.transaction_total) transaction_total,
                 a.attribute1 originators_name, a.attribute2 originators_dept,
                 a.attribute3 originator_telephone_extension,
                 a.trx_number original_transaction_number,
                 a.internal_notes comments
            FROM ra_customer_trx_lines rctl,
                 ra_customer_trx a,
                 ra_customer_trx_partial_v v,
                 hz_cust_accounts b,
                 hz_parties party,
                 hz_cust_site_uses u_bill,
                 hz_cust_acct_sites a_bill,
                 hz_party_sites party_site,
                 hz_locations loc,
                 ar_vat_tax avt,
                 ra_cust_trx_types TYPES,
                 ra_terms_lines tl,
                 ra_terms t,
                 ar_payment_schedules p,
                 ar_adjustments com_adj,
                 ar_lookups l_types,
                 ar_system_parameters asp,
                 oe_order_lines ool,
                 (SELECT   customer_trx_id,
                           SUM (amount_line_items_original) lines_total,
                           SUM (tax_original) tax_total,
                           SUM (amount_due_original) transaction_total
                      FROM ar_payment_schedules_all
                  GROUP BY customer_trx_id) aps
           WHERE rctl.customer_trx_id = a.customer_trx_id
          --   AND a.trx_number in ('10000000','50000005','50000003')--sunil kalal
             AND rctl.line_type<>'TAX'--added by Sunil Kalal
             AND a.customer_trx_id = aps.customer_trx_id(+)
             AND a.trx_number = v.trx_number
             AND a.bill_to_customer_id = b.cust_account_id
             AND rctl.vat_tax_id=avt.vat_tax_id
             AND rctl.set_of_books_id=avt.set_of_books_id
             AND p.payment_schedule_id + DECODE (p.CLASS, 'INV', 0, '') = com_adj.payment_schedule_id(+)
             AND com_adj.subsequent_trx_id IS NULL
             AND 'C' = com_adj.adjustment_type(+)
             AND a.complete_flag = 'Y'
             AND a.customer_trx_id = p.customer_trx_id
             AND a.cust_trx_type_id = TYPES.cust_trx_type_id
             AND l_types.lookup_type = 'INV/CM/ADJ'
             AND a.printing_option IN ('PRI', 'REP')
             AND TYPES.TYPE IN ('INV', 'CM')
             AND l_types.lookup_code =
                                 DECODE (TYPES.TYPE,
                                         'DEP', 'INV',
                                         TYPES.TYPE
                                        )
             AND NVL (t.printing_lead_days, 0) > 0
             AND a.bill_to_site_use_id = u_bill.site_use_id
             AND u_bill.cust_acct_site_id = a_bill.cust_acct_site_id
             AND a_bill.party_site_id = party_site.party_site_id
             AND b.party_id = party.party_id
             AND loc.location_id = party_site.location_id
             AND NVL (loc.LANGUAGE, 'US') = 'US'
             AND NVL (p.terms_sequence_number, tl.sequence_num) =
                                                               tl.sequence_num
             AND NVL (p.amount_due_remaining, 0) <> 0
             AND t.term_id = p.term_id
             AND tl.term_id(+) = t.term_id
             AND a.org_id = asp.org_id
             AND rctl.customer_trx_line_id = ool.reference_customer_trx_line_id(+)
             AND (TYPES.TYPE = p_cust_trx_class OR p_cust_trx_class IS NULL)
             AND (   TYPES.cust_trx_type_id = p_cust_trx_type_id
                  OR p_cust_trx_type_id IS NULL
                 )
            -- AND a.trx_number BETWEEN NVL (p_trx_number_low, 0)--COMMENTED BY SUNIL KALAL AS SUGGESTED BY 
              --                    AND NVL (p_trx_number_high,--NAVIN KAMAT AND KIRAN KUMAR
                --                           99999999999999999999--FOR THE SAKE OF DUNNING LETTER PROGRAMME
                  --                        )  DATED 17 OCT 2006
                AND (
            (
             (P_FROM_DUNNING = 'N') and (a.trx_number BETWEEN NVL (p_trx_number_low, 0) AND NVL (p_trx_number_high,99999999999999999999))
            )
          OR
            (P_FROM_DUNNING = 'Y' and a.trx_number in (select trx_number from XXC_AR901_COPY_INVOICES)) 
                 )      --ADDED BY SUNIL KALAL AS SUGGESTED BY NAVI KAMAT AND KIRAN KUMAR ON 17 OCT 2006          
                       -- FOR THE SAKE OF DUNNING LETTER PROGRAMME                           
             AND (   b.customer_class_code = p_customer_class_code
                  OR p_customer_class_code IS NULL
                 )
             AND (b.cust_account_id = p_customer_id OR p_customer_id IS NULL
                 )
             AND (   TRUNC (p.due_date) BETWEEN   TRUNC (p_dates_low)
                                                + NVL (t.printing_lead_days,
                                                       0)
                                            AND   TRUNC (p_dates_high)
                                                + NVL (t.printing_lead_days,
                                                       0)
                  OR (p_dates_low IS NULL AND p_dates_high IS NULL)
                 )
             AND (   tl.sequence_num = p_installment_number
                  OR p_installment_number IS NULL
                 )
             AND DECODE (p_open_invoice,
                         'Y', DECODE (   a.previous_customer_trx_id
                                      || a.initial_customer_trx_id,
                                      '', NVL (p.amount_due_remaining, 1),
                                      1
                                     ),
                         1
                        ) <> 0
             AND (   asp.tax_registration_number = p_tax_registration_number
                  OR p_tax_registration_number IS NULL
                 )
                  Order by transaction_number,line_number)
      LOOP
         l_csr_rec_count := l_csr_rec_count + 1;
         
         IF     l_csr_rec.trx_class = 'INV'
            AND UPPER (l_csr_rec.transaction_type) = 'COUNTY SUPPLIES'
            AND l_csr_rec.trx_line_type = 'FREIGHT'
            AND l_csr_rec.freight_amount > 0
         THEN
            SELECT MAX (rctl.customer_trx_line_id)
              INTO l_max_cust_trx_line_id
              FROM ra_customer_trx_lines rctl
             WHERE rctl.customer_trx_id = l_csr_rec.transaction_id
               AND line_type = 'FREIGHT'
               AND extended_amount > 0;
            IF l_max_cust_trx_line_id = l_csr_rec.cust_trx_line_id
            THEN
               l_copy_invoices_tab (l_csr_rec_count).rec_line_description :=
                                                     'Delivery/Admin Charges';
               l_copy_invoices_tab (l_csr_rec_count).rec_line_extended_amount :=
                                                     l_csr_rec.freight_amount;
            ELSE
               l_copy_invoices_tab (l_csr_rec_count).rec_line_description :=
                                                                         NULL;
               l_copy_invoices_tab (l_csr_rec_count).rec_line_extended_amount :=
                                                                         NULL;
            END IF;
         ELSE
            l_copy_invoices_tab (l_csr_rec_count).rec_line_description :=
                                                   l_csr_rec.line_description;
            l_copy_invoices_tab (l_csr_rec_count).rec_line_extended_amount :=
                                               l_csr_rec.line_extended_amount;
         END IF;
         IF l_csr_rec.trx_class='INV'   --added by sunil kalal
         THEN l_cust_trx_class :='$INV';
         ELSE l_cust_trx_class :='$CM';
         END IF;
         l_ship_to :='$SHIPTO';
         l_lines := '$LINE$';
         l_end := '$END';
         l_page_tag :='$PAGE';   
         l_transaction_count := l_transaction_count + 1;
         
         l_copy_invoices_tab (l_csr_rec_count).rec_cust_trx_class_tag:=l_cust_trx_class;
         
         l_copy_invoices_tab (l_csr_rec_count).rec_transaction_number := l_csr_rec.transaction_number;
         
         l_copy_invoices_tab (l_csr_rec_count).rec_reference_field := l_csr_rec.reference_field;
         
         l_copy_invoices_tab (l_csr_rec_count).rec_transaction_date := l_csr_rec.transaction_date;
         
         l_copy_invoices_tab (l_csr_rec_count).rec_due_date := l_csr_rec.due_date;
         
         l_copy_invoices_tab (l_csr_rec_count).rec_bill_to_customer_title :=l_csr_rec.bill_to_customer_title;
         
         l_copy_invoices_tab (l_csr_rec_count).rec_bill_to_customer_name := l_csr_rec.bill_to_customer_name;
         
         l_copy_invoices_tab (l_csr_rec_count).rec_bill_to_contact := l_csr_rec.bill_to_contact;
         
         l_copy_invoices_tab (l_csr_rec_count).rec_bill_to_address_line_1 :=l_csr_rec.bill_to_customer_address_line1;
         
         l_copy_invoices_tab (l_csr_rec_count).rec_bill_to_address_line_2 := l_csr_rec.bill_to_customer_address_line2;
         
         l_copy_invoices_tab (l_csr_rec_count).rec_bill_to_address_line_3 :=l_csr_rec.bill_to_customer_address_line3;
         
         l_copy_invoices_tab (l_csr_rec_count).rec_bill_to_town_city :=l_csr_rec.bill_to_town_city;
         
         l_copy_invoices_tab (l_csr_rec_count).rec_bill_to_county := l_csr_rec.bill_to_county;
         
         l_copy_invoices_tab (l_csr_rec_count).rec_bill_to_postal_code :=l_csr_rec.bill_to_postal_code;
         
         l_copy_invoices_tab (l_csr_rec_count).rec_ship_to_tag :=l_ship_to;                                        
         
         l_copy_invoices_tab (l_csr_rec_count).rec_ship_to_customer_name :=l_csr_rec.ship_to_customer_name;
         
         l_copy_invoices_tab (l_csr_rec_count).rec_ship_to_contact :=l_csr_rec.ship_to_contact;
         
         l_copy_invoices_tab (l_csr_rec_count).rec_ship_to_address_line_1 := l_csr_rec.ship_to_customer_address_line1;
         
         l_copy_invoices_tab (l_csr_rec_count).rec_ship_to_address_line_2 := l_csr_rec.ship_to_customer_address_line2;
         
         l_copy_invoices_tab (l_csr_rec_count).rec_ship_to_address_line_3 := l_csr_rec.ship_to_customer_address_line3;
         
         l_copy_invoices_tab (l_csr_rec_count).rec_ship_to_town_city := l_csr_rec.ship_to_town_city;
         
         l_copy_invoices_tab (l_csr_rec_count).rec_ship_to_county :=l_csr_rec.ship_to_county;
         
         l_copy_invoices_tab (l_csr_rec_count).rec_ship_to_postal_code := l_csr_rec.ship_to_postal_code;
         
         l_copy_invoices_tab (l_csr_rec_count).rec_tax_extended_amount := l_csr_rec.tax_extended_amount;
         
         l_copy_invoices_tab (l_csr_rec_count).rec_tax_rate := l_csr_rec.tax_rate;
         
         l_copy_invoices_tab (l_csr_rec_count).rec_lines_total := l_csr_rec.lines_total;
         
         l_copy_invoices_tab (l_csr_rec_count).rec_tax_total := l_csr_rec.tax_total;
         
         l_copy_invoices_tab (l_csr_rec_count).rec_transaction_total := l_csr_rec.transaction_total;
         
         l_copy_invoices_tab (l_csr_rec_count).rec_originators_name := l_csr_rec.originators_name;
         
         l_copy_invoices_tab (l_csr_rec_count).rec_originators_dept :=l_csr_rec.originators_dept;
         
         l_copy_invoices_tab (l_csr_rec_count).rec_originators_extension :=l_csr_rec.originator_telephone_extension;
         
         l_copy_invoices_tab (l_csr_rec_count).rec_original_trx_number := l_csr_rec.original_transaction_number;
         
         l_copy_invoices_tab (l_csr_rec_count).rec_comments := l_csr_rec.comments;
         
         l_copy_invoices_tab (l_csr_rec_count).rec_lines_tag :=l_lines;
         
         l_copy_invoices_tab (l_csr_rec_count).rec_page_tag :=l_page_tag;
         
         l_copy_invoices_tab (l_csr_rec_count).rec_end_tag :=l_end;
        
        END LOOP;
--------------------------------------------------------------------------
-- This will generate the Header Record of extract file
--------------------------------------------------------------------------
      fnd_file.put_line (fnd_file.LOG, 'Before UTL File');
      l_file_handle := UTL_FILE.fopen (l_output_dir, l_output_file, 'w');
      l_data :=
            'Tag1'
         || CHR (10) 
         || 'Transaction Number'
         || CHR (10)
         || 'Reference Field'
         || CHR (10)
         || 'Transaction Date'
         || CHR (10)
         || 'Due Date'
         || CHR (10)
         || 'Bill To Customer Title'
         || CHR (10)
         || 'Bill To Customer Name'
         || CHR (10)
         || 'Bill To Contact'
         || CHR (10)
         || 'Bill To Customer Address Line 1'
         || CHR (10)
         || 'Bill To Customer Address Line 2'
         || CHR (10)
         || 'Bill To Customer Address Line 3'
         || CHR (10)
         || 'Town/City'
         || CHR (10)
         || 'County'
         || CHR (10)
         || 'Postal Code'
         || CHR (10)
         || 'TAG 2'
         || CHR (10)
         || 'Ship To Customer Name'
         || CHR (10)
         || 'Ship To Contact'
         || CHR (10)
         || 'Ship To Customer Address Line 1'
         || CHR (10)
         || 'Ship To Customer Address Line 2'
         || CHR (10)
         || 'Ship To Customer Address Line 3'
         || CHR (10)
         || 'Town/City'
         || CHR (10)
         || 'County'
         || CHR (10)
         || 'Postal Code'
         || CHR (10)
         || 'Lines Total'
         || CHR (10)
         || 'Tax Total'
         || CHR (10)
         || 'Transaction Total'
         || CHR (10)
         || 'Originatorís Name'
         || CHR (10)
         || 'Originatorís Dept'
         || CHR (10)
         || 'Originatorís Telephone Extension'
         || CHR (10)
         || 'Original Transaction Number'
         || CHR (10)
         || 'Comments'
         || CHR (10)
         || 'Tag 5'
         || CHR (10)
         || 'Line Description'
         || '  '
         || 'Line Extended Amount'
         || '  '
         || 'Tax Extended Amount'
         || '  '
         || 'Tax Rate'
         ||' '
         || 'Tag 3'
         ||' '
         || 'Tag 4';
      UTL_FILE.put_line (l_file_handle, l_data);
--------------------------------------------------------------------------
-- Extract Data from PL-SQL Table for file generation
--------------------------------------------------------------------------
--IF l_copy_invoices_tab.COUNT > 0
      IF l_copy_invoices_tab.EXISTS (1)
      THEN
         FOR l_ctr IN l_copy_invoices_tab.FIRST .. l_copy_invoices_tab.LAST
         LOOP
            BEGIN
            l_data_transaction_count := l_data_transaction_count + 1;
            l_last_count := l_last_count + 1;
         IF l_last_count=l_copy_invoices_tab.COUNT
         THEN l_last_end_tag :=CHR(10)||'$END';
         ELSE l_last_end_tag:='';
         END IF;
         IF  l_data_transaction_count =1
        THEN l_data_transaction_no1 :=l_copy_invoices_tab (l_ctr).rec_transaction_number;
               l_end_tag1:='';
        ELSE
               l_end_tag1:=l_copy_invoices_tab (l_csr_rec_count).rec_end_tag;
        END IF;
        IF (l_data_transaction_count >=2)
       THEN l_data_transaction_no2 := l_copy_invoices_tab (l_ctr).rec_transaction_number;
        END IF;
           --Logic For Word Wrapped Comments field
      IF l_copy_invoices_tab (l_ctr).rec_comments IS NOT NULL
      THEN
         l_comments_from_function := WORD_WRAP(l_copy_invoices_tab (l_ctr).rec_comments,40,l_inv_comments);
      ELSE l_comments_from_function:='';
       END IF;
            ----Logic for $PAGE Tag
            IF l_overflow='N'
            THEN
            l_lines_available:= l_page_invoice_lines;
            ELSE 
            l_lines_available:=l_page_overflow_lines;
            END IF;
           IF l_copy_invoices_tab (l_ctr).rec_line_description IS NOT NULL
            THEN
            l_description_word_wrap := WORD_WRAP(l_copy_invoices_tab (l_ctr).rec_line_description ,40,l_inv_comments);
            l_description_lines:= nvl(LINE_COUNT(l_description_word_wrap),0);
            ELSE 
            l_description_word_wrap:='';
            l_description_lines:=1;
            END IF;
            l_lines_used := l_lines_used + l_description_lines;
            IF ((l_lines_used > l_lines_available)AND (l_data_transaction_no1=l_data_transaction_no2))
            THEN
              l_page_tag1 := l_copy_invoices_tab (l_csr_rec_count).rec_page_tag;
              l_lines_used :=l_lines_used-l_lines_available;
              l_lines_available :=l_page_overflow_lines;
              l_overflow:='Y';
          ELSE 
              l_page_tag1 :='';
              IF ((l_data_transaction_no1=l_data_transaction_no2) AND  (l_overflow='Y'))
             THEN l_lines_available :=l_page_overflow_lines;
                l_overflow:='Y';
             ELSE
             l_lines_available :=l_page_invoice_lines;
              l_overflow:='N';
             END iF;
                  END IF;
             IF ((l_data_transaction_count =1)  OR
             ((l_data_transaction_count >=2) AND (l_data_transaction_no1<>l_data_transaction_no2)))
               THEN
        l_data := l_end_tag1
                  || CHR (10)
                  ||  l_copy_invoices_tab (l_ctr).rec_cust_trx_class_tag
                  || CHR (10)
                  ||  l_copy_invoices_tab (l_ctr).rec_transaction_number
                  || CHR (10)
                  ||l_copy_invoices_tab (l_ctr).rec_reference_field
                  ||CHR(10)
                  || l_copy_invoices_tab (l_ctr).rec_transaction_date
                  || CHR (10)
                  || l_copy_invoices_tab (l_ctr).rec_due_date
                  || CHR (10);                 
                IF l_copy_invoices_tab (l_ctr).rec_bill_to_customer_title IS NOT NULL
                  THEN
                  l_data:= l_data 
                          ||l_copy_invoices_tab (l_ctr).rec_bill_to_customer_title
                          ||CHR(10);
                END IF;
                IF l_copy_invoices_tab (l_ctr).rec_bill_to_customer_name IS NOT NULL
                  THEN
                  l_data:= l_data 
                          ||l_copy_invoices_tab (l_ctr).rec_bill_to_customer_name
                          ||CHR(10);
                END IF;
                IF l_copy_invoices_tab (l_ctr).rec_bill_to_contact IS NOT NULL
                  THEN
                  l_data:= l_data 
                          ||l_copy_invoices_tab (l_ctr).rec_bill_to_contact
                          ||CHR(10);
                END IF;
                IF l_copy_invoices_tab (l_ctr).rec_bill_to_address_line_1 IS NOT NULL
                  THEN
                  l_data:= l_data 
                          ||l_copy_invoices_tab (l_ctr).rec_bill_to_address_line_1
                          ||CHR(10);
                END IF;
                IF l_copy_invoices_tab (l_ctr).rec_bill_to_address_line_2 IS NOT NULL
                  THEN
                  l_data:= l_data 
                          ||l_copy_invoices_tab (l_ctr).rec_bill_to_address_line_2
                          ||CHR(10);
                END IF;
                IF l_copy_invoices_tab (l_ctr).rec_bill_to_address_line_3 IS NOT NULL
                  THEN
                  l_data:= l_data 
                          ||l_copy_invoices_tab (l_ctr).rec_bill_to_address_line_3
                          ||CHR(10);
                END IF;
                IF l_copy_invoices_tab (l_ctr).rec_bill_to_town_city IS NOT NULL
                  THEN
                  l_data:= l_data 
                          ||l_copy_invoices_tab (l_ctr).rec_bill_to_town_city
                          ||CHR(10);
                END IF;
                IF l_copy_invoices_tab (l_ctr).rec_bill_to_county IS NOT NULL
                  THEN
                  l_data:= l_data 
                          ||l_copy_invoices_tab (l_ctr).rec_bill_to_county
                          ||CHR(10);
                END IF;
                IF l_copy_invoices_tab (l_ctr).rec_bill_to_postal_code IS NOT NULL
                  THEN
                  l_data:= l_data 
                          ||l_copy_invoices_tab (l_ctr).rec_bill_to_postal_code
                          ||CHR(10);
                END IF;
             l_data:=l_data
                    || l_copy_invoices_tab (l_ctr).rec_ship_to_tag
                    || CHR (10);
                IF l_copy_invoices_tab (l_ctr).rec_ship_to_customer_name IS NOT NULL
                  THEN
                  l_data:= l_data 
                          ||l_copy_invoices_tab (l_ctr).rec_ship_to_customer_name
                          ||CHR(10);
                END IF;
                 IF l_copy_invoices_tab (l_ctr).rec_ship_to_contact IS NOT NULL
                  THEN
                  l_data:= l_data 
                          ||l_copy_invoices_tab (l_ctr).rec_ship_to_contact
                          ||CHR(10);
                END IF;
                IF l_copy_invoices_tab (l_ctr).rec_ship_to_address_line_1 IS NOT NULL
                  THEN
                  l_data:= l_data 
                          ||l_copy_invoices_tab (l_ctr).rec_ship_to_address_line_1
                          ||CHR(10);
                END IF;
                IF l_copy_invoices_tab (l_ctr).rec_ship_to_address_line_2 IS NOT NULL
                  THEN
                  l_data:= l_data 
                          ||l_copy_invoices_tab (l_ctr).rec_ship_to_address_line_2
                          ||CHR(10);
                END IF;
                IF l_copy_invoices_tab (l_ctr).rec_ship_to_address_line_3 IS NOT NULL
                  THEN
                  l_data:= l_data 
                          ||l_copy_invoices_tab (l_ctr).rec_ship_to_address_line_3
                          ||CHR(10);
                END IF;
                IF l_copy_invoices_tab (l_ctr).rec_ship_to_town_city IS NOT NULL
                  THEN
                  l_data:= l_data 
                          ||l_copy_invoices_tab (l_ctr).rec_ship_to_town_city
                          ||CHR(10);
                END IF;
                IF l_copy_invoices_tab (l_ctr).rec_ship_to_county IS NOT NULL
                  THEN
                  l_data:= l_data 
                          ||l_copy_invoices_tab (l_ctr).rec_ship_to_county
                          ||CHR(10);
                END IF;
                IF l_copy_invoices_tab (l_ctr).rec_ship_to_postal_code IS NOT NULL
                  THEN
                  l_data:= l_data 
                          ||l_copy_invoices_tab (l_ctr).rec_ship_to_postal_code
                          ||CHR(10);
                END IF;    
                l_data:=l_data 
                  || l_copy_invoices_tab (l_ctr).rec_lines_total
                  || CHR (10)
                  || l_copy_invoices_tab (l_ctr).rec_tax_total
                  || CHR (10)
                  || l_copy_invoices_tab (l_ctr).rec_transaction_total
                  || CHR (10);
                  IF l_copy_invoices_tab (l_ctr).rec_originators_name IS NOT NULL
                  THEN
                  l_data:= l_data 
                          ||l_copy_invoices_tab (l_ctr).rec_originators_name
                          ||CHR(10);
                END IF;
                 IF l_copy_invoices_tab (l_ctr).rec_originators_dept IS NOT NULL
                  THEN
                  l_data:= l_data 
                          ||l_copy_invoices_tab (l_ctr).rec_originators_dept
                          ||CHR(10);
                END IF; 
                 IF l_copy_invoices_tab (l_ctr).rec_originators_extension IS NOT NULL
                  THEN
                  l_data:= l_data 
                          ||l_copy_invoices_tab (l_ctr).rec_originators_extension
                          ||CHR(10);
                END IF;
                 IF l_copy_invoices_tab (l_ctr).rec_original_trx_number IS NOT NULL
                  THEN
                  l_data:= l_data 
                          ||l_copy_invoices_tab (l_ctr).rec_original_trx_number
                          ||CHR(10);
                END IF; 
                  IF l_copy_invoices_tab (l_ctr).rec_comments IS NOT NULL
                  THEN
                  l_data:= l_data 
                          ||l_comments_from_function
                          ||CHR(10);
                END IF;         
              l_data:=l_data
                    || l_copy_invoices_tab (l_csr_rec_count).rec_lines_tag
                    || CHR (10);
                 IF l_copy_invoices_tab (l_ctr).rec_line_description IS NOT NULL
                  THEN
                  l_data:= l_data 
                          ||l_description_word_wrap
                          ||'';
                END IF;     
                 --   ||l_description_word_wrap
                --  || l_copy_invoices_tab (l_ctr).rec_line_description
               l_data:=l_data||' '
                    || l_copy_invoices_tab (l_ctr).rec_line_extended_amount
                    ||'  '
                    || l_copy_invoices_tab (l_ctr).rec_tax_extended_amount
                    ||'  '
                    || l_copy_invoices_tab (l_ctr).rec_tax_rate
                    ||' '
                    ||l_last_end_tag;
            END IF;
             IF ((l_data_transaction_count >=2) AND (l_data_transaction_no1=l_data_transaction_no2))
             AND l_page_tag1 IS NOT NULL
           THEN
        l_data  :=  l_page_tag1 
                  ||CHR (10)
                  ||l_description_word_wrap
                  || ' '
                  || l_copy_invoices_tab (l_ctr).rec_line_extended_amount
                  ||' '
                  ||l_copy_invoices_tab (l_ctr).rec_tax_extended_amount
                  ||' '
                  ||l_copy_invoices_tab (l_ctr).rec_tax_rate
                  ||''
                  ||l_last_end_tag;
                --|| l_copy_invoices_tab (l_ctr).rec_line_description
       END IF;    
       IF ((l_data_transaction_count >=2) AND (l_data_transaction_no1=l_data_transaction_no2))
           AND l_page_tag1 IS NULL
           THEN
        l_data  := l_description_word_wrap
                  || ' '
                  || l_copy_invoices_tab (l_ctr).rec_line_extended_amount
                  ||' '
                  ||l_copy_invoices_tab (l_ctr).rec_tax_extended_amount
                  ||' '
                  ||l_copy_invoices_tab (l_ctr).rec_tax_rate
                  ||''
                  ||l_last_end_tag;
                --|| l_copy_invoices_tab (l_ctr).rec_line_description
       END IF;   
          IF ((l_data_transaction_count >=2) AND (l_data_transaction_no1 <> l_data_transaction_no2 ))
                  THEN l_data_transaction_count :=1;
                       l_data_transaction_no1 :=l_copy_invoices_tab (l_ctr).rec_transaction_number;
                       l_data_transaction_no2 :=0;
                       l_lines_used :=l_description_lines;
                 END IF;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_errcode := SQLCODE;
                  l_errdesc := SUBSTR (SQLERRM, 1, 250);
                  fnd_file.put_line
                        (fnd_file.LOG,
                            l_copy_invoices_tab (l_ctr).rec_transaction_number
                         || ' with Oracle Error Number : '
                         || l_errcode
                         || ' and error description as '
                         || l_errdesc
                        );
                  l_total_error_rec := l_total_error_rec + 1;
            END;
            l_total_succeed_rec := l_total_succeed_rec + 1;
            UTL_FILE.put_line (l_file_handle, l_data);
         END LOOP;
         fnd_file.put_line (fnd_file.LOG,
                            'File Created with name ' || l_output_file
                           );
         fnd_file.put_line (fnd_file.LOG,
                               'Total No. of Records Processed:'
                            || l_copy_invoices_tab.COUNT
                           );
         l_file_location := l_output_dir || '/' || l_output_file;
         l_printq_profile := fnd_profile.VALUE ('CCS_DP_PRINT');
         --   l_printQ_profile :='dp_captest';
         fnd_file.put_line (fnd_file.LOG, 'Profile name ' || l_printq_profile);
         fnd_file.put_line (fnd_file.LOG, 'Before Submit Request');
         l_request_id_file :=
            fnd_request.submit_request ('XXC',                  -- application
                                        'XXCDU803',                 -- program
                                        '',                     -- description
                                        NULL,                    -- start_time
                                        FALSE,                  -- sub_request
                                        l_printq_profile,         -- argument1
                                        l_file_location,          -- argument2
                                        '',                       -- argument3
                                        '',                       -- argument4
                                        '',                       -- argument5
                                        '',                       -- argument6
                                        '',                       -- argument7
                                        '',                       -- argument8
                                        '',                       -- argument9
                                        '',                      -- argument10
                                        '',                      -- argument11
                                        '',                      -- argument12
                                        '',                      -- argument13
                                        '',                      -- argument14
                                        '',                      -- argument15
                                        '',                      -- argument16
                                        '',                      -- argument17
                                        '',                      -- argument18
                                        '',                      -- argument19
                                        '',                      -- argument20
                                        '',                      -- argument21
                                        '',                      -- argument22
                                        '',                      -- argument23
                                        '',                      -- argument24
                                        '',                      -- argument25
                                        '',                      -- argument26
                                        '',                      -- argument27
                                        '',                      -- argument28
                                        '',                      -- argument29
                                        '',                      -- argument30
                                        '',                      -- argument31
                                        '',                      -- argument32
                                        '',                      -- argument33
                                        '',                      -- argument34
                                        '',                      -- argument35
                                        '',                      -- argument36
                                        '',                      -- argument37
                                        '',                      -- argument38
                                        '',                      -- argument39
                                        '',                      -- argument40
                                        '',                      -- argument41
                                        '',                      -- argument42
                                        '',                      -- argument43
                                        '',                      -- argument44
                                        '',                      -- argument45
                                        '',                      -- argument46
                                        '',                      -- argument47
                                        '',                      -- argument48
                                        '',                      -- argument49
                                        '',                      -- argument50
                                        '',                      -- argument51
                                        '',                      -- argument52
                                        '',                      -- argument53
                                        '',                      -- argument54
                                        '',                      -- argument55
                                        '',                      -- argument56
                                        '',                      -- argument57
                                        '',                      -- argument58
                                        '',                      -- argument59
                                        '',                      -- argument60
                                        '',                      -- argument61
                                        '',                      -- argument62
                                        '',                      -- argument63
                                        '',                      -- argument64
                                        '',                      -- argument65
                                        '',                      -- argument66
                                        '',                      -- argument67
                                        '',                      -- argument68
                                        '',                      -- argument69
                                        '',                      -- argument70
                                        '',                      -- argument71
                                        '',                      -- argument72
                                        '',                      -- argument73
                                        '',                      -- argument74
                                        '',                      -- argument75
                                        '',                      -- argument76
                                        '',                      -- argument77
                                        '',                      -- argument78
                                        '',                      -- argument79
                                        '',                      -- argument80
                                        '',                      -- argument81
                                        '',                      -- argument82
                                        '',                      -- argument83
                                        '',                      -- argument84
                                        '',                      -- argument85
                                        '',                      -- argument86
                                        '',                      -- argument87
                                        '',                      -- argument88
                                        '',                      -- argument89
                                        '',                      -- argument90
                                        '',                      -- argument91
                                        '',                      -- argument92
                                        '',                      -- argument93
                                        '',                      -- argument94
                                        '',                      -- argument95
                                        '',                      -- argument96
                                        '',                      -- argument97
                                        '',                      -- argument98
                                        '',                      -- argument99
                                        ''                      -- argument100
                                       );
         fnd_file.put_line (fnd_file.LOG, 'After Submit Request');
      ELSE
         fnd_file.put_line (fnd_file.LOG, 'No Data Available For This File');
         p_retcode := 2;
      END IF;
      UTL_FILE.fclose (l_file_handle);
     --------------------------------------------------------------------------
     -- This called procedure will generate a output file as per standard
     --------------------------------------------------------------------------
--xxc_utils.get_run_report_output(l_request_id,l_total_succeed_rec,l_copy_invoices_tab.COUNT,l_total_error_rec,'I');
   EXCEPTION
      WHEN UTL_FILE.invalid_filehandle
      THEN
         fnd_file.put_line (fnd_file.LOG, 'File Handle Exception raised');
         app_exception.raise_exception;
         UTL_FILE.fclose (l_file_handle);
         ROLLBACK;
         p_retcode := 2;
      WHEN UTL_FILE.invalid_path
      THEN
         fnd_file.put_line (fnd_file.LOG, 'Path Exception');
         app_exception.raise_exception;
         UTL_FILE.fclose (l_file_handle);
         ROLLBACK;
         p_retcode := 2;
      WHEN UTL_FILE.invalid_mode
      THEN
         fnd_file.put_line (fnd_file.LOG, 'MODE Exception');
         app_exception.raise_exception;
         UTL_FILE.fclose (l_file_handle);
         ROLLBACK;
         p_retcode := 2;
      WHEN UTL_FILE.invalid_operation
      THEN
         fnd_file.put_line (fnd_file.LOG, 'OPERATION Exception');
         app_exception.raise_exception;
         UTL_FILE.fclose (l_file_handle);
         ROLLBACK;
         p_retcode := 2;
      WHEN OTHERS
      THEN
         p_errbuf := SUBSTR (SQLERRM, 1, 250);
         p_retcode := SQLCODE;
         app_exception.raise_exception;
   END xxc_main;
-- ----------------------------------------------------
-- End of XXC0AR007_PRINT_COPY_TRX
-- ----------------------------------------------------
END xxc0ar007_print_copy_trx;
