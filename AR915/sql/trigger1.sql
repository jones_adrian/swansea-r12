CREATE OR REPLACE TRIGGER dispute_trig
AFTER INSERT 
   ON AR_DISPUTE_HISTORY
   FOR EACH ROW
   
DECLARE
   v_res varchar2(150);
   v_pay_id NUMBER(15);



  
BEGIN
    
  v_pay_id := :new.payment_schedule_id;
  
   SELECT attribute1 INTO v_res
   FROM ar_payment_schedules_all 
   where payment_schedule_id = v_pay_id ;
   
   
   INSERT INTO XXC_AR_DISPUTE_HISTORY
   ( DISPUTE_HISTORY_ID,
     PAYMENT_SCHEDULE_ID,
     AMOUNT_DUE_REMAINING,
     AMOUNT_IN_DISPUTE,
     DISPUTE_AMOUNT,
     START_DATE,
     END_DATE,
     CREATION_DATE,
     CREATED_BY,
     LAST_UPDATE_DATE,
     LAST_UPDATED_BY,
     LAST_UPDATE_LOGIN,
     CREATED_FROM,
     DISPUTE_REASON
     
   )
   VALUES
   (       :new.DISPUTE_HISTORY_ID,
           :new.PAYMENT_SCHEDULE_ID,
    
           :new.AMOUNT_DUE_REMAINING,
           :new.AMOUNT_IN_DISPUTE,
           :new.DISPUTE_AMOUNT,
           :new.START_DATE,
           :new.END_DATE,
           :new.CREATION_DATE,
           :new.CREATED_BY,
           :new.LAST_UPDATE_DATE,
           :new.LAST_UPDATED_BY,
           :new.LAST_UPDATE_LOGIN,
           :new.CREATED_FROM,
                 v_res );
     
END;
