
CREATE OR REPLACE TRIGGER dispute_trig1
after insert or update of attribute1
   ON ar_payment_schedules_all
   FOR EACH ROW
   
DECLARE
   
   v_pay_id NUMBER(15);

   v_his_id NUMBER(15);
   
BEGIN
    
  v_pay_id := :new.payment_schedule_id;
  
  
  
   SELECT max(DISPUTE_HISTORY_ID) INTO v_his_id
   FROM XXC_AR_DISPUTE_HISTORY 
   where payment_schedule_id = v_pay_id ;
   
     UPDATE XXC_AR_DISPUTE_HISTORY
    SET DISPUTE_REASON = :NEW.attribute1
    WHERE payment_schedule_id = v_pay_id and 
   DISPUTE_HISTORY_ID=  v_his_id;
 
     
END;