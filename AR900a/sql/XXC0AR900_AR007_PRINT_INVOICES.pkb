CREATE OR REPLACE PACKAGE BODY APPS.XXC0AR900_AR007_PRINT_INVOICES
AS
/*************************************************************
$Header$
Original Author: Yousuf Husaini
Module Type    : SQL*Plus
Description    : This package will extract all Copy Invoices from Account
                 Receivables where print option is 'PRI' along with the
                 customer details.
Modification History:
$Log$
21/08/2006     Yousuf Husaini     Initial Creation
10/11/2006     Sunil Kalal        Changes done till this date per requirement
                                  along with Suzaane.
14/11/2006     Dhaval Thakore     Changes to handle exception when setup for
                                  particular invoice is not available pass blank
                                  value for originator name, originator dept and
                                  originator telephone.
15/11/2006     Dhaval Thakore     Changes to incorporate description impact for
                                  Invoices having Freight as a charge.
21/11/2006     Dhaval Thakore     IS_UPPER functional included as per discussion
                                  with Richard for wrapping of Customer Name and
                                  Description
22/11/2006     Dhaval Thakore      Changes incorporate as per discussion with
                                  Richard for order by clause
28/11/2006     Dhaval Thakore     Changes for integrity with AR901, AR907,
                                  Title changes, File path, required comment
29-NOV-2006    Dhaval Thakore     Changes as for Print Queue Profile option
19-DEC-2006    Sunil Kalal        Added new temporary table
                                  XXC_AR900_AR007_INVOICES_1
                                  for AR007 which is used for TRX_NUMBER
                                  insertion. So now,for AR900 the TRX_NUMBER
                                  will be inserted in XXC_AR900_AR007_INVOICES
                                  and for AR007 the TRX_NUMBER
                                  will be inserted in XXC_AR900_AR007_INVOICES_1
08-FEB-2007    Sunil Kalal        Changed the size of rec_reference_field from
                                  ra_customer_trx_all.customer_reference%TYPE
                                  which is VARCHAR2(30)to
                                  ra_customer_trx_all.purchase_order%TYPE
                                  which is varchar2(50)as the Purchase Order
                                  has more than 30 characters.
                                  This change was a result of program failure
                                  in Prod environment.
28-Mar-2007    Subbarayudu        Replace SUBSTR in place of SUBSTRB in
                                  WORD_WRAP function for EARS00020310131.
10-APR-2007    Sunil Kalal        Additional code by Sunil Kalal for handling
                                  the Exception when the department code is
                                  numeric. This was a solution to the
                                  EARS call no 19821427.
18-JUL-2007    Sandeep S.      Added outer join in the selection query to print
                  items missed because of no tax code.
                  EARS Ref: EARS00021028699
17-SEP-2012	   Virendra Sharma    Changes made to fix for artf2138409 and
								  artf2138421
18-SEP-2012	   Virendra Sharma    Code fix for the R12 upgrade. Changes made 
                                  for artf2138420
*******************************************************************************/
   FUNCTION      LINE_COUNT (p_input_string VARCHAR2)
   RETURN PLS_INTEGER AS
      CR CONSTANT VARCHAR2(1) := CHR(10);
      l_line_count PLS_INTEGER := 0;
      l_loop_count PLS_INTEGER := 0;
      l_currpos    PLS_INTEGER;
      l_startpos   PLS_INTEGER := 1;
   BEGIN
      IF p_input_string IS NOT NULL THEN
         LOOP
            l_loop_count := l_loop_count + 1;
            l_currpos := INSTR(p_input_string, CR, l_startpos);
            IF l_currpos > 0 THEN
               l_startpos := l_currpos + 1;
            ELSE
               EXIT;
            END IF;
         END LOOP;
         l_line_count := l_loop_count;
      ELSE
         l_line_count := 0;
      END IF;

      RETURN l_line_count;
   END LINE_COUNT;

   FUNCTION      WORD_WRAP (
      p_input_string VARCHAR2,
      p_column_width NUMBER DEFAULT 40,
      p_max_input_length NUMBER DEFAULT 240)
   RETURN VARCHAR2 AS
      CR CONSTANT VARCHAR2(1) := CHR(10);
      SP CONSTANT VARCHAR2(1) := ' ';

      l_input_string VARCHAR2(4001) := SUBSTR(REPLACE(p_input_string,CR,SP)
                                   ,1,p_max_input_length);
      l_output_string VARCHAR2(4001);
      l_tmp_string    VARCHAR2(4001);
      l_input_string_length PLS_INTEGER;
      l_startpos            PLS_INTEGER := 1;
      l_endpos              PLS_INTEGER := 0;
      l_wrappos             PLS_INTEGER;
      l_loop_count          PLS_INTEGER := 0;
   BEGIN
      l_input_string_length := LENGTH(l_input_string);
      --dbms_output.put_line(l_input_string||' '||l_input_string_length);
      IF l_input_string_length <= p_column_width THEN
         l_output_string := l_input_string;
      ELSE
         LOOP
            l_loop_count := l_loop_count + 1;
            -- Changes done for EARS00020310131 -----
        --        EXIT WHEN l_startpos >= l_input_string_length;

        EXIT WHEN l_startpos > l_input_string_length;

            --dbms_output.put_line('l_startpos='||l_startpos||' p_column_width='||p_column_width);

        -- Changes done for EARS00020310131 -----
        --l_tmp_string := SUBSTRB(l_input_string, l_startpos, p_column_width);

        l_tmp_string := SUBSTR(l_input_string, l_startpos, p_column_width);

            IF LENGTH(l_tmp_string) = p_column_width THEN
               l_wrappos := INSTR(l_tmp_string,SP,-1);
                  IF l_wrappos = 0 THEN
                     l_wrappos := p_column_width;
                  END IF;

                  l_tmp_string := SUBSTRB(l_tmp_string,1,l_wrappos);
                  --dbms_output.put_line('l_tmp_string='||l_tmp_string);
                  l_endpos := l_endpos + l_wrappos;
                  l_startpos := l_endpos + 1;
                  l_output_string := l_output_string || l_tmp_string ||CHR(10);
            ELSE
               l_output_string := l_output_string || l_tmp_string;
               EXIT;
            END IF;
            --dbms_output.put_line('l_output_string='||l_output_string);
         END LOOP;
     END IF;
     RETURN l_output_string;
   END WORD_WRAP;

   ------------------------------------------------------------------------
   -- Function will check for the nos of captial characters available in
   -- string.
   ------------------------------------------------------------------------
    FUNCTION IS_UPPER (
      p_input_string IN VARCHAR2,
      p_pct_upper IN PLS_INTEGER DEFAULT 75)
    RETURN BOOLEAN
    IS
      c_ascii_A     CONSTANT PLS_INTEGER := ASCII('A');
      c_ascii_Z     CONSTANT PLS_INTEGER := ASCII('Z');
      c_ascii_sp    CONSTANT PLS_INTEGER := ASCII(' ');
      l_input_len   PLS_INTEGER := LENGTH(p_input_string);
      l_upper_count PLS_INTEGER := 0;
      l_pct_upper   PLS_INTEGER;
      l_retval      BOOLEAN := FALSE;
      l_curr_char   PLS_INTEGER;
    BEGIN
      --DBMS_OUTPUT.PUT_LINE('l_input_len='||l_input_len);
      FOR n IN 1..l_input_len
      LOOP
        l_curr_char := ASCII(SUBSTR(p_input_string,n,1));
        IF l_curr_char BETWEEN c_ascii_A AND c_ascii_Z THEN
          l_upper_count := l_upper_count + 1;
          l_pct_upper := ROUND((l_upper_count/l_input_len)*100);
          --DBMS_OUTPUT.PUT_LINE('l_upper_count='||l_upper_count);
          --DBMS_OUTPUT.PUT_LINE('Char '||n||'='||ASCII(SUBSTR(p_input_string,n,1))||', l_pct_upper='||l_pct_upper);
          IF l_pct_upper >= p_pct_upper THEN
            l_retval := TRUE;
            EXIT;
          END IF;
        ELSIF l_curr_char=c_ascii_sp THEN
          l_input_len := l_input_len -1;
        END IF;
      END LOOP;
      RETURN l_retval;
    END IS_UPPER;

   PROCEDURE xxc_main (
      p_errbuf                    OUT      VARCHAR2,
      p_retcode                   OUT      NUMBER,
      p_cust_trx_class            IN       VARCHAR2 DEFAULT NULL,
      p_cust_trx_type_id          IN       NUMBER DEFAULT NULL,
      p_trx_number_low            IN       VARCHAR2,
      p_trx_number_high           IN       VARCHAR2,
      p_dates_low                 IN       DATE DEFAULT NULL,
      p_dates_high                IN       DATE DEFAULT NULL,
      p_customer_class_code       IN       VARCHAR2,
      p_customer_id               IN       NUMBER DEFAULT NULL,
      p_installment_number        IN       NUMBER DEFAULT NULL,
      p_open_invoice              IN       VARCHAR2 DEFAULT NULL,
      p_tax_registration_number   IN       NUMBER DEFAULT NULL,
      p_from_dunning              IN       VARCHAR2,
      p_new_invoices              IN       VARCHAR2

   )
   IS
-- ----------------------------------------------------------------------------
-- Procedure: XXC_MAIN
--
-- Description: This procedure extracts all Copy Invoices from Account
--              Receivables along with the customer details.
--
-- Parameters:
-- Name                       IN/OUT   Description
-- --------------------       -------  ---------------------------------------------
--
-- p_errbuf                    OUT      Error Message
-- p_retcode                   OUT      Error Code
-- p_cust_trx_class            IN       Customer Transaction Class
-- p_cust_trx_type_id          IN       Customer Transaction Type Id
-- p_trx_number_low            IN       Customer Transaction Number Low
-- p_trx_number_high           IN       Customer Transaction Number High
-- p_dates_low                 IN       Print Date Low
-- p_dates_high                IN       Print Date High
-- p_customer_class_code       IN       Customer Class Code
-- p_customer_id               IN       Customer Id
-- p_installment_number        IN       Installment Number
-- p_open_invoice              IN       Open Invoice
-- p_tax_registration_number   IN       Tax Registration Number
-- p_from_dunning              IN       From Dunning will decide if this program is called
--                                      from Dunning Letter Program or
--                                      Print Copy Invoices Program
-- ----------------------------------------------------------------------------

---------------------------------------------
-- Variable Declaration
---------------------------------------------
      l_data                      VARCHAR2 (4000);
      l_file_handle               UTL_FILE.file_type;
      l_output_dir                VARCHAR2 (100)
                                   := fnd_profile.VALUE ('XXC_COPY_INVOICES');
      l_output_file               VARCHAR2 (100);
      l_request_id                NUMBER;
      l_total_succeed_rec         NUMBER                                 := 0;
      l_total_error_rec           NUMBER                                 := 0;
      l_csr_rec_count             NUMBER                                 := 0;
      l_errcode                   NUMBER;
      l_errdesc                   VARCHAR2 (250);
      l_printq_profile            VARCHAR2 (250);
      l_request_id_file           NUMBER;
      l_file_location             VARCHAR2 (250);
      l_max_cust_trx_line_id      ra_customer_trx_lines_all.customer_trx_line_id%TYPE;
      l_freight_extended_amount   ra_customer_trx_lines_all.extended_amount%TYPE;
      l_cust_trx_class            VARCHAR2(10);--sunil kalal
      l_ship_to                   VARCHAR2(10) ;--sunil kalal
      l_transaction_no1            ra_customer_trx_all.trx_number%TYPE     :=0;
      l_transaction_no2            ra_customer_trx_all.trx_number%TYPE     :=0;
      l_transaction_count         NUMBER                                   :=0;
      l_data_transaction_count    NUMBER                                   :=0;
      l_data_transaction_no1      NUMBER                                   :=0;
      l_data_transaction_no2      NUMBER                                   :=0;
      l_customer_title            VARCHAR2(3000);
      l_lines                     VARCHAR2(10);
      l_end                       VARCHAR2(10);
      l_end_tag                   VARCHAR2(10);
      l_end_tag1                  VARCHAR2(10);
      l_count_end_tag             NUMBER                                    :=0;
      l_page_invoice_lines        NUMBER
                                  :=nvl(fnd_profile.VALUE('XXC_PAGE1_INVOICE_LINES'),6);
      l_page_overflow_lines       NUMBER
                                  :=nvl(fnd_profile.VALUE('XXC_OVERFLOW_LINES'),40);
      l_inv_comments               NUMBER
                                  :=nvl(fnd_profile.VALUE('XXC_INV_COMMENTS'),40);
      l_inv_max_commnets_desc      NUMBER
                                  :=nvl(fnd_profile.VALUE('XXC_INV_COMMENTS_MAX_LENGTH'),240);
      l_inv_line_desc              NUMBER
                                  :=nvl(fnd_profile.VALUE('XXC_INVDESCRWIDTH'),40);
      l_inv_max_line_desc          NUMBER
                                  :=nvl(fnd_profile.VALUE('XXC_INV_DESC_MAX_LENGTH'),240);
      l_inv_emp_name_max_length    NUMBER
                                  :=nvl(fnd_profile.VALUE('XXC_INV_EMP_NAME_MAX_LENGTH'),240);
      l_inv_emp_name_desc          NUMBER
                                  :=nvl(fnd_profile.VALUE('XXC_INV_EMP_NAME_DESC'),40);

      -------------------------------------------------------------------
      -- Added by Dhaval on 21-Nov-06 to incorporate all capital letters
      -- less margin than actual one
      -------------------------------------------------------------------
      l_inv_emp_name_cap_desc     NUMBER := 28;

      l_comments_word_wrap         VARCHAR2(3000);
      l_comments_from_function     VARCHAR2(3000);
      CR                           CONSTANT VARCHAR2(1) := CHR(10);
      SP                           CONSTANT VARCHAR2(1) := ' ';
      l_lines_available            NUMBER                                    :=0;
      l_lines_used                 NUMBER                                    :=0;
      l_total_lines_used           NUMBER                                    :=0;
      l_description_word_wrap      VARCHAR2(3000);
      l_description_lines          NUMBER;
      l_freight_comment            VARCHAR2(100);
      l_page_tag                   VARCHAR2(10);
      l_page_tag1                  VARCHAR2(10);
      l_last_end_tag               VARCHAR2(10);
      l_last_count                 NUMBER :=0;
      l_overflow                   VARCHAR2(1) :='N';
      l_line_position_count        NUMBER:=0;
      l_line_CHR10                 NUMBER:=0;
      l_cust_name_wrap             VARCHAR2(3000);
      l_cust_name_wrap_2           VARCHAR2(3000);
      l_originator_name            VARCHAR2(300);
      l_originator_dept            VARCHAR2(150);
      l_originator_tel_ext         VARCHAR2(150);
      l_last_descr_line_length     PLS_INTEGER;

---------------------------------------------------------------------------------------------------
-- Table Declaration for all Copy Invoices.
---------------------------------------------------------------------------------------------------
      TYPE l_copy_invoices_rec IS RECORD (
         rec_cust_trx_class_tag       VARCHAR2(10),
         rec_transaction_number       ra_customer_trx_all.trx_number%TYPE,
         rec_reference_field          ra_customer_trx_all.purchase_order%TYPE,
         rec_job_number               ra_customer_trx_all.ct_reference%TYPE,
         rec_transaction_date         ra_customer_trx_all.trx_date%TYPE,
         rec_due_date                 ra_customer_trx_all.term_due_date%TYPE,
         rec_bill_to_customer_title   hz_parties.person_pre_name_adjunct%TYPE,
         rec_bill_to_customer_name    hz_parties.party_name%TYPE,
         rec_bill_to_contact          VARCHAR2 (200),
         rec_bill_to_address_line_1   hz_locations.address1%TYPE,
         rec_bill_to_address_line_2   hz_locations.address2%TYPE,
         rec_bill_to_address_line_3   hz_locations.address3%TYPE,
         rec_bill_to_town_city        hz_locations.city%TYPE,
         rec_bill_to_county           hz_locations.county%TYPE,
         rec_bill_to_postal_code      hz_locations.postal_code%TYPE,
         rec_ship_to_tag              VARCHAR2(10),
         rec_ship_to_customer_name    hz_parties.party_name%TYPE,
         rec_ship_to_contact          VARCHAR2 (200),
         rec_ship_to_address_line_1   hz_locations.address1%TYPE,
         rec_ship_to_address_line_2   hz_locations.address2%TYPE,
         rec_ship_to_address_line_3   hz_locations.address3%TYPE,
         rec_ship_to_town_city        hz_locations.city%TYPE,
         rec_ship_to_county           hz_locations.county%TYPE,
         rec_ship_to_postal_code      hz_locations.postal_code%TYPE,
         rec_line_description         ra_customer_trx_lines_all.description%TYPE,
         rec_line_extended_amount     ra_customer_trx_lines_all.extended_amount%TYPE,
         rec_tax_extended_amount      ra_customer_trx_lines_all.extended_amount%TYPE,
         rec_tax_rate                 ar_vat_tax.description%TYPE,
         rec_lines_total              ar_payment_schedules_all.amount_line_items_original%TYPE,
         rec_tax_total                ar_payment_schedules_all.tax_original%TYPE,
         rec_transaction_total        ar_payment_schedules_all.amount_due_original%TYPE,
         rec_originators_name         VARCHAR2(150),
         rec_originators_dept         VARCHAR2(150),
         rec_originators_extension    VARCHAR2(150),
         rec_original_trx_number      ra_customer_trx_all.trx_number%TYPE,
         --
         -- Added the below column as per RFC by Vinod Narayan on 09th May 2007
         rec_original_trx_date        ra_customer_trx_all.trx_date%TYPE,
         --
         rec_comments                 ra_customer_trx_all.internal_notes%TYPE,
         rec_lines_tag                VARCHAR2(10),
         rec_page_tag                 VARCHAR2(10),
         rec_end_tag                  VARCHAR2(10),
         rec_freight_check            VARCHAR2(1),
         rec_freight_amount           ra_customer_trx_lines_all.extended_amount%TYPE
      );

      TYPE l_copy_invoices_table_type IS TABLE OF l_copy_invoices_rec
           INDEX BY BINARY_INTEGER;

      l_copy_invoices_tab         l_copy_invoices_table_type;
      l_check_dunning NUMBER := 0;
   BEGIN
      l_request_id := fnd_global.conc_request_id;

      IF P_FROM_DUNNING = 'Y' THEN
        SELECT count(*)
         INTO   l_check_dunning
         FROM   XXC_AR901_COPY_INVOICES;
        FND_FILE.PUT_LINE(FND_FILE.LOG, 'Invoice From Dunning Letter - ' || l_check_dunning);
      END IF;

      IF p_new_invoices ='N'
      THEN
         l_output_file :=
                          'XXC0AR007_COPY_INVOICES'
                          || '_'
                          || TO_CHAR (SYSDATE, 'YYYYMMDDHHMISS')
                          || '.dat';

      ELSE
         l_output_file :=
                          'XXC0AR900_NEW_INVOICES'
                          || '_'
                          || TO_CHAR (SYSDATE, 'YYYYMMDDHHMISS')
                          || '.dat';

      END IF;

      fnd_file.put_line(fnd_file.log, 'Checking Main Query ');

              SELECT COUNT(a.trx_number)
              INTO l_check_dunning
              FROM
                ra_customer_trx  a,
                ra_customer_trx  a1,
                ra_cust_trx_types TYPES,
                ra_customer_trx_lines  rctl,
                ra_customer_trx_lines  rctl1,
                ar_lookups l_types,
                hz_cust_accounts b1,
                mtl_units_of_measure    u,
                mtl_system_items  msi,
                ar_system_parameters asp,
                ra_batch_sources batch,
                --ra_customers racBt, /*commented by rakesh r12 changes 23-july-2012 */
                --hz_parties racBt, /*added by rakesh r12 upgrade 23-july-2012 */  -- Commented by Virendra on 28-SEP-12 for artf2138420
                hz_cust_accounts racBt,  -- Added by Virendra on 28-SEP-12 for artf2138420
                ar_vat_tax avt,
                hz_parties party1,
                hz_parties party2,
                hz_party_sites hpsbt,
                hz_cust_acct_sites_all hcabt,
                hz_cust_site_uses_all hcsbt,
                hz_locations locbt,
                --ra_customers racst, /*commented by rakesh r12 changes 23-july-2012 */
                --hz_parties racst, /*added by rakesh r12 upgrade 23-july-2012 */  -- Commented by Virendra on 28-SEP-12 for artf2138420
                hz_cust_accounts racst, -- Added by Virendra on 28-SEP-12 for artf2138420
                hz_party_sites hpsst,
                hz_cust_acct_sites_all hcast,
                hz_cust_site_uses_all hcsst,
                hz_locations locst,
                hz_cust_account_roles hcar,
                hz_relationships hzre,
                hz_cust_account_roles hcar1,
                hz_relationships hzre1,
               (SELECT   customer_trx_id,
                           SUM (freight_original) Freight,
                           SUM (amount_line_items_original) lines_total,
                           SUM (tax_original) tax_total,
                           SUM (amount_due_original) transaction_total
                      FROM ar_payment_schedules_all
                  GROUP BY customer_trx_id) aps,
                (SELECT
                         CASE WHEN SUM(p.amount_due_remaining) != 0 THEN 'OPEN' END status1 ,
                         CASE WHEN SUM(p.amount_due_remaining) = 0 OR SUM(p.amount_due_remaining) != 0
                         THEN 'BOTH'  END  AS STATUS2,
                         p.customer_trx_id
                 FROM     ar_payment_schedules p
                 GROUP BY  p.customer_trx_id) p2
          WHERE
              a.printing_option IN ('PRI', 'REP')
              AND a.complete_flag = 'Y'
              AND a1.customer_trx_id(+) = a.previous_customer_trx_id
              AND a.customer_trx_id = aps.customer_trx_id(+)
              AND types.cust_trx_type_id=a.cust_trx_type_id
              AND rctl.customer_trx_id=a.customer_trx_id
              AND rctl.line_type <>'TAX'
              AND rctl1.customer_trx_id=a.customer_trx_id
              AND rctl.CUSTOMER_TRX_LINE_ID=rctl1.LINK_TO_CUST_TRX_LINE_ID
              AND rctl1.line_type ='TAX'
              AND l_types.lookup_code = types.type
              AND l_types.lookup_type = 'INV/CM/ADJ'
              AND rctl.vat_tax_id=avt.vat_tax_id(+)        -- Outer join is added by Sandeep on 19-JUL-07 for EARS00021028699
              AND a.bill_to_customer_id = b1.cust_account_id
              AND rctl.uom_code = u.uom_code(+)
              AND rctl.inventory_item_id = msi.inventory_item_id(+)
              AND rctl.warehouse_id=msi.organization_id(+)
              AND a.org_id = asp.org_id
              AND asp.tax_registration_number
                     = nvl(p_tax_registration_number,asp.tax_registration_number)
              AND a.batch_source_id=batch.batch_source_id
              AND a.customer_trx_id = p2.customer_trx_id
              AND
                 (p2.status2 = decode(p_open_invoice,'N',p2.status2,p2.status1)
                  OR
                  p2.status1 = decode(p_open_invoice,'Y',p2.status1,p2.status2)
                  )
              --AND a.bill_to_customer_id = racbt.party_id  -- Commented by Virendra on 28-SEP-12 for artf2138420
              AND a.bill_to_customer_id = racbt.cust_account_id  -- Added by Virendra on 28-SEP-12 for artf2138420
              AND hcsbt.site_use_id=a.bill_to_site_use_id
              AND hcabt.cust_acct_site_id = hcsbt.cust_acct_site_id
              AND hpsbt.party_site_id = hcabt.party_site_id
              AND locbt.location_id = hpsbt.location_id
              --AND a.ship_to_customer_id = racst.party_id (+)  -- Commented by Virendra on 28-SEP-12 for artf2138420
              AND a.ship_to_customer_id = racst.cust_account_id(+) -- Added by Virendra on 28-SEP-12 for artf2138420
              AND hcsst.site_use_id (+) =a.ship_to_site_use_id
              AND hcast.cust_acct_site_id (+) = hcsst.cust_acct_site_id
              AND hpsst.party_site_id (+) = hcast.party_site_id
              AND locst.location_id (+)= hpsst.location_id
              AND a.bill_to_contact_id=hcar.cust_account_role_id(+)
              AND hcar.party_id=hzre.party_id(+)
              AND hzre.subject_table_name(+) = 'HZ_PARTIES'
              AND hzre.object_table_name(+) = 'HZ_PARTIES'
              AND hzre.directional_flag(+)  = 'F'
              AND hcar.role_type(+) = 'CONTACT'
              AND hzre.subject_id = party1.party_id(+)
              AND a.ship_to_contact_id = hcar1.cust_account_role_id(+)
              AND hcar1.party_id=hzre1.party_id(+)
              AND hzre1.subject_table_name(+) = 'HZ_PARTIES'
              AND hzre1.object_table_name(+) = 'HZ_PARTIES'
              AND hzre1.directional_flag(+)  = 'F'
              AND hcar1.role_type(+)          = 'CONTACT'
              AND hzre1.subject_id = party2.party_id(+)
              AND TYPES.TYPE = nvl(p_cust_trx_class,TYPES.TYPE)
              AND TYPES.cust_trx_type_id = nvl(p_cust_trx_type_id,types.cust_trx_type_id)
              AND (
                      (
                       (P_FROM_DUNNING = 'N')
                          and (a.trx_number BETWEEN NVL (p_trx_number_low, 0)
                                AND NVL (p_trx_number_high,99999999999999999999)
                               )
                          and(
                          (p_new_invoices ='N') and
                          (a.trx_number IN (SELECT trx_number FROM XXC_AR900_AR007_INVOICES_1))--Changed by Sunil Kalal on 19 dec 2006
                       OR
                      (p_new_invoices ='Y') and
                          (a.trx_number IN (SELECT trx_number FROM XXC_AR900_AR007_INVOICES))

                      )
                      )
                  OR
                      (P_FROM_DUNNING = 'Y'
                        and a.trx_number in (SELECT trx_number FROM XXC_AR901_COPY_INVOICES))
                           )
              AND nvl(b1.customer_class_code,'1') = nvl(p_customer_class_code, nvl(b1.customer_class_code,'1'))
              AND b1.cust_account_id = nvl(p_customer_id,b1.cust_account_id )
              AND ( TRUNC(a.trx_date) BETWEEN  (p_dates_low) AND (p_dates_high)
                            OR (p_dates_low IS NULL AND p_dates_high IS NULL) )
              ;

      FND_FILE.PUT_LINE(FND_FILE.LOG, 'Invoice From Query - ' || l_check_dunning);

      FOR l_csr_rec IN
          (SELECT a.customer_trx_id   transaction_id,
                 aps.freight          freight_amount,
                 rctl.customer_trx_line_id cust_trx_line_id,
                 rctl.line_type            trx_line_type,
                 rctl1.line_type           trx_line_type1,
                 TYPES.TYPE trx_class,
                 TYPES.NAME transaction_type,
                 a.trx_number transaction_number,
                 /*DECODE (a.purchase_order,
                         NULL, a.ct_reference,
                         '/' || a.purchase_order
                        ) reference_field, */  --Commented By Priyaranjan
--nvl(rpad(a.purchase_order,50),'                                                  ') reference_field,

                -- a.purchase_order  reference_field,                
                 a.trx_date transaction_date,
--nvl(rpad(a.ct_reference,30),'                              ') job_number,
nvl(substr(rpad(a.purchase_order,50),1,50),'                                                  ') reference_field,
nvl(substr(rpad(a.ct_reference,30),1,30),'                              ') job_number,
                 --a.ct_reference job_number,   -- added by Dhaval
                 ARPT_SQL_FUNC_UTIL.GET_FIRST_REAL_DUE_DATE(a.CUSTOMER_TRX_ID,
                 a.TERM_ID,a.TRX_DATE) due_date,
                 --racbt.person_pre_name_adjunct bill_to_customer_title,-- Commented by Virendra on 28-SEP-12 for artf2138420
                 (SELECT person_pre_name_adjunct FROM hz_parties WHERE party_id = racbt.party_id) bill_to_customer_title, -- Added by Virendra on 28-SEP-12 for artf2138420
                 --racbt.party_name          bill_to_customer_name, -- Commented by Virendra on 28-SEP-12 for artf2138420
                 (SELECT party_name FROM hz_parties WHERE party_id = racbt.party_id) bill_to_customer_name, -- Added by Virendra on 28-SEP-12 for artf2138420
                 party1.party_name            bill_to_contact,
                 locbt.address1               bill_to_customer_address_line1,
                 locbt.address2               bill_to_customer_address_line2,
                 locbt.address3               bill_to_customer_address_line3,
                 locbt.city                   bill_to_town_city,
                 locbt.county                 bill_to_county,
                 locbt.postal_code            bill_to_postal_code,
                 --racst.party_name          ship_to_customer_name, -- Commented by Virendra on 28-SEP-12 for artf2138420
                 (SELECT party_name FROM hz_parties WHERE party_id = racst.party_id) ship_to_customer_name, -- Added by Virendra on 28-SEP-12 for artf2138420
                 party2.party_name            ship_to_contact,
                 locst.address1               ship_to_customer_address_line1,
                 locst.address2               ship_to_customer_address_line2,
                 locst.address3               ship_to_customer_address_line3,
                 locst.city                   ship_to_town_city,
                 locst.county                 ship_to_county,
                 locst.postal_code            ship_to_postal_code,
                 DECODE(batch.Name,'ORDER ENTRY',msi.segment1
                                             ||' '
                                             || rctl.description
                                             ||' '
                                             || abs(rctl.quantity_invoiced)
                                             ||' '
                                             || '@'
                                             ||' '
                                             || abs(rctl.unit_selling_price),
                                          DECODE(batch.Name,'ORDER ENTRY RETURNS',
                                              msi.segment1
                                             ||' '
                                             || rctl.description
                                             ||' '
                                             || abs(rctl.quantity_invoiced)
                                             ||' '
                                             || '@'
                                             ||' '
                                             || abs(rctl.unit_selling_price),
                                            rctl.description )
                                             )line_description,
                  DECODE (rctl.line_type,
                         'LINE', abs(rctl.extended_amount),
                         'FREIGHT', abs(rctl.extended_amount),
                         NULL
                        ) line_extended_amount,
                  abs(rctl1.extended_amount) tax_extended_amount,
                  DECODE (rctl.line_type,
                         'FREIGHT',NULL,
                         avt.description
                          ) tax_rate,
                 (abs(aps.lines_total)+ abs(aps.Freight)) lines_total,
                 abs(aps.tax_total) tax_total,
                 abs(aps.transaction_total) transaction_total,
                 a.attribute1 originators_name, a.attribute2 originators_dept,
                 a.attribute3 originator_telephone_extension,
                 a1.trx_number original_transaction_number,
                 a1.trx_date   original_transaction_date,
                 a.internal_notes comments
           FROM
                 ra_customer_trx_lines  rctl,
                 ra_customer_trx_lines  rctl1,
                 mtl_units_of_measure    u,
                 mtl_system_items  msi,
                 ra_cust_trx_types TYPES,
                 ra_customer_trx  a,
                 ra_customer_trx  a1,
                 ar_lookups l_types,
                 ar_vat_tax avt,
               (SELECT   customer_trx_id,
                           SUM (freight_original) Freight,
                           SUM (amount_line_items_original) lines_total,
                           SUM (tax_original) tax_total,
                           SUM (amount_due_original) transaction_total
                      FROM ar_payment_schedules_all
                  GROUP BY customer_trx_id) aps,
                hz_cust_accounts b1,
                hz_parties party1,
                hz_parties party2,
                --ra_customers racst, /*commented by rakesh r12 changes 23-july-2012 */
                --hz_parties racst, /*added by rakesh r12 upgrade 23-july-2012 */  -- Commented by Virendra on 28-SEP-12 for artf2138420
                hz_cust_accounts racst, -- Added by Virendra on 28-SEP-12 for artf2138420
                hz_party_sites hpsbt,
                hz_cust_acct_sites_all hcabt,
                hz_cust_site_uses_all hcsbt,
                hz_locations locbt,
                --ra_customers racBt, /*commented by rakesh r12 changes 23-july-2012 */
                --hz_parties racBt, /*added by rakesh r12 upgrade 23-july-2012 */  -- Commented by Virendra on 28-SEP-12 for artf2138420
                hz_cust_accounts racBt,  -- Added by Virendra on 28-SEP-12 for artf2138420
                hz_party_sites hpsst,
                hz_cust_acct_sites_all hcast,
                hz_cust_site_uses_all hcsst,
                hz_locations locst,
                hz_cust_account_roles hcar,
                hz_relationships hzre,
                hz_cust_account_roles hcar1,
                hz_relationships hzre1,
                ra_batch_sources batch,
                ar_system_parameters asp,
                (SELECT
                         CASE WHEN SUM(p.amount_due_remaining) != 0 THEN 'OPEN' END status1 ,
                         CASE WHEN SUM(p.amount_due_remaining) = 0 OR SUM(p.amount_due_remaining) != 0
                         THEN 'BOTH'  END  AS STATUS2,
                         p.customer_trx_id
                 FROM     ar_payment_schedules p
                 GROUP BY  p.customer_trx_id) p2
          WHERE           rctl.uom_code = u.uom_code(+)
              AND rctl.inventory_item_id = msi.inventory_item_id(+)
              AND rctl.warehouse_id=msi.organization_id(+)
              AND types.cust_trx_type_id=a.cust_trx_type_id
              AND rctl.customer_trx_id=a.customer_trx_id
              AND rctl1.customer_trx_id=a.customer_trx_id
              AND rctl.line_type <>'TAX'
              AND rctl.CUSTOMER_TRX_LINE_ID=rctl1.LINK_TO_CUST_TRX_LINE_ID
              AND rctl1.line_type ='TAX'
              AND a.complete_flag = 'Y'
              ----AND a.trx_number IN
              ----                (SELECT trx_number FROM XXC_AR901_COPY_INVOICES)
                              --XXC_AR900_AR007_INVOICES )
              AND a.previous_customer_trx_id=a1.customer_trx_id(+)
              AND l_types.lookup_code = types.type
              AND l_types.lookup_type = 'INV/CM/ADJ'
              AND a.printing_option IN ('PRI', 'REP')
              AND TYPES.TYPE IN ('INV', 'CM')
              AND rctl.vat_tax_id=avt.vat_tax_id(+)        -- Outer join is added by Sandeep on 19-JUL-07 for EARS00021028699
              AND a.customer_trx_id = aps.customer_trx_id(+)
              AND a.bill_to_customer_id = b1.cust_account_id
              AND a.batch_source_id=batch.batch_source_id
              AND NVL (locst.LANGUAGE, 'US') = 'US'
              AND NVL (locbt.LANGUAGE, 'US') = 'US'
              AND a.org_id = asp.org_id
              AND a.customer_trx_id = p2.customer_trx_id
              --AND a.bill_to_customer_id = racbt.party_id  -- Commented by Virendra on 28-SEP-12 for artf2138420
              AND a.bill_to_customer_id = racbt.cust_account_id  -- Added by Virendra on 28-SEP-12 for artf2138420
              AND hcsbt.site_use_id=a.bill_to_site_use_id
              AND hcabt.cust_acct_site_id = hcsbt.cust_acct_site_id
              AND hpsbt.party_site_id = hcabt.party_site_id
              AND locbt.location_id = hpsbt.location_id
              --AND a.ship_to_customer_id = racst.party_id (+)  -- Commented by Virendra on 28-SEP-12 for artf2138420
              AND a.ship_to_customer_id = racst.cust_account_id(+) -- Added by Virendra on 28-SEP-12 for artf2138420
              AND hcsst.site_use_id (+) =a.ship_to_site_use_id
              AND hcast.cust_acct_site_id (+) = hcsst.cust_acct_site_id
              AND hpsst.party_site_id (+) = hcast.party_site_id
              AND locst.location_id (+)= hpsst.location_id
              AND a.bill_to_contact_id=hcar.cust_account_role_id(+)
              AND hcar.party_id=hzre.party_id(+)
              AND hzre.subject_table_name(+) = 'HZ_PARTIES'
              AND hzre.object_table_name(+) = 'HZ_PARTIES'
              AND hzre.directional_flag(+)  = 'F'
              AND hcar.role_type(+) = 'CONTACT'
              AND hzre.subject_id = party1.party_id(+)
              AND a.ship_to_contact_id = hcar1.cust_account_role_id(+)
              AND hcar1.party_id=hzre1.party_id(+)
              AND hzre1.subject_table_name(+) = 'HZ_PARTIES'
              AND hzre1.object_table_name(+) = 'HZ_PARTIES'
              AND hzre1.directional_flag(+)  = 'F'
              AND hcar1.role_type(+)          = 'CONTACT'
              AND hzre1.subject_id = party2.party_id(+)
              AND
                 (p2.status2 = decode(p_open_invoice,'N',p2.status2,p2.status1)
                  OR
                  p2.status1 = decode(p_open_invoice,'Y',p2.status1,p2.status2)
                  )
              AND NVL(p_installment_number,1)= NVL(p_installment_number,1)
              AND TYPES.TYPE = nvl(p_cust_trx_class,TYPES.TYPE)
              AND TYPES.cust_trx_type_id = nvl(p_cust_trx_type_id,types.cust_trx_type_id)
              AND (
                      (
                       (P_FROM_DUNNING = 'N')
                          and (a.trx_number BETWEEN NVL (p_trx_number_low, 0)
                                AND NVL (p_trx_number_high,99999999999999999999)
                               )
                          and(
                          (p_new_invoices ='N') and
                          (a.trx_number IN (SELECT trx_number FROM XXC_AR900_AR007_INVOICES_1))--Changed by Sunil Kalal on 19 dec 2006
                       OR
                      (p_new_invoices ='Y') and
                          (a.trx_number IN (SELECT trx_number FROM XXC_AR900_AR007_INVOICES))

                      )
                      )
                  OR
                      (P_FROM_DUNNING = 'Y'
                        and a.trx_number in (SELECT trx_number FROM XXC_AR901_COPY_INVOICES))
                           )
              AND nvl(b1.customer_class_code,'1') = nvl(p_customer_class_code, nvl(b1.customer_class_code,'1'))
              AND b1.cust_account_id = nvl(p_customer_id,b1.cust_account_id )
              AND ( TRUNC(a.trx_date) BETWEEN  (p_dates_low) AND (p_dates_high)
                            OR (p_dates_low IS NULL AND p_dates_high IS NULL) )
              AND asp.tax_registration_number
                     = nvl(p_tax_registration_number,asp.tax_registration_number)
              ORDER BY TYPES.TYPE DESC, a.trx_number ASC,rctl.line_number
              )
      LOOP
         l_csr_rec_count := l_csr_rec_count + 1;
         l_freight_comment := 'N';
         fnd_file.put_line (fnd_file.LOG, 'Current Invoice is ' ||
                                          l_csr_rec.transaction_number ||
                                          ' and counter is '||l_csr_rec_count);
         /*
         fnd_file.put_line (fnd_file.LOG, '****Checking Ling Type for ' || l_csr_rec.transaction_id
                                          || ' is ' || l_csr_rec.trx_line_type
                                          || ' Freight Amt is ' ||
                                          NVL(l_csr_rec.freight_amount, 0) );
          */
         --fnd_file.put_line (fnd_file.log, 'Line Type ' || l_csr_rec.trx_line_type1 || ' line id ' || l_csr_rec.cust_trx_line_id);
         /*
         IF UPPER(l_csr_rec.trx_line_type) = 'FREIGHT'
            AND NVL(l_csr_rec.freight_amount, 0) > 0
            --AND l_csr_rec.freight_amount > 0
          */
         ------------------------------------------------------------------
         -- Logic Changed By Dhaval on 15-Nov-06 as per discussion with Suzaane
         -- as freight description should be override by 'Delivery/Admin Charges'
         ------------------------------------------------------------------
         IF NVL(l_csr_rec.freight_amount, 0) != 0
         THEN

            --fnd_file.put_line(fnd_file.log, 'Before Select Statement' || l_csr_rec.transaction_id);

            SELECT MAX (rctl.customer_trx_line_id)
            INTO l_max_cust_trx_line_id
            FROM ra_customer_trx_lines_all rctl
            WHERE rctl.customer_trx_id = l_csr_rec.transaction_id
            AND line_type = 'FREIGHT'
            AND extended_amount > 0;

            --fnd_file.put_line(fnd_file.log, 'After Select Statement' || l_max_cust_trx_line_id);
            IF SQL%FOUND
            --IF l_max_cust_trx_line_id = l_csr_rec.cust_trx_line_id
            THEN
               fnd_file.put_line(fnd_file.log, l_csr_rec.transaction_number ||
                                              ' Invoice has Freight. ' || l_csr_rec.line_extended_amount
                                              || ' ' || l_csr_rec.tax_extended_amount );
               l_copy_invoices_tab (l_csr_rec_count).rec_line_description :=
                             l_csr_rec.line_description;

               l_copy_invoices_tab (l_csr_rec_count).rec_freight_amount
                                                    := l_csr_rec.freight_amount;

               l_freight_comment := 'Y'; --' Delivery/Admin Charges';
               -- Changed by dhaval on 16-Nov-2006 as it was overriding line amount
               l_copy_invoices_tab (l_csr_rec_count).rec_line_extended_amount :=
                                                     l_csr_rec.line_extended_amount;
                                                     --l_csr_rec.freight_amount;
               -- Added By Dhaval
               l_copy_invoices_tab (l_csr_rec_count).rec_freight_check := 'Y';
            ELSE
               l_freight_comment := 'N';
               l_copy_invoices_tab (l_csr_rec_count).rec_line_description :=
                                                                         NULL;
               l_copy_invoices_tab (l_csr_rec_count).rec_line_extended_amount :=
                                                                         NULL;
               -- Added By Dhaval
               l_copy_invoices_tab (l_csr_rec_count).rec_freight_check := 'N';
            END IF;
         ELSE
            --fnd_file.put_line(fnd_file.log, 'Invoice is without Freight.');
            l_freight_comment := 'N';
            l_copy_invoices_tab (l_csr_rec_count).rec_line_description :=
                                                   l_csr_rec.line_description;
            l_copy_invoices_tab (l_csr_rec_count).rec_line_extended_amount :=
                                                   l_csr_rec.line_extended_amount;
               -- Added By Dhaval
            l_copy_invoices_tab (l_csr_rec_count).rec_freight_check := 'N';
         END IF;
         IF l_csr_rec.trx_class='INV' AND l_csr_rec.originators_name IS NOT NULL
            THEN
               IF substr(l_csr_rec.originators_name,1,1)
                  IN ('1','2','3','4','5','6','7','8','9','0')
               THEN
                 BEGIN -- Changes made by Virendra on 17-SEP-2012 to fix for artf2138409 and artf2138421
                  SELECT FIRST_NAME||'  '|| LAST_NAME
                  INTO  l_originator_name
                  FROM PER_ALL_PEOPLE_F
                  WHERE SYSDATE BETWEEN EFFECTIVE_START_DATE AND EFFECTIVE_END_DATE
                  AND PERSON_TYPE_ID IN
                                      (SELECT PERSON_TYPE_ID FROM PER_PERSON_TYPES
                                        WHERE USER_PERSON_TYPE='Employee')
                  AND PERSON_ID=l_csr_rec.originators_name;
                  --BEGIN: Changes made by Virendra on 17-SEP-2012 to fix for artf2138409 and artf2138421
                  EXCEPTION
				  WHEN NO_DATA_FOUND THEN
				    l_originator_name := l_csr_rec.originators_name;
				  WHEN OTHERS THEN
				    l_originator_name := l_csr_rec.originators_name;
                 END;
                 --END: Changes made by Virendra on 17-SEP-2012 to fix for artf2138409 and artf2138421
               ELSE
                  l_originator_name:=l_csr_rec.originators_name;
               END IF;
         END IF;

         IF l_csr_rec.trx_class='INV' AND  l_csr_rec.originators_name IS NULL
         THEN
            ---------------------------------------------------------------------
            -- Exception handling done by Dhaval for incomplete setup for AR
            -- in case of particular invoice or transaction type
            ---------------------------------------------------------------------
            BEGIN
               SELECT
                   DEFAULT_VALUE
               INTO
                   l_originator_name
               FROM
                   FND_DESCR_FLEX_COLUMN_USAGES
               WHERE DESCRIPTIVE_FLEXFIELD_NAME LIKE 'RA_CUSTOMER_TRX'
               AND APPLICATION_COLUMN_NAME='ATTRIBUTE1'
               AND DESCRIPTIVE_FLEX_CONTEXT_CODE=l_csr_rec.transaction_type;
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  l_originator_name := '';
            END;

         END IF;
        ------------------------------------------------------------------
        --Additional code by Sunil Kalal for handling the Exception
        --when the department code is numeric.
        -- Added on 10-APR-2007
        ------------------------------------------------------------------
         IF l_csr_rec.trx_class='INV' AND l_csr_rec.originators_dept IS NOT NULL
            THEN
               IF substr(l_csr_rec.originators_dept,1,1)
                  IN ('1','2','3','4','5','6','7','8','9','0')
               THEN
            --   fnd_file.put_line(fnd_file.log,'In the dept if clause' );
               BEGIN
                  SELECT NAME
                  INTO  l_originator_dept
                  FROM HR_ALL_ORGANIZATION_UNITS
                  WHERE SYSDATE BETWEEN date_from AND NVL(date_to,to_date('31/12/4712','DD/MM/YYYY'))
                  AND    organization_id=l_csr_rec.originators_dept;
                  EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                 l_originator_dept := l_csr_rec.originators_dept;
                 END;
                ELSE
          --      fnd_file.put_line(fnd_file.log,'In the dept else clause' );
                  l_originator_dept:=l_csr_rec.originators_dept;
               END IF;
         END IF;
        --------
        --------
         IF l_csr_rec.trx_class='INV' AND l_csr_rec.originators_dept IS NULL
         THEN
            ---------------------------------------------------------------------
            -- Exception handling done by Dhaval for incomplete setup for AR
            -- in case of particular invoice or transaction type
            ---------------------------------------------------------------------
        --   fnd_file.put_line(fnd_file.log,'In the Null clause' );
            BEGIN
               SELECT
                     DEFAULT_VALUE
               INTO
                     l_originator_dept
               FROM
                     FND_DESCR_FLEX_COLUMN_USAGES
               WHERE DESCRIPTIVE_FLEXFIELD_NAME LIKE 'RA_CUSTOMER_TRX'
               AND APPLICATION_COLUMN_NAME='ATTRIBUTE2'
               AND DESCRIPTIVE_FLEX_CONTEXT_CODE=l_csr_rec.transaction_type;
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                 l_originator_dept := '';
            END;

      --   ELSE
        --    l_originator_dept:=l_csr_rec.originators_dept;
         END IF;

         IF l_csr_rec.trx_class='INV'
            AND l_csr_rec.originator_telephone_extension IS NULL
         THEN
            ---------------------------------------------------------------------
            -- Exception handling done by Dhaval for incomplete setup for AR
            -- in case of particular invoice or transaction type
            ---------------------------------------------------------------------

            BEGIN
               SELECT
                   DEFAULT_VALUE
               INTO
                   l_originator_tel_ext
               FROM
                   FND_DESCR_FLEX_COLUMN_USAGES
               WHERE DESCRIPTIVE_FLEXFIELD_NAME LIKE 'RA_CUSTOMER_TRX'
               AND APPLICATION_COLUMN_NAME='ATTRIBUTE3'
               AND DESCRIPTIVE_FLEX_CONTEXT_CODE=l_csr_rec.transaction_type;
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  l_originator_tel_ext := '';
            END;
         ELSE
            l_originator_tel_ext:=l_csr_rec.originator_telephone_extension;
         END IF;

         IF l_csr_rec.trx_class='INV'   --added by sunil kalal
         THEN l_cust_trx_class :='$INV';
         ELSE l_cust_trx_class :='$CN';
         END IF;
         l_ship_to :='$SHIPTO';
         l_lines := '$LINE';
         l_end := '$END';
         l_page_tag :='$PAGE';

         l_transaction_count := l_transaction_count + 1;

         l_copy_invoices_tab (l_csr_rec_count).rec_cust_trx_class_tag:=l_cust_trx_class;
         l_copy_invoices_tab (l_csr_rec_count).rec_transaction_number := l_csr_rec.transaction_number;
         l_copy_invoices_tab (l_csr_rec_count).rec_reference_field := l_csr_rec.reference_field;
         l_copy_invoices_tab (l_csr_rec_count).rec_transaction_date := l_csr_rec.transaction_date;
         --- Added by Dhaval on 29-Nov-06
         l_copy_invoices_tab (l_csr_rec_count).rec_job_number := l_csr_rec.job_number;
         l_copy_invoices_tab (l_csr_rec_count).rec_due_date := l_csr_rec.due_date;
         l_copy_invoices_tab (l_csr_rec_count).rec_bill_to_customer_title :=l_csr_rec.bill_to_customer_title;
         l_copy_invoices_tab (l_csr_rec_count).rec_bill_to_customer_name := l_csr_rec.bill_to_customer_name;
         l_copy_invoices_tab (l_csr_rec_count).rec_bill_to_contact := l_csr_rec.bill_to_contact;
         l_copy_invoices_tab (l_csr_rec_count).rec_bill_to_address_line_1 :=l_csr_rec.bill_to_customer_address_line1;
         l_copy_invoices_tab (l_csr_rec_count).rec_bill_to_address_line_2 := l_csr_rec.bill_to_customer_address_line2;
         l_copy_invoices_tab (l_csr_rec_count).rec_bill_to_address_line_3 :=l_csr_rec.bill_to_customer_address_line3;
         l_copy_invoices_tab (l_csr_rec_count).rec_bill_to_town_city :=l_csr_rec.bill_to_town_city;
         l_copy_invoices_tab (l_csr_rec_count).rec_bill_to_county := l_csr_rec.bill_to_county;
         l_copy_invoices_tab (l_csr_rec_count).rec_bill_to_postal_code :=l_csr_rec.bill_to_postal_code;
         l_copy_invoices_tab (l_csr_rec_count).rec_ship_to_tag :=l_ship_to;
         l_copy_invoices_tab (l_csr_rec_count).rec_ship_to_customer_name :=l_csr_rec.ship_to_customer_name;
         l_copy_invoices_tab (l_csr_rec_count).rec_ship_to_contact :=l_csr_rec.ship_to_contact;
         l_copy_invoices_tab (l_csr_rec_count).rec_ship_to_address_line_1 := l_csr_rec.ship_to_customer_address_line1;
         l_copy_invoices_tab (l_csr_rec_count).rec_ship_to_address_line_2 := l_csr_rec.ship_to_customer_address_line2;
         l_copy_invoices_tab (l_csr_rec_count).rec_ship_to_address_line_3 := l_csr_rec.ship_to_customer_address_line3;
         l_copy_invoices_tab (l_csr_rec_count).rec_ship_to_town_city := l_csr_rec.ship_to_town_city;
         l_copy_invoices_tab (l_csr_rec_count).rec_ship_to_county :=l_csr_rec.ship_to_county;
         l_copy_invoices_tab (l_csr_rec_count).rec_ship_to_postal_code := l_csr_rec.ship_to_postal_code;
         l_copy_invoices_tab (l_csr_rec_count).rec_tax_extended_amount := l_csr_rec.tax_extended_amount;
         l_copy_invoices_tab (l_csr_rec_count).rec_tax_rate := l_csr_rec.tax_rate;
         l_copy_invoices_tab (l_csr_rec_count).rec_lines_total := l_csr_rec.lines_total;
         l_copy_invoices_tab (l_csr_rec_count).rec_tax_total := l_csr_rec.tax_total;
         l_copy_invoices_tab (l_csr_rec_count).rec_transaction_total := l_csr_rec.transaction_total;
         l_copy_invoices_tab (l_csr_rec_count).rec_originators_name := l_originator_name;
         l_copy_invoices_tab (l_csr_rec_count).rec_originators_dept :=l_originator_dept;
         l_copy_invoices_tab (l_csr_rec_count).rec_originators_extension :=l_originator_tel_ext;
         l_copy_invoices_tab (l_csr_rec_count).rec_original_trx_number := l_csr_rec.original_transaction_number;
         --
         -- Added by Vinod Narayan on 09th May 2007 as per RFC
         l_copy_invoices_tab (l_csr_rec_count).rec_original_trx_date := l_csr_rec.original_transaction_date;
         --
         l_copy_invoices_tab (l_csr_rec_count).rec_comments := l_csr_rec.comments;
         l_copy_invoices_tab (l_csr_rec_count).rec_lines_tag :=l_lines;
         l_copy_invoices_tab (l_csr_rec_count).rec_page_tag :=l_page_tag;
         l_copy_invoices_tab (l_csr_rec_count).rec_end_tag :=l_end;

        END LOOP;

--------------------------------------------------------------------------
-- This will generate the Header Record of extract file
--------------------------------------------------------------------------
      fnd_file.put_line (fnd_file.LOG, '------------------------------------.');
      fnd_file.put_line (fnd_file.LOG, 'Data file generation Start Point.');
      fnd_file.put_line (fnd_file.LOG, '------------------------------------.');

      l_file_handle := UTL_FILE.fopen (l_output_dir, l_output_file, 'w');
      l_data :=
            'Tag1'
         || CHR (10)
         || 'Transaction Number'
         || CHR (10)
         || 'Reference Field'
         || CHR (10)
         || 'Transaction Date'
         || CHR (10)
         || 'Due Date'
         || CHR (10)
         || 'Bill To Customer Title'
         || CHR (10)
         || 'Bill To Customer Name'
         || CHR (10)
         || 'Bill To Contact'
         || CHR (10)
         || 'Bill To Customer Address Line 1'
         || CHR (10)
         || 'Bill To Customer Address Line 2'
         || CHR (10)
         || 'Bill To Customer Address Line 3'
         || CHR (10)
         || 'Town/City'
         || CHR (10)
         || 'County'
         || CHR (10)
         || 'Postal Code'
         || CHR (10)
         || 'TAG 2'
         || CHR (10)
         || 'Ship To Customer Name'
         || CHR (10)
         || 'Ship To Contact'
         || CHR (10)
         || 'Ship To Customer Address Line 1'
         || CHR (10)
         || 'Ship To Customer Address Line 2'
         || CHR (10)
         || 'Ship To Customer Address Line 3'
         || CHR (10)
         || 'Town/City'
      --   || CHR (10)
      --   || 'County'
         || CHR (10)
         || 'Postal Code'
         || CHR (10)
      -- Removed Net, VAT and Gross Totals from here
         || 'Originator?s Name'
         || CHR (10)
         || 'Originator?s Dept'
         || CHR (10)
         || 'Originator?s Telephone Extension'
         || CHR (10)
         || 'Original Transaction Number'
         || CHR (10)
         -- Added by Vinod on 09th May 2007 as per RFC
         || 'Original Transaction Date'
         || CHR (10)
         --
         || 'Comments'
         || CHR (10)
         || 'Tag 5'
         || CHR (10)
         || 'Line Description'
         || '  '
         || 'Line Extended Amount'
         || '  '
         || 'Tax Extended Amount'
         || '  '
         || 'Tax Rate'
         ||' '
         || 'Tag 3'
         ||' '
         || 'Tag 4';
      UTL_FILE.put_line (l_file_handle, l_data);

      fnd_file.put_line(fnd_file.log, 'File Header created.');
--------------------------------------------------------------------------
-- Extract Data from PL-SQL Table for file generation
--------------------------------------------------------------------------
      --IF l_copy_invoices_tab.COUNT > 0
      fnd_file.put_line (fnd_file.LOG, 'Total Records count is '||l_copy_invoices_tab.COUNT);
      IF l_copy_invoices_tab.EXISTS (1)
      THEN
         fnd_file.put_line (fnd_file.LOG, 'Getting Records from Table to generate File');

         FOR l_ctr IN l_copy_invoices_tab.FIRST .. l_copy_invoices_tab.LAST
         LOOP
            BEGIN
               l_data_transaction_count := l_data_transaction_count + 1;
               l_last_count := l_last_count + 1;

               IF l_last_count=l_copy_invoices_tab.COUNT
               THEN
                  l_last_end_tag :=CHR(10)||'$END';
               ELSE
                  l_last_end_tag:='';
               END IF;

               IF  l_data_transaction_count =1
               THEN
                  l_data_transaction_no1 :=l_copy_invoices_tab (l_ctr).rec_transaction_number;
                  l_end_tag1:='';
               ELSE
                  l_end_tag1:=l_copy_invoices_tab (l_ctr).rec_end_tag;
               END IF;

               IF (l_data_transaction_count >=2)
               THEN
                  l_data_transaction_no2 := l_copy_invoices_tab (l_ctr).rec_transaction_number;
               END IF;

               --Logic For Word Wrapped Comments field
               IF l_copy_invoices_tab (l_ctr).rec_comments IS NOT NULL
               THEN
                  l_comments_from_function := WORD_WRAP(
                                                l_copy_invoices_tab (l_ctr).rec_comments,
                                                l_inv_comments,l_inv_max_commnets_desc);
               ELSE
                  l_comments_from_function:='';
               END IF;

                ----Logic for $PAGE Tag
                IF l_overflow='N'
                THEN
                   l_lines_available:= l_page_invoice_lines;
                ELSE
                   l_lines_available:=l_page_overflow_lines;
                END IF;

                IF l_copy_invoices_tab (l_ctr).rec_line_description IS NOT NULL
                THEN
                   l_description_word_wrap := WORD_WRAP(
                   l_copy_invoices_tab (l_ctr).rec_line_description ,
                   l_inv_line_desc,l_inv_max_line_desc);
                   l_description_lines:= nvl(LINE_COUNT(l_description_word_wrap),0);
                   -- Calculate length of last description line
                   IF l_description_lines = 1 THEN
                      l_last_descr_line_length := LENGTH(l_description_word_wrap);
                   ELSIF l_description_lines > 1 THEN
                      l_last_descr_line_length :=
                      LENGTH(SUBSTR(l_description_word_wrap,
                                INSTR(l_description_word_wrap,CHR(10),-1)+1));
                   ELSE
                      l_last_descr_line_length := 0;
                   END IF;
                ELSE
                   l_description_word_wrap:='';
                   l_description_lines:=1;
                END IF;

                l_lines_used := l_lines_used + l_description_lines;

                IF ((l_lines_used > l_lines_available)
                    AND (l_data_transaction_no1=l_data_transaction_no2))
                THEN
                   -- Commented by Dhaval as per discussion with Richard on
                   -- 14-11-2006
                   ---- 1
                   ----l_page_tag1 := l_copy_invoices_tab (l_ctr).rec_page_tag;
                   l_lines_used :=l_lines_used-l_lines_available;
                   l_lines_available :=l_page_overflow_lines;
                   l_overflow:='Y';
                ELSE
                   -- Commented by Dhaval as per discussion with Richard on
                   -- 14-11-2006
                   ---- 2
                   ----l_page_tag1 :='';
                   IF ((l_data_transaction_no1=l_data_transaction_no2)
                       AND  (l_overflow='Y'))
                   THEN
                      l_lines_available :=l_page_overflow_lines;
                      l_overflow:='Y';
                   ELSE
                      l_lines_available :=l_page_invoice_lines;
                      l_overflow:='N';
                   END iF;
                END IF;
                -----------------------------------
                -- Start of Transaction Count
                -----------------------------------

                IF ((l_data_transaction_count =1)  OR
                 ((l_data_transaction_count >=2) AND
                 (l_data_transaction_no1<>l_data_transaction_no2)))
                THEN
                    -- Format output
                   l_data := l_end_tag1
                      || CHR (10)
                      ||  l_copy_invoices_tab (l_ctr).rec_cust_trx_class_tag
                      || CHR (10)
                      ||  l_copy_invoices_tab (l_ctr).rec_transaction_number
                      || CHR (10)
                      ||l_copy_invoices_tab (l_ctr).rec_reference_field||l_copy_invoices_tab (l_ctr).rec_job_number
                      ||CHR(10)
                      || l_copy_invoices_tab (l_ctr).rec_transaction_date
                      || CHR (10)
                      -- add by dhaval on 29-Nov-06
                      --||l_copy_invoices_tab (l_ctr).rec_job_number
                      --|| CHR (10)
                      || l_copy_invoices_tab (l_ctr).rec_due_date
                      || CHR (10);
                       -- Invoice Totals
                        l_data:=l_data
                      || ltrim(to_char( l_copy_invoices_tab (l_ctr).rec_lines_total,'99,999,999,990.99'))
                      || CHR (10)
                      || ltrim(to_char( l_copy_invoices_tab(l_ctr).rec_tax_total,'99,999,999,990.99'))
                      || CHR (10)
                      || ltrim(to_char( l_copy_invoices_tab (l_ctr).rec_transaction_total,'99,999,999,990.99'))
                      || CHR (10);

                      IF l_copy_invoices_tab(l_ctr).rec_cust_trx_class_tag='$INV'
                      AND l_copy_invoices_tab (l_ctr).rec_originators_name IS NOT NULL
                      THEN
                         l_data := l_data
                                 || l_copy_invoices_tab (l_ctr).rec_originators_name
                                 ||CHR(10);
                      ELSE
                         l_data:=l_data
                                 ||CHR(10);
                      END IF;

                      IF l_copy_invoices_tab(l_ctr).rec_cust_trx_class_tag='$INV'
                      AND l_copy_invoices_tab (l_ctr).rec_originators_dept IS NOT NULL
                      THEN
                         l_data := l_data
                                 || l_copy_invoices_tab (l_ctr).rec_originators_dept
                                 || CHR(10);
                      ELSE
                         l_data:=l_data || CHR(10);
                      END IF;

                      IF l_copy_invoices_tab(l_ctr).rec_cust_trx_class_tag='$INV'
                      AND l_copy_invoices_tab (l_ctr).rec_originators_extension IS NOT NULL
                      THEN
                         l_data := l_data
                                 ||l_copy_invoices_tab (l_ctr).rec_originators_extension
                                 ||CHR(10);
                      ELSE
                         l_data:=l_data  || CHR(10);
                      END IF;

                      IF l_copy_invoices_tab(l_ctr).rec_cust_trx_class_tag='$CN'
                      AND l_copy_invoices_tab (l_ctr).rec_original_trx_number IS NOT NULL
                      THEN
                         l_data:= l_data
                                  ||l_copy_invoices_tab (l_ctr).rec_original_trx_number
                                  ||CHR(10)
                                  -- Added by Vinod on 09th May 2007 as per RFC
                                  ||l_copy_invoices_tab (l_ctr).rec_original_trx_date
                                  ||CHR(10);
                      ELSE
                         IF l_copy_invoices_tab (l_ctr).rec_cust_trx_class_tag='$CN'
                         AND l_copy_invoices_tab (l_ctr).rec_original_trx_number IS NULL
                         THEN
                             l_data := l_data ||CHR(10);
                         END IF;
                      END IF;

                       -- Bill-to Address
                      IF l_copy_invoices_tab (l_ctr).rec_bill_to_customer_title IS NULL
                      THEN
                         l_data:= l_data||'';
                      END IF;

                      IF l_copy_invoices_tab (l_ctr).rec_bill_to_customer_title IS NOT NULL
                      THEN
              -----------------------------------------------------
                      -- Comment By Dhaval on 28-11-2006
              -----------------------------------------------------
                  l_copy_invoices_tab (l_ctr).rec_bill_to_customer_name :=
                    l_copy_invoices_tab (l_ctr).rec_bill_to_customer_title
                    || ' ' ||
                    l_copy_invoices_tab (l_ctr).rec_bill_to_customer_name;

              /*
                      l_data := l_data
                              ||l_copy_invoices_tab (l_ctr).rec_bill_to_customer_title
                              ||CHR(10);
                               l_line_position_count:= l_line_position_count + 1;
                      */
                      END IF;

-- Original Code for Customer Name
/*
                      IF l_copy_invoices_tab (l_ctr).rec_bill_to_customer_name IS NOT NULL
                      THEN
                         l_cust_name_wrap :=WORD_WRAP(
                                              l_copy_invoices_tab (l_ctr).rec_bill_to_customer_name,
                                              l_inv_emp_name_desc,
                                              l_inv_emp_name_max_length);

                         l_data:= l_data
                              ||l_cust_name_wrap
                              ||CHR(10);

                               l_line_position_count:= l_line_position_count + 1;
                      END IF;
*/
-- Changes done by Dhaval
                      IF l_copy_invoices_tab (l_ctr).rec_bill_to_customer_name IS NOT NULL
                      THEN

                         ------------------------------------------------------------------
                         -- Added by Dhaval on 21-Nov-2006 for Customer Name
                         -- in capital will have different margin than actual setup
                         ------------------------------------------------------------------
                         IF IS_UPPER(l_copy_invoices_tab (l_ctr).rec_bill_to_customer_name)
                         THEN
                            fnd_file.put_line(fnd_file.log, 'Bill to Customer name in Captial with more 75% of characters');
                            l_cust_name_wrap :=WORD_WRAP(
                                              l_copy_invoices_tab (l_ctr).rec_bill_to_customer_name,
                                              l_inv_emp_name_cap_desc,
                                              l_inv_emp_name_max_length);

                         ELSE
                            -- Use Actual Margin
                            l_cust_name_wrap :=WORD_WRAP(
                                              l_copy_invoices_tab (l_ctr).rec_bill_to_customer_name,
                                              l_inv_emp_name_desc,
                                              l_inv_emp_name_max_length);

                         END IF;
                         l_data:= l_data
                              ||l_cust_name_wrap
                              ||CHR(10);

                               l_line_position_count:= l_line_position_count + 1;
                      END IF;

                      IF l_copy_invoices_tab (l_ctr).rec_bill_to_contact IS NOT NULL
                      THEN
                         l_data:= l_data
                              ||l_copy_invoices_tab (l_ctr).rec_bill_to_contact
                              ||CHR(10);
                               l_line_position_count:= l_line_position_count + 1;
                      END IF;

                      IF l_copy_invoices_tab (l_ctr).rec_bill_to_address_line_1 IS NOT NULL
                      THEN
                         l_data:= l_data
                              ||l_copy_invoices_tab (l_ctr).rec_bill_to_address_line_1
                              ||CHR(10);
                               l_line_position_count:= l_line_position_count + 1;
                      END IF;

                      IF l_copy_invoices_tab (l_ctr).rec_bill_to_address_line_2 IS NOT NULL
                      THEN
                         l_data:= l_data
                              ||l_copy_invoices_tab (l_ctr).rec_bill_to_address_line_2
                              ||CHR(10);
                               l_line_position_count:= l_line_position_count + 1;
                      END IF;

                      IF l_copy_invoices_tab (l_ctr).rec_bill_to_address_line_3 IS NOT NULL
                      THEN
                         l_data:= l_data
                              ||l_copy_invoices_tab (l_ctr).rec_bill_to_address_line_3
                              ||CHR(10);
                               l_line_position_count:= l_line_position_count + 1;
                      END IF;

                      IF l_copy_invoices_tab (l_ctr).rec_bill_to_town_city IS NOT NULL
                      THEN
                         l_data:= l_data
                              ||l_copy_invoices_tab (l_ctr).rec_bill_to_town_city
                              ||CHR(10);
                               l_line_position_count:= l_line_position_count + 1;
                      END IF;

                      IF l_copy_invoices_tab (l_ctr).rec_bill_to_county IS NOT NULL
                      THEN
                         l_data:= l_data
                              ||l_copy_invoices_tab (l_ctr).rec_bill_to_county
                              ||CHR(10);
                               l_line_position_count:= l_line_position_count + 1;
                      END IF;

                      IF l_copy_invoices_tab (l_ctr).rec_bill_to_postal_code IS NOT NULL
                      THEN
                         l_data:= l_data
                              ||l_copy_invoices_tab (l_ctr).rec_bill_to_postal_code
                              ||CHR(10);
                               l_line_position_count:= l_line_position_count + 1;
                      END IF;

                     -- Ship-to Address
                      l_data:=l_data
                              || l_copy_invoices_tab (l_ctr).rec_ship_to_tag
                              || CHR (10);

                      l_line_position_count:= l_line_position_count + 1;


                      IF l_copy_invoices_tab (l_ctr).rec_ship_to_customer_name IS NOT NULL
                      THEN
                         ------------------------------------------------------------------
                         -- Added by Dhaval on 21-Nov-2006 for Customer Name
                         -- in capital will have different margin than actual setup
                         ------------------------------------------------------------------
                         IF IS_UPPER(l_copy_invoices_tab (l_ctr).rec_ship_to_customer_name)
                         THEN
                            fnd_file.put_line(fnd_file.log, 'Ship to Customer name in Captial with more 75% of characters');
                            l_cust_name_wrap_2 :=WORD_WRAP(
                                              l_copy_invoices_tab (l_ctr).rec_ship_to_customer_name,
                                              l_inv_emp_name_cap_desc,
                                              l_inv_emp_name_max_length);

                         ELSE
                           -- Use actual one
                           l_cust_name_wrap_2 := WORD_WRAP(
                                                 l_copy_invoices_tab (l_ctr).rec_ship_to_customer_name,
                                                 l_inv_emp_name_desc,
                                                 l_inv_emp_name_max_length);
                         END IF;
                         l_data:= l_data
                              ||l_cust_name_wrap_2
                              ||CHR(10);
                               l_line_position_count:= l_line_position_count + 1;
                      END IF;

                      /*
                      IF l_copy_invoices_tab (l_ctr).rec_ship_to_customer_name IS NOT NULL
                      THEN
                         l_cust_name_wrap_2 := WORD_WRAP(
                                               l_copy_invoices_tab (l_ctr).rec_ship_to_customer_name,
                                               l_inv_emp_name_desc,
                                               l_inv_emp_name_max_length);

                         l_data:= l_data
                              ||l_cust_name_wrap_2
                              ||CHR(10);
                               l_line_position_count:= l_line_position_count + 1;
                      END IF;
                      */
                      IF l_copy_invoices_tab (l_ctr).rec_ship_to_contact IS NOT NULL
                      THEN
                      l_data:= l_data
                              ||l_copy_invoices_tab (l_ctr).rec_ship_to_contact
                              ||CHR(10);
                               l_line_position_count:= l_line_position_count + 1;
                      END IF;

                      IF l_copy_invoices_tab (l_ctr).rec_ship_to_address_line_1 IS NOT NULL
                      THEN
                         l_data:= l_data
                              ||l_copy_invoices_tab (l_ctr).rec_ship_to_address_line_1
                              ||CHR(10);
                               l_line_position_count:= l_line_position_count + 1;
                      END IF;

                      IF l_copy_invoices_tab (l_ctr).rec_ship_to_address_line_2 IS NOT NULL
                      THEN
                         l_data:= l_data
                              ||l_copy_invoices_tab (l_ctr).rec_ship_to_address_line_2
                              ||CHR(10);
                               l_line_position_count:= l_line_position_count + 1;
                      END IF;

                      IF l_copy_invoices_tab (l_ctr).rec_ship_to_address_line_3 IS NOT NULL
                      THEN
                         l_data:= l_data
                              ||l_copy_invoices_tab (l_ctr).rec_ship_to_address_line_3
                              ||CHR(10);
                               l_line_position_count:= l_line_position_count + 1;
                      END IF;

                      IF l_copy_invoices_tab (l_ctr).rec_ship_to_town_city IS NOT NULL
                      THEN
                         l_data:= l_data
                              ||l_copy_invoices_tab (l_ctr).rec_ship_to_town_city
                              ||CHR(10);
                               l_line_position_count:= l_line_position_count + 1;
                      END IF;

                      /* remove county field
                      IF l_copy_invoices_tab (l_ctr).rec_ship_to_county IS NOT NULL
                      THEN
                        l_data:= l_data
                              ||l_copy_invoices_tab (l_ctr).rec_ship_to_county
                              ||CHR(10);
                               l_line_position_count:= l_line_position_count + 1;
                      END IF;*/

                      IF l_copy_invoices_tab (l_ctr).rec_ship_to_postal_code IS NOT NULL
                      THEN
                         l_data:= l_data
                              ||l_copy_invoices_tab (l_ctr).rec_ship_to_postal_code
                              ||CHR(10);
                               l_line_position_count:= l_line_position_count + 1;
                      END IF;

                      IF  l_line_position_count < gc_line_position
                      THEN
                         l_line_CHR10 := gc_line_position-l_line_position_count;
                         For i IN 1..l_line_CHR10
                         LOOP
                            l_data:=l_data||CHR(10);
                         END LOOP;
                      END IF;

                      l_line_position_count:=0;
                      -- Moved the 3 totals up to before the To Address

                      /*
                      l_data:=l_data
                      || ltrim(to_char(l_copy_invoices_tab (l_ctr).rec_lines_total,'99,999,999,990.99'))
                      || CHR (10)
                      || ltrim(to_char(l_copy_invoices_tab (l_ctr).rec_tax_total,'99,999,999,990.99'))
                      || CHR (10)
                      || ltrim(to_char(l_copy_invoices_tab (l_ctr).rec_transaction_total,'99,999,999,990.99'))
                      || CHR (10);*/

                      IF l_copy_invoices_tab (l_ctr).rec_cust_trx_class_tag='$INV'
                      THEN
                         l_data:= l_data ||CHR(10);
                      END IF;

                      -- Add comments tag
                      l_data := l_data||gc_comments_tag||CHR(10);
                      IF l_copy_invoices_tab (l_ctr).rec_comments IS NOT NULL
                      THEN
                         l_data:= l_data
                              ||l_comments_from_function
                              ||CHR(10);
                      END IF;

                      l_data:=l_data
                        || l_copy_invoices_tab (l_csr_rec_count).rec_lines_tag
                        || CHR (10);

                      IF l_copy_invoices_tab (l_ctr).rec_line_description IS NOT NULL
                      THEN
                         l_data:= l_data
                              ||l_description_word_wrap
                              ||'';
                      END IF;
                      /*
                      l_data:=l_data
                        ||LPAD(
                            LTRIM(
                              TO_CHAR(
                                l_copy_invoices_tab(l_ctr).rec_line_extended_amount,
                                  '99,999,999,990.99')),
                                    gc_net_amount_margin-l_last_descr_line_length)
                        ||LPAD(
                            LTRIM(
                              TO_CHAR(
                                l_copy_invoices_tab(l_ctr).rec_tax_extended_amount,
                                  '99,999,999,990.99')),
                                    gc_vat_amount_margin-gc_net_amount_margin)
                        || LPAD(l_copy_invoices_tab(l_ctr).rec_tax_rate,
                                gc_vat_rate_margin-gc_vat_amount_margin)
                        ||l_last_end_tag;
                        */
----                      IF NVL(l_freight_comment , 'N') = 'N' THEN
               -- Added By Dhaval
                    IF l_copy_invoices_tab (l_ctr).rec_freight_check = 'N'
                    THEN
                        l_data:=l_data
                          ||LPAD(
                              LTRIM(
                                TO_CHAR(
                                  l_copy_invoices_tab(l_ctr).rec_line_extended_amount,
                                    '99,999,999,990.99')),
                                      gc_net_amount_margin-l_last_descr_line_length)
                          ||LPAD(
                              LTRIM(
                                TO_CHAR(
                                  l_copy_invoices_tab(l_ctr).rec_tax_extended_amount,
                                    '99,999,999,990.99')),
                                      gc_vat_amount_margin-gc_net_amount_margin)
                          || LPAD(l_copy_invoices_tab(l_ctr).rec_tax_rate,
                                  gc_vat_rate_margin-gc_vat_amount_margin)
                          || l_last_end_tag;
                          --fnd_file.put_line(fnd_file.log, 'Without Freight 1 '
                          --    || l_copy_invoices_tab (l_ctr).rec_transaction_number);
                       ELSE
                        l_data:=l_data
                          ||LPAD(
                              LTRIM(
                                TO_CHAR(
                                  l_copy_invoices_tab(l_ctr).rec_line_extended_amount,
                                    '99,999,999,990.99')),
                                      gc_net_amount_margin-l_last_descr_line_length)
                          ||LPAD(
                              LTRIM(
                                TO_CHAR(
                                  l_copy_invoices_tab(l_ctr).rec_tax_extended_amount,
                                    '99,999,999,990.99')),
                                      gc_vat_amount_margin-gc_net_amount_margin)
                          || LPAD(l_copy_invoices_tab(l_ctr).rec_tax_rate,
                                  gc_vat_rate_margin-gc_vat_amount_margin)
                          || CHR(10)
                          || 'Delivery/Admin Charges'
                          ||LPAD(
                              LTRIM(
                                TO_CHAR(
                                  l_copy_invoices_tab (l_ctr).rec_freight_amount,
                                    '99,999,999,990.99')),
                                      gc_net_amount_margin-LENGTH('Delivery/Admin Charges'))
                          || l_last_end_tag;
                          --fnd_file.put_line(fnd_file.log, 'With Freight 1 '
                          --|| l_copy_invoices_tab (l_ctr).rec_transaction_number);
                       END IF;
                    END IF;

                    -----------------------------------
                    -- Start of Transaction Count
                    -----------------------------------
                    ---- 3
                     IF ((l_data_transaction_count >=2)
                        AND (l_data_transaction_no1=l_data_transaction_no2))
                        AND l_page_tag1 IS NOT NULL
                     THEN
---                        IF NVL(l_freight_comment , 'N') = 'N' THEN
                     IF l_copy_invoices_tab (l_ctr).rec_freight_check = 'N'
                     THEN

                            l_data  :=  l_page_tag1
                                ||CHR (10)
                                ||l_description_word_wrap
                                ||LPAD(
                                    LTRIM(
                                      TO_CHAR(
                                        l_copy_invoices_tab (l_ctr).rec_line_extended_amount,
                                          '99,999,999,990.99')),
                                            gc_net_amount_margin-l_last_descr_line_length)
                                ||LPAD(
                                    LTRIM(
                                      TO_CHAR(
                                        l_copy_invoices_tab (l_ctr).rec_tax_extended_amount,
                                          '99,999,999,990.99')),
                                            gc_vat_amount_margin-gc_net_amount_margin)
                                ||LPAD(l_copy_invoices_tab (l_ctr).rec_tax_rate,
                                        gc_vat_rate_margin-gc_vat_amount_margin)
                                || l_last_end_tag
                                --|| l_copy_invoices_tab (l_ctr).rec_line_description
                                ;
                                --fnd_file.put_line(fnd_file.log, 'Without Freight 2 '
                                --|| l_copy_invoices_tab (l_ctr).rec_transaction_number);
                          ELSE

                            l_data  :=  l_page_tag1
                               ||CHR (10)
                               ||l_description_word_wrap
                               ||LPAD(
                                    LTRIM(
                                      TO_CHAR(
                                        l_copy_invoices_tab (l_ctr).rec_line_extended_amount,
                                          '99,999,999,990.99')),
                                            gc_net_amount_margin-l_last_descr_line_length)
                                ||LPAD(
                                    LTRIM(
                                      TO_CHAR(
                                        l_copy_invoices_tab (l_ctr).rec_tax_extended_amount,
                                          '99,999,999,990.99')),
                                            gc_vat_amount_margin-gc_net_amount_margin)
                                ||LPAD(l_copy_invoices_tab (l_ctr).rec_tax_rate,
                                        gc_vat_rate_margin-gc_vat_amount_margin)
                                || CHR(10)
                                || 'Delivery/Admin Charges'
                                ||LPAD(
                                    LTRIM(
                                      TO_CHAR(
                                        l_copy_invoices_tab (l_ctr).rec_freight_amount,
                                          '99,999,999,990.99')),
                                            gc_net_amount_margin-LENGTH('Delivery/Admin Charges'))
                                || l_last_end_tag
                                --|| l_copy_invoices_tab (l_ctr).rec_line_description
                                ;
                                --fnd_file.put_line(fnd_file.log, 'With Freight 2 '
                                --|| l_copy_invoices_tab (l_ctr).rec_transaction_number);
                          END IF;

                     END IF;

                     ---- 4
                     IF ((l_data_transaction_count >=2)
                        AND (l_data_transaction_no1=l_data_transaction_no2))
                        AND l_page_tag1 IS NULL
                     THEN
                        l_data  := l_description_word_wrap
                         ||LPAD(
                                LTRIM(
                                  TO_CHAR(
                                    l_copy_invoices_tab(l_ctr).rec_line_extended_amount,
                                      '99,999,999,990.99')),
                                        gc_net_amount_margin-l_last_descr_line_length)
                            ||LPAD(
                                LTRIM(
                                  TO_CHAR(
                                    l_copy_invoices_tab(l_ctr).rec_tax_extended_amount,
                                      '99,999,999,990.99')),
                                        gc_vat_amount_margin-gc_net_amount_margin)
                            || LPAD(l_copy_invoices_tab(l_ctr).rec_tax_rate,
                                    gc_vat_rate_margin-gc_vat_amount_margin)
                            || l_last_end_tag
                            --|| l_copy_invoices_tab (l_ctr).rec_line_description
                            ;
                     END IF;

                     IF ((l_data_transaction_count >=2)
                        AND (l_data_transaction_no1 <> l_data_transaction_no2 ))
                     THEN
                        l_data_transaction_count := 1;
                        l_data_transaction_no1   := l_copy_invoices_tab (l_ctr).rec_transaction_number;
                        l_data_transaction_no2   := 0;
                        l_lines_used := l_description_lines;
                     END IF;
           EXCEPTION
              WHEN OTHERS THEN
                  l_errcode := SQLCODE;
                  l_errdesc := SUBSTR (SQLERRM, 1, 250);
                  fnd_file.put_line
                                (fnd_file.LOG,
                                    l_copy_invoices_tab (l_ctr).rec_transaction_number
                                 || ' with Oracle Error Number : '
                                 || l_errcode
                                 || ' and error description as '
                                 || l_errdesc
                                );
                   l_total_error_rec := l_total_error_rec + 1;
           END;

           l_total_succeed_rec := l_total_succeed_rec + 1;
           UTL_FILE.put_line (l_file_handle, l_data);

         END LOOP;

         fnd_file.put_line (fnd_file.LOG, '------------------------------------.');
         fnd_file.put_line (fnd_file.LOG, 'Data file generation End Point.');
         fnd_file.put_line (fnd_file.LOG, '------------------------------------.');

         fnd_file.put_line (fnd_file.LOG,
                            'File Created with name ' || l_output_file
                           );
         fnd_file.put_line (fnd_file.LOG,
                               'Total No. of Records Processed: '
                            || l_copy_invoices_tab.COUNT
                           );

         ----------------------------------------------------
         -- Changed By Dhaval on 28-Nov-06 as in production
         -- it pointing to $XXC_TOP/out and XXCDU803 is
         -- taking care for this location
         ----------------------------------------------------
         --l_file_location := l_output_dir || '/' || l_output_file;
         l_file_location := l_output_file;
         --l_printq_profile := fnd_profile.VALUE ('CCS_DP_PRINT');
         -- Changed By Dhaval on 29-Nov-2006 as Production will have different
         -- set up as per richard
          --l_printq_profile := fnd_profile.VALUE ('XXC_INVOICE_DP_QUEUE');
          --l_printq_profile := fnd_profile.VALUE ('XXC_VAT_DP_QUEUE');
          --------------------------------------------------------------------
           --Added By Priyaranjan
         -----------------------------------------------------------------------
          If l_copy_invoices_tab (l_csr_rec_count).rec_cust_trx_class_tag='$INV' then
          l_printq_profile := fnd_profile.VALUE ('XXC_INVOICE_DP_QUEUE');
          End if;
          IF l_copy_invoices_tab (l_csr_rec_count).rec_cust_trx_class_tag='$CN' then
          l_printq_profile := fnd_profile.VALUE ('XXC_VAT_DP_QUEUE');
          End if;
          --------------------------------------------------------------------
          --Code ended by Priyaranjan
         --------------------------------------------------------------------
         -----------------------------------------------------------------------
         -- Added by Dhaval on 15-Nov-2006
         -----------------------------------------------------------------------
         IF l_printq_profile is NULL THEN
            p_retcode := 2;
            p_errbuf := 'Profile CCS_DP_PRINT is not available or '
                      || 'incomplete setup exist; Contact Administrator.';
            fnd_file.put_line (fnd_file.LOG, p_errbuf);
            app_exception.raise_exception;

         END IF;

         --   l_printQ_profile :='dp_captest';
         fnd_file.put_line (fnd_file.LOG, 'Sending Output file to queue : ' || l_printq_profile);
         fnd_file.put_line (fnd_file.LOG, 'Before Submit Request for calling Printing Queue Program');

         -----------------------------------------------------------------------
         -- Call Printing Queue program to send generated output file to required
         -- specific printer queue.
         -----------------------------------------------------------------------
         l_request_id_file :=
            fnd_request.submit_request ('XXC',                  -- application
                                        'XXCDU803',                 -- program
                                        '',                     -- description
                                        NULL,                    -- start_time
                                        FALSE,                  -- sub_request
                                        l_printq_profile,         -- argument1
                                        l_file_location,          -- argument2
                                        '',                       -- argument3
                                        '',                       -- argument4
                                        '',                       -- argument5
                                        '',                       -- argument6
                                        '',                       -- argument7
                                        '',                       -- argument8
                                        '',                       -- argument9
                                        '',                      -- argument10
                                        '',                      -- argument11
                                        '',                      -- argument12
                                        '',                      -- argument13
                                        '',                      -- argument14
                                        '',                      -- argument15
                                        '',                      -- argument16
                                        '',                      -- argument17
                                        '',                      -- argument18
                                        '',                      -- argument19
                                        '',                      -- argument20
                                        '',                      -- argument21
                                        '',                      -- argument22
                                        '',                      -- argument23
                                        '',                      -- argument24
                                        '',                      -- argument25
                                        '',                      -- argument26
                                        '',                      -- argument27
                                        '',                      -- argument28
                                        '',                      -- argument29
                                        '',                      -- argument30
                                        '',                      -- argument31
                                        '',                      -- argument32
                                        '',                      -- argument33
                                        '',                      -- argument34
                                        '',                      -- argument35
                                        '',                      -- argument36
                                        '',                      -- argument37
                                        '',                      -- argument38
                                        '',                      -- argument39
                                        '',                      -- argument40
                                        '',                      -- argument41
                                        '',                      -- argument42
                                        '',                      -- argument43
                                        '',                      -- argument44
                                        '',                      -- argument45
                                        '',                      -- argument46
                                        '',                      -- argument47
                                        '',                      -- argument48
                                        '',                      -- argument49
                                        '',                      -- argument50
                                        '',                      -- argument51
                                        '',                      -- argument52
                                        '',                      -- argument53
                                        '',                      -- argument54
                                        '',                      -- argument55
                                        '',                      -- argument56
                                        '',                      -- argument57
                                        '',                      -- argument58
                                        '',                      -- argument59
                                        '',                      -- argument60
                                        '',                      -- argument61
                                        '',                      -- argument62
                                        '',                      -- argument63
                                        '',                      -- argument64
                                        '',                      -- argument65
                                        '',                      -- argument66
                                        '',                      -- argument67
                                        '',                      -- argument68
                                        '',                      -- argument69
                                        '',                      -- argument70
                                        '',                      -- argument71
                                        '',                      -- argument72
                                        '',                      -- argument73
                                        '',                      -- argument74
                                        '',                      -- argument75
                                        '',                      -- argument76
                                        '',                      -- argument77
                                        '',                      -- argument78
                                        '',                      -- argument79
                                        '',                      -- argument80
                                        '',                      -- argument81
                                        '',                      -- argument82
                                        '',                      -- argument83
                                        '',                      -- argument84
                                        '',                      -- argument85
                                        '',                      -- argument86
                                        '',                      -- argument87
                                        '',                      -- argument88
                                        '',                      -- argument89
                                        '',                      -- argument90
                                        '',                      -- argument91
                                        '',                      -- argument92
                                        '',                      -- argument93
                                        '',                      -- argument94
                                        '',                      -- argument95
                                        '',                      -- argument96
                                        '',                      -- argument97
                                        '',                      -- argument98
                                        '',                      -- argument99
                                        ''                      -- argument100
                                       );
         fnd_file.put_line (fnd_file.LOG, 'After Submit Request for Printing Queue program : '
                                          || l_request_id_file);
         p_retcode := SQLCODE;
         p_errbuf  := 'Successful';
      ELSE
         fnd_file.put_line (fnd_file.LOG, 'No Data Available For This File');
         p_retcode := SQLCODE;
         p_errbuf := nvl(SUBSTR(SQLERRM,1,250),'No Data Available');
      END IF;
      UTL_FILE.fclose (l_file_handle);
     --------------------------------------------------------------------------
     -- This called procedure will generate a output file as per standard
     --------------------------------------------------------------------------
--xxc_utils.get_run_report_output(l_request_id,l_total_succeed_rec,l_copy_invoices_tab.COUNT,l_total_error_rec,'I');
   EXCEPTION
      WHEN UTL_FILE.invalid_filehandle
      THEN
         fnd_file.put_line (fnd_file.LOG, 'File Handle Exception raised');
         app_exception.raise_exception;
         UTL_FILE.fclose (l_file_handle);
         ROLLBACK;
         p_retcode := 2;
         p_errbuf := nvl(SUBSTR(SQLERRM,1,250),'File Handle Exception error');
      WHEN UTL_FILE.invalid_path
      THEN
         fnd_file.put_line (fnd_file.LOG, 'Path Exception');
         app_exception.raise_exception;
         UTL_FILE.fclose (l_file_handle);
         ROLLBACK;
         p_retcode := 2;
         p_errbuf := nvl(SUBSTR(SQLERRM,1,250),'Path Exception error');
      WHEN UTL_FILE.invalid_mode
      THEN
         fnd_file.put_line (fnd_file.LOG, 'MODE Exception');
         app_exception.raise_exception;
         UTL_FILE.fclose (l_file_handle);
         ROLLBACK;
         p_retcode := 2;
         p_errbuf := nvl(SUBSTR (SQLERRM, 1, 250),'MODE Exception error');
      WHEN UTL_FILE.invalid_operation
      THEN
         fnd_file.put_line (fnd_file.LOG, 'OPERATION Exception');
         app_exception.raise_exception;
         UTL_FILE.fclose (l_file_handle);
         ROLLBACK;
         p_retcode := 2;
         p_errbuf :=nvl(SUBSTR (SQLERRM, 1, 250),'OPERATION Exception error');
      WHEN OTHERS
      THEN
        fnd_file.put_line (fnd_file.LOG, 'When Others error');
         p_errbuf := nvl(SUBSTR (SQLERRM, 1, 250),'When Others error');
         p_retcode := SQLCODE;
         app_exception.raise_exception;
   END xxc_main;
-- ----------------------------------------------------
-- End of XXC0AR900_AR007_PRINT_INVOICES
-- ----------------------------------------------------
END XXC0AR900_AR007_PRINT_INVOICES;
/
