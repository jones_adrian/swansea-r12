create or replace PACKAGE XXC0AR900_AR007_PRINT_INVOICES
AS
/*************************************************************
$Header$

Original Author: Sunil Kalal

Module Type    : SQL*Plus

Description    : This package will extract all New/Copy Invoices from Account
                 Receivables where print option is 'PRI' along with the customer details.


Modification History:
$Log$

21/08/2006     Yousuf Husaini    Initial Creation
**************************************************************/
    gc_comments_wrap_margin CONSTANT PLS_INTEGER := 40;
  gc_descr_wrap_margin    CONSTANT PLS_INTEGER := 40;
  gc_net_amount_margin    CONSTANT PLS_INTEGER := 55;
  gc_vat_amount_margin    CONSTANT PLS_INTEGER := 70;
  gc_vat_rate_margin      CONSTANT PLS_INTEGER := 85;
  gc_page_invoice_lines   CONSTANT PLS_INTEGER := 
    NVL(fnd_profile.VALUE('XXC_PAGE1_INVOICE_LINES'),12);
  gc_line_position        CONSTANT PLS_INTEGER := 18;
  gc_page_overflow_lines  CONSTANT PLS_INTEGER := 
    NVL(fnd_profile.VALUE('XXC_OVERFLOW_LINES'),40);
  gc_comments_tag         CONSTANT VARCHAR2(30) := '$COMMENTS';

    
    FUNCTION LINE_COUNT ( 
      p_input_string VARCHAR2)   RETURN PLS_INTEGER;
   FUNCTION WORD_WRAP (
      p_input_string VARCHAR2,
      p_column_width NUMBER DEFAULT 40,
      p_max_input_length NUMBER DEFAULT 240)RETURN VARCHAR2;
   PROCEDURE xxc_main (
      p_errbuf                    OUT      VARCHAR2,
      p_retcode                   OUT      NUMBER,
      p_cust_trx_class            IN       VARCHAR2 default null,
      p_cust_trx_type_id          IN       NUMBER default null,
      p_trx_number_low            IN       VARCHAR2, 
      p_trx_number_high           IN       VARCHAR2,
      p_dates_low                 IN       DATE default null,
      p_dates_high                IN       DATE default null,
      p_customer_class_code       IN       VARCHAR2,
      p_customer_id               IN       NUMBER default null,
      p_installment_number        IN       NUMBER default null,
      p_open_invoice              IN       VARCHAR2 default null,
      p_tax_registration_number   IN       NUMBER default null,
      p_from_dunning              IN       VARCHAR2,
      p_new_invoices              IN       VARCHAR2
     
   );
END XXC0AR900_AR007_PRINT_INVOICES;
/

