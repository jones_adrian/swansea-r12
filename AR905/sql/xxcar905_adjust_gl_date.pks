CREATE OR REPLACE PACKAGE xxcar905_adjust_gl_date
AS
/* =============================================================================
*
* $Header$
*
* Module         : xxcar905_adjust_gl_date
* Module Type    : PL/SQL Package Header
* Original Author: Richard Chapman
*
* Description    : This package uses an api to sweep the GL dates on pending
*                  adjustments into the next period.
*
* Log in as      : Oracle APPS database user
* Run Env.       : SQL*Plus
*
* Procedure Name           Description
*
* adjust_gl_date           Procedure adjust_gl_date will use an api to sweep 
*                          the GL dates on pending adjustments into the next
*                          period.
*
* Parameters
* ==========
*
* Parm Name          I/O   Description
* -----------------  ----  ----------------------------------------------------
* errbuf             OUT   Error message.
* retcode            OUT   Error code. Returns 0 if no errors, 1 (Warning) for
*                          data errors, otherwise returns 2 (Error).
*
* Change History
* $Log$
*
* Version Name             Date        Description of Change
* ------- ---------------- ----------- -----------------------------------------
* 0.01    Richard Chapman  08-OCT-2013 Initial creation for R12 artf2534778 
*                                      EARS00034391730 CHMN00034544247. 
* ==============================================================================
*/
PROCEDURE adjust_gl_date (
  p_errbuf           OUT      VARCHAR2,
  p_retcode          OUT      NUMBER,
  p_gl_date          IN       VARCHAR2,
  p_mode             IN       VARCHAR2
  );
END xxcar905_adjust_gl_date;