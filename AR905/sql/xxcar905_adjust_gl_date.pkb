CREATE OR REPLACE PACKAGE BODY xxcar905_adjust_gl_date
AS
/* =============================================================================
*
* $Header$
*
* Module         : xxcar905_adjust_gl_date
* Module Type    : PL/SQL Package Header
* Original Author: Richard Chapman
*
* Description    : This package uses an api to sweep the GL dates on pending
*                  adjustments into the next period.
*
* Log in as      : Oracle APPS database user
* Run Env.       : SQL*Plus
*
* Procedure Name           Description
*
* adjust_gl_date           Procedure adjust_gl_date will use an api to sweep 
*                          the GL dates on pending adjustments into the next
*                          period.
*
* Parameters
* ==========
*
* Parm Name          I/O   Description
* -----------------  ----  ----------------------------------------------------
* errbuf             OUT   Error message.
* retcode            OUT   Error code. Returns 0 if no errors, 1 (Warning) for
*                          data errors, otherwise returns 2 (Error).
*
* Change History
* $Log$
*
* Version Name             Date        Description of Change
* ------- ---------------- ----------- -----------------------------------------
* 0.02    Richard Chapman  18-DEC-2013 Skip adjustments that fail but update all 
*                                      the others. 
* 0.01    Richard Chapman  08-OCT-2013 Initial creation for R12 artf2534778 
*                                      EARS00034391730 CHMN00034544247. 
* ==============================================================================
*/
----------------------------------------------------
-- Start of adjust_gl_date
----------------------------------------------------   
PROCEDURE adjust_gl_date (
  p_errbuf           OUT      VARCHAR2,
  p_retcode          OUT      NUMBER,
  p_gl_date          IN       VARCHAR2,
  p_mode             IN       VARCHAR2
  )
IS
  CURSOR adjust_records (p_gl_date VARCHAR2) IS
    SELECT adjustment_id, gl_date, type, status, TO_CHAR(ROUND(amount,2),'999,999,999.99') amount, 
    adjustment_number, add_months(gl_date,1) new_gl_date
    FROM AR_ADJUSTMENTS_ALL
    WHERE status = 'W'
    AND TO_CHAR(gl_date,'YYYY-MM') = p_gl_date
    AND org_id = fnd_global.org_id
    ORDER BY adjustment_number; 
    
  l_rec_ct             NUMBER      := 0;
  l_error_ct           NUMBER      := 0;
  v_msg_count          number(4);
  v_msg_data           varchar2(1000);
  v_return_status      varchar2(5);
  v_old_adjustment_id  ar_adjustments.adjustment_id%type;
  v_new_adjustment_id  ar_adjustments.adjustment_id%type;
  v_adj_rec            ar_adjustments%rowtype; 
  error_flag           VARCHAR2(1) := 'N';
 
  BEGIN
    p_retcode := 0;
    FND_FILE.PUT_LINE(FND_FILE.LOG, '--------- Amend GL dates on pending AR adjustments: '||RPAD(p_mode,7)||' mode ----------');
    FND_FILE.PUT_LINE(FND_FILE.LOG, '---------------- Advance adjustments for period: '||RPAD(p_gl_date,7)||' ------------------');
    FND_FILE.PUT_LINE(FND_FILE.LOG, '-------------- Organisation: '||RPAD(fnd_global.org_name,30)||' ---------------');
  
    FND_FILE.PUT_LINE(FND_FILE.LOG, '---------------------------------------------------------------------------');    
    FND_FILE.PUT_LINE(FND_FILE.LOG, 'Adjustment Adjustment  Original       New     Record');         
    FND_FILE.PUT_LINE(FND_FILE.LOG, '    Number         Id   GL Date   GL Date       Type Status          Amount');  
    FND_FILE.PUT_LINE(FND_FILE.LOG, '---------- ---------- --------- --------- ---------- ------ ---------------'); 

    FOR l_rec in adjust_records (p_gl_date)LOOP
      FND_FILE.PUT_LINE(FND_FILE.LOG, LPAD(l_rec.adjustment_number,10)||' '||LPAD(TO_CHAR(l_rec.adjustment_id),10)
      ||' '||l_rec.gl_date||' '||l_rec.new_gl_date||' '||LPAD(l_rec.type,10)||' '||LPAD(l_rec.status,6)||' '
      ||l_rec.amount);
      l_rec_ct := l_rec_ct + 1;
      
      v_old_adjustment_id := l_rec.adjustment_id; --adjustment_id_to_modified;
      --v_old_adjustment_id := 90433
      v_adj_rec.comments := '';
      v_adj_rec.gl_date := l_rec.new_gl_date;
      v_adj_rec.status := '';

      IF p_mode = 'Update' THEN
        --Instructions for using this API from Doc 808758.1:
        AR_ADJUST_PUB.Modify_Adjustment(
        p_api_name => 'AR_ADJUST_PUB',
        p_api_version => 1.0,
        p_msg_count => v_msg_count ,
        p_msg_data => v_msg_data,
        p_return_status => v_return_status,
        p_adj_rec => v_adj_rec,
        p_old_adjust_id => v_old_adjustment_id);

        --Display returned messages: 
        FND_FILE.PUT_LINE(FND_FILE.LOG,'Return status '||v_return_status||' Message count '|| v_msg_count);
        IF v_return_status <> 'S' or v_return_status IS NULL THEN
          error_flag := 'Y';
          l_error_ct := l_error_ct + 1;
          IF v_msg_count = 1 Then
            FND_FILE.PUT_LINE(FND_FILE.LOG,'Message '||v_msg_data);
          ELSIF v_msg_count > 1 Then
            LOOP
              v_msg_data := FND_MSG_PUB.Get(FND_MSG_PUB.G_NEXT,FND_API.G_FALSE);
              IF v_msg_data is NULL THEN
                EXIT;
              END IF;
              FND_FILE.PUT_LINE(FND_FILE.LOG,'Message '||v_msg_data);
            END LOOP;
          END IF;
          ROLLBACK;
          FND_FILE.PUT_LINE(FND_FILE.LOG, 'ERROR encountered so calling ROLLBACK');
          p_retcode := 1;
        ELSE
          COMMIT;
          --FND_FILE.PUT_LINE(FND_FILE.LOG, 'OK so committing');  
        END IF;  
      ELSE
        FND_FILE.PUT_LINE(FND_FILE.LOG,'No update - Preview mode');
      END IF;
    END LOOP;

    FND_FILE.PUT_LINE(FND_FILE.LOG, '---------------------------------------------------------------------------');        
    FND_FILE.PUT_LINE(FND_FILE.LOG, 'Total number of records = '||l_rec_ct||' of which '||l_error_ct||' in error');              
    
    IF p_mode = 'Update' THEN
      IF error_flag = 'N' THEN
        FND_FILE.PUT_LINE(FND_FILE.LOG, 'Updates successful');
      ELSE
        FND_FILE.PUT_LINE(FND_FILE.LOG, 'ERROR some updates unsuccessful - search for word ERROR to locate');
      END IF;
    END IF;    

    FND_FILE.PUT_LINE(FND_FILE.LOG, '---------------------------------------------------------------------------');        
    FND_FILE.PUT_LINE(FND_FILE.LOG, ' ');        
               
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      p_retcode := 2;
      p_errbuf  := 'ERROR in xxcar905_adjust_gl_date'||'.'||'adjust_gl_date'||': '||SQLERRM
      ||' '||dbms_utility.format_error_backtrace();
      FND_FILE.PUT_LINE(FND_FILE.LOG, p_errbuf);
  END;
----------------------------------------------------
-- End of adjust_gl_date
---------------------------------------------------- 
END xxcar905_adjust_gl_date;