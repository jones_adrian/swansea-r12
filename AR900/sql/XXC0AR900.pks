create or replace PACKAGE XXC0AR900_PRINT_NEW_TRX
AS
/*************************************************************
$Header$

Original Author: Yousuf Husaini

Module Type    : SQL*Plus

Description    : This package will extract all New Invoices from Account
                 Receivables where print option is 'PRI' along with the customer details.


Modification History:
$Log$

21/08/2006     Yousuf Husaini    Initial Creation
**************************************************************/
   FUNCTION LINE_COUNT ( 
      p_input_string VARCHAR2)   RETURN PLS_INTEGER;
   FUNCTION WORD_WRAP (
      p_input_string VARCHAR2,
      p_column_width NUMBER DEFAULT 40,
      p_max_input_length NUMBER DEFAULT 240)RETURN VARCHAR2;
   PROCEDURE XXC_MAIN (
      p_errbuf                    OUT      VARCHAR2,
      p_retcode                   OUT      NUMBER,
      p_cust_trx_class            IN       VARCHAR2 default null,
      p_cust_trx_type_id          IN       NUMBER  default null,
      p_dates_low                 IN       DATE default null,
      p_dates_high                IN       DATE default null,
      p_installment_number        IN       NUMBER default null,
      p_open_invoice              IN       VARCHAR2 default null,
      p_tax_registration_number   IN       VARCHAR2 default null
   );
END XXC0AR900_PRINT_NEW_TRX;
