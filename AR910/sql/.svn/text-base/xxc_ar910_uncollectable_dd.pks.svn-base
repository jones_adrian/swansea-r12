CREATE OR REPLACE PACKAGE xxc_ar910_uncollectable_dd
AS
/* =============================================================================
*
* $Header$
*
* Module         : xxc_ar910_uncollectable_dd
* Module Type    : PL/SQL Package Header
* Original Author: Richard Chapman
*
* Description    : This package lists receipts on a select receipt batch that are
*                  uncollectable as direct debits because the AUDDIS authorisation
*                  will not yet have been cleared.
*
* Log in as      : Oracle APPS database user
* Run Env.       : SQL*Plus
*
* Procedure Name           Description
*
* main_process             list uncollectable direct debits. 
*
* Parameters
* ==========
*
* Parm Name          I/O   Description
* -----------------  ----  ----------------------------------------------------
* errbuf             OUT   Error message.
* retcode            OUT   Error code. Returns 0 if no errors, 1 (Warning) for
*                          data errors, otherwise returns 2 (Error).
*
* Change History
* $Log$
*
* Version Name             Date        Description of Change
* ------- ---------------- ----------- -----------------------------------------
* 0.01    Richard Chapman  01-JUL-2014 Initial creation for AUDDIS project 
*                                      CRQ000000015284.
* ==============================================================================
*/
PROCEDURE main_process (
  p_errbuf           OUT      VARCHAR2,
  p_retcode          OUT      NUMBER,
  p_org_id           IN       NUMBER,
  p_batch_name       IN       VARCHAR2
  );
END xxc_ar910_uncollectable_dd;