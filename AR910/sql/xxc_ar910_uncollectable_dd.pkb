CREATE OR REPLACE PACKAGE BODY xxc_ar910_uncollectable_dd
AS
/* =============================================================================
*
* $Header$
*
* Module         : xxc_ar910_uncollectable_dd
* Module Type    : PL/SQL Package Header
* Original Author: Richard Chapman
*
* Description    : This package lists receipts on a select receipt batch that are
*                  uncollectable as direct debits because the AUDDIS authorisation
*                  will not yet have been cleared.
*
* Log in as      : Oracle APPS database user
* Run Env.       : SQL*Plus
*
* Procedure Name           Description
*
* main_process             list uncollectable direct debits. 
*
* Parameters
* ==========
*
* Parm Name          I/O   Description
* -----------------  ----  ----------------------------------------------------
* errbuf             OUT   Error message.
* retcode            OUT   Error code. Returns 0 if no errors, 1 (Warning) for
*                          data errors, otherwise returns 2 (Error).
*
* Change History
* $Log$
*
* Version Name             Date        Description of Change
* ------- ---------------- ----------- -----------------------------------------
* 0.01    Richard Chapman  01-JUL-2014 Initial creation for AUDDIS project 
*                                      CRQ000000015284.
* ==============================================================================
*/
----------------------------------------------------
-- Start of main_process
----------------------------------------------------   
PROCEDURE main_process (
  p_errbuf           OUT      VARCHAR2,
  p_retcode          OUT      NUMBER,
  p_org_id           IN       NUMBER,
  p_batch_name       IN       VARCHAR2
  )
IS
  CURSOR adjust_records (p_batch_name VARCHAR2) IS
    SELECT ps1.cash_receipt_id, 
    ps1.amount_due_remaining transaction_value,                                    
    -- using amount_due_remaining column instead of amount applied because in MD 50
    -- it is mentioned to derive ampount applied field from AR _BOE_AUTO_RECEIPTS_V
    -- and in this view amount applied is taken from amount_due_remaining of ar_payment_schedules.
    rcta.trx_number invoice_num, 
    haou.NAME org_name,
    hzp.party_name cust_name,
    hcas.attribute9,
    hcas.attribute10
    FROM ar_payment_schedules ps1,
    ra_customer_trx rcta,
    -- AR_BATCHES ARB
    hz_parties hzp,
    hz_cust_accounts hca,
    hz_cust_acct_sites hcas,
    hr_organization_units haou,
    hz_cust_site_uses hcsu
    WHERE 1 = 1
    --AND ps1.terms_sequence_number = --***THIS removed 20140729 - RC***
    --  (SELECT MAX (terms_sequence_number)
    --  FROM ar_payment_schedules ps2
    --  WHERE ps1.customer_trx_id = ps2.customer_trx_id
    --  AND ps2.status LIKE 'OP%')
    AND ps1.customer_trx_id = rcta.customer_trx_id
    AND ps1.selected_for_receipt_batch_id = 
      (SELECT batch_id
      FROM ar_batches_v
      WHERE NAME = p_batch_name)
    AND haou.organization_id = rcta.org_id
    AND ps1.customer_id = hca.cust_account_id
    AND ps1.customer_site_use_id = hcsu.site_use_id
    AND hcsu.cust_acct_site_id = hcas.cust_acct_site_id
    --AND hcas.attribute10 = '0N'
    AND (
    hcas.attribute10 NOT IN ('01', '17') 
    OR (hcas.attribute10 = '01' AND NVL(TRUNC(SYSDATE) - TRUNC(TO_DATE(hcas.attribute9,'YYYY/MM/DD HH24:MI:SS')),0) < 14) 
    OR hcas.attribute10 IS NULL
    ) --updated for artf3084510
    AND hca.party_id = hzp.party_id
    ORDER BY hcas.attribute10, hcas.attribute9, invoice_num;
    
  l_rec_ct             NUMBER      := 0;
  l_total              NUMBER      := 0;
 
  BEGIN
    p_retcode := 0;
    FND_FILE.PUT_LINE(FND_FILE.LOG, ' ');                
    FND_FILE.PUT_LINE(FND_FILE.LOG, '------------------------------------------------------------------------------------------');        
    FND_FILE.PUT_LINE(FND_FILE.LOG, '------------ Uncollectable Direct Debits to be removed from batch '||RPAD(p_batch_name,10)||' -------------');
    FND_FILE.PUT_LINE(FND_FILE.LOG, '-------------------- Organisation: '||p_org_id||' '||RPAD(fnd_global.org_name,30)||' --------------------');
  
    FND_FILE.PUT_LINE(FND_FILE.LOG, '------------------------------------------------------------------------------------------');    
    FND_FILE.PUT_LINE(FND_FILE.LOG, '   Invoice Customer Name                                           Amount    AUDDIS AUDDIS');         
    FND_FILE.PUT_LINE(FND_FILE.LOG, '    Number                                                                     Date Status');  
    FND_FILE.PUT_LINE(FND_FILE.LOG, '---------- ---------------------------------------- --------------------- --------- ------'); 

    FOR l_rec in adjust_records (p_batch_name)LOOP
      FND_FILE.PUT_LINE(FND_FILE.LOG, LPAD(l_rec.invoice_num,10)||' '||RPAD(l_rec.cust_name,40)
      ||' '||LPAD(TO_CHAR(l_rec.transaction_value),21)||' '
      ||NVL(TO_CHAR(TRUNC(TO_DATE(l_rec.attribute9,'YYYY/MM/DD HH24:MI:SS')),'DD-MON-YY'),'         ')
      ||' '||LPAD(l_rec.attribute10,6));
      l_rec_ct := l_rec_ct + 1;
      l_total  := l_total  + l_rec.transaction_value;
    END LOOP;

    FND_FILE.PUT_LINE(FND_FILE.LOG, '------------------------------------------------------------------------------------------');        
    FND_FILE.PUT_LINE(FND_FILE.LOG, 'Total number of records = '||l_rec_ct||' total amount = '||l_total);              
    
    FND_FILE.PUT_LINE(FND_FILE.LOG, '------------------------------------------------------------------------------------------');        
    FND_FILE.PUT_LINE(FND_FILE.LOG, ' ');        
               
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      p_retcode := 2;
      p_errbuf  := 'ERROR in xxc_ar910_uncollectable_dd'||'.'||'main_process'||': '||SQLERRM
      ||' '||dbms_utility.format_error_backtrace();
      FND_FILE.PUT_LINE(FND_FILE.LOG, p_errbuf);
  END;
----------------------------------------------------
-- End of main_process
---------------------------------------------------- 
END xxc_ar910_uncollectable_dd;