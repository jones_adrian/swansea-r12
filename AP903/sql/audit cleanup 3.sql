create table fnd_audit_groups_20131015 as select * from fnd_audit_groups;

create table fnd_audit_columns_20131015 as select * from fnd_audit_columns;

create table fnd_audit_tables_20131015 as select * from fnd_audit_tables;


drop procedure AP_SYSTEM_PARAMETERS_ALL_AIP;

drop procedure AP_SYSTEM_PARAMETERS_ALL_AUP;

drop procedure AP_SYSTEM_PARAMETERS_ALL_ADP;

drop trigger AP_SYSTEM_PARAMETERS_ALL_AI;

drop trigger AP_SYSTEM_PARAMETERS_ALL_AD;

drop trigger AP_SYSTEM_PARAMETERS_ALL_AU;

drop view AP_SYSTEM_PARAMETERS_ALL_AC1;

--select table_id from fnd_tables where table_name = 'AP_SYSTEM_PARAMETERS_ALL';

--50268

delete from fnd_audit_tables where table_id = 50268;

delete from fnd_audit_columns where table_id = 50268;

delete from fnd_audit_groups where group_name = 'AP.AP_SYSTEM_PARAMETERS_ALL';

commit;

drop procedure CE_SYSTEM_PARAMETERS_AIP;

drop procedure CE_SYSTEM_PARAMETERS_AUP; 

drop procedure CE_SYSTEM_PARAMETERS_ADP;

drop trigger CE_SYSTEM_PARAMETERS_AI;

drop trigger CE_SYSTEM_PARAMETERS_AD;

drop trigger CE_SYSTEM_PARAMETERS_AU;

drop view CE_SYSTEM_PARAMETERS_AC1;

select --object_name, 
* from user_objects
where object_name like 'CE_SYSTEM_PARAMETERS_AV%';

--Before doing any changes to tables below, please make sure to backup these tables: fnd_audit_groups, fnd_audit_columns and fnd_audit_tables.

--select table_id from fnd_tables where table_name = 'CE_SYSTEM_PARAMETERS';

--92234

delete from fnd_audit_tables where table_id = 92234;

delete from fnd_audit_columns where table_id = 92234;

delete from fnd_audit_groups where group_name = 'CE.CE_SYSTEM_PARAMETERS';

commit;

drop procedure FA_SYSTEM_CONTROLS_AIP

drop procedure FA_SYSTEM_CONTROLS_AUP

drop procedure FA_SYSTEM_CONTROLS_ADP

drop trigger FA_SYSTEM_CONTROLS_AI

drop trigger FA_SYSTEM_CONTROLS_AD

drop trigger FA_SYSTEM_CONTROLS_AU

drop view FA_SYSTEM_CONTROLS_AC1

--select table_id from fnd_tables where table_name = 'FA_SYSTEM_CONTROLS';

--50436

delete from fnd_audit_tables where table_id = 50436;

delete from fnd_audit_columns where table_id = 50436;

delete from fnd_audit_groups where group_name = 'OFA.FA_SYSTEM_CONTROLS';

commit;

drop procedure FV_SYSTEM_PARAMETERS_AIP

drop procedure FV_SYSTEM_PARAMETERS_AUP

drop procedure FV_SYSTEM_PARAMETERS_ADP

drop trigger FV_SYSTEM_PARAMETERS_AI

drop trigger FV_SYSTEM_PARAMETERS_AD

drop trigger FV_SYSTEM_PARAMETERS_AU

drop view FV_SYSTEM_PARAMETERS_AC1

--select table_id from fnd_tables where table_name = 'FV_SYSTEM_PARAMETERS';

--80363

delete from fnd_audit_tables where table_id = 80363;

delete from fnd_audit_columns where table_id = 80363;

delete from fnd_audit_groups where group_name = 'FV_SYSTEM_PARAMETERS'

commit;


drop procedure MTL_PARAMETERS_AIP

drop procedure MTL_PARAMETERS_AUP

drop procedure MTL_PARAMETERS_ADP

drop trigger MTL_PARAMETERS_AI

drop trigger MTL_PARAMETERS_AD

drop trigger MTL_PARAMETERS_AU

drop view MTL_PARAMETERS_AC1


--select table_id from fnd_tables where table_name = 'MTL_PARAMETERS';

--51167

delete from fnd_audit_tables where table_id = 51167;

delete from fnd_audit_columns where table_id = 51167;

delete from fnd_audit_groups where group_name = 'INV.MTL_PARAMETERS'

commit;


drop procedure WSH_SHIPPING_PARAMETERS_AIP;

drop procedure WSH_SHIPPING_PARAMETERS_AUP;

drop procedure WSH_SHIPPING_PARAMETERS_ADP;

drop trigger WSH_SHIPPING_PARAMETERS_AI;

drop trigger WSH_SHIPPING_PARAMETERS_AD;

drop trigger WSH_SHIPPING_PARAMETERS_AU;

drop view WSH_SHIPPING_PARAMETERS_AC1;

select table_id from fnd_tables where table_name = 'WSH_SHIPPING_PARAMETERS';

--71272

delete from fnd_audit_tables where table_id = 71272;

delete from fnd_audit_columns where table_id = 71272;

delete from fnd_audit_groups where group_name = 'WSH.WSH_SHIPPING_PARAMETERS';

commit;

drop procedure ZX_PRODUCT_OPTIONS_ALL_AIP;

drop procedure ZX_PRODUCT_OPTIONS_ALL_AUP; 

drop procedure ZX_PRODUCT_OPTIONS_ALL_ADP;

drop trigger ZX_PRODUCT_OPTIONS_ALL_AI;

drop trigger ZX_PRODUCT_OPTIONS_ALL_AD;

drop trigger ZX_PRODUCT_OPTIONS_ALL_AU;

drop view ZX_PRODUCT_OPTIONS_ALL_AC1;


select table_id from fnd_tables where table_name = 'ZX_PRODUCT_OPTIONS_ALL';

--92234

delete from fnd_audit_tables where table_id = 91852;

delete from fnd_audit_columns where table_id = 91852;

delete from fnd_audit_groups where group_name = 'ZX.ZX_PRODUCT_OPTIONS_ALL';

commit;

