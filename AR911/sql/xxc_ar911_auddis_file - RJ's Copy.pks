CREATE OR REPLACE PACKAGE xxc_ar911_auddis_file 
AS
/* =============================================================================
 * Module Type   : PL/SQL
 * Module Name   : xxc_ar911_auddis_file.pks
 * Description   : This package creates a DIRECT DEBIT mandate file (AUDDIS file).
 * Run Env.      : SQL*Plus
 *
 * Procedure Name        Description
 * EXTRACT_DD_MANDATE    Procedure is used to extract customer
 *                       bank account details that require authorisation for use 
 *                       with BACS (AUDDIS).
 *
 * Calling Module: None
 * Dependencies  : MOD_UTIL_007,MOD_EX_009
 * MOD_REG_ID    : MOD_IN_058
 * Known Bugs and Restrictions: None
 *
 * History
 * =======
 * Version Name             Date        Description of Change
 * ------- ---------------  ----------- ----------------------------------------
 * 0.01    Richard Chapman  07-JUL-2014 Initial creation for AUDDIS project 
 *                                      CRQ000000015284. 
 * ============================================================================
 */
--------------------------------------------------------------------------------
-- Actual Code Start Here
--------------------------------------------------------------------------------
-- Procedure   : EXTRACT_DD_MANDATE
-- Description : Procedure is used to extract customer
--               bank account details that require authorisation for use 
--               with BACS (AUDDIS).
--
-- Parameters:
-- Parm Name          I/O   Description
-- -----------------  ----  ----------------------------------------------------
-- p_errbuf           OUT   Error message.
-- p_retcode          OUT   Error code. Returns 0 if no errors otherwise returns 1.
-- p_ou               IN    Orgnization ID
--------------------------------------------------------------------------------
PROCEDURE extract_dd_mandate
  (
  p_errbuf    OUT  VARCHAR2,
  p_retcode   OUT  NUMBER
  );
END xxc_ar911_auddis_file;