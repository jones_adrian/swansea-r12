CREATE OR REPLACE PACKAGE BODY xxc_ar911_ba_auddis_file
AS
/* =============================================================================
 * Module Type   : PL/SQL
 * Module Name   : xxc_ar911_ba_auddis_file.pkb
 * Description   : This package extracts all active customer bank account details.
 * Run Env.      : SQL*Plus
 *
 * Procedure Name        Description
 * EXTRACT_DD_MANDATE    Procedure is used to extract all active customer
 *                       bank account details.
 *
 * Calling Module: None
 * Dependencies  : MOD_UTIL_007,MOD_EX_009
 * MOD_REG_ID    : MOD_IN_058
 * Known Bugs and Restrictions: None
 *
 * History
 * =======
 * Version Name             Date        Description of Change
 * ------- ---------------  ----------- ----------------------------------------
 * 0.01    Richard Chapman  21-Nov-2014 Initial creation for AUDDIS project 
 *                                      CRQ000000015284.
 * ============================================================================
 */  
--------------------------------------------------------------------------------
-- Variable Declaration Start Here
--------------------------------------------------------------------------------
g_master_audit_id                 NUMBER;
g_error_msg                       VARCHAR2 (100)  := NULL;
g_log_type_info          CONSTANT NUMBER := 1;
--                                 := xxc_comm_audit_log_pkg.g_log_type_info;
g_det_status_inprocess   CONSTANT NUMBER := 1;
--                          := xxc_comm_audit_log_pkg.g_det_status_inprocess;
g_det_status_error       CONSTANT NUMBER := 1;
--                              := xxc_comm_audit_log_pkg.g_det_status_error;
g_det_status_complete    CONSTANT NUMBER := 1;
--                           := xxc_comm_audit_log_pkg.g_det_status_complete;
g_sqlcode                         VARCHAR2 (200);
g_sqlerrm                         VARCHAR2 (4000);
gc_mod_code              CONSTANT VARCHAR2 (100)  := 'MOD_IN_058';

--------------------------------------------------------------------------------
-- Actual Code Start Here
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-- Procedure   : EXTRACT_DD_MANDATE
-- EXTRACT_DD_MANDATE    Procedure is used to extract all active customer
--                       bank account details.
-- Parameters:
-- Parm Name          I/O   Description
-- -----------------  ----  ----------------------------------------------------
-- p_errbuf           OUT   Error message.
-- p_retcode          OUT   Error code. Returns 0 if no errors otherwise returns 1.
--------------------------------------------------------------------------------
PROCEDURE extract_dd_mandate (p_errbuf OUT VARCHAR2, p_retcode OUT NUMBER)
  IS
  -- Standard declaration
  l_proc_name               VARCHAR2 (100)
                      := 'xxc_ar911_ba_auddis_file.EXTRACT_DD_MANDATE';
                                                            -- Procedure Name
  l_audit_master_id         NUMBER                                := NULL;
  l_msg_code                VARCHAR2 (10)                     := 'ERR000';
  l_cust_acct_site_rec      hz_cust_account_site_v2pub.cust_acct_site_rec_type;
  l_return_status           VARCHAR2 (5);
  l_msg_count               NUMBER;
  l_msg_data                VARCHAR2 (2000);
  l_object_version_number   NUMBER;
  l_audit_error_string      VARCHAR2 (4000);
  l_file_name               VARCHAR2 (150);
  l_file_name1              VARCHAR2 (150);
  l_file_name2              VARCHAR2 (150);
  l_line_number             NUMBER                                   := 0;
  l_string                  VARCHAR2 (2000);
  l_bus_grp_id              NUMBER;
  l_transaction_name        VARCHAR2 (100);
  l_status                  VARCHAR2 (10);
  l_org_id                  NUMBER;
  lc_dd_trx_code            VARCHAR2 (10)                         := '01';
  lc_dd_canc_code           VARCHAR2 (10)                         := '1C';
  l_file_count              NUMBER (10);
  l_line_number1            NUMBER (10)                              := 0;
  l_string1                 VARCHAR2 (500);
  l_output_dir              VARCHAR2 (500);
  l_file_handle1            UTL_FILE.file_type; 
  l_file_handle2            UTL_FILE.file_type; 
  l_request_id              NUMBER;
  l_total_process_rec       NUMBER               := 0;
  l_total_error_rec         NUMBER               := 0;  
  l_errcode                 VARCHAR2 (500);
  l_errdesc                 VARCHAR2 (500);
  l_error_count             NUMBER               := 0;
  l_account_number          VARCHAR2 (30)        := NULL;
  l_dupl                    VARCHAR2 (3)         := NULL;

  -- Define Cursor to select records from base table
  -- Updated cursor query to fix SIT defect artf2928385
  CURSOR src_cur IS
    SELECT LPAD (NVL (cbv.branch_number, ' '), 15, ' ')
           || LPAD (NVL (iba.bank_account_num, ' '), 33, ' ')
           || LPAD (NVL (hcas.attribute11, ' '), 30, ' ')
           || LPAD (NVL (hp.party_name, ' '), 35, ' ')
           || LPAD ('0', 18, '0')
           || '    D    '
           || LPAD (NVL (hps.party_site_number, '0'), 25, '0')
           || LPAD (TO_CHAR (SYSDATE, 'YYYYMMDD'), 14, ' ')
           || LPAD (NVL (hps.party_site_number, '0'), 20, '0')
           || LPAD (' ', 53, ' ')
           || '0S' data_string,           
             
           LPAD (NVL (cbv.branch_number, ' '), 6, '0')
           || LPAD (NVL (iba.bank_account_num, ' '), 8, '0')
           || '0'
           || '0S'
           || '111111'
           || '22222222'
           || '    '
           || '00000000000C+C '
           || 'SWANSEA ISIS  '
           || RPAD(LPAD(NVL(hca.account_number, ' '), 6, '0'), 18, ' ') 
           || RPAD (NVL (hp.party_name, ' '), 42, ' ') data_string_ccs,
           
           LPAD( LPAD( NVL(cbv.branch_number,' ') ,6,'0') ,15,' ')
           || LPAD( LPAD( NVL(iba.bank_account_num,' ') ,8,'0') ,33,' ')
           || LPAD( NVL(hcas.attribute11,' ') ,30,' ')
           || LPAD( NVL(hp.party_name,' ') ,35,' ')
           || LPAD('0',18,'0')
           || '    D    '
           || LPAD( NVL(hca.account_number,' ') ,25,'0')
           || LPAD( TO_CHAR(SYSDATE,'YYYYMMDD') ,14,' ')
           || LPAD( NVL(hps.party_site_number,'0') ,20,'0')
           || LPAD(' ',53,' ')
           || '0S' data_string_hybrid,
           
           'SD,SD,'
           || RPAD( NVL(hp.party_name,' ') ,18,' ') || ',"'
           || LPAD( NVL(iba.bank_account_num,' ') ,8,'0') || '","'
           || LPAD( NVL(cbv.branch_number,' ') ,6,'0') || '",0,000000000000000,"'
           || LPAD( NVL(hcas.attribute10,' ') ,2,' ') || '",'
           || RPAD( LPAD( NVL(hca.account_number,' ') ,6,'0') ,18,' ') data_string_isis,                      

           RPAD( NVL(hcas.attribute9,' ') ,19,' ') auddis_date,                           
           hcas.attribute10 auddis_collection_type,
           iba.ext_bank_account_id,
           iep.ext_payer_id,
           hp.party_number,
           hp.party_name,
           hps.party_site_name,
           hcas.cust_acct_site_id,
           hca.account_number
    FROM ar.hz_parties hp, --s
    iby.IBY_EXTERNAL_PAYERS_ALL iep, --p
    iby.IBY_PMT_INSTR_USES_ALL ipiu, --i
    iby.IBY_EXT_BANK_ACCOUNTS iba, --a
    ar.hz_cust_site_uses_all hcsu, --hcsu
    ar.hz_cust_acct_sites_all hcas, --hcas
    ar.hz_party_sites hps, --ps
    ar.hz_cust_accounts hca, --ca
    iby_ext_bank_branches_v cbv
    WHERE hp.party_id = iep.party_id
    AND hps.party_id = hp.party_id
    AND iep.ext_payer_id = ipiu.ext_pmt_party_id
    AND ipiu.instrument_id = iba.ext_bank_account_id
    AND ipiu.payment_flow = 'FUNDS_CAPTURE'
    AND ipiu.payment_function = 'CUSTOMER_PAYMENT'
    AND ipiu.end_date IS NULL
    AND iba.end_date IS NULL
    AND SYSDATE BETWEEN NVL(ipiu.start_date,SYSDATE) AND NVL (ipiu.end_date,SYSDATE)
    AND SYSDATE BETWEEN NVL(iba.start_date,SYSDATE) AND NVL (iba.end_date,SYSDATE)
    AND iep.acct_site_use_id = hcsu.site_use_id
    AND hcas.cust_acct_site_id = hcsu.cust_acct_site_id
    AND hcas.cust_account_id = hca.cust_account_id
    --AND hcas.attribute10 is null
    AND hcas.status = 'A'
    AND hcas.party_site_id = hps.party_site_id
    AND cbv.bank_party_id = iba.bank_id
    AND iba.branch_id = cbv.branch_party_id
    AND hca.status = 'A' 
    AND hp.status = 'A' 
    AND hcsu.status = 'A' 
    AND hps.status = 'A' 
    --ORDER BY hp.party_name, iba.bank_account_num;
    ORDER BY hca.account_number, iba.bank_account_num;

  -- Cursor is used to select custemer details to be updated
  CURSOR l_cust_acct_site_cur (p_cust_acct_site_id IN NUMBER) IS
    SELECT cust_acct_site_id,
           cust_account_id,
           party_site_id,
           org_id,
           object_version_number
    FROM   hz_cust_acct_sites
    WHERE  cust_acct_site_id = p_cust_acct_site_id;

  /*      CURSOR l_cur_get_details
      IS
         SELECT xcp.council_identifier,
                xig.interface_group_short_name,
                xig.outbound_file_extension extension
         FROM   xxc_council_interfaces xci,
                xxc_council_parameters xcp,
                xxc_interface_groups xig
         WHERE  xci.business_group_id = xcp.business_group_id
         AND    xci.interface_group_id = xig.interface_group_id
         AND    xcp.business_group_id = fnd_global.per_business_group_id
         AND    xig.interface_group_id = 'MOD_IN_058';

  cur_rec_get_details       l_cur_get_details%ROWTYPE; */

  BEGIN
    ---------------------------------------------------
    -- Get BUSINESS_GROUP_ID
    ---------------------------------------------------
    l_bus_grp_id               := fnd_global.per_business_group_id;
    l_org_id                   := fnd_global.org_id;
    l_request_id               := fnd_global.conc_request_id;
    p_retcode                  := 0;

    /*      OPEN l_cur_get_details;
      FETCH l_cur_get_details
      INTO  cur_rec_get_details;

      CLOSE l_cur_get_details;

      l_file_name1               :=
            cur_rec_get_details.council_identifier
         || '_'
         || cur_rec_get_details.interface_group_short_name
         || '_'
         || fnd_global.conc_request_id
         || '_'
         || TO_CHAR (SYSDATE, 'YYYYMMDDHH24MISS')
         || '_'
         || LPAD (1, 2, '0')
         || NVL (cur_rec_get_details.extension, '.dat');
      l_file_name2               :=
            cur_rec_get_details.council_identifier
         || '_'
         || cur_rec_get_details.interface_group_short_name
         || '_'
         || fnd_global.conc_request_id
         || '_'
         || TO_CHAR (SYSDATE, 'YYYYMMDDHH24MISS')
         || '_'
         || LPAD (2, 2, '0')
    || NVL (cur_rec_get_details.extension, '.dat'); */

    fnd_file.put_line (fnd_file.LOG, 'Business Group Id : ' || l_bus_grp_id);
    fnd_file.put_line (fnd_file.LOG, 'Org Id : ' || l_org_id);
    fnd_file.put_line (fnd_file.LOG, ' ');

    ---------------------------------------------------
    -- Insert into xxc_interface_file table
    ---------------------------------------------------
    -- Insert into xxc_interface_file table
    /*      xxc_interface_parameter_pkg.schedule_interface_ebs
                                                  (fnd_global.conc_request_id,
                                                   l_bus_grp_id,
                                                   gc_mod_code,
                                                   NULL,
                                                   l_transaction_name,
                                                   l_file_name,
                                                   l_status
                                                  ); */

    --IF DD_TRX_CODE is '0N' then update it to '01'.
    FOR src_rec IN src_cur
    LOOP
      IF l_line_number = 0 THEN
        l_file_name1   := 'XXOF_AR_BA_AUDDIS_1_'|| TO_CHAR (SYSDATE, 'DDMMYYYY_HH24MISS')||'.csv';
        l_output_dir   := fnd_profile.value ('XXC_AR_TO_AUDDIS'); --***this*** 
        --l_file_handle1 := UTL_FILE.fopen (l_output_dir, l_file_name1, 'w') ;
      END IF;    
      l_line_number  := l_line_number + 1;
      IF l_account_number = src_rec.account_number THEN
        l_dupl := '***';
      ELSE
        l_dupl := '   ';
      END IF;    
      l_account_number := src_rec.account_number;
      
      
   
      l_string := src_rec.data_string_isis;
      fnd_file.put_line (fnd_file.LOG,'Record written: '||l_string||','||src_rec.auddis_date||','||l_dupl);
      fnd_file.put_line (fnd_file.output,l_string||','||src_rec.auddis_date||','||l_dupl);
      --UTL_FILE.put_line (l_file_handle1, l_string);

      /*         xxc_interface_parameter_pkg.insert_interface_outbound
                                                  (fnd_global.conc_request_id,
                                                   l_file_name1,
                                                   1,
                                                   l_line_number,
                                                   l_string,
                                                   l_status
                                                  ); */
      --l_line_number1             := l_line_number1 + 1;
      --l_string1                  := src_rec.party_number
      --|| LPAD (src_rec.party_name, 30, ' ')
      --|| LPAD (src_rec.party_site_name, 20, ' ')||l_string;
      --fnd_file.put_line (fnd_file.LOG,l_string1);
      --UTL_FILE.put_line (l_file_handle2, l_string1) ;

      /*         xxc_interface_parameter_pkg.insert_interface_outbound
                                                  (fnd_global.conc_request_id,
                                                   l_file_name2,
                                                   2,
                                                   l_line_number1,
                                                   l_string1,
                                                   l_status
                                                  ); */
    END LOOP;
      
    --xxc_utils.get_run_report_output(l_request_id,l_line_number,l_line_number,l_error_count,'I');
       
    IF l_line_number = 0 THEN
      --IF l_line_number <= 0 AND l_line_number1 <= 0 THEN
      /*         xxc_interface_parameter_pkg.update_process_status
                                                         (l_transaction_name,
                                                          'S'
                                                         );
      xxc_comm_audit_log_pkg.insert_audit (fnd_global.conc_request_id,
                                              l_transaction_name,
                                              fnd_global.conc_request_id,
                                              l_proc_name,
                                              g_log_type_info,
                                              g_det_status_complete,
                                              'MSG043',
                                              NULL,
                                              NULL,
                                              g_master_audit_id
                                             ); */
      fnd_file.put_line (fnd_file.LOG, 'No records extracted');
      fnd_file.put_line (fnd_file.LOG, ' ');
      p_retcode                  := 1;
    ELSE
      --UTL_FILE.fclose (l_file_handle1);
      fnd_file.put_line(fnd_file.LOG,'Inserted '||l_line_number
      ||' records into '||l_output_dir||'/'||l_file_name1);
      fnd_file.put_line(fnd_file.LOG,' ');
      --l_string                   :=
      --  'Total number of records included in the submission :'
      --  || TRIM (TO_CHAR (l_line_number1, '000000'));
      --fnd_file.put_line (fnd_file.LOG,'Inserting Footer into XXC_INTERFACE_OUTBOUND');
      --fnd_file.put_line (fnd_file.LOG, ' ');
      /*         xxc_interface_parameter_pkg.insert_interface_outbound
                                                  (fnd_global.conc_request_id,
                                                   l_file_name2,
                                                   2,
                                                   l_line_number1 + 1,
                                                   l_string,
                                                   l_status
                                                  ); */

      /* IF l_status = 'E' THEN
            raise_application_error
               (-20001,
                'Unexpected Error while inserting into xxc_interface_parameter_pkg. insert_interface_outbound,
                 please check audit log for more details.'
               );
         END IF; 
      */

      -- Create audit log for Ar Inv records insert completed sucessfully
      /*         xxc_comm_audit_log_pkg.insert_audit (fnd_global.conc_request_id,
                                              l_transaction_name,
                                              NULL,      --p_master_request_id
                                              l_proc_name,
                                              g_log_type_info,
                                              g_det_status_complete,
                                              'MSG013',           --p_msg_code
                                              'RECORD_COUNT',
                                              l_line_number,      --p_msg_desc
                                              l_audit_master_id
                                             );
      xxc_interface_parameter_pkg.update_process_status
                                                          (l_transaction_name,
                                                           'O'
                                                          );  */
      --fnd_file.put_line (fnd_file.LOG, 'File Write Process Initiated .');
      --fnd_file.put_line (fnd_file.LOG, ' ');
    END IF;

    /* fnd_file.put_line
         (fnd_file.LOG,
             'For more details please refer to audit logs with transaction reference '
          || l_file_name1
          || '-'
          || fnd_global.conc_request_id
          || ','
          || l_file_name2
          || '-'
          || fnd_global.conc_request_id
         );

    fnd_file.put_line (fnd_file.LOG, ' ');
    */         

    /* p_errbuf                   :=
            'Process Successful. Please check the Log Viewer Screen with Reference No : '
         || l_file_name1
         || '-'
         || fnd_global.conc_request_id
         || ','
         || l_file_name2
         || '-'
         || fnd_global.conc_request_id
         || ' for more details';
     */

  IF p_retcode = 0 AND l_line_number > 0 THEN
    COMMIT;
    fnd_file.put_line(fnd_file.log,'COMMIT issued');    
  ELSE
    ROLLBACK;
    fnd_file.put_line(fnd_file.log,'E007 ROLLBACK issued');        
  END IF;
          
  EXCEPTION
    WHEN UTL_FILE.invalid_filehandle THEN
      ROLLBACK;
      l_total_error_rec := l_total_error_rec + 1;
      fnd_file.put_line(fnd_file.log,'E002 Fle handle Exception; ROLLBACK issued');
      xxc_utils.get_run_report_output(l_request_id,l_line_number,l_line_number,l_error_count,'I');
      p_retcode         := 2;
      p_errbuf          := 'Error - terminating';
      UTL_FILE.fclose(l_file_handle1);
      --UTL_FILE.fclose(l_file_handle2);      
    WHEN UTL_FILE.invalid_path THEN
      ROLLBACK;
      l_total_error_rec := l_total_error_rec+1;
      fnd_file.put_line(fnd_file.log,'E003 Path Exception: '||l_output_dir||' '||l_file_name1||'; ROLLBACK issued');
      xxc_utils.get_run_report_output(l_request_id,l_line_number,l_line_number,l_error_count,'I');
      p_retcode         := 2;
      p_errbuf          := 'Error - terminating';
      UTL_FILE.fclose(l_file_handle1);
      --UTL_FILE.fclose(l_file_handle2);      
    WHEN UTL_FILE.invalid_mode THEN
      ROLLBACK;
      l_total_error_rec := l_total_error_rec+1;
      fnd_file.put_line(fnd_file.log,'E004 MODE Exception; ROLLBACK issued');
      xxc_utils.get_run_report_output(l_request_id,l_line_number,l_line_number,l_error_count,'I');
      p_retcode         := 2;
      p_errbuf          := 'Error - terminating';      
      UTL_FILE.fclose(l_file_handle1);
      --UTL_FILE.fclose(l_file_handle2);                  
    WHEN UTL_FILE.invalid_operation THEN
      ROLLBACK;
      l_total_error_rec := l_total_error_rec + 1;
      fnd_file.put_line(fnd_file.log,'E005 OPERATION Exception; ROLLBACK issued');
      xxc_utils.get_run_report_output(l_request_id,l_line_number,l_line_number,l_error_count,'I');
      p_retcode         := 2;
      p_errbuf          := 'Error - terminating';
      UTL_FILE.fclose(l_file_handle1);
      --UTL_FILE.fclose(l_file_handle2);                  
    WHEN others THEN
      ROLLBACK;
      l_total_error_rec := l_total_error_rec + 1;
      fnd_file.put_line(fnd_file.LOG,'E006 ROLLBACK issued and error: '
      || SQLCODE || ' - ' || SQLERRM || ' - ' 
      ||DBMS_UTILITY.format_error_backtrace);
      xxc_utils.get_run_report_output(l_request_id,l_line_number,l_line_number,l_error_count,'I');
      p_retcode         := 2;
      p_errbuf          := 'Error - terminating';
      UTL_FILE.fclose(l_file_handle1);
      --UTL_FILE.fclose(l_file_handle2);

      /* l_errcode         := sqlcode;
      l_errdesc         := substr(sqlerrm,1,250);
      fnd_file.put_line (fnd_file.log,'Error In Procedure'
        ||'with oracle error number : '
        || l_errcode || 'and error description as '
        || l_errdesc) ;
      /*         xxc_interface_parameter_pkg.update_process_status
                                                         (l_transaction_name,
                                                          'F'
                                                         ); */
      /* g_sqlcode                  := SQLCODE;
      g_sqlerrm                  := SQLERRM;
      l_audit_error_string       :=
      SUBSTR (   'Unexpected error - '
        || g_sqlcode
        || ' - '
        || g_sqlerrm
        || CHR (5)
        || DBMS_UTILITY.format_error_backtrace,
        1,
        4000
        );

      /*         IF l_transaction_name IS NOT NULL
         THEN
            xxc_comm_audit_log_pkg.insert_audit
                               (fnd_global.conc_request_id,     --p_request_id
                                l_transaction_name,       --p_transaction_name
                                fnd_global.conc_request_id, --p_transaction_id
                                l_proc_name,                     --p_proc_name
                                g_log_type_info,                  --p_log_type
                                g_det_status_error,             --p_det_status
                                l_msg_code,                       --p_msg_code
                                NULL,                               --p_entity
                                l_audit_error_string,             --p_msg_desc
                                g_master_audit_id          --p_audit_master_id
                               );
         END IF; */
 
      /* p_retcode                  := 2;
      p_errbuf                   := l_audit_error_string;
      */
END extract_dd_mandate;
-------------------------------------------------------------------------------
-- End of EXTRACT_DD_MANDATE Procedure.
--------------------------------------------------------------------------------
END xxc_ar911_ba_auddis_file;
