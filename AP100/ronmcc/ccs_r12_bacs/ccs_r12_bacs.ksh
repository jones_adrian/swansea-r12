#!/bin/ksh 
#-------------------------------------------------- 
# Name:         ccs_r12_bacs.ksh 
# Description:  Script to apply release R12 BACS
# Author:       Ron McConville
# Date:         03-Dec-2012
#
# Arguments:    $1 = APPS password
#-------------------------------------------------- 
# Check apps password supplied
#-------------------------------------------------- 
es=1
progname=${0##*/}
do_setup=Y

if (( ${#} == 1 )); then
  apps_pwd=${1}
else
  print 'Error: apps password must be supplied as argument $1\n'
  exit ${es}
fi

tput clear
{
print "\nStarting ${progname%\.ksh} at $(date)"
#-------------------------------------------------- 
# Copy new shell script into environment & add symbolic link
#-------------------------------------------------- 
fname=XXCDU801CP.prog
print "\nCopying ${fname} script and creating link..."

cp -p ${fname} ${XXC_TOP}/bin
chmod +x ${XXC_TOP}/bin/${fname}
ln -sf ${FND_TOP}/bin/fndcpesr ${XXC_TOP}/bin/${fname%\.prog} 
ls -l ${XXC_TOP}/bin/${fname%\.prog}*
#-------------------------------------------------- 
# Load package body using SQL*Plus
#-------------------------------------------------- 
fname=IBY_FD_EXTRACT_EXT_PUB.pkb
print "\nUsing sqlplus to load package body ${fname}..."

{
  print apps/${apps_pwd}
  cat ${fname}
} | sqlplus -s 

if [[ ${do_setup} != N ]]; then
  #-------------------------------------------------- 
  # Application Set-up
  #-------------------------------------------------- 
  # Payments File Transfer Concurrent Program
  #-------------------------------------------------- 
  ### Download (for info only)
  #FNDLOAD apps/${apps_pwd} 0 Y DOWNLOAD $FND_TOP/patch/115/import/afcpprog.lct cp_payments_xfer.ldt \
  #  PROGRAM APPLICATION_SHORT_NAME="XXC" CONCURRENT_PROGRAM_NAME="XXCDU801CP" ### "XXC: File Transfer for Payments"
  ### Upload

  print "\nUploading Concurrent Program set-up for: XXC: File Transfer for Payments"
  FNDLOAD apps/${apps_pwd} 0 Y UPLOAD $FND_TOP/patch/115/import/afcpprog.lct cp_payments_xfer.ldt CUSTOM_MODE=FORCE 
  cat L*.log | sed "/^$/d"; rm -f L*.log
  #-------------------------------------------------- 
  # Payables All Reports Request Group
  #-------------------------------------------------- 
  ### Download (for info only)
  #FNDLOAD apps/${apps_pwd} 0 Y DOWNLOAD $FND_TOP/patch/115/import/afcpreqg.lct ap_all_reports.ldt \
  #  REQUEST_GROUP REQUEST_GROUP_NAME="All Reports" APPLICATION_SHORT_NAME="SQLAP"
  ### Upload

  print "\nUploading Request Group set-up for: Payables All Reports"
  FNDLOAD apps/${apps_pwd} 0 Y UPLOAD $FND_TOP/patch/115/import/afcpreqg.lct ap_all_reports.ldt \
    UPLOAD_MODE=REPLACE CUSTOM_MODE=FORCE 
  cat L*.log | sed "/^$/d"; rm -f L*.log
  #-------------------------------------------------- 
  # Vendor Sites DFF
  #-------------------------------------------------- 
  ### Download (for info only)
  #FNDLOAD apps/${apps_pwd} 0 Y DOWNLOAD $FND_TOP/patch/115/import/afffload.lct dff_vendor_sites.ldt \
  #  DESC_FLEX APPLICATION_SHORT_NAME="PO" P_LEVEL="COL_ALL:REF_ALL:CTX_ONE" \
  #  DESCRIPTIVE_FLEXFIELD_NAME="PO_VENDOR_SITES" ### "Vendor Sites"
  ### Upload

  print "\nUploading Vendor Sites DFF set-up for: new segment for Roll Number"
  FNDLOAD apps/${apps_pwd} 0 Y UPLOAD $FND_TOP/patch/115/import/afffload.lct dff_vendor_sites.ldt \
    UPLOAD_MODE=REPLACE CUSTOM_MODE=FORCE
  cat L*.log | sed "/^$/d"; rm -f L*.log
fi
#-------------------------------------------------- 
# Finish
#-------------------------------------------------- 
print "\nEnding   ${progname%\.ksh} at $(date)"
} | tee  ${0%\.ksh}_${LOGNAME}.log 2>&1 && es=0

exit ${es}
