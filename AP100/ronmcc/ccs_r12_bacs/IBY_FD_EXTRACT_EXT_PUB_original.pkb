CREATE OR REPLACE PACKAGE BODY APPS.IBY_FD_EXTRACT_EXT_PUB  AS
/* $Header: ibyfdxeb.pls 120.2 2006/09/20 18:52:12 frzhang noship $ */

  -- 1 Ron McConville  BACS file building soc roll number and FTP job.
  -- 2 Michael Brown   Amount in words for cheques.
  -- 3 Vickie Challis  Handle cheques for 9,999,999.99.
  -- 4 Richard Chapman PaymentDatePlus3 tag for BACS RA.
  --
  -- This API is called once only for the payment instruction.
  -- Implementor should construct the extract extension elements
  -- at the payment instruction level as a SQLX XML Aggregate
  -- and return the aggregate.
  --
  -- Below is an example implementation:
/*
  FUNCTION Get_Ins_Ext_Agg(p_payment_instruction_id IN NUMBER)
  RETURN XMLTYPE
  IS
    l_ins_ext_agg XMLTYPE;

    CURSOR l_ins_ext_csr (p_payment_instruction_id IN NUMBER) IS
    SELECT XMLConcat(
             XMLElement("Extend",
               XMLElement("Name", ext_table.attr_name1),
               XMLElement("Value", ext_table.attr_value1)),
             XMLElement("Extend",
               XMLElement("Name", ext_table.attr_name2),
               XMLElement("Value", ext_table.attr_value2))
           )
      FROM your_pay_instruction_lvl_table ext_table
     WHERE ext_table.payment_instruction_id = p_payment_instruction_id;

  BEGIN

    OPEN l_ins_ext_csr (p_payment_instruction_id);
    FETCH l_ins_ext_csr INTO l_ins_ext_agg;
    CLOSE l_ins_ext_csr;

    RETURN l_ins_ext_agg;

  END Get_Ins_Ext_Agg;
*/
/*
  FUNCTION Get_Ins_Ext_Agg(p_payment_instruction_id IN NUMBER)
  RETURN XMLTYPE
  IS
  BEGIN
    RETURN NULL;
  END Get_Ins_Ext_Agg;
*/

  -- Added by R McConville 29/11/2012

  FUNCTION Get_Ins_Ext_Agg(p_payment_instruction_id IN NUMBER)
  RETURN XMLTYPE
  IS
    lc_function_name      CONSTANT VARCHAR2(100) := 'Get_Ins_Ext_Agg (' || TO_CHAR(p_payment_instruction_id) || ')';

    lc_text_pmt_program   CONSTANT fnd_concurrent_programs_vl.user_concurrent_program_name%TYPE := 'Format Payment Instructions with Text Output';

    lc_urn                CONSTANT VARCHAR2(5) := NVL(SUBSTR(fnd_profile.VALUE ('XXC_AP_TO_BACS'),-5),'p_052');

    lc_bacs_format        CONSTANT iby_formats_tl.format_name%TYPE := 'CCS_BACS_Format';

    lc_nf                 CONSTANT VARCHAR2(10) := 'not found';

    l_ins_ext_agg         XMLTYPE;

    l_ins_status          iby_pay_instructions_all.payment_instruction_status%TYPE;
    l_format_name         iby_formats_tl.format_name%TYPE;

    --
    -- Local Function to determine Concurrent Program running the Payment Instruction
    --
    FUNCTION payment_program
      (p_request_id IN fnd_concurrent_requests.request_id%TYPE)
    RETURN fnd_concurrent_programs_vl.user_concurrent_program_name%TYPE
    IS
      l_program fnd_concurrent_programs_vl.user_concurrent_program_name%TYPE DEFAULT lc_nf;
    BEGIN

      SELECT fcp.user_concurrent_program_name
        INTO l_program
        FROM fnd_concurrent_programs_vl fcp
             JOIN fnd_concurrent_requests fcr
               ON fcr.concurrent_program_id = fcp.concurrent_program_id
       WHERE fcr.status_code != 'E'
         AND fcr.argument1    = TO_CHAR(p_payment_instruction_id)
         AND fcr.request_id   = p_request_id;

      RETURN NVL(l_program,lc_nf);

    EXCEPTION
      WHEN OTHERS THEN
        RETURN lc_nf;

    END payment_program;
    --
    -- Local Procedure to determine Payment Instruction Status and Format
    --
    PROCEDURE payment_instruction_info
      (p_ins_status  OUT iby_pay_instructions_all.payment_instruction_status%TYPE
      ,p_format_name OUT iby_formats_tl.format_name%TYPE)
    IS
    BEGIN
      p_ins_status  := lc_nf;
      p_format_name := lc_nf;

      SELECT ips.payment_instruction_status
           , ibf.format_name
        INTO p_ins_status
           , p_format_name
        FROM iby_pay_instructions_all ips
             JOIN iby_acct_pmt_profiles_b iap
               ON iap.payment_profile_id = ips.payment_profile_id
             JOIN iby_sys_pmt_profiles_b isp
               ON isp.system_profile_code = iap.system_profile_code
             JOIN iby_formats_tl ibf
               ON ibf.format_code = isp.payment_format_code
       WHERE NVL(iap.inactive_date,SYSDATE) > TRUNC(SYSDATE)
         AND ips.payment_instruction_id = p_payment_instruction_id;

    EXCEPTION
      WHEN OTHERS THEN
        p_ins_status  := lc_nf;
        p_format_name := lc_nf;

    END payment_instruction_info;
    --
    -- Local Procedure to submit concurrent request to reformat BACS to csv file
    -- and copy it to FTP server
    --
    PROCEDURE send_bacs_file
      (p_request_id IN fnd_concurrent_requests.request_id%TYPE)
    IS
      PRAGMA AUTONOMOUS_TRANSACTION;

      l_from_to           VARCHAR2(100) := xxcdu801.get_urn_type(lc_urn);
      l_host_name         VARCHAR2(100) := xxcdu801.get_urn_legacy_host(lc_urn);
      l_login             VARCHAR2(100) := xxcdu801.get_urn_legacy_login(lc_urn);
      l_password          VARCHAR2(100) := xxcdu801.get_urn_legacy_pwd(lc_urn);
      l_legacy_directory  VARCHAR2(100) := xxcdu801.get_urn_legacy_directory(lc_urn);
      l_ftp_host          VARCHAR2(100) := xxcdu801.get_urn_ftp_host;
      l_ftp_base_dir      VARCHAR2(100) := xxcdu801.get_urn_ftp_basedir;
      l_stage_legacy      VARCHAR2(100) := xxcdu801.get_urn_option_type(lc_urn);
      l_file_name         VARCHAR2(100) := TO_CHAR(p_request_id);

      l_request_id        fnd_concurrent_requests.request_id%TYPE;

    BEGIN
      l_request_id := fnd_request.submit_request (application => 'XXC'
                                                 ,program     => 'XXCDU801CP'
                                                 ,argument1   => lc_urn
                                                 ,argument2   => l_from_to
                                                 ,argument3   => l_host_name
                                                 ,argument4   => l_login
                                                 ,argument5   => l_password
                                                 ,argument6   => l_legacy_directory
                                                 ,argument7   => l_ftp_host
                                                 ,argument8   => l_ftp_base_dir
                                                 ,argument9   => l_stage_legacy
                                                 ,argument10  => l_file_name);

      COMMIT;

      fnd_file.put_line (fnd_file.log,lc_function_name || ': File Transfer Request Id: ' || to_char(l_request_id));

    END send_bacs_file;
  --
  -- Main Program (Get_Ins_Ext_Agg)
  --
  BEGIN
    --
    -- Confirm this Concurrent Request is for Payment Instruction with text output
    --
    IF payment_program (fnd_global.conc_request_id) = lc_text_pmt_program
    THEN
      --
      -- Determine Instruction status and format
      --
      payment_instruction_info (l_ins_status
                               ,l_format_name);

      IF l_format_name = lc_bacs_format
      THEN
        --
        -- CCS BACS Payment Instruction
        --
        IF l_ins_status IN ('FORMATTED_ELECTRONIC', 'CREATED')
        THEN
          --
          -- Instruction status is ok so submit concurrent request
          -- to reformat BACS to csv and copy to FTP server
          --
          send_bacs_file (fnd_global.conc_request_id);

        ELSE
          fnd_file.put_line (fnd_file.log,lc_function_name || ': File Transfer NOT run, as Payment Instruction status: ' || l_ins_status);
        END IF;

      ELSE
        fnd_file.put_line (fnd_file.log,  lc_function_name || ': File Transfer NOT run, as format name: ' || l_format_name);
      END IF;

    ELSE
      fnd_file.put_line (fnd_file.log,    lc_function_name || ': File Transfer NOT run, as not text payment instruction program');
    END IF;

    RETURN NULL;

  EXCEPTION
    WHEN OTHERS THEN
      fnd_file.put_line (fnd_file.log,lc_function_name || ': ERROR: ' || SUBSTR(SQLERRM,1,40));
      RETURN NULL;

  END Get_Ins_Ext_Agg;

  --
  -- This API is called once per payment.
  -- Implementor should construct the extract extension elements
  -- at the payment level as a SQLX XML Aggregate
  -- and return the aggregate.
  --
/*
  FUNCTION Get_Pmt_Ext_Agg(p_payment_id IN NUMBER)
  RETURN XMLTYPE
  IS
  BEGIN
    RETURN NULL;
  END Get_Pmt_Ext_Agg;
*/
  -- Added by R McConville 21/11/2012
--Amened by Micheal Brown and R McConville 23/1/2013 to add in custom amount in words for cheque printing
--Amended by Vickie Challis30-Jan-2013 corercted format mas from 999,999.99 to 9,999,999.99

  FUNCTION Get_Pmt_Ext_Agg(p_payment_id          IN NUMBER)
  RETURN XMLTYPE
  IS
    l_pmt_ext_agg    XMLTYPE;
    l_phone          hr_locations_all.telephone_number_1%TYPE;
    --
    -- Local Function to get 'FAU' Telephone Number
    --
    FUNCTION fau_phone_number
    RETURN VARCHAR2
    IS
      l_telephone_no  hr_locations_all.telephone_number_1%TYPE := '(01792) 636000';
    BEGIN
    
      SELECT NVL(telephone_number_1, '(01792) 636000')
        INTO l_telephone_no
        FROM hr_locations_all
       WHERE location_code = 'FAU';
    
      RETURN l_telephone_no;
    EXCEPTION
      WHEN OTHERS THEN
        RETURN l_telephone_no;

    END fau_phone_number;
  --
  -- Main Program
  --
  BEGIN
    --
    -- Get the FAU phone number
    --
    l_phone := fau_phone_number();
    --
    -- Select statement is used to generate new custom tags and their values
    -- based on the Payment Id
    --
    SELECT XMLCONCAT (XMLELEMENT ("CustomerNumber", cust_number)
                     ,XMLELEMENT ("XXCPayId"      , payment_id)
                     ,XMLELEMENT ("Custom"
                                 ,XMLELEMENT ("Amount"       , amt_star)
                                 ,XMLELEMENT ("SSFlag"       , CASE WHEN payment_amount <= 50000 THEN 'SS' ELSE '' END)
                                 ,XMLELEMENT ("TelephoneNo"  , l_phone)
                                 ,XMLELEMENT ("Value1"       , REGEXP_SUBSTR(amt_words, '[^ ]+', 1, 1))
                                 ,XMLELEMENT ("Value2"       , REGEXP_SUBSTR(amt_words, '[^ ]+', 1, 2))
                                 ,XMLELEMENT ("Value3"       , REGEXP_SUBSTR(amt_words, '[^ ]+', 1, 3))
                                 ,XMLELEMENT ("Value4"       , REGEXP_SUBSTR(amt_words, '[^ ]+', 1, 4))
                                 ,XMLELEMENT ("Value5"       , REGEXP_SUBSTR(amt_words, '[^ ]+', 1, 5))
                                 ,XMLELEMENT ("Value6"       , REGEXP_SUBSTR(amt_words, '[^ ]+', 1, 6))
                                 ,XMLELEMENT ("Value7"       , REGEXP_SUBSTR(amt_words, '[^ ]+', 1, 7))
                                 ,XMLELEMENT ("AmountInWords", amt_words)
                                 )
                     ,XMLELEMENT ("PaymentDatePlus3", payment_date))
      INTO l_pmt_ext_agg
      FROM (SELECT COALESCE(ass.attribute15,TO_CHAR(ibp.payment_reference_number),TO_CHAR(ibp.payment_id)) cust_number
                 , ibp.payment_id
                 , ibp.payment_amount
                -- , LPAD(TRIM(TO_CHAR(ibp.payment_amount,'999,999.99')),12,'*') amt_star -- VC replaced with below line to cater for larger numbers
                 , LPAD('*', 12- length(TRIM(TO_CHAR(ibp.payment_amount,'999,999,999,999.99'))), '*') || 
                               TRIM(TO_CHAR(ibp.payment_amount,'999,999,999,999.99')) amt_star
                 , REPLACE(REPLACE(REPLACE
                   (REPLACE(REPLACE(REPLACE
                   (REPLACE(REPLACE(REPLACE
                   (REPLACE(LPAD(TO_CHAR(TRUNC(ibp.payment_amount)), 7, '0')
                   , 1, '   One  ') , 2, '   Two  ') , 3, '  Three ') , 4, '  Four  ') , 5, '  Five  ')
                   , 6, '   Six  ') , 7, '  Seven ') , 8, '  Eight ') , 9, '  Nine  ') , 0, '  Zero  ') amt_words
                 , to_char(ibp.payment_date+3,'DD/MM/YYYY') payment_date  
              FROM iby_payments_all ibp
                 , ap_supplier_sites_all ass
             WHERE ibp.supplier_site_id = ass.vendor_site_id
               AND ibp.payment_id = p_payment_id);

    RETURN l_pmt_ext_agg;

  EXCEPTION
    WHEN OTHERS THEN
      fnd_file.put_line (fnd_file.log,'Error at Get_Pmt_Ext_Agg function: ' || SUBSTR(SQLERRM,1,40));
      RETURN NULL;

  END Get_Pmt_Ext_Agg;

  --
  -- This API is called once per document payable.
  -- Implementor should construct the extract extension elements
  -- at the document level as a SQLX XML Aggregate
  -- and return the aggregate.
  --

  FUNCTION Get_Doc_Ext_Agg(p_document_payable_id IN NUMBER)
  RETURN XMLTYPE
  IS
  BEGIN
    RETURN NULL;
  END Get_Doc_Ext_Agg;

  --
  -- This API is called once per document payable line.
  -- Implementor should construct the extract extension elements
  -- at the doc line level as a SQLX XML Aggregate
  -- and return the aggregate.
  --
  -- Parameters:
  --   p_document_payable_id: primary key of IBY iby_docs_payable_all table
  --   p_line_number: calling app doc line number. For AP this is
  --   ap_invoice_lines_all.line_number.
  --
  -- The combination of p_document_payable_id and p_line_number
  -- can uniquely locate a document line.
  -- For example if the calling product of a doc is AP
  -- p_document_payable_id can locate
  -- iby_docs_payable_all/ap_documents_payable.calling_app_doc_unique_ref2,
  -- which is ap_invoice_all.invoice_id. The combination of invoice_id and
  -- p_line_number will uniquely identify the doc line.
  --

  FUNCTION Get_Docline_Ext_Agg(p_document_payable_id IN NUMBER, p_line_number IN NUMBER)
  RETURN XMLTYPE
  IS
  BEGIN
    RETURN NULL;
  END Get_Docline_Ext_Agg;


  --
  -- This API is called once only for the payment process request.
  -- Implementor should construct the extract extension elements
  -- at the payment request level as a SQLX XML Aggregate
  -- and return the aggregate.
  --
  FUNCTION Get_Ppr_Ext_Agg(p_payment_service_request_id IN NUMBER)
  RETURN XMLTYPE
  IS
  BEGIN
    RETURN NULL;
  END Get_Ppr_Ext_Agg;


END IBY_FD_EXTRACT_EXT_PUB;
/
