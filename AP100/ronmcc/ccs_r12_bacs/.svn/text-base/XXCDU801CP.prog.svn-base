#!/bin/ksh 
#+========================================================================+
#|
#|  $Id: XXCDU801CP.prog,v 0.01 2012/12/04 10:49:45 rmcconville Exp $
#|
#|  $Source        : $XXC_TOP/bin/XXCDU801CP.prog,v $
#|
#|  MODULE         : DU801CP
#|
#|  MODULE TYPE    : Unix shell script
#|
#|  Customized by  : Ron McConville
#|
#|  Description    : R@S File Transfer Interface script
#|
#|                   This script reformats the Oracle BACS Payments file
#|                   to a Bottomline CSV file which is then passed to the 
#|                   Main File Transfer script (XXCDU801C.prog) to transfer 
#|                   to the FTP Server.
#|
#|                   Dependency - XXCDU801C.prog (Main File Transfer Utility)
#|
#|  Parameters     : 14 in total & same use as XXCDU801C.prog except:
#|                   ${14} is request id of Payments Format job which is
#|                   used to get Oracle BACS (output) file, whereas for
#|                   file transfer '*' pattern is passed. 
#|
#+========================================================================+
#|  Version History
#+========================================================================+
# $Log: XXCDU801CP.prog,v $
#
# Revision 0.01  2012/12/04 rmcconville    Initial Script.
#+========================================================================+
# Diverts stderr to stdout so all output in logfile
exec 2>&1

progname=${0##*/}
es=1

XXCDU801C=${XXC_TOP}/bin/XXCDU801C.prog
CSVFILE="XXOF_AP_BACS_$(date +%d%m%Y_%H%M%S).csv"
#-----------------------------------------------------------
# Define Internal Functions
#-----------------------------------------------------------
function valid_job { ### confirm expected arguments, file & directory
  fes=1

  if (( ${#} != 14 )); then
    msg="number of arguments: '${#}' invalid"
  else
    #
    # Check BACS file, script & staging directory are accessible
    #     ${14} = Request Id for Payments Format job 
    #     ${5}  = Interface Id ("URN")
    #
    BACSFILE=${APPLCSF}/${APPLOUT}/o${14}.out
    STGDIR=${XXC_TOP}/interfaces/out/stage/${5}
    #
    # As Payments file may not be ready be prepared to sleep
    # (for up to a minute)
    #
    let cnt=1
    while (( cnt <= 10 )); do
      if [[ -f ${BACSFILE} && -r ${BACSFILE} && -s ${BACSFILE} ]]; then
        # Payments file is available (so exit loop)
        let cnt=100
      else
        print "Waiting for ${14} Payments File ${cnt}..."
        sleep 12
        let cnt=cnt+1
      fi
    done

    if [[ ! -f ${BACSFILE} || ! -r ${BACSFILE} || ! -s ${BACSFILE} ]]; then
      msg="${BACSFILE} payments file not found or empty"
    elif [[ ! -f ${XXCDU801C} || ! -x ${XXCDU801C} ]]; then
      msg="${XXCDU801C} file transfer script not accessible"
    elif [[ ! -d ${STGDIR} || ! -w ${STGDIR} ]]; then
      msg="${STGDIR} staging directory not accessible"
    else
      fes=0
    fi
  fi

  return ${fes}
}
#-----------------------------------------------------------
function reformat_bacs { ### convert oracle bacs to bottomline csv
  fes=1

  #
  # Use awk to split fixed length (100 chars) into comma separated
  # fields (lengths in BACS template). Select those lines from file
  # starting with numeric & with payment type (field 4) = '99'.
  # Formate & create output file (csv) in staging directory, ready
  # for file transfer.
  #
  awk -v FIELDWIDTHS='6 8 1 2 6 8 4 11 18 18 18' '     # Input column widths
    /^[0-9]/ && $4=="99" {       # Select payment lines type = "99"
      $1=$1 ""
      printf ("%s,%s,%s,%s,%-8s,%s,%-15.2f,%s,%18s\n", # Output format
              "\"#A\"",
              "\"CR\"",
              "\042"$11"\042",   # Name
              "\042"$2"\042",    # Account
              $1,                # Sort Code
              "\"0\"",
              $8/100,            # Amount
              "\"99\"",
              "\042"$10"\042") } # Payment Reference or Roll Number
    ' ${BACSFILE} > ${STGDIR}/${CSVFILE} && fes=0

  return ${fes}
}
#-----------------------------------------------------------
function display_csv { ### display csv file

  underline="+--------------------------------------------"
  underline="${underline}-------------------------------+"

  print "==============="
  print " CSV File name "
  print "==============="
  print "${STGDIR}/${CSVFILE}"
  print "${underline}"
  cat ${STGDIR}/${CSVFILE}
  print "${underline}\n"

  return
}
#-----------------------------------------------------------
function run_file_transfer { ### run main DU801C script

  ft_tmpfile=/tmp/DU801C_${4}.tmp
  #
  # Run Main File Transfer script using same parameters as
  # this "wrapper" script but trimming spaces & replacing 
  # ${14} with '*'.
  #
  # Send output to temp file to determine success/fail
  #
  ft_args="${@%% }"
  ft_args=${ft_args% *}

  ${XXCDU801C} ${ft_args} '*' > ${ft_tmpfile} 2>&1
  fes=${?}
  #
  # Search for errors then display output
  #
  grep -q 'Error Message' ${ft_tmpfile} && fes=1
  cat ${ft_tmpfile}
  #
  # Remove temp file
  #
  rm -f ${ft_tmpfile}

  return ${fes}
}
#-----------------------------------------------------------
# Main Program
# ============
#-----------------------------------------------------------
if valid_job "${@}" && reformat_bacs; then

  display_csv

  run_file_transfer "${@}" && es=0

fi

#
# Display warning/error messages (if any) & Finish
#
[[ -n ${msg} ]] && print "\n${progname%\.prog}: error:\n\t${msg}\n"
print

exit ${es}

